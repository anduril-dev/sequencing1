Changelog by month
===================
## 2014-10
- Upgraded MethylExtract to latest version
- Upgraded TrimGalore to be compatible with latest fastqc version and upgraded trim_galore to latest version.

## 2014-9
- BamStats: added parameter "paired" to support single-end data

## 2014-08
- VariantCombiner now allows Array input
- ControlFreeC: fixed a bug when control is not specified
- BamStats: fixed a bug causing integer overflow in coverage calculation
- BICseq: The new version can optionally run clone-specific copy number analysis with THetA

## 2014-07
- BismarkAlign V1.1: Updated bismark version and added a new parameter "extract" to the component which can suppress running methylation exraction.

