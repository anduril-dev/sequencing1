<!-- 
    Installs the following sequencing software and useful resources:
    * BWA - Fast short-read aligner (standard, "modern MAQ")
    * Bowtie - Fast short-read aligner
    * Picard-tools - Alignment manipulator (sorting, metrics etc.)
    * GATK - Genome Analysis Toolkit (Broad Institute) tool variant calling and analysis (standard)
    * Samtools - For viewing or SAM manipulation (a bit oldish but nifty; can also be used for e.g. sorting, pileup creation and variant calling)
    
    @author: Rony Lindell
-->

<project name="Sequencing-Installations" basedir="." default="run">

    <!-- Local directories for downloads, installations, resources and binary (path) files -->
    <property name="downloads.dir" value="${resource.dir}/downloads"/>
    <property name="applications.dir" value="/usr/local/share"/>
    <property name="bin.dir" value="/usr/local/bin"/>
    <property name="resources.dir" value="${resource.dir}"/>
    
    <!-- Change the versions here to the most current stable ones -->
    <property name="bwa.version" value="0.5.9"/>
    <property name="bowtie.version" value="0.12.7"/>
    <property name="tophat.version" value="1.4.1"/>
    <property name="picard.version" value="1.61"/>
    <property name="gatk.version" value="latest"/>
    <property name="samtools.version" value="0.1.18"/>
    <property name="varscan.version" value="2.2.8"/>

    <!-- Software files that will be downloaded, uncompressed and installed -->
    <property name="bwa.url" value="http://sourceforge.net/projects/bio-bwa/files"/>
    <property name="bwa.tar" value="bwa-${bwa.version}.tar.bz2"/>    
    <property name="bwa.dir" value="bwa-${bwa.version}"/>
    
    <property name="bowtie.url" value="http://downloads.sourceforge.net/project/bowtie-bio/bowtie/${bowtie.version}"/>
    <property name="bowtie.zip" value="bowtie-${bowtie.version}-src.zip"/>    
    <property name="bowtie.dir" value="bowtie-${bowtie.version}"/>
    
    <property name="tophat.url" value="http://tophat.cbcb.umd.edu/downloads"/>
    <property name="tophat.tar" value="tophat-${tophat.version}.tar.gz"/>
    <property name="tophat.dir" value="tophat-${tophat.version}"/>
    <property name="tophat.sambin" value="/usr/local"/>
    
    <property name="picard.url" value="http://sourceforge.net/projects/picard/files/picard-tools/${picard.version}"/>
    <property name="picard.zip" value="picard-tools-${picard.version}.zip"/>
    <property name="picard.dir" value="picard-tools-${picard.version}"/>
    
    <property name="gatk.url" value="ftp://ftp.broadinstitute.org/pub/gsa/GenomeAnalysisTK"/>
    <property name="gatk.tar" value="GenomeAnalysisTK-${gatk.version}.tar.bz2"/>
    
    <property name="samtools.url" value="http://sourceforge.net/projects/samtools/files/samtools/${samtools.version}"/>
    <property name="samtools.tar" value="samtools-${samtools.version}.tar.bz2"/>
    <property name="samtools.dir" value="samtools-${samtools.version}"/>
    
    <property name="varscan.url" value="http://sourceforge.net/projects/varscan/files/"/>
    <property name="varscan.jar" value="VarScan.v${varscan.version}.jar"/>
    <property name="varscan.dir" value="VarScan-${varscan.version}"/>
    
    <!-- You might want to overrule these by chosing e.g. bwa/indexes and bowtie/indexes as the index directories for local use -->
    <property name="bwa.resources.indexes.dir" value="${resources.dir}/bwa-indexes"/>
    <property name="bwa.resources.indexes.colorspace.dir" value="${resources.dir}/bwa-indexes-colorspace"/>
    <property name="bowtie.resources.indexes.dir" value="${resources.dir}/bowtie-indexes"/>
    <property name="bowtie.resources.indexes.colorspace.dir" value="${resources.dir}/bowtie-indexes-colorspace"/>
    
    <!-- Resources structure as it is in GATK bundle and also locally, resources/b37, resources/hg19 etc. -->
    <property name="gatk.resources.bundle.dir" value="bundle/1.2"/>
    <property name="gatk.resources.b36.dir" value="b36"/>
    <property name="gatk.resources.b37.dir" value="b37"/>
    <property name="gatk.resources.hg18.dir" value="hg18"/>
    <property name="gatk.resources.hg19.dir" value="hg19"/>
    <property name="gatk.resources.examples.dir" value="exampleFASTA"/>
    <property name="gatk.resources.liftover.dir" value="liftover"/>
    
    <!-- Fasta files that will have alignment-index files created for use in BWA/Bowtie etc. -->
    <property name="gatk.resources.b36.fasta" value="human_b36_both"/>
    <property name="gatk.resources.b37.fasta" value="human_g1k_v37"/>
    <property name="gatk.resources.hg18.fasta" value="Homo_sapiens_assembly18"/>
    <property name="gatk.resources.hg19.fasta" value="ucsc.hg19"/>
    
    <!-- Runs all install and resource procedures -->
    <target name="run" depends="init,install,resources"/>

    <!-- Download and install all programs -->
    <target name="install" depends="init-install,install-bwa,install-bowtie,install-samtools,install-tophat,install-picard,install-gatk,install-varscan"/>

    <!-- Download all resources -->
    <target name="resources" depends="init-resources,resources-gatk,resources-bwa,resources-bowtie"/>

    <!-- Delete all files and directories created -->
    <target name="clean" depends="clean-install,clean-resources"/>

    <!-- Delete all files and directories created in installation -->
    <target name="clean-install" depends="clean-bwa,clean-bowtie,clean-samtools,clean-tophat,clean-picard,clean-gatk"/>

    <!-- Delete all resources files -->
    <target name="clean-resources" depends="clean-resources-bwa,clean-resources-bowtie,clean-resources-gatk"/>

    <!-- Initiate all by creating required directories -->
    <target name="init" depends="init-install,init-resources"/>

    <!-- Initiate installs by creating required directories -->
    <target name="init-install">
        <mkdir dir="${downloads.dir}"/>
        <mkdir dir="${applications.dir}"/>
        <mkdir dir="${bin.dir}"/>
    </target>

    <!-- Initiate resources by creating required directories -->
    <target name="init-resources">
        <mkdir dir="${resources.dir}"/> 
    </target>
    
    <!-- Delete BWA files and directories -->
    <target name="clean-bwa">
        <symlink link="${applications.dir}/bwa" action="delete"/>
        <symlink link="${bin.dir}/bwa" action="delete"/>
        <delete dir="${applications.dir}/${bwa.dir}"/>
    </target>

    <!-- Delete Bowtie files and directories -->
    <target name="clean-bowtie">
        <symlink link="${applications.dir}/bowtie" action="delete"/>
        <symlink link="${bin.dir}/bowtie" action="delete"/>
        <symlink link="${bin.dir}/bowtie-build" action="delete"/>  
        <symlink link="${bin.dir}/bowtie-inspect" action="delete"/>  
        <delete dir="${applications.dir}/${bowtie.dir}"/>
    </target>

    <!-- Delete Tophat files and directories -->
    <target name="clean-tophat">
        <symlink link="${bin.dir}/bam2fastx" action="delete"/>
        <symlink link="${bin.dir}/bed_to_juncs" action="delete"/>
        <symlink link="${bin.dir}/contig_to_chr_coords" action="delete"/>
        <symlink link="${bin.dir}/fix_map_ordering" action="delete"/>
        <symlink link="${bin.dir}/gtf_to_fasta" action="delete"/>
        <symlink link="${bin.dir}/library_stats" action="delete"/>
        <symlink link="${bin.dir}/map2gtf" action="delete"/>
        <symlink link="${bin.dir}/prep_reads" action="delete"/>
        <symlink link="${bin.dir}/segment_juncs" action="delete"/>
        <symlink link="${bin.dir}/tophat" action="delete"/>
        <symlink link="${bin.dir}/wiggles" action="delete"/>
        <symlink link="${bin.dir}/bam_merge" action="delete"/>
        <symlink link="${bin.dir}/closure_juncs" action="delete"/>
        <symlink link="${bin.dir}/extract_reads" action="delete"/>
        <symlink link="${bin.dir}/gtf_juncs" action="delete"/>
        <symlink link="${bin.dir}/juncs_db" action="delete"/>
        <symlink link="${bin.dir}/long_spanning_reads" action="delete"/>
        <symlink link="${bin.dir}/mask_sam" action="delete"/>
        <symlink link="${bin.dir}/sam_juncs" action="delete"/>
        <symlink link="${bin.dir}/sra_to_solid" action="delete"/>
        <symlink link="${bin.dir}/tophat_reports" action="delete"/>
        <symlink link="${applications.dir}/tophat" action="delete"/>
        <delete dir="${applications.dir}/${tophat.dir}"/>
    </target>
    
    <!-- Delete Picard-tools files and directories -->
    <target name="clean-picard">
        <symlink link="${applications.dir}/picard" action="delete"/>
        <delete dir="${applications.dir}/${picard.dir}"/>
    </target>
    
    <!-- Delete Gatk files and directories -->
    <target name="clean-gatk">
        <symlink link="${applications.dir}/gatk" action="delete"/>
        <path id="gatk.version.dir">
            <dirset dir="${applications.dir}">
                <include name="GenomeAnalysisTK-*"/>
            </dirset>
        </path>
        <property name="gatk.dir" refid="gatk.version.dir"/>
        <delete dir="${gatk.dir}"/>
    </target>
    
    <!-- Delete Samtools files and directories -->
    <target name="clean-samtools">
        <symlink link="${applications.dir}/samtools" action="delete"/>
        <symlink link="${bin.dir}/samtools" action="delete"/>
        <symlink link="${bin.dir}/bcftools" action="delete"/>
        <delete dir="${applications.dir}/${samtools.dir}"/>
    </target>

    <!-- Delete VarScan files and directories -->
    <target name="clean-varscan">
        <symlink link="${applications.dir}/varscan" action="delete"/>
        <delete dir="${applications.dir}/${varscan.dir}"/>
    </target>
    
    <!-- Delete Gatk resources files -->
    <target name="clean-resources-gatk" depends="clean-resources-gatk-b36,clean-resources-gatk-b37,clean-resources-gatk-hg18,
        clean-resources-gatk-hg19,clean-resources-gatk-examples,clean-resources-gatk-liftover"/>

    <!-- Delete Gatk resources files -->
    <target name="clean-resources-gatk-b36">
        <delete dir="${resources.dir}/${gatk.resources.b36.dir}"/>
    </target>

    <!-- Delete Gatk resources files -->
    <target name="clean-resources-gatk-b37">
        <delete dir="${resources.dir}/${gatk.resources.b37.dir}"/>
    </target>

    <!-- Delete Gatk resources files -->
    <target name="clean-resources-gatk-hg18">
        <delete dir="${resources.dir}/${gatk.resources.hg18.dir}"/>
    </target>

    <!-- Delete Gatk resources files -->
    <target name="clean-resources-gatk-hg19">
        <delete dir="${resources.dir}/${gatk.resources.hg19.dir}"/>
    </target>

    <!-- Delete Gatk resources files -->
    <target name="clean-resources-gatk-examples">
        <delete dir="${resources.dir}/${gatk.resources.examples.dir}"/>
    </target>

    <!-- Delete Gatk resources files -->
    <target name="clean-resources-gatk-liftover">
        <delete dir="${resources.dir}/${gatk.resources.liftover.dir}"/>
    </target>
    
    <!-- Delete bwa resources files -->
    <target name="clean-resources-bwa">
        <delete dir="${bwa.resources.indexes.dir}"/>
        <delete dir="${bwa.resources.indexes.colorspace.dir}"/>
    </target>

    <!-- Delete Bowtie resources files -->
    <target name="clean-resources-bowtie">
        <delete dir="${bowtie.resources.indexes.dir}"/>
        <delete dir="${bowtie.resources.indexes.colorspace.dir}"/>
    </target>
    
    <!-- Download and install Bwa -->
    <target name="install-bwa" depends="init-install">
        <echo message="***************************************************"/>
        <echo message=" Installing BWA version ${bwa.version}."/>
        <echo message="***************************************************"/>
        <get src="${bwa.url}/${bwa.tar}"
            dest="${downloads.dir}/${bwa.tar}"
            usetimestamp="true"/>
        <untar src="${downloads.dir}/${bwa.tar}"
            dest="${applications.dir}"
            compression="bzip2"/>
        <exec executable="make" dir="${applications.dir}/${bwa.dir}"/>
        <symlink link="${bin.dir}/bwa" resource="${applications.dir}/${bwa.dir}/bwa" overwrite="true"/>
        <symlink link="${applications.dir}/bwa" resource="${applications.dir}/${bwa.dir}" overwrite="true"/>
    </target>

    <!-- Download and install Bowtie -->
    <target name="install-bowtie" depends="init-install">
        <echo message="***************************************************"/>
        <echo message=" Installing Bowtie version ${bowtie.version}."/>
        <echo message="***************************************************"/>
        <get src="${bowtie.url}/${bowtie.zip}"
            dest="${downloads.dir}/${bowtie.zip}"
            usetimestamp="true"/>
        <unzip src="${downloads.dir}/${bowtie.zip}"
            dest="${applications.dir}">
        </unzip>
        <exec executable="make" dir="${applications.dir}/${bowtie.dir}"/>
        <symlink link="${bin.dir}/bowtie" resource="${applications.dir}/${bowtie.dir}/bowtie" overwrite="true"/>
        <symlink link="${bin.dir}/bowtie-build" resource="${applications.dir}/${bowtie.dir}/bowtie-build" overwrite="true"/>
        <symlink link="${bin.dir}/bowtie-inspect" resource="${applications.dir}/${bowtie.dir}/bowtie-inspect" overwrite="true"/>
        <symlink link="${applications.dir}/bowtie" resource="${applications.dir}/${bowtie.dir}" overwrite="true"/>
    </target>

    <!-- Download and install Tophat.
        * Note: Bowtie and Samtools must be installed prior to installing Tophat and program files copied to appropriate locations.
        * Executables: bam2fastx  bed_to_juncs   contig_to_chr_coords  fix_map_ordering  gtf_to_fasta  library_stats        map2gtf   prep_reads  segment_juncs  tophat          wiggles
        bam_merge  closure_juncs  extract_reads         gtf_juncs         juncs_db      long_spanning_reads  mask_sam  sam_juncs   sra_to_solid   tophat_reports
    -->
    <target name="install-tophat" depends="init-install,install-bowtie,install-samtools">
        <echo message="***************************************************"/>
        <echo message=" Installing Tophat version ${tophat.version}."/>
        <echo message="***************************************************"/>
        <get src="${tophat.url}/${tophat.tar}"
            dest="${downloads.dir}/${tophat.tar}"
            usetimestamp="true"/>
        <untar src="${downloads.dir}/${tophat.tar}"
            dest="${applications.dir}"
            compression="gzip"/>
        <mkdir dir="${tophat.sambin}/lib"/>
        <mkdir dir="${tophat.sambin}/include"/>
        <mkdir dir="${tophat.sambin}/include/bam"/>
        <copy file="${applications.dir}/${samtools.dir}/libbam.a" todir="${tophat.sambin}/lib"/>
        <copy todir="${tophat.sambin}/include/bam">
            <fileset dir="${applications.dir}/${samtools.dir}">
                <include name="*.h"/>
            </fileset>
        </copy>
        <chmod file="${applications.dir}/${tophat.dir}/configure" perm="ugo+rwx"/>
        <exec executable="./configure" dir="${applications.dir}/${tophat.dir}">
            <arg value="--prefix=${applications.dir}/${tophat.dir}"/>
            <arg value="--with-bam=${tophat.sambin}"/>
        </exec>
        <exec executable="make" dir="${applications.dir}/${tophat.dir}"/>
        <exec executable="make" dir="${applications.dir}/${tophat.dir}">
            <arg value="install"/>
        </exec>
        <symlink link="${applications.dir}/tophat" resource="${applications.dir}/${tophat.dir}" overwrite="true"/>
        <symlink link="${bin.dir}/bam2fastx" resource="${applications.dir}/tophat/bin/bam2fastx" overwrite="true"/>
        <symlink link="${bin.dir}/bed_to_juncs" resource="${applications.dir}/tophat/bin/bed_to_juncs" overwrite="true"/>
        <symlink link="${bin.dir}/contig_to_chr_coords" resource="${applications.dir}/tophat/bin/contig_to_chr_coords" overwrite="true"/>
        <symlink link="${bin.dir}/fix_map_ordering" resource="${applications.dir}/tophat/bin/fix_map_ordering" overwrite="true"/>
        <symlink link="${bin.dir}/gtf_to_fasta" resource="${applications.dir}/tophat/bin/gtf_to_fasta" overwrite="true"/>
        <symlink link="${bin.dir}/library_stats" resource="${applications.dir}/tophat/bin/library_stats" overwrite="true"/>
        <symlink link="${bin.dir}/map2gtf" resource="${applications.dir}/tophat/bin/map2gtf" overwrite="true"/>
        <symlink link="${bin.dir}/prep_reads" resource="${applications.dir}/tophat/bin/prep_reads" overwrite="true"/>
        <symlink link="${bin.dir}/segment_juncs" resource="${applications.dir}/tophat/bin/segment_juncs" overwrite="true"/>
        <symlink link="${bin.dir}/tophat" resource="${applications.dir}/tophat/bin/tophat" overwrite="true"/>
        <symlink link="${bin.dir}/wiggles" resource="${applications.dir}/tophat/bin/wiggles" overwrite="true"/>
        <symlink link="${bin.dir}/bam_merge" resource="${applications.dir}/tophat/bin/bam_merge" overwrite="true"/>
        <symlink link="${bin.dir}/closure_juncs" resource="${applications.dir}/tophat/bin/closure_juncs" overwrite="true"/>
        <symlink link="${bin.dir}/extract_reads" resource="${applications.dir}/tophat/bin/extract_reads" overwrite="true"/>
        <symlink link="${bin.dir}/gtf_juncs" resource="${applications.dir}/tophat/bin/gtf_juncs" overwrite="true"/>
        <symlink link="${bin.dir}/juncs_db" resource="${applications.dir}/tophat/bin/juncs_db" overwrite="true"/>
        <symlink link="${bin.dir}/long_spanning_reads" resource="${applications.dir}/tophat/bin/long_spanning_reads" overwrite="true"/>
        <symlink link="${bin.dir}/mask_sam" resource="${applications.dir}/tophat/bin/mask_sam" overwrite="true"/>
        <symlink link="${bin.dir}/sam_juncs" resource="${applications.dir}/tophat/bin/sam_juncs" overwrite="true"/>
        <symlink link="${bin.dir}/sra_to_solid" resource="${applications.dir}/tophat/bin/sra_to_solid" overwrite="true"/>
        <symlink link="${bin.dir}/tophat_reports" resource="${applications.dir}/tophat/bin/tophat_reports" overwrite="true"/>
    </target>
    
    <!-- Download and install Picard-tools -->
    <target name="install-picard" depends="init-install">
        <echo message="***************************************************"/>
        <echo message=" Installing Picard version ${picard.version}."/>
        <echo message="***************************************************"/>
        <get src="${picard.url}/${picard.zip}"
            dest="${downloads.dir}/${picard.zip}"
            usetimestamp="true"/>
        <unzip src="${downloads.dir}/${picard.zip}"
            dest="${applications.dir}">
            <patternset>
                <exclude name="snappy*"/>
            </patternset>
        </unzip>
        <symlink link="${applications.dir}/picard" resource="${applications.dir}/${picard.dir}" overwrite="true"/>
    </target>
    
    <!-- Download and install Gatk -->
    <target name="install-gatk" depends="init-install">
        <echo message="***************************************************"/>
        <echo message=" Installing Gatk version ${gatk.version}."/>
        <echo message="***************************************************"/>
        <get src="${gatk.url}/${gatk.tar}"
            dest="${downloads.dir}/${gatk.tar}"
            usetimestamp="true"/>
        <untar src="${downloads.dir}/${gatk.tar}"
            dest="${applications.dir}"
            compression="bzip2"/>
        <path id="gatk.version.dir">
            <dirset dir="${applications.dir}">
                <include name="GenomeAnalysisTK-*"/>
            </dirset>
        </path>
        <property name="gatk.dir" refid="gatk.version.dir"/>
        <symlink link="${applications.dir}/gatk" resource="${gatk.dir}" overwrite="true"/>
    </target>

    <!-- Download and install Samtools -->
    <target name="install-samtools" depends="init-install">
        <echo message="***************************************************"/>
        <echo message=" Installing Samtools version ${samtools.version}."/>
        <echo message="***************************************************"/>
        <get src="${samtools.url}/${samtools.tar}"
            dest="${downloads.dir}/${samtools.tar}"
            usetimestamp="true"/>
        <untar src="${downloads.dir}/${samtools.tar}"
            dest="${applications.dir}"
            compression="bzip2"/>
        <exec executable="make" dir="${applications.dir}/${samtools.dir}"/>
        <symlink link="${applications.dir}/samtools" resource="${applications.dir}/${samtools.dir}" overwrite="true"/>
        <symlink link="${bin.dir}/samtools" resource="${applications.dir}/${samtools.dir}/samtools" overwrite="true"/>
        <symlink link="${bin.dir}/bcftools" resource="${applications.dir}/${samtools.dir}/bcftools/bcftools" overwrite="true"/>
    </target>

    <!-- Download and install VarScan -->
    <target name="install-varscan" depends="init-install">
        <echo message="***************************************************"/>
        <echo message=" Installing VarScan version ${varscan.version}."/>
        <echo message="***************************************************"/>
        <get src="${varscan.url}/${varscan.jar}"
            dest="${downloads.dir}/${varscan.jar}"
            usetimestamp="true"/>
        <mkdir dir="${applications.dir}/${varscan.dir}"/>
        <copy file="${downloads.dir}/${varscan.jar}" todir="${applications.dir}/${varscan.dir}"/>
        <symlink link="${applications.dir}/varscan" resource="${applications.dir}/${varscan.dir}" overwrite="true"/>
        <symlink link="${applications.dir}/${varscan.dir}/VarScan.jar" resource="${applications.dir}/${varscan.dir}/VarScan.v${varscan.version}.jar"/>
    </target>

    <!-- Download b36 build from Broad Institute FTP site -->
    <target name="resources-gatk" depends="init-resources,resources-gatk-b36,resources-gatk-b37,resources-gatk-hg18,resources-gatk-hg19,
        resources-gatk-examples,resources-gatk-liftover"/>

    <!-- Download b36 build from Broad Institute FTP site -->
    <target name="resources-gatk-b36" depends="init-resources">
        <mkdir dir="${resources.dir}/${gatk.resources.b36.dir}"/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="gatk-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.b36.dir}"/>
            <arg value="${gatk.resources.bundle.dir}/${gatk.resources.b36.dir}"/>
        </exec>
    </target>

    <!-- Download b37 build from Broad Institute FTP site -->
    <target name="resources-gatk-b37" depends="init-resources">
        <mkdir dir="${resources.dir}/${gatk.resources.b37.dir}"/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="gatk-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.b37.dir}"/>
            <arg value="${gatk.resources.bundle.dir}/${gatk.resources.b37.dir}"/>
        </exec>
    </target>

    <!-- Download hg18 build from Broad Institute FTP site -->
    <target name="resources-gatk-hg18" depends="init-resources">
        <mkdir dir="${resources.dir}/${gatk.resources.hg18.dir}"/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="gatk-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.hg18.dir}"/>
            <arg value="${gatk.resources.bundle.dir}/${gatk.resources.hg18.dir}"/>
        </exec>
    </target>

    <!-- Download hg19 build from Broad Institute FTP site -->
    <target name="resources-gatk-hg19" depends="init-resources">
        <mkdir dir="${resources.dir}/${gatk.resources.hg19.dir}"/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="gatk-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.hg19.dir}"/>
            <arg value="${gatk.resources.bundle.dir}/${gatk.resources.hg19.dir}"/>
        </exec>
    </target>

    <!-- Download example files from Broad Institute FTP site -->
    <target name="resources-gatk-examples" depends="init-resources">
        <mkdir dir="${resources.dir}/${gatk.resources.examples.dir}"/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="gatk-resources-examples.sh"/>
            <arg value="${resources.dir}/${gatk.resources.examples.dir}"/>
            <arg value="${gatk.resources.bundle.dir}/${gatk.resources.examples.dir}"/>
        </exec>
    </target>

    <!-- Download example files from Broad Institute FTP site -->
    <target name="resources-gatk-liftover" depends="init-resources">
        <mkdir dir="${resources.dir}/${gatk.resources.liftover.dir}"/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="gatk-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.liftover.dir}"/>
            <arg value="${gatk.resources.liftover.dir}"/>
        </exec>
    </target>

    <!-- Create BWA reference index files -->
    <target name="resources-bwa" depends="resources-bwa-b36,resources-bwa-b37,resources-bwa-hg18,resources-bwa-hg19"/>

    <!-- Create reference index files for build b36 -->
    <target name="resources-bwa-b36" depends="init-resources">
        <mkdir dir="${bwa.resources.indexes.dir}"/>
        <mkdir dir="${bwa.resources.indexes.colorspace.dir}"/>
        <symlink link="${bwa.resources.indexes.dir}/${gatk.resources.b36.fasta}.fasta" 
            resource="${resources.dir}/${gatk.resources.b36.dir}/${gatk.resources.b36.fasta}.fasta"/>
        <symlink link="${bwa.resources.indexes.colorspace.dir}/${gatk.resources.b36.fasta}.fasta" 
            resource="${resources.dir}/${gatk.resources.b36.dir}/${gatk.resources.b36.fasta}.fasta"/>
        <echo message="Writing BWA index files for build b36. This may take a while..."/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="bwa-resources.sh"/>
            <arg value="${bwa.resources.indexes.dir}"/>
            <arg value="${bwa.resources.indexes.colorspace.dir}"/>
            <arg value="${gatk.resources.b36.fasta}"/>
        </exec>
    </target>

    <!-- Create reference index files for build b37 -->
    <target name="resources-bwa-b37" depends="init-resources">
        <mkdir dir="${bwa.resources.indexes.dir}"/>
        <mkdir dir="${bwa.resources.indexes.colorspace.dir}"/>
        <symlink link="${bwa.resources.indexes.dir}/${gatk.resources.b37.fasta}.fasta" 
            resource="${resources.dir}/${gatk.resources.b37.dir}/${gatk.resources.b37.fasta}.fasta"/>
         <symlink link="${bwa.resources.indexes.colorspace.dir}/${gatk.resources.b37.fasta}.fasta" 
            resource="${resources.dir}/${gatk.resources.b37.dir}/${gatk.resources.b37.fasta}.fasta"/>
        <echo message="Writing BWA index files for build b37. This may take a while..."/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="bwa-resources.sh"/>
            <arg value="${bwa.resources.indexes.dir}"/>
            <arg value="${bwa.resources.indexes.colorspace.dir}"/>
            <arg value="${gatk.resources.b37.fasta}"/>
        </exec>
    </target>

    <!-- Create reference index files for build hg18 -->
    <target name="resources-bwa-hg18" depends="init-resources">
        <mkdir dir="${bwa.resources.indexes.dir}"/>
        <mkdir dir="${bwa.resources.indexes.colorspace.dir}"/>
        <symlink link="${bwa.resources.indexes.dir}/${gatk.resources.hg18.fasta}.fasta" 
            resource="${resources.dir}/${gatk.resources.hg18.dir}/${gatk.resources.hg18.fasta}.fasta"/>
        <symlink link="${bwa.resources.indexes.colorspace.dir}/${gatk.resources.hg18.fasta}.fasta" 
            resource="${resources.dir}/${gatk.resources.hg18.dir}/${gatk.resources.hg18.fasta}.fasta"/>
        <echo message="Writing BWA index files for build hg18. This may take a while..."/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="bwa-resources.sh"/>
            <arg value="${bwa.resources.indexes.dir}"/>
            <arg value="${bwa.resources.indexes.colorspace.dir}"/>
            <arg value="${gatk.resources.hg18.fasta}"/>
        </exec>
    </target>
    
    <!-- Create reference index files for build hg19 -->
    <target name="resources-bwa-hg19" depends="init-resources">
        <mkdir dir="${bwa.resources.indexes.dir}"/>
        <mkdir dir="${bwa.resources.indexes.colorspace.dir}"/>
        <symlink link="${bwa.resources.indexes.dir}/${gatk.resources.hg19.fasta}.fasta" 
            resource="${resources.dir}/${gatk.resources.hg19.dir}/${gatk.resources.hg19.fasta}.fasta"/>
        <symlink link="${bwa.resources.indexes.colorspace.dir}/${gatk.resources.hg19.fasta}.fasta" 
            resource="${resources.dir}/${gatk.resources.hg19.dir}/${gatk.resources.hg19.fasta}.fasta"/>
        <echo message="Writing BWA index files for build hg19. This may take a while..."/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="bwa-resources.sh"/>
            <arg value="${bwa.resources.indexes.dir}"/>
            <arg value="${bwa.resources.indexes.colorspace.dir}"/>
            <arg value="${gatk.resources.hg19.fasta}"/>
        </exec>
    </target>

    <!-- Create Bowtie reference index files -->
    <target name="resources-bowtie" depends="resources-bowtie-b36,resources-bowtie-b37,resources-bowtie-hg18,resources-bowtie-hg19"/>

    <!-- Create reference index files for build b36 -->
    <target name="resources-bowtie-b36" depends="init-resources">
        <mkdir dir="${bowtie.resources.indexes.dir}"/>
        <mkdir dir="${bowtie.resources.indexes.colorspace.dir}"/>
        <echo message="Writing Bowtie index files for build b36. This may take a while..."/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="bowtie-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.b36.dir}"/>
            <arg value="${bowtie.resources.indexes.dir}"/>
            <arg value="${bowtie.resources.indexes.colorspace.dir}"/>
            <arg value="${gatk.resources.b36.fasta}"/>
        </exec>
    </target>

    <!-- Create reference index files for build b37 -->
    <target name="resources-bowtie-b37" depends="init-resources">
        <mkdir dir="${bowtie.resources.indexes.dir}"/>
        <mkdir dir="${bowtie.resources.indexes.colorspace.dir}"/>
        <echo message="Writing Bowtie index files for build b37. This may take a while..."/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="bowtie-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.b37.dir}"/>
            <arg value="${bowtie.resources.indexes.dir}"/>
            <arg value="${bowtie.resources.indexes.colorspace.dir}"/>
            <arg value="${gatk.resources.b37.fasta}"/>
        </exec>
    </target>

    <!-- Create reference index files for build hg18 -->
    <target name="resources-bowtie-hg18" depends="init-resources">
        <mkdir dir="${bowtie.resources.indexes.dir}"/>
        <mkdir dir="${bowtie.resources.indexes.colorspace.dir}"/>
        <echo message="Writing Bowtie index files for build hg18. This may take a while..."/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="bowtie-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.hg18.dir}"/>
            <arg value="${bowtie.resources.indexes.dir}"/>
            <arg value="${bowtie.resources.indexes.colorspace.dir}"/>
            <arg value="${gatk.resources.hg18.fasta}"/>
        </exec>
    </target>

    <!-- Create reference index files for build hg19 -->
    <target name="resources-bowtie-hg19" depends="init-resources">
        <mkdir dir="${bowtie.resources.indexes.dir}"/>
        <mkdir dir="${bowtie.resources.indexes.colorspace.dir}"/>
        <echo message="Writing Bowtie index files for build hg19. This may take a while..."/>
        <exec executable="/bin/bash"
            dir="${basedir}">
            <arg value="bowtie-resources.sh"/>
            <arg value="${resources.dir}/${gatk.resources.hg19.dir}"/>
            <arg value="${bowtie.resources.indexes.dir}"/>
            <arg value="${bowtie.resources.indexes.colorspace.dir}"/>
            <arg value="${gatk.resources.hg19.fasta}"/>
        </exec>
    </target>

</project>

