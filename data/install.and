std.echo("Preparing the resources for the microarray bundle...")

COLUMN_GENE_ID = "EnsemblId"
mappings = "source\ttarget\tfile\n"

// Fetch Ensembl genes

genesIn = BiomartAnnotator(attributes      = "ensembl_gene_id,external_gene_id",
                           constantFilters = "status=KNOWN",
                           @keep           = false)
genes = CSVCleaner(genesIn.annotations,
                   rename    = "ensembl_gene_id="+COLUMN_GENE_ID+",external_gene_id=name",
                   skipQuota = "*")

// Biomart mappings for the gene identifiers


bmruns = record(
  bm1 = record(source = "ensembl_gene_id",
               target = "illumina_humanht_12",
               desc=""),

  bm2 = record(source = "ensembl_gene_id",
               target = "agilent_wholegenome",
               desc=""),

  bm3 = record(source = "ensembl_gene_id",
               target = "affy_hc_g110",
               desc="HC-G110"),

  bm4 = record(source = "ensembl_gene_id",
               target = "affy_hg_focus",
               desc="HG-FOCUS"),

  bm5 = record(source = "ensembl_gene_id",
               target = "affy_hg_u133_plus_2",
               desc="HG-U133_PLUS_2"),

  bm6 = record(source = "ensembl_gene_id",
               target = "affy_hg_u133a_2",
               desc="HG-U133A_2"),

  bm7 = record(source = "ensembl_gene_id",
               target = "affy_hg_u133a",
               desc="HG-U133A"),
               
  bm8 = record(source = "ensembl_gene_id",
               target = "affy_hg_u133b",
               desc="HG-U133B"),
               
  bm9 = record(source = "ensembl_gene_id",
               target = "affy_hg_u95av2",
               desc="HG-U95Av2"),
               
  bm10 = record(source = "ensembl_gene_id",
               target = "affy_hg_u95b",
               desc="HG-U95B"),
               
  bm11 = record(source = "ensembl_gene_id",
               target = "affy_hg_u95c",
               desc="HG-U95C"),
               
  bm12 = record(source = "ensembl_gene_id",
               target = "affy_hg_u95d",
               desc="HG-U95D"),
               
  bm13 = record(source = "ensembl_gene_id",
               target = "affy_hg_u95e",
               desc="HG-U95E"),
               
  bm14 = record(source = "ensembl_gene_id",
               target = "affy_hg_u95a",
               desc="HG-U95A"),
               
  bm15 = record(source = "ensembl_gene_id",
               target = "affy_hugenefl",
               desc="HuGeneFL"),
               
  bm16 = record(source = "ensembl_gene_id",
               target = "affy_huex_1_0_st_v2",
               desc="HuEx-1_0-st-v2"),
               
  bm17 = record(source = "ensembl_gene_id",
               target = "affy_hugene_1_0_st_v1",
               desc="HuGene-1_0-st-v1"),
               
  bm18 = record(source = "ensembl_gene_id",
               target = "affy_u133_x3p",
               desc="U133-X3P"),
               
  bm19 = record(source = "ensembl_gene_id",
               target = "refseq_mrna",
               desc="RefSeq nRNA Id"),

  bm20 = record(source = "ensembl_gene_id",
               target = "hgnc_symbol",
               desc="HGNS symbol")
               
               
  
)


for bmid, bmrun : bmruns {
  std.echo(bmid, "convert", bmrun.source, "to", bmrun.target)
  file     = "idmaps/"+bmrun.source+"_to_"+bmrun.target+".csv"
  mappings = mappings+bmrun.source+"\t"+bmrun.target+"\t"+file+"\n"
  bm = BiomartAnnotator(filter        = genes,
                        filterColumns = COLUMN_GENE_ID,
                        filterTypes   = bmrun.source,
                        attributes    = bmrun.target,
                        batchSize     = 10000,
                        @name         = bmid)
  
  mappingCopy = BashEvaluate(script = 'cd `dirname @var1@`;cp @var1@ ../../'+file,
                             var1   = bm.annotations,
                             @keep  = false,
                             @name	= "mappingCopy_"+bmid)
}

mappingFile = StringInput(content = mappings)
