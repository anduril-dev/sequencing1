// This network is used to test LaTeX descriptions of the components
// * Components are tested in disabled state

useFunctions = true

// INPUTS OF VARIOUS TYPES

aFile   = "network.and" // A file that exists
aFloat  = 0.5           // A floating point number
anInt   = 123           // An integer number
aString = "aRecord"     // Some text

// data types:
// * Make sure that you hide these entries in ComponentViewRules.csv
dtArray                 = INPUT(path=aFile, @enabled=false) // Generic
// Real data types
dtAffyDirectory         = INPUT(path=aFile, @enabled=false)
dtAgilentDirectory      = INPUT(path=aFile, @enabled=false)
dtAnnotationTable       = INPUT(path=aFile, @enabled=false)
dtBAM                   = INPUT(path=aFile, @enabled=false)
dtBinaryFile            = INPUT(path=aFile, @enabled=false)
dtBinaryFolder          = INPUT(path=aFile, @enabled=false)
dtCSV                   = INPUT(path=aFile, @enabled=false)
dtCSVList               = INPUT(path=aFile, @enabled=false)
dtDNARegion             = INPUT(path=aFile, @enabled=false)
dtExcel                 = INPUT(path=aFile, @enabled=false)
dtFASTA                 = INPUT(path=aFile, @enabled=false)
dtGenePixDirectory      = INPUT(path=aFile, @enabled=false)
dtGraphML               = INPUT(path=aFile, @enabled=false)
dtIDList                = INPUT(path=aFile, @enabled=false)
dtIlluminaDirectory     = INPUT(path=aFile, @enabled=false)
dtImageList             = INPUT(path=aFile, @enabled=false)
dtLatex                 = INPUT(path=aFile, @enabled=false)
dtListTransformation    = INPUT(path=aFile, @enabled=false)
dtLogMatrix             = INPUT(path=aFile, @enabled=false)
dtMatlabSource          = INPUT(path=aFile, @enabled=false)
dtMatrix                = INPUT(path=aFile, @enabled=false)
dtModelica              = INPUT(path=aFile, @enabled=false)
dtMotifSet              = INPUT(path=aFile, @enabled=false)
dtOntology              = INPUT(path=aFile, @enabled=false)
dtProperties            = INPUT(path=aFile, @enabled=false)
dtRSource               = INPUT(path=aFile, @enabled=false)
dtSampleGroupTable      = INPUT(path=aFile, @enabled=false)
dtSBML                  = INPUT(path=aFile, @enabled=false)
dtSetList               = INPUT(path=aFile, @enabled=false)
dtSNPFreq               = INPUT(path=aFile, @enabled=false)
dtSNPMatrix             = INPUT(path=aFile, @enabled=false)
dtSQL                   = INPUT(path=aFile, @enabled=false)
dtTextFile              = INPUT(path=aFile, @enabled=false)
dtVCF                   = INPUT(path=aFile, @enabled=false)
dtXML                   = INPUT(path=aFile, @enabled=false)

// LIST OF FUNCTIONS
if (useFunctions) {

  Align_I = Align(reference=dtFASTA, reads=dtBinaryFile)

  BamCombiner_I = BamCombiner(reference=dtFASTA)

  BamReducer_I = BamReducer(reference=dtFASTA, bam=dtBAM)

  BamRegrouper_I = BamRegrouper(bam=dtBAM, LB=aString, PL=aString, PU=aString, SM=aString)

  BamReorder_I = BamReorder(reference=dtFASTA, bam=dtBAM)

  BaseRecalibrator_I = BaseRecalibrator(reference=dtFASTA, bam=dtBAM, dbsnp=dtVCF)

  DuplicateMarker_I = DuplicateMarker(reference=dtFASTA, bam=dtBAM)

  Realigner_I = Realigner(reference=dtFASTA, bam=dtBAM)

  ReferenceIndexer_I = ReferenceIndexer(reference=dtFASTA, aligner=aString)

  Sam2Fastq_I = Sam2Fastq(alignment=dtBAM)

  VariantAnnotator_I = VariantAnnotator(variantQuery=dtVCF, @enabled=false)

  VariantCaller_I = VariantCaller(reference=dtFASTA)

  VariantCombiner_I = VariantCombiner(reference=dtFASTA)

  VariantLiftover_I = VariantLiftover(chain=dtTextFile, oldReference=dtFASTA, newReference=dtFASTA, variants=dtVCF)

  VariantRecalibrator_I = VariantRecalibrator(reference=dtFASTA)

  VariantSelector_I = VariantSelector(reference=dtFASTA, variants=dtVCF)

  VariantValidator_I = VariantValidator(reference=dtFASTA, variants=dtVCF)

}

// LIST OF COMPONENTS

Blast_I = Blast(query=dtFASTA, dbSeq=dtFASTA)

ControlFreeC_I = ControlFreeC(gender=aString, inputFormat=aString, mateOrientation=aString, ploidy=anInt, @enabled=false)

RegionConvert_I = RegionConvert(@enabled=false)

RegionTransformer_I = RegionTransformer(@enabled=false)

// -------------------------------------------
// FUNCTIONAL PART OF THE NETWORK
// -------------------------------------------

/** Display properties for the component descriptions */
componentDescProp = INPUT(path="ComponentViewRules.csv")
/** Sequencing bundle bibtex. */
seqBibtex = INPUT(path=$ANDURIL_HOME+"/sequencing/components/report-BibTeX/sequencing.bib")

/** This instance description should be seen together with the component description. */
cfgReport = ConfigurationReport(compStyles=componentDescProp)

reportTemplate = LatexTemplate(authors  = "Marko Laakso, Riku Louhimo",
                               bibtex1  = seqBibtex,
                               printTOC = true,
                               title    = "Descriptions of \\Pipeline{} Components")

/** This is the only description shown for the PDF compiler */
summaryReport = LatexPDF(header   = reportTemplate.header,
                         footer   = reportTemplate.footer,
                         document = cfgReport.report,
                         verbose  = false,
                         useRefs  = true)

/** End report for the study */
OUTPUT(summaryReport.document, @enabled=false)

