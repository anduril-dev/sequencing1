// ***************************************************************
// Functions used:
// - Sam2Fastq: convert input data BAM to unaligned reads
// - ReferenceIndexer: create .dict, .fai and aligner index files 
// for reference
// - Align: aligns reads to the reference
// - DuplicateMarker: mark PCR duplicates
// - Realigner: do local realignment around indels
// - BaseRecalibrator: recalibrate base qualities
// - BamReducer: compresses alignment to reduced format
// ***************************************************************

// Create auxiliary files for the reference
reference = ReferenceIndexer(
    reference=fasta,
    aligner=aligner,
    length="short"
    )

// Combine reads into one file
combined = BamCombiner(
    reference=reference,
    bam1=bam1,
    bam2=bam2,
    memory=memory
    )

// Convert to unaligned reads
// Dummy step in order to get unaligned reads for testing of Align
unaligned = Sam2Fastq(
    alignment=combined
    )

// Align reads to the reference
aligned = Align(
    reference=reference,
    reads=unaligned.reads,
    mate=unaligned.mate,
    memory=memory,
    aligner=aligner,
    sample="dnaseq"
    )

// Mark duplicates
marked = DuplicateMarker(
    reference=reference,
    bam=aligned.alignment,
    memory=memory
    )
OUTPUT(marked.metrics)

// Combine background indels just for the fun
// Both indels can also be input separately into the Realigner
indels = VariantCombiner(
    reference=reference,
    variants1=indels1,
    variants2=indels2,
    memory=memory
    )

// Realign around indels
realigned = Realigner(
    reference=reference,
    bam=marked.alignment,
    indels1=indels,
    memory=memory
    )

// Recalibrate bases
calibrated = BaseRecalibrator(
    reference=reference,
    bam=realigned.alignment,
    dbsnp=dbsnp,
    memory=memory,
    plot=calibration_plot
    )

// Reduce alignment
// Reduced alignments can be called using GATK
reduced = BamReducer(
    reference=reference,
    bam=calibrated.alignment,
    memory=memory
    )
