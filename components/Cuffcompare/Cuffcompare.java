import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;

import java.io.IOException;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class Cuffcompare extends SkeletonComponent {

    private File reference_annotation;
    private File seq_dir;
    
    private String command="";
    private File outdir;
    
    private IndexFile arrayIn;
    private IndexFile arrayOut;

    private File[] filenames;
    
   	private boolean help;
   	private String outprefix;
    private boolean contained;
    private boolean verbose;
    private boolean R;
    
    private int size;
    private String inputFiles="";
	
	
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        
        // Get inputs, outputs and parameters from command file
        getInsOuts(cf);
        getParameters(cf);
        
        /* Since cuffcompare creates some output files in the input directory
        first we have to move the inputs to the output directory to avoid this problem*/
        copyInputs();
         
        // Create the command for Cuffcompare
        createCommand(cf);
        System.out.println("Execution command is: " + command);       
        int status = IOTools.launch(command, outdir);
        // Check for error in execution
        if (status > 0) {
            return ErrorCode.ERROR;
        }
         // Rename output files
         
        try {
            prepareOutputDir(cf);
        } catch (IOException exception) {
            throw exception;
        }
        // Return success
        return ErrorCode.OK;
    }
    
    /**
     * Gets inputs and outputs from the command file
     *
     */
    private void getInsOuts(CommandFile cf) throws Exception{
        int i=1;

        if (cf.inputDefined("reference_annotation")) 
            reference_annotation = cf.getInput("reference_annotation");
        if (cf.inputDefined("seq_dir")) 
            seq_dir = cf.getInput("seq_dir");

        arrayIn = cf.readInputArrayIndex("arrayIn"); 
        cf.getOutput("arrayOut").mkdirs();
        arrayOut = new IndexFile();
        
        size=arrayIn.size();
        filenames= new File[size];
        
        outdir=cf.getOutput("arrayOut");

        System.out.println("outdir "+outdir.getName()+" "+outdir.getAbsolutePath());    
        
    }
    
     /**
     * Gets parameters from the command file
     *
     */
    private void getParameters(CommandFile cf) {
        help = cf.getBooleanParameter("help");
        outprefix = cf.getParameter("outprefix");
        if (outprefix.equals(""))
            outprefix="cuffcmp";
        contained = cf.getBooleanParameter("contained");
        verbose = cf.getBooleanParameter("verbose");
        R = cf.getBooleanParameter("R");
 
    }
    
   /**
     * Copies input files to the output directory
     *
     */
    private void copyInputs() throws Exception {
        int i=0, status;
        String cmmd, filename;
	    for(String key : arrayIn) {	
	        
			File file = arrayIn.getFile(key);
			cmmd="cp "+file.getAbsolutePath()+" "+outdir.getAbsolutePath()+"/"+key+"_"+file.getName();
			System.out.println(cmmd);
			status = IOTools.launch(cmmd);
			
			filename=outdir.getAbsolutePath()+"/"+key+"_"+file.getName();
			
	        inputFiles += " " + filename;
	        
	        filenames[i] = new File(filename);
	       
            //System.out.println("f: "+filename);
	        i++;
	    }
    }
    
    
    private void createCommand(CommandFile cf) {
        command = "cuffcompare";
         if (cf.inputDefined("outprefix"))
            command +=" -o "+outprefix;
        if (help) 
            command +=" -h";  
        if (cf.inputDefined("reference_annotation") )
            command +=" -r "+reference_annotation.getAbsolutePath();
        if (R)
            command +=" -R";
        if (cf.inputDefined("seq_dir"))
            command +=" -s "+seq_dir.getAbsolutePath();
	    if (contained) 
	        command +=" -C";  
	    if (verbose) 
	        command +=" -V"; 
	    
	    command+=inputFiles;

    }
    
    /**
	 * Rename Cuffdiff output files to the component output files
	 *
	 */
	private void prepareOutputDir(CommandFile cf) throws Exception {
	    String filename, key;
	    String [] tmp = new String[3];
	    int j;
	    System.out.println("in rename files ");
	    
	    try {
	        FilenameFilter filter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.contains(outprefix);
                }
            };
        
            File[] children;
            if (outdir.isDirectory()) {
                children = outdir.listFiles(filter);
                if (children != null) {
                    for (int i=0; i<children.length; i++) {
                        tmp=children[i].getName().split(outprefix+".");
                        key=tmp[1];
                        arrayOut.add(key,children[i]);
                     }
                }
            }
            arrayOut.write(cf, "arrayOut");
            for (int i=0; i<filenames.length; i++) {
                System.out.println("Cleaning output directory: "+filenames[i].getName());
                filenames[i].delete();
                
            }
        } catch (Exception exception) {
	        throw exception;
	    }
                  
	}
    /**
     * @param args
     */
    public static void main(String[] args) {
        new Cuffcompare().run(args);
    }

}
