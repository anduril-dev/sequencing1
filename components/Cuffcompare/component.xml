<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>Cuffcompare</name>
    <version>1.0</version>
    <doc>
    Cufflinks includes a program that you can use to help analyze the transfrags you assemble. The program cuffcompare helps you:

    Compare your assembled transcripts to a reference annotation
    Track Cufflinks transcripts across multiple experiments (e.g. across a time course)

    Cuffcompare takes Cufflinks' GTF output as input, and optionally can take a "reference" annotation (such as from Ensembl) 
    </doc>
    <author email="alejandra.cerverataboada@helsinki.fi">Alejandra Cervera</author>
    <launcher type="java">
        <argument name="class" value="Cuffcompare" />
        <argument name="source" value="Cuffcompare.java" />
    </launcher>
    <requires>Cufflinks</requires>
    <requires name="installer" optional="false">
            <resource type="bash">install.bash</resource>
    </requires>
    <type-parameters>
        <type-parameter name="T1"/>
    </type-parameters>
    <inputs>
    	<input name="arrayIn" type="T1" array="true">
            <doc>An array with the transcripts to be compared.</doc>
        </input>
        <input name="reference_annotation" type="GTF" optional="true">
            <doc>An optional "reference" annotation GFF file. Each sample is matched against this file, and sample isoforms are 
            tagged as overlapping, matching, or novel where appropriate. See the refmap and tmap output file descriptions below. 
            </doc>
        </input>
        <input name="seq_dir" type="BinaryFolder" optional="true">
            <doc>Causes cuffcompare to look into for fasta files with the underlying genomic sequences (one file per contig) 
            against which your reads were aligned for some optional classification functions. For example, Cufflinks transcripts
            consisting mostly of lower-case bases are classified as repeats. Note that seq_dir must contain one fasta file per
            reference chromosome, and each file must be named after the chromosome, and have a .fa or .fasta extension. 
            </doc>
        </input>
    </inputs>
    <outputs>
        
        <output name="arrayOut" type="T1" array="true">
            <doc>An array with all output files from cuffcompare. Check cuffcompare documentation for details.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="outprefix" type="string" default="">
            <doc>All output files created by Cuffcompare will have this prefix (e.g. outprefix.loci, outprefix.tracking, etc.). 
            If this option is not provided the default output prefix being used is: "cuffcmp"</doc>
        </parameter>
        <parameter name="help" type="boolean" default="false">
            <doc>Prints the help message and exits.</doc>
        </parameter>
        <parameter name="R" type="boolean" default="false">
            <doc>If -r was specified, this option causes cuffcompare to ignore reference transcripts that are not overlapped by any
             transcript in one of cuff1.gtf,...,cuffN.gtf. Useful for ignoring annotated transcripts that are not present in your 
             RNA-Seq samples and thus adjusting the "sensitivity" calculation in the accuracy report written in the outprefix file 
            </doc>
        </parameter>
        <parameter name="contained" type="boolean" default="false">
            <doc>Enables the "contained" transcripts to be also written in the outprefix.combined.gtffile, with the attribute
             "contained_in" showing the first container transfrag found. By default, without this option, cuffcompare does not 
             write in that file isoforms that were found to be fully contained/covered (with the same compatible intron structure) 
             by other transfrags in the same locus. 
            </doc>
        </parameter>
        <parameter name="verbose" type="boolean" default="false">
            <doc>Print lots of status updates and other diagnostic information. </doc>
        </parameter>

    </parameters>
</component>
