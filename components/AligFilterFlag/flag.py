#! /usr/bin/env python

#for using the flag in a sam file and splitting the alignments 

import sys
import component_skeleton.main
import csv
import re

def execute(cf):
    fileIn = cf.get_input('alignment')
    fileOut1= cf.get_output('splitAlignments1')
    fileOut2= cf.get_output('splitAlignments2')
    bits = cf.get_parameter("flag")
    bitList = bits.split(",")
    print bitList
    cf.write_log('read: '+fileIn)
    f = open(fileIn, 'rt')
    p = open(fileOut1, 'wt')
    q = open(fileOut2, 'wt')
    reader = csv.reader(f, delimiter='\t')
    writer1 = csv.writer(p, delimiter='\t')
    writer2 = csv.writer(q, delimiter='\t')
    for row in reader:
        col1 = row[0]
        if (col1.startswith("@")):
            writer1.writerow(row)
            writer2.writerow(row)
        else:
	    #row[0] is the read name
            flag = row[1]
            x=1
            for bit in bitList:
                if (int(flag) & int(bit))==0:
					x=0
            if (x>0):
                writer1.writerow(row)
            else:
                writer2.writerow(row)
    #f.close()
    return 0

component_skeleton.main.main(execute)

