<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>GenomeSlider</name>
    <version>1.0</version>
    <doc>Summarized genomic events at sliding window intervals and/or at predifined regions. The summarizing function can be specified eg. mean, median, sum, max, min. Missing values are handled. 
    </doc>
    <author email="Amjad.Alkodsi@Helsinki.FI">Amjad Alkodsi</author>
    <category>Analysis</category>
    <launcher type="R">
        <argument name="file" value="GenomeSlider.R" />
    </launcher>
    <inputs>
        <input name="matrix" type="CSV" optional="false">
            <doc>Input csv file having the data to be summarized.</doc>
        </input>
        <input name="regions" type="CSV" optional="true">
            <doc>Predefined regions for which summarizing will be performed. If not provided, only sliding window will be performed.</doc>
        </input>
        <input name="chrLengths" type="CSV" optional="true">
            <doc>CSV file containing chromosome lengths. Needed only for sliding window summarization.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="regionsSummary" type="CSV">
            <doc>Summarization over predifined regions.</doc>
        </output>     
        <output name="windowsSummary" type="CSV">
            <doc>Summarization over sliding windows.</doc>
        </output>  
    </outputs>
    <parameters>
        <parameter name="func" type="string" default="">
            <doc>The summarization function can be one of the following: "mean","median","sum","max","min". If empty string (default) was selected, the summarization will count events in <code>matrix</code> that overlaps with predefined regions or windows.
            </doc>
        </parameter>
        <parameter name="windowSize" type="int" default="1000">
            <doc>The size of sliding window.</doc>
        </parameter>
        <parameter name="stepSize" type="int" default="0">
            <doc>Step size for sliding window. If zero (default), non-overlapping windows will be generated.
            </doc>
        </parameter>
        <parameter name="matrixDataColumns" type="string" default="">
            <doc>R expression for selecting the data columns in <code>matrix</code> input e.g. "c(1,2,3)", "1:3", "c('column1','column2','column3')". If empty (default), the summarization will count events in <code>matrix</code> that overlaps with predefined regions or windows.
            </doc>
        </parameter>
        <parameter name="matrixRegionColumns" type="string" default="">
            <doc>Comma-separated column names for the chr-start-end columns in <code>matrix</code> input respectively. If empty (default), first three columns will be considered.
            </doc>
        </parameter>
        <parameter name="regionColumns" type="string" default="">
            <doc>Comma-separated column names for the chr-start-end columns in <code>regions</code> input respectively. If empty (default), first three columns will be considered.
            </doc>
        </parameter>
        <parameter name="echoRegionColumns" type="string" default="">
            <doc>Comma-separated column names for the columns from <code>regions</code> input to be echoed in the <code>regionSummary</code> output. If empty (default), no columns will be echoed.
            </doc>
        </parameter>
        <parameter name="matrixCategoryColumn" type="string" default="">
            <doc>Name of the column that has different categories. The output will have one column for each category in this column with values corresponding to category counts in each genomic region.
            </doc>
        </parameter>
        <parameter name="matrixCollapseColumn" type="string" default="">
            <doc>Name of the column that has different categories or IDs. The output will have one column having collapsed (comma-separated) unique categories or IDs present in the coresponding genomic region.
            </doc>
        </parameter>
    </parameters>
</component>
