library(componentSkeleton)
library(GenomicRanges)

		
execute <- function(cf) {
    # Parameters
    func	          	<- get.parameter(cf, 'func',           type='string')
    windowSize	  	  	<- get.parameter(cf, 'windowSize', 		   type='int')
    stepSize		  	<- get.parameter(cf, 'stepSize', 		   type='int')
    matrixDataColumns 	<- get.parameter(cf, 'matrixDataColumns',  type='string')
    matrixRegionColumns <- get.parameter(cf, 'matrixRegionColumns',type='string')
    regionColumns	  	<- get.parameter(cf, 'regionColumns',	   type='string')
    echoRegionColumns 	<- get.parameter(cf, 'echoRegionColumns',  type='string')
	matrixCategoryColumn<- get.parameter(cf, 'matrixCategoryColumn',type='string')
	matrixCollapseColumn<- get.parameter(cf, 'matrixCollapseColumn',type='string')
	
	# Input
	m <- CSV.read(get.input(cf,"matrix"))
	# used function
	if(func=="max")
		f <- function(x)max(x,na.rm=T)
	if(func=="min")
		f <- function(x)min(x,na.rm=T)
	if(func=="sum")
		f <- function(x)sum(x,na.rm=T)
	if(func=="mean")
		f <- function(x)mean(x,na.rm=T)
	if(func=="median")
		f <- function(x)median(x,na.rm=T)
	if(matrixDataColumns=="" | !func %in% c("max","min","sum","mean","median")){
		f <- function(x)length(x)
		matrixDataColumns <- "1"
	}	
	
	#preparing matrix input
	if (matrixCategoryColumn != "")
		m[,matrixCategoryColumn] = as.factor(m[,matrixCategoryColumn])
	if (matrixCollapseColumn != "")
		m[,matrixCollapseColumn] = as.character(m[,matrixCollapseColumn])

	if (matrixRegionColumns==""){
			chrColumn 	<- 1
			startColumn <- 2
			endColumn	<- 3
		}

	if (matrixRegionColumns!=""){
			r <- unlist(strsplit(matrixRegionColumns,","))
			if(length(r)!=3)
				write.error(cf,"matrixRegionColumns should have three values or empty")
			chrColumn 	<- r[1]
			startColumn <- r[2]
			endColumn	<- r[3]
		}

	mGR <- GRanges(m[,chrColumn],IRanges(m[,startColumn],m[,endColumn]))
	
	if (!input.defined(cf,"regions") & !input.defined(cf,"chrLengths"))
		write.error(cf,"either regions or chrLengths input must be provided")
		
	#### region mode ###
	if (input.defined(cf,"regions")){
		regions <- CSV.read(get.input(cf,"regions"))
		if (regionColumns==""){
			chrColumn 	<- 1
			startColumn <- 2
			endColumn	<- 3
		}
		if (regionColumns!=""){
			r <- unlist(strsplit(regionColumns,","))
			if(length(r)!=3)
				write.error(cf,"regionColumns should have three values or empty")
			chrColumn 	<- r[1]
			startColumn <- r[2]
			endColumn	<- r[3]
		}

		regions$uniqueID = as.character(paste0("ID",c(1:nrow(regions))))

		regionsGR <- GRanges(regions[,chrColumn],IRanges(regions[,startColumn],regions[,endColumn]))

		m$overlapingRegion <- NA
		overlaps <- as.data.frame(findOverlaps(mGR,regionsGR))
		mRep <- m[overlaps[,1],]
		regionsRep <- regions[overlaps[,2],]
		mRep[,"overlapingRegion"] <- regionsRep[,"uniqueID"]
		mOv <- mRep[!is.na(mRep$overlapingRegion),]
		#mOvSplit <- split(mOv,mOv$overlapingRegion)
		#mOvSummarized <- do.call(rbind, lapply(mOvSplit,function(x)apply(as.data.frame(x[,eval(parse(text=matrixDataColumns))]),2,f)))
		mOvSummarized <- apply(as.data.frame(mOv[,eval(parse(text=matrixDataColumns))]),2,function(x)tapply(x,mOv$overlapingRegion,f))
		if(matrixDataColumns=="" | !func %in% c("max","min","sum","mean","median")){
			mOvSummarized = data.frame(mOvSummarized)
			colnames(mOvSummarized) = "counts"
			}
		if(matrixCategoryColumn !=""){
			#categories <- do.call(rbind, lapply(mOvSplit, function(x)table(x[,matrixCategoryColumn])))
			categories <- do.call(rbind,tapply(mOv[,matrixCategoryColumn],mOv$overlapingRegion,table))
			mOvSummarized <- data.frame(mOvSummarized,categories)
		}
		if(matrixCollapseColumn !=""){
			#collapsed <- do.call(rbind, lapply(mOvSplit, function(x)as.data.frame(paste(unique(x[,matrixCollapseColumn]),collapse=","))))
			collapsed <- as.data.frame(as.character(tapply(mOv[,matrixCollapseColumn],mOv$overlapingRegion,function(x)paste(unique(x),collapse=","))))
			colnames(collapsed) = matrixCollapseColumn
			mOvSummarized <- data.frame(mOvSummarized,collapsed)
		}
		regionsFil <- regions[regions$uniqueID %in% rownames(mOvSummarized),]
		counts <- mOvSummarized[order(match(rownames(mOvSummarized),regionsFil$uniqueID)),]
		if(echoRegionColumns != ""){
			echoed <- unlist(strsplit(echoRegionColumns,","))
			output <- data.frame(regionsFil[,echoed],counts)
			if (length(echoed)==1)
				colnames(output)[1] <- echoed
		}
		if(echoRegionColumns == "")
			output <- data.frame(regionsFil[,c(chrColumn,startColumn,endColumn)],counts)
		rownames(output) <- NULL
		CSV.write(get.output(cf, 'regionsSummary'), output)

	}
	if (!input.defined(cf,"regions"))
		touch.output(cf, 'regionsSummary')


	## sliding window mode ##
	if (input.defined(cf,"chrLengths")){
		chrLengths <- CSV.read(get.input(cf,"chrLengths"))	
		slidingWindows = data.frame()
		if(stepSize>0){
			for (i in 1:nrow(chrLengths)){
				starts <- seq(from=1,to=chrLengths[i,2]-windowSize,by=stepSize)
				ends <- starts + windowSize - 1
				ends[length(starts)] <- chrLengths[i,2]
				chr <- rep(chrLengths[i,1],length(starts))
				chrWindows = data.frame(chr,starts,ends)
				slidingWindows = rbind(slidingWindows,chrWindows)
			}
		}
		if(stepSize<=0){
			for (i in 1:nrow(chrLengths)){
				starts <- seq(from=1,to=chrLengths[i,2]-windowSize,by=windowSize)
				ends <- starts + windowSize - 1
				ends[length(starts)] <- chrLengths[i,2]
				chr <- rep(chrLengths[i,1],length(starts))
				chrWindows = data.frame(chr,starts,ends)
				slidingWindows = rbind(slidingWindows,chrWindows)
			}
		}
		regions <- slidingWindows
		regions$uniqueID <- as.character(paste0("ID",c(1:nrow(regions))))
		regionsGR <- GRanges(regions[,1],IRanges(regions[,2],regions[,3]))
		
		m$overlapingRegion <- NA
		overlaps <- as.data.frame(findOverlaps(mGR,regionsGR))
		mRep <- m[overlaps[,1],]
		regionsRep <- regions[overlaps[,2],]
		mRep[,"overlapingRegion"] <- regionsRep[,"uniqueID"]
		mOv <- mRep[!is.na(mRep$overlapingRegion),]
		#mOvSplit <- split(mOv,mOv$overlapingRegion)
		#mOvSummarized <- do.call(rbind, lapply(mOvSplit,function(x)apply(as.data.frame(x[,eval(parse(text=matrixDataColumns))]),2,f)))
		mOvSummarized <- apply(as.data.frame(mOv[,eval(parse(text=matrixDataColumns))]),2,function(x)tapply(x,mOv$overlapingRegion,f))
		if(matrixDataColumns=="" | !func %in% c("max","min","sum","mean","median"))
			colnames(mOvSummarized) = "counts"
		if(matrixCategoryColumn !=""){
			#categories <- do.call(rbind, lapply(mOvSplit, function(x)table(x[,matrixCategoryColumn])))
			categories <- do.call(rbind,tapply(mOv[,matrixCategoryColumn],mOv$overlapingRegion,table))
			mOvSummarized <- data.frame(mOvSummarized,categories)
		}
		if(matrixCollapseColumn !=""){
			#collapsed <- do.call(rbind, lapply(mOvSplit, function(x)as.data.frame(paste(unique(x[,matrixCollapseColumn]),collapse=","))))
			collapsed <- as.data.frame(as.character(tapply(mOv[,matrixCollapseColumn],mOv$overlapingRegion,function(x)paste(unique(x),collapse=","))))
			colnames(collapsed) = matrixCollapseColumn
			mOvSummarized <- data.frame(mOvSummarized,collapsed)
		}
		regionsFil <- regions[regions$uniqueID %in% rownames(mOvSummarized),]
		counts <- mOvSummarized[order(match(rownames(mOvSummarized),regionsFil$uniqueID)),]
		summarizedSW <- data.frame(regionsFil[,1:3],counts)
		rownames(summarizedSW) <- NULL
		CSV.write(get.output(cf, 'windowsSummary'), summarizedSW)
}
	if (!input.defined(cf,"chrLengths"))
		touch.output(cf, 'windowsSummary')

}
main(execute)
