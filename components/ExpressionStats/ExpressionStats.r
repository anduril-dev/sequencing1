# Take as input treatment info and expression matrices. Output mean, median, st.dev per molecule
library(componentSkeleton)

# Read an optional table
table.in <- function(cf, tn) {
  if (input.defined(cf, tn)) {
     data <- CSV.read(get.input(cf, tn))
     write.log(cf, paste("Input ",tn," contains ",ncol(data)," columns and ",nrow(data)," rows.",sep=""))
  } else {
     data <- NULL
  }
  data
}

execute <- function(cf) {

    # Inputs
    expr <- table.in(cf,"expr")
    ref <- table.in(cf,"ref")
    refID <- get.parameter(cf,"refID","string")
    refGroup <- get.parameter(cf,"refGroup","string")
    exprID <- get.parameter(cf, "exprID", "string")
    geneSet <- table.in(cf,"geneSet")
    bodyMap <- table.in(cf,"bodyMap")
    bodySite <- get.parameter(cf,"bodySite","string")
    topNum <- get.parameter(cf,"topNum","int")
    makeVisuals <- get.parameter(cf,"makeVisuals","string")
    biotype <- get.parameter(cf,"biotype","string")
    biotype_min <- get.parameter(cf,"biotype_min","float")

    # Create latex document
    report.dir <- get.output(cf, 'report')
    dir.create(report.dir, recursive=TRUE)
    report.out <- sprintf('%s produced no \\LaTeX{} output.', get.metadata(cf, 'instanceName'))

   # Create array output dir.
    statsArray <- get.output(cf, "statsArray")
    if (!file.exists(statsArray)) {
        dir.create(statsArray, recursive=TRUE)
    }

    # Array object
    statsArray.output <- Array.new()


    # If refID or exprID is empty, take the first columns
    if (refID == "") {
        refID <- colnames(ref)[1] 
    }
    if (exprID == "") {
        exprID <- colnames(expr)[1]
    }
    print(paste("Parameters used: refID=",refID,", refGroup=", refGroup, ", exprID=", exprID,sep=""))

    # Verify parameter columns exist
    if (length(grep(paste("^",refID,"$",sep=""),colnames(ref))) != 1) {   
        stop(paste(refID, " is not a unique column name of input file REF.",sep=""))
    }
    if (length(grep(paste("^",refGroup,"$",sep=""),colnames(ref))) != 1) {   
        stop(paste(refGroup, " is not a unique column name of input file REF.",sep=""))
    }
    if (length(grep(paste("^",exprID,"$",sep=""),colnames(expr))) != 1) {   
        stop(paste(exprID, " is not a unique column name of input file EXPR.",sep=""))
    }

    # ID and initialize group types
    groupTypes <- unique(ref[,refGroup])
    groups <- list()
    stats <- list()
    for (G in groupTypes) {
        groups[[G]] <- data.frame()
        stats[[G]] <- data.frame()
    }

    # Sort samples into groups
    for (name in colnames(expr)) {
        grouped <- ref[grep(paste("^",name,"$",sep=""), ref[,refID]),refGroup]
        if (length(grouped) > 0) {
            groups[[grouped]] <- c(unlist(groups[[grouped]]),name)
        }
    }

    # Calculate mean, median and st.dev for each group
    for (G in names(groups)) {
        overlap <- intersect(groups[[G]],colnames(expr))
        meanG <- rowMeans(expr[,overlap])
        medianG <- apply(expr[,overlap],1, function(x) median(x))
        sdevG <- apply(expr[,overlap],1,function(x) sd(x))
        stats[[G]] <- data.frame(mean=meanG, median=medianG, sdev=sdevG)
        row.names(stats[[G]]) <- expr[,exprID]
        colnames(stats[[G]]) <- paste(G,colnames(stats[[G]]),sep="_")
    }

    # merge all data
    mergedStats <- data.frame(ID=expr[,exprID])
    row.names(mergedStats) <- expr[,exprID]
    for (G in names(stats)) {
        mergedStats <- cbind(mergedStats,stats[[G]])
}

    # Remove rows with zero
    mergedStats <- mergedStats[which(rowSums(mergedStats[,-1]) > 0),]

    # Write output
    CSV.write(get.output(cf,"stats"), mergedStats) 

    ### Make visuals
    if (makeVisuals == "true") {
        library(reshape)
        library(ggplot2)
        library(MASS)
        setwd(report.dir)

        # Check that inputs exist
        if (is.null(geneSet)) {   
            stop("Input geneSet is missing.")
        }
        if (is.null(bodyMap)) {   
            stop("Input bodyMap is missing.")
        }

        #get numeric columns
        mask <- sapply(expr, is.numeric)
        numCols <- expr[,mask]

        #add back ids as rownames to matrix
        rownames(expr) <- expr[,exprID]
        idsNums <-cbind(id=rownames(expr),numCols)

        #remove non-expressed rows
        expressed <- idsNums[rowMeans(idsNums[,-1], na.rm=TRUE)>0,]

        #repeat same thing for geneSet
        if (length(grep(paste("^",exprID,"$",sep=""),colnames(geneSet))) != 1) {   
            stop(paste(exprID, " is not a unique column name of input file geneSet.",sep=""))
        }
        rownames(geneSet) <- geneSet[,exprID]
        oncoExpressed <-merge(geneSet,expressed, by.x=exprID, by.y="id")

        #find top most expressed genes
        a<-c()
        for (i in 2:ncol(expressed)) {
            a<-union(a,as.character(expressed[order(expressed[,i],decreasing=TRUE),1][1:min(topNum,nrow(expressed))]))
        }

        #top table
        top <- expressed[expressed[,1]%in%a,]

        #find top most expressed oncogenes
        a<-c()
        for (i in 2:ncol(oncoExpressed)) {
            a<-union(a,as.character(oncoExpressed[order(oncoExpressed[,i],decreasing=TRUE),1][1:min(topNum,nrow(expressed))]))
        }

        #top table
        topOnco <- oncoExpressed[oncoExpressed[,1]%in%a,]

        #get numeric columns from tissue expression matrix
        rownames(bodyMap) <- bodyMap[,1]

        #remove non expressed rows
        tissue <- merge(bodyMap,expressed,by.x=colnames(bodyMap)[1], by.y=colnames(expressed)[1])

        if (bodySite=="body") {
            a<-c()
            for (i in 2:ncol(tissue)) {
                a<-union(a,as.character(tissue[order(tissue[,i],decreasing=TRUE),1][1:min(topNum,nrow(expressed))]))
            }
        } else {
            a<-tissue[order(tissue[,bodySite],decreasing=TRUE),1][1:min(topNum,nrow(expressed))]
        }

        tissueTop <- expressed[expressed[,1]%in%a,]

        if (biotype!="skip") {
            bioNums <- cbind(bioType=expr$"bio_type",numCols)
            aggZero <- aggregate(bioNums, by=list(bioNums$"bioType"), FUN=function(x) {sum(x==0, na.rm=TRUE)})
            aggLow <- aggregate(bioNums, by=list(bioNums$"bioType"), FUN=function(x) {sum(x>0 & x<=2.5, na.rm=TRUE)})
            aggHigh <- aggregate(bioNums, by=list(bioNums$"bioType"), FUN=function(x) {sum(x>2.5, na.rm=TRUE)})
            aggExpr <- aggregate(bioNums, by=list(bioNums$"bioType"), FUN=function(x) {sum(x>biotype_min, na.rm=TRUE)})

            exprBio <- aggExpr[,3:ncol(aggExpr)]
            bioType <- as.character(aggExpr[,1])
            min <- apply(exprBio,1,min)
            #mean <- apply(exprBio,1,mean)
            max <- apply(exprBio,1,max)
            summary <- cbind(bioType,min,max)
            sorted <- summary[order(-max),]

            statsArray.list <- list(topExpressed=top,topExpressedByTissue=tissueTop,zero=aggZero[,-2],low=aggLow[,-2],high=aggHigh[,-2],summary=sorted,topOnco=topOnco)
        } else {
            statsArray.list <- list(topExpressed=top,topExpressedByTissue=tissueTop,zero=data.frame(),low=data.frame(),high=data.frame(),summary=data.frame(),topOnco=topOnco)
        }

        # Write array outputs
        for (i in 1:length(statsArray.list)) {
            key <- names(statsArray.list)[i]
            filename <- paste(key,".csv",sep="")
            CSV.write(paste(statsArray, "/",filename, sep=""), statsArray.list[[i]])
            statsArray.output <- Array.add(statsArray.output, key, filename)
        }

        #dendogram
        tExpressed <-t(expressed[,-1])
        hc = hclust(dist(tExpressed))
        pdf("hierarchical_clustering.pdf") 
        plot(hc,hang=-1,xlab="Samples",sub="")
        dev.off()

        #plots
        groupLimits = colnames(expressed)
        data = melt(expressed)
        clean <-subset(data,data$value>0)

        p1=ggplot(clean,aes(x=value),colour=variable) + geom_density(alpha=0.2) + theme_bw() + 
    theme(plot.background = element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + xlab("log2 FPKMs")+ylab("Density")+ggtitle("Gene expression density")
        ggsave(file="density.pdf",plot=p1)

## HERE
        p2=ggplot(clean,aes(x=variable,y=value,fill=variable)) + geom_boxplot(outlier.shape = NA) +
            stat_summary(fun.y=mean, geom="point", shape=5, size=4) + xlab("samples")+ylab("log2 FPKMs")+ggtitle("Gene expression")
        ggsave(file="boxplot.pdf",plot=p2)

        p3=ggplot(clean,aes(value,fill=variable)) + geom_histogram(alpha=0.3,position="identity",aes(y=(..count..)/sum(..count..))) + theme_bw() + 
            theme(plot.background = element_blank(), panel.grid.major = element_blank(), panel.grid.minor = element_blank()) + xlab("log2 FPKMs")+ylab("Proportion")+ggtitle("Gene expression histogram")
        ggsave(file="histogram.pdf",plot=p3)

        fig1<-latex.figure(filename="histogram.pdf")
        fig2<-latex.figure(filename="density.pdf")
        fig3<-latex.figure(filename="boxplot.pdf")
        fig4<-latex.figure(filename="hierarchical_clustering.pdf")
        report.out <-c(fig1,fig2,fig3,fig4)
        file.remove("Rplots.pdf")
    }
    latex.write.main(cf, 'report', report.out)
    Array.write(cf, statsArray.output, "statsArray")
}

main(execute)
