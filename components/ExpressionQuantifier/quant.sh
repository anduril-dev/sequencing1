# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
set -ex


if [[ $array == "" ]]
then
    input=$( getinput alignment )
else
    input=$( getarrayfiles array | tr '\n' ','  | sed 's/,$//' )
fi

reference=$( getinput reference )
referenceFolder=$( getinput referenceFolder )
mask=$( getinput mask )
annotation=$( getinput annotation )
tool=$( getparameter tool )
myFolder=$( getoutput folder )
parameters=$( getparameter parameters )
genes=$( getoutput genes )
isoforms=$( getoutput isoforms )
extra=$( getparameter extra )
outString="out"
mkdir $myFolder

#options=$parameters" -o "$myFolder

echo "Tool selected: "$tool
echo "Alignment: "$input
echo "Inputs:" $reference $referenceFolder $annotation $mask
echo "Parameters: "$parameters
echo "Output folder: "$myFolder

if [[ $tool == "cufflinks" ]]
then
    options=$parameters" -o "$myFolder
    if [[ $annotation != "" ]]
    then
        options=$parameters" -o "$myFolder" -G "$annotation
    fi
    if [[ $reference != "" ]]
    then 
        options=$options" -b "$reference
    fi
    if [[ $mask != "" ]]
    then 
        options=$options" -M "$mask
    fi
    command=$options" "$input
    echo $tool $command
    $tool $command
    if [[ -f ${myFolder}/isoforms.fpkm_tracking ]]
    then
        outFile1=${myFolder}/isoforms.fpkm_tracking
        outFile2=${myFolder}/genes.fpkm_tracking
    else
        echo "output missing"
    fi

elif [[ $tool == "express" ]]
then
    options=$parameters" -o "$myFolder
    command=$options" "$reference" "$input
    echo $tool $command
    $tool $command
    if [[ -f ${myFolder}/results.xprs ]]
    then 
        outFile1=${myFolder}/results.xprs
        echo $outFile1 outfile
    else
        echo "output missing"
    fi

elif [[ $tool == "rsem" ]]
then
    currDir=$( pwd )
    cd $myFolder
    echo $rsemRef
    command=$parameters" "$input" "$referenceFolder/${extra}" "$outString
    echo rsem-calculate-expression $command 
    rsem-calculate-expression $command
    if [[ -f out.genes.results ]]
    then 
        outFile1=${myFolder}/out.genes.results
        outFile2=${myFolder}/out.isoforms.results
    else
        echo "output missing"
    fi  
    cd $currDir

elif [[ $tool == "bitseq" ]]
then
    currDir=$( pwd )
    cd $myFolder
    command1=$input" --outFile "$outString".prob --trSeqFile "$reference" --trInfoFile "$outString".tr "$parameters
    echo parseAlignment $command1
    parseAlignment $command1
    command2=$outString".prob --outPrefix "$outString" --trInfoFile "$outString".tr "$extra
    echo estimateExpression $command2
    estimateExpression $command2 
    if [[ -f ${myFolder}/out.rpkm ]]
    then 
        tmpFile=${myFolder}/out.rpkm
    elif [[ -f ${myFolder}/out.counts ]]
    then 
        tmpFile=${myFolder}/out.counts
    elif [[ -f ${myFolder}/out.theta ]]
    then
        tmpFile=${myFolder}/out.theta
    elif [[ -f ${myFolder}/out.tau ]]
    then
        tmpFile=${myFolder}/out.tau
    fi
    ext=${FILE#*.}
    getVariance -o ${myFolder}/transcripts.$ext $tmpFile
    getGeneExpression -o ${myFolder}/geneTmp.$ext -t ${myFolder}/out.tr -G ${myFolder}/out.tr ${myFolder}/transcripts.$ext
    #one below works
    #getGeneExpression -o out.geneExpr -t execute/ExpressionQuantifier/case4_bitseq/component/folder/out.tr -G geneFile.txt execute/ExpressionQuantifier/case4_bitseq/component/folder/out.rpkm 
    #getVariance -o out.out out.geneExpr
fi

rm -f $genes
rm -f $isoforms
ln -s $outFile1 $isoforms
if [[ -f $outFile2 ]]
then 
    ln -s $outFile2 $genes
else 
    touch $genes
fi

