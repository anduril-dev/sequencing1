#!usr/bin/perl

use strict;
use componentSkeleton;
use Encode::Unicode;

sub fetch_seeds {
    
    my ($seq, $id, $path) = @_;
    
    open(FILEOUT1, ">".$path);
    for(my $i=0;$i<length($seq)-14;$i++){
	my $subseq = substr($seq, $i, 15);
	print FILEOUT1 $id."|".($i+8)."|".substr($seq, $i+7, 1)."|".substr($seq, $i+7, 1)."\t".$subseq."\n";
    }
    close(FILEOUT1);
}

sub execute {
    my ($cf_ref) = @_;

    my $utrseq = get_input($cf_ref, "utrseq");
    my $seedsOUT = get_output($cf_ref, "seeds");
    system("mkdir ".$seedsOUT);
    #system("mkdir ".$seedsOUT."/array");

    # read utr sequences
    my %hash_utr_seq;
    open(FILEIN,$utrseq);
    my $i=0;
    while(<FILEIN>){
	if($i==0){
                $i++;
                next;
        }
	chomp($_);
        my @tmp = split(/\t/,$_);
	$hash_utr_seq{$tmp[0]} = $tmp[1];
	$i++;
    }
    close(FILEIN);

    # generate seeds
    open(FILEOUT, ">".$seedsOUT."/_index");
    print FILEOUT "Key\tFile\n";
    foreach my $transcript (keys %hash_utr_seq){
	my $this_seq = $hash_utr_seq{$transcript};
	fetch_seeds($this_seq, $transcript, $seedsOUT."/".$transcript.".csv");
        print FILEOUT $transcript."\t".$seedsOUT."/".$transcript.".csv\n";
    }
    close(FILEOUT);
}

if(scalar @ARGV == 0){
    print "NO_COMMAND_FILE";
    exit;
}
my %cf = parse_command_file($ARGV[0]);
execute(\%cf);

