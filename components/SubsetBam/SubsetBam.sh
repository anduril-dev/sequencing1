# running functions.sh 
source "$ANDURIL_HOME/bash/functions.sh"
export_command

set -e

temp_folder=$( gettempdir )
mkdir "${temp_folder}/bams"

if [[ $parameter_chrCol == *","* ]] ; then 
  printf "%s:%s-%s\n" $( tsvcut -c $parameter_chrCol -t ${input_annotation} ) > "${temp_folder}/foo" ;
  else
  echo "Using DNARegion format...";
  tsvcut -c $parameter_chrCol -t ${input_annotation} > "${temp_folder}/foo" ;
fi

while read -r line
do
  samtools view -h ${input_in} "$line" -b > "${temp_folder}/bams/part-$line.bam"
done < "${temp_folder}/foo"

samtools cat "${temp_folder}/bams/"*.bam > "${output_out}"
