library(componentSkeleton)

execute <- function(cf) {

    # Inputs
    table2 <- CSV.read(get.input(cf, "reference"))    
    file <- CSV.read(get.input(cf, "exprMatrix"))

    # Extract treatment conditions
    keys <- colnames(file)
    keys <- keys[-1] # Remove miRBAse ID
    keys <- gsub("^X","",keys) # Remove X from beginning of name that starts with a number
			
    TreatmentGroup <- c()
    for (n in 1:length(keys)) {
        TreatmentGroup <- c(TreatmentGroup,as.character(table2[grep(paste("^",keys[n],"$|^",gsub("_","-",keys[n]),"$",sep=""),table2[,1]),3]))
    }
		
    filed <- cbind(keys,TreatmentGroup)
    colnames(filed) <- c("Members","Treatment")

    # Reformat to SampleGroupTable format
    # Members must match normalized column names
    id <- unique(filed[,"Treatment"])
    members <- c()
    for (i in 1:length(id)) {
        newMembers <- paste(filed[which(filed[,2] == id[i]),1],collapse=",")
        newMembers <- paste(gsub("(^)([[:digit:]])","X\\2",unlist(strsplit(newMembers,","))),collapse=",")
        members <- c(members,newMembers)
    }
			
    filed <- cbind(id,members,"sample")
    colnames(filed) <- c("ID","Members","Type")

    # Write outputs
    CSV.write(get.output(cf,"table"),filed) 
    return(0)
}

main(execute)
