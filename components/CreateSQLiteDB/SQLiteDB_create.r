library(componentSkeleton)

execute <- function(cf) {

    # If required package not installed, install it
    is.installed <- function(pkg) {
                           is.element(pkg, installed.packages()[,1])
                           }

    if (!is.installed("sqldf")) {
                        print("Downloading required R package SQLDF")
                        update.packages(checkBuilt=TRUE, ask=FALSE)
                        install.packages("sqldf",repos="http://cran.us.r-project.org", dependencies=TRUE)
                        }

    # Function from MEAP R source package 
    # http://csbi.ltdk.helsinki.fi/meap
    .create.db.from.file <- function(file.path,db.path,dbname,table.name,sep="\t",header=TRUE,index.key,db.new=TRUE){
        db <- paste(db.path,dbname,sep="")
        if(db.new==TRUE){
            cat("...Creating databases ...\n")
            if(file.exists(db)){
                unlink(db)
            }
            stm <- paste("attach '",db,"' as new",sep="")
            sqldf(stm)
        }	
        cat("...Creating tables ...\n")
        f <- file(file.path)
        stm <- paste("CREATE TABLE ",table.name," as select * from f",sep="")
        sqldf(stm,dbname=db,file.format=list(sep=sep,header=header))
	
        stm <- paste("CREATE INDEX idx_",table.name," ON ",table.name," (",paste(index.key,collapse=","),")",sep="")
        sqldf(stm,dbname=db)
}

    # Parameters + input
    table1 <- CSV.read(get.input(cf, "DBlist"))

    library(sqldf)
    # Input table must contain ALL required TP and Validated files, 
    # with 5 tab-separated columns 
    # ALWAYS in the order: table name, dbname, table path, db destination folder, case-sensitive column keys
    # and AT LEAST 1 ROW OF CONTENT after a header line.
    # The script will always create a new database(s) when executed to ensure only
    # current files are always used for reference.
    currentDB <- NA
    dbnew <- TRUE
    for (row in 1:nrow(table1)) {
        if (is.na(currentDB) | currentDB != table1[row,2]) {
            currentDB <- table1[row,2]
            dbnew <- TRUE
        } else {
            dbnew <- FALSE
        }

        file.path <- table1[row,3]
        db.path <- table1[row,4]
        dbname <- table1[row,2]
        table.name <- table1[row,1]
        index.key <- unlist(strsplit(gsub("[[:blank:]]","",table1[row,5]),","))

        # file.path = string path to annotation table
        # db.path = string path to SQL db
        # dbname = give a name to database
        # table.name = give a name to annotation table when in database
        # index.key = a vector of referenceable COLUMN NAMES in FILE.PATH
        # db.new = TRUE when new table and db, FALSE when appending new table to existing db
        .create.db.from.file(file.path, db.path, dbname, table.name, sep="\t", header=TRUE, index.key, db.new=dbnew)

        # remove quotations from table.
        for (key in index.key) {
            sqldf(paste("UPDATE ",table.name, " SET ", key, " = REPLACE(", key, ", '\"', '')", sep=""), dbname=paste(db.path, dbname,sep=""))
        }
    # Test upload
    test <- sqldf(paste("SELECT * FROM",table.name,"LIMIT 1",sep=" "), dbname=paste(db.path,dbname,sep="")) 
    if (nrow(test) != 1) {
        print(paste(table.name, "was not uploaded to SQLite correctly. Please check that input files and parameters are correct.",sep=" "))
        } else {
        print(paste(table.name, "uploaded to SQLite database successfully.",sep=" "))
        }
    }
    return(0)
}

main(execute)
