#!/usr/bin/python

import anduril
from anduril.arrayio import *
import grok

has_writer_been_used = False
old_writer=grok.writer

from grok import *

import os, errno

# Could be in anduril library, cause also used from pythonevaluate.py
def createFile(f,content=""):
    fid=open(f,'w')
    fid.write(content)
    fid.close()

# Could be in anduril library
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def execute(cf):
    global has_writer_been_used

    folderOutput = cf.get_output('folder')
    mkdir_p(folderOutput)

    array = AndurilOutputArray(cf, 'array')
    array.set_type("csv") # Default to CSV - document that it can be changed

    scriptFile = cf.get_input('script')
    scriptParam = cf.get_parameter("script")

    resultOut=cf.get_output("result")
    createFile(resultOut)

    # Prefer script input over script parameter
    if scriptFile:
        script=open(scriptFile).read()
    else:
        script=scriptParam

    if not script:
        cf.write_error("No script. Cannot proceed.")
        return anduril.constants.PARAMETER_ERROR 
    
    # Read region set array
    readers={}   
    if cf.get_input('regions'):
        for row in get_array(cf,'regions'):
            readers[row[0]]=grok.reader(row[1])
   
    readers['region_set']=None
    if cf.get_input('region_set'):
        readers['region_set']=grok.reader(cf.get_input('region_set'))

    folder={}   
    if cf.get_input('folder'):
        for f in os.listdir(cf.get_input('folder')):
            new_reader=grok.reader(os.path.join(cf.get_input('folder'), f))
            if new_reader:
                folder[f]=new_reader

    # Read transformation expression
    try:
        import copy
        result=None

        # If passed variables aren't global they cannot be seen from functions
        script_globals=copy.copy(globals())
        script_globals.update(readers)
        script_globals['result']=result
        script_globals['folder']=folder
        script_globals['folderOutput']=folderOutput
        script_globals['cf']=cf
        script_globals['array']=array
        script_globals['readers']=readers
        if scriptFile:
            execfile(scriptFile, script_globals) # Results may be accessed via script_locals
            result=script_globals["result"]
        else:
            result=eval(script, script_globals)

    except SyntaxError as e:
        if scriptFile:
            cf.write_error("Error running transformation script %r, %s"%(scriptFile, e))
        else:
            cf.write_error("Invalid transformation expression %r: %s"%(cf.get_parameter("script"), e))

        return anduril.constants.PARAMETER_ERROR

    if not os.path.exists(cf.get_output("result")) and result:
        writer = result.writer(resultOut)
        writer.add(result)
    
    return anduril.constants.OK

anduril.main(execute)

