<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>RegionTransformer</name>
    <version>0.3</version>
    <doc>
    Computes DNA region set operations such as union and overlap.
    Transformations are defined using an expression syntax like the following:
    union(r1, length(5, 15, "r2")). Here, union and length are functions and r1
    and r2 refer to region sets keys in input array. Region set references can
    be quoted or unquoted. See SetTransformer component for an analogous
    interface for computing set operations.
    
    This component is implemented using, and follows the API of, GROK (Genomic
    Region Operation Kit). See <a
    href="http://csbi.ltdk.helsinki.fi/grok/">GROK</a> documentation for
    details. The GROK API can be used to implement more complex analyses than is
    possible with the simple function expression described below; a full Python
    script can be supplied with the <code>script</code> input port. Test cases
    present some common use cases. However they weren't written for this Anduril
    component originally, so they may not be representative of real use of this
    component - please contribute.
    
    Region transformation functions are divided into three basic types. Type P
    (identity preserving) functions preserve region identity and consider
    regions as indivisible relations. Type A (annotation preserving) functions
    modify properties of regions, but migrate annotations to new regions. Type N
    (non-preserving) functions operate at sequence (location) level and only
    preserve score annotations; new scores are computed using customizable
    aggregate functions.
    
    Function definitions are below. Here, <code>regs...</code> indicates
    any number of region set arguments and <code>reg/reg1/reg2</code>
    indicate single region sets. <code>[X]</code> indicates optional argument.
    Functions may also have integer or string arguments. Functions with
    definitions of the form reg.function are method calls: "reg" is a
    region set reference, such as reg1.
    <table>
        <tr>
            <th align="left">Definition</th>
            <th align="left">Type</th>
            <th align="left">Description</th>
        </tr>
        <tr>
            <td>union(regs...)</td>
            <td>P</td>
            <td>Regions that are present in any region set.</td>
        </tr>
        <tr>
            <td>unionL(regs...)</td>
            <td>N</td>
            <td>Locations that are present in any region set.</td>
        </tr>
        <tr>
            <td>intersection(regs...)</td>
            <td>P</td>
            <td>Regions that are present in all region sets.</td>
        </tr>
        <tr>
            <td>intersectionL(regs...)</td>
            <td>N</td>
            <td>Locations that are present in all region sets.</td>
        </tr>
        <tr>
            <td>freq(low, high, regs...)</td>
            <td>P</td>
            <td>Regions that are present in at least <code>low</code> and
            at most <code>high</code> region sets.</td>
        </tr>
        <tr>
            <td>freqL(low, high, regs...)</td>
            <td>N</td>
            <td>Locations that are present in at least <code>low</code> and
            at most <code>high</code> region sets.</td>
        </tr>
        <tr>
            <td>diff(reg1, reg2)</td>
            <td>P</td>
            <td>Regions that are present in <code>reg1</code> but not in <code>reg2</code>.</td>
        </tr>
        <tr>
            <td>diffL(reg1, reg2)</td>
            <td>N</td>
            <td>Locations that are present in <code>reg1</code> but not in <code>reg2</code>.</td>
        </tr>
        <!--
        <tr>
            <td>chr(pattern, reg)</td>
            <td>P</td>
            <td>Regions whose chromosome names match given Java regular
            expression pattern.</td>
        </tr>
        <tr>
            <td>length(low, [high], reg)</td>
            <td>P</td>
            <td>Regions whose length is at least <code>low</code> and
            at most <code>high</code>. If <code>high</code> is omitted,
            there is no upper boundary.</td>
        </tr>
        -->
        <tr>
            <td>reg.strand(n)</td>
            <td>P</td>
            <td>Regions whose strand matches given <code>n</code> (numeric):
            -1 for reverse, 0 for any or 1 for forward strand.</td>
        </tr>
        <tr>
            <td>reg.expand(start, end)</td>
            <td>A</td>
            <td>Expand regions by <code>start</code> elements from start
            of region and by <code>end</code> elements from end of region.
            Negative values shrink regions. Takes strands into account.</td>
        </tr>
        <tr>
            <td>reg.shift(n)</td>
            <td>A</td>
            <td>Shift regions by <code>n</code> positions in sense direction
            (right for forward strand, left for reverse strand). Negative values
            shift in anti-sense direction.</td>
        </tr>
        <tr>
            <td>reg.flip([fixed])</td>
            <td>A</td>
            <td>Change the strand of regions: forward becomes reverse and
            vice versa. If <code>fixed</code> (numeric: -1/0/1) is given, all
            regions have this strand instead.</td>
        </tr>
        <tr>
            <td>reg.merge([gap])</td>
            <td>N</td>
            <td>Merge regions whose gap is at most <code>gap</code> positions.
            <code>gap</code> defaults to 0.
            </td>
        </tr>
    </table>
    
    <!--
    Aggregate functions are used in location level operations to combine
    scores from several region sets into one. The following aggregates are
    defined:
    <table>
        <tr>
            <th align="left">Function</th>
            <th align="left">Description</th>
        </tr>
        <tr>
            <td>count</td>
            <td>Number of region sets containing location</td>
        </tr>
        <tr>
            <td>first</td>
            <td>Score in first region set</td>
        </tr>
        <tr>
            <td>last</td>
            <td>Score in last region set</td>
        </tr>
        <tr>
            <td>min</td>
            <td>Minimum score</td>
        </tr>
        <tr>
            <td>max</td>
            <td>Maximum score</td>
        </tr>
        <tr>
            <td>product</td>
            <td>Produce of scores</td>
        </tr>
        <tr>
            <td>sum</td>
            <td>Sum of scores</td>
        </tr>
        <tr>
            <td>zero</td>
            <td>Constant score of zero</td>
        </tr>
    </table>
    -->
    
    </doc>
    <author email="kristian.ovaska@helsinki.fi">Kristian Ovaska</author>
    <author email="lauri.lyly@helsinki.fi">Lauri Lyly</author>
    <launcher type="python">
        <argument name="file" value="RegionTransformer.py" />
    </launcher>
    <requires URL="http://www.python.org/" type="manual">python</requires>
    <requires type="DEB">python-dev</requires>
    <requires name="installer" optional="false">
        <resource type="bash">install.sh</resource>
    </requires>
    <inputs>
        <input name="regions" type="DNARegion2" array="true" optional="true">
            <doc>Source region sets. In the transformation expression,
                each region set is referred to by its key in the array. I.e. if the
                key is "r1" then a FileReader region set will be in the variable "r1". An alternative
                way to access the region sets is via the "readers" dictionary. That is, readers["r1"]
            would yield the same object.</doc>
        </input>
        <input name="region_set" type="DNARegion2" optional="true">
            <doc>Single source region set. In the transformation expression, this region set is referred to as "region_set".
            </doc>
        </input>

        <input name="folder" type="BinaryFolder" optional="true">
            <doc>Source region sets - can be of any file type known by GROK.
            In the transformation expression, each region set is referred to via
            its filename, as folder["my_regions.csv"]. This yields a FileReader region set.</doc>
        </input>

        <input name="script" type="PythonSource" array="false" optional="true">
            <doc>Python script to evaluate instead of the transform function parameter, if specified. The "regions" array's keys are turned into local variables and may be used to refer to corresponding FileReader region stores in the script.
            
            The script will have certain other variables visible in its global scope, including all GROK functions, and variables corresponding to the inputs and outputs.</doc></input>
    </inputs>
    <outputs>
        <output name="result" type="DNARegion2">
            <doc>Result region set, which is set if you specify the transformation expression in the "script" parameter instead of the input. You can also write to this from a script by storing a region set in the "result" variable. The result will be written to this output port. An alternative way is to write to the file cf.get_output("result") directly, with a writer or without.</doc>
        </output>
        <output name="array" type="DNARegion2" array="true">
            <doc>Optional array of produced region sets. Files are automatically added to the array by writing array["myfile"]. The file extension is appended, defaults to "csv" and may be specified with e.g. array.set_type("bam"). All GROK's output types are supported. Inside the script, the variable called array is actually an "AndurilOutputArray" from the Python anduril module.</doc>
        </output>
        <output name="folder" type="BinaryFolder">
            <doc>Optional output folder. The path is visible in the script as "folderOutput". A convenient way to specify the path is then e.g. os.path.join(folderOutput, "myfile.csv"). For this, you need to import the os module.</doc>
        </output>

    </outputs>
    <parameters>
        <!--
        <parameter name="aggregate" type="string" default="mean">
            <doc>Default aggregate function used for combining scores
            in location level operations (functions unionL, intersectionL,
            freqL and diffL). Legal values are in table above.</doc>
        </parameter>
        -->
        <parameter name="script" type="string" default="">
            <doc>Same as the script input file, except can contain only a single expression whose value is written to the "result" output. This is evaluated with Python's "eval" function. Used only if corresponding input is unspecified.</doc>
        </parameter>
    </parameters>
</component>
