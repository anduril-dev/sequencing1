import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.IndexFile;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;

import java.io.IOException;
import java.io.File;
import java.io.FilenameFilter;

public class Cuffdiff extends SkeletonComponent {

    private File transcripts;
    private File maskFile;
    private File genome;
    private File isoforms;
    private File genes;
    private File cds;
    private File tss_groups;
    private File isoform_exp;
    private File gene_exp;
    private File cds_exp;
    private File tss_group_exp;
    private File splicing;
    private File cds_output;
    private File promoters;
 
    private IndexFile Idx; 
    
    private String command="";
    private String outdir;
    
   	private boolean help;
    private String labels;
    private int num_threads;
    private boolean time_series;
    private boolean upper_quartile_norm;
    private boolean total_hits_norm;
    private boolean compatible_hits_norm;
    private boolean frag_bias_correct;
    private boolean multi_read_correct;	
    private int min_alignment_count;
    private double FDR;
    private String library_type;
    private int frag_len_mean;
	private int frag_len_std_dev;
	private int num_importance_samples;
	private int max_mle_iterations;
	private boolean verbose;
	private boolean quiet;
	private boolean no_update_check;
	private boolean poisson_dispersion;
	private boolean emit_count_tables;
	private double min_isoform_fraction;
    private int max_bundle_frags;	
	
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        // Get inputs, outputs and parameters from command file
        getInsOuts(cf);
        getParameters(cf);
        // Create the command for Cufflinks
        createCommand(cf);
        System.out.println("Execution command is: " + command);       
        int status = IOTools.launch(command, null, null, "Cuffdiff_stdOut", "Cuffdiff_stdErr");
        // Check for error in execution
        if (status > 0) {
            return ErrorCode.ERROR;
        }
         // Rename output files
         
        try {
            renameFiles();
        } catch (IOException exception) {
            throw exception;
        }
        // Return success
        return ErrorCode.OK;
    }
    
    /**
     * Gets inputs and outputs from the command file
     *
     */
    private void getInsOuts(CommandFile cf) throws Exception{
        int i=1;
        if (cf.inputDefined("transcripts")) 
            transcripts = cf.getInput("transcripts");

        if (cf.inputDefined("maskFile")) 
            maskFile = cf.getInput("maskFile");
        if (cf.inputDefined("genome")) 
            genome = cf.getInput("genome");
            
        isoforms = cf.getOutput("isoforms");
        genes = cf.getOutput("genes");
        cds = cf.getOutput("cds");
        tss_groups = cf.getOutput("tss_groups");
        isoform_exp = cf.getOutput("isoform_exp");
        gene_exp = cf.getOutput("gene_exp");
        cds_exp = cf.getOutput("cds_exp");
        tss_group_exp = cf.getOutput("tss_group_exp");
        splicing = cf.getOutput("splicing");
        cds_output = cf.getOutput("cds_output");
        promoters = cf.getOutput("promoters");
     
        outdir= isoforms.getParent();
        
        Idx = cf.readInputArrayIndex("array");
        
        
    }
    
     /**
     * Gets parameters from the command file
     *
     */
    private void getParameters(CommandFile cf) {
        help = cf.getBooleanParameter("help");
        labels = cf.getParameter("labels");
        num_threads = cf.getIntParameter("num_threads");
        time_series = cf.getBooleanParameter("time_series");
        upper_quartile_norm = cf.getBooleanParameter("upper_quartile_norm");
        total_hits_norm = cf.getBooleanParameter("total_hits_norm");
        compatible_hits_norm = cf.getBooleanParameter("compatible_hits_norm");
        multi_read_correct = cf.getBooleanParameter("multi_read_correct");
        min_alignment_count = cf.getIntParameter("min_alignment_count");
        FDR = cf.getDoubleParameter("FDR");
        library_type = cf.getParameter("library_type");
        frag_len_mean = cf.getIntParameter("frag_len_mean");
        frag_len_std_dev = cf.getIntParameter("frag_len_std_dev");
        num_importance_samples = cf.getIntParameter("num_importance_samples");
        max_mle_iterations = cf.getIntParameter("max_mle_iterations");
        verbose = cf.getBooleanParameter("verbose");
        quiet = cf.getBooleanParameter("quiet");
        no_update_check = cf.getBooleanParameter("no_update_check");
        poisson_dispersion = cf.getBooleanParameter("poisson_dispersion");
        emit_count_tables = cf.getBooleanParameter("emit_count_tables");
        min_isoform_fraction = cf.getDoubleParameter("min_isoform_fraction");
        max_bundle_frags = cf.getIntParameter("max_bundle_frags");
    }
    
    private void createCommand(CommandFile cf) {
        command = "cuffdiff";
        command += " -o "+outdir;
        if (help) 
            command +=" -h";
        if (!labels.equals(""))
	        command += " -L "+labels;
        if (num_threads != -1)
            command +=" -p "+num_threads;
        if (time_series)
            command +=" -T ";
	    if (upper_quartile_norm)
	        command += " -N";
	    if (!total_hits_norm)
	        command += " --total-hits-norm=false";
	    if (compatible_hits_norm)
	        command += " --compatible-hits-norm";
        if (cf.inputDefined("genome")) 
	        command += " -b "+genome.getAbsolutePath();
	    if (multi_read_correct)
	        command +=" -u";	
	    if (min_alignment_count != -1)
	        command += " -c "+min_alignment_count;
        if (cf.inputDefined("maskFile")) 
	        command += " -M "+maskFile.getAbsolutePath();
	    if (FDR != -1)
	        command += " --FDR "+FDR;     
	    if (!library_type.equals(""))
	        command += " --library-type "+library_type;
	    if (frag_len_mean != -1)
	        command += " -m "+frag_len_mean;
	    if (frag_len_std_dev != -1)
	        command += " -s "+frag_len_std_dev;   
	    if (num_importance_samples != -1)
	        command += " --num-importance-samples "+num_importance_samples;
	    if (max_mle_iterations != -1)
	        command += " --max-mle-iterations "+max_mle_iterations;
	    if (verbose)
	        command +=" -v";	
	    if (quiet)
	        command +=" -q";	
	    if (no_update_check)
	        command +=" --no_update_check";	
	    if (min_isoform_fraction != -1)
	        command += " -F "+min_isoform_fraction;
	    if (emit_count_tables)
            command +=" --emit-count-tables";    
	    if (max_bundle_frags != -1)
            command += " --max-bundle-frags "+max_bundle_frags;
                        
        command += " " + transcripts.getAbsolutePath()+" ";
	    System.out.println(command);
	    FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                //this allows bam and sam files but also other stuff ending in "am"
                return name.endsWith("am");
            }
        };
        
        for(String key : Idx) {	
            //System.out.println("Reading index file");
			File file = Idx.getFile(key);
	        String[] children;
            System.out.println("key "+key+" "+file.getAbsolutePath());
            if (file.isDirectory()) {
                children = file.list(filter);
                if (children != null) {
                    for (int i=0; i<children.length; i++) {
                        command += file.getAbsolutePath()+"/"+children[i]+",";
                    }
                }
                int k = command.lastIndexOf(",");
                if (k>=0) {
                    command=command.substring(0, k);
                }
                command+=" ";
            }
            else {
                command += file.getAbsolutePath()+" ";
            }
         }
        System.out.println(command);

    }
    
    /**
	 * Rename Cuffdiff output files to the component output files
	 *
	 */
	private void renameFiles() throws Exception {
	    //System.out.println("in rename files ");
	    File a = new File(outdir+"/isoforms.fpkm_tracking");
        File b = new File(outdir+"/genes.fpkm_tracking");
        File c = new File(outdir+"/cds.fpkm_tracking");
        File d = new File(outdir+"/tss_groups.fpkm_tracking");
        File e = new File(outdir+"/isoform_exp.diff");
        File f = new File(outdir+"/gene_exp.diff");
        File g = new File(outdir+"/cds_exp.diff");
        File h = new File(outdir+"/tss_group_exp.diff");
        File i = new File(outdir+"/splicing.diff");
        File j = new File(outdir+"/cds.diff");
        File k = new File(outdir+"/promoters.diff");
	   
	    // Rename files or create new empty files
	    
        try {
            if (a.exists()) 
                a.renameTo(isoforms);
            if (b.exists()) 
                b.renameTo(genes);
            if (c.exists()) 
                c.renameTo(cds);
            if (d.exists()) 
                d.renameTo(tss_groups);
            if (e.exists()) 
                e.renameTo(isoform_exp);
            if (f.exists()) 
                f.renameTo(gene_exp);
            if (g.exists()) 
                g.renameTo(cds_exp);
            if (h.exists()) 
                h.renameTo(tss_group_exp);
            if (i.exists()) 
                i.renameTo(splicing);
            if (j.exists()) 
                j.renameTo(cds_output);
            if (k.exists()) 
                k.renameTo(promoters);
	    } catch (Exception exception) {
	        throw exception;
	    }
	}
    /**
     * @param args
     */
    public static void main(String[] args) {
        new Cuffdiff().run(args);
    }

}
