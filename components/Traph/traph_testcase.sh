#!/bin/bash

#FILTEROUT

# Define inputs and outputs for the usage example

PATH=$PATH:/opt/Traph
input_fragments="traph_anduril_test_case/tophat_out/accepted_hits.bam"
output_folder="traph_output"
parameter_readLength="76"
parameter_options=""

# The actual script that will become the component's main script

#FILTERIN

# Run Traph

runtraph.py -i "${input_fragments}" -o "${output_folder}" -l "${parameter_readLength}" ${parameter_options}

