#!/bin/bash
set -e

source "$ANDURIL_HOME/bash/functions.sh"

INPUT=$(getinput in)
CADD_HOME=$(getparameter cadd)
METHOD=$(getparameter method)
OUT=$(getoutput out)

if [ "$METHOD" = "cadd" ]
then
	echo $CADD_HOME
	TEMPDIR="$(gettempdir)"

	sed -e 's/chr//g' -e 's/\"//g' "$INPUT" > "$TEMPDIR"/in.vcf

	gzip -c "$TEMPDIR"/in.vcf > "$TEMPDIR"/in.vcf.gz 
	"$CADD_HOME" "$TEMPDIR"/in.vcf.gz "$TEMPDIR"/out.csv.gz 
	gunzip -c "$TEMPDIR"/out.csv.gz | tail -n +2 | tail -c +2 >> "$OUT"
	exit 0
fi

writeerror "No such method "$METHOD
exit 1
