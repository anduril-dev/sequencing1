#!/bin/bash -x

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"

TOPHATPATH=$( readlink -f ../../lib/tophat )

export PATH=$PATH:$TOPHATPATH

opts="$( getparameter options )"

IFS=$'\n'
keys=( $( getarraykeys alignments ) )
files=( $( getarrayfiles alignments ) )
annotations=$( getparameter annotationsDir )
out=$( getoutput results )
index=$( getparameter referenceDir )
tmp1=$( echo $index | cut -d'.' -f1 )
tmp2="${tmp1#\"}"
prefix="${tmp2%\"}"
echo $prefix
dir=$( dirname $index )
export BOWTIE_INDEXES=$dir

mkdir -p "$out"
cd "$out"
ln -s $(eval echo $annotations)/ensGene.txt ensGene.txt
ln -s $(eval echo $annotations)/refGene.txt refGene.txt
ln -s $(eval echo $annotations)/mcl mcl
ln -s $(eval echo $annotations)/blast blast

for (( i=0; i<${#keys[@]};i++ ))
do
   ln -s "${files[$i]}" tophat_${keys[$i]}
done

set -x
eval tophat-fusion-post ${opts} ${prefix}
