package dbConstruction;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;

public class AnnotationProperties {

	// annotation file header
	private String pos;
	private String chrom;
	private String ref;
	private String alt;

	public AnnotationProperties(CommandFile cf) {
		pos = cf.getParameter(Port.ANNO_POS_NAME);
		if (pos == null) {
			pos = "start";
		}

		chrom = cf.getParameter(Port.ANNO_CHROM_NAME);
		if (chrom == null) {
			chrom = "chrom";
		}

		ref = cf.getParameter(Port.ANNO_REF_NAME);
		if (ref == null) {
			ref = "ref";
		}

		alt = cf.getParameter(Port.ANNO_ALT_NAME);
		if (alt == null) {
			alt = "alt";
		}
	}

	/**
	 * 
	 * @return Name of the position-column in the given annotation file
	 */
	public String getPos() {
		return pos;
	}

	/**
	 * 
	 * @return Name of the chromosome-column in the given annotation file
	 */
	public String getChrom() {
		return chrom;
	}

	/**
	 * 
	 * @return Name of the reference allele-column in the given annotation file
	 */
	public String getRef() {
		return ref;
	}

	/**
	 * 
	 * @return Name of the alternative allele-column in the given annotation
	 *         file
	 */
	public String getAlt() {
		return alt;
	}

}
