package dbConstruction;

import java.util.LinkedHashSet;

public class PatientIDSet extends LinkedHashSet<String>{
	
	public PatientIDSet() {
		super();
	}
	
	/**
	 * @param s
	 *            String to add to the set ignoring the case
	 * @return true if String is newly added
	 */
	public boolean add(String s) {
		return super.add(s == null ? null : s.toUpperCase());
	}

	/**
	 * @param s
	 *            String to check if it is contained in set
	 * @return true if set contains String
	 */
	public boolean contains(String s) {
		return super.contains(s == null ? null : s.toUpperCase());
	}

}
