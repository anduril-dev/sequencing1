package dbConstruction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;

public class Table {

	static final String PRIMARY_KEY = "PRIMARY KEY(%s)";
	static final String FOREIGN_KEY = "FOREIGN KEY(%s) REFERENCES %s (%s)";

	// table names
	static final String VARIANT = "variant";
	static final String ALT = "alt";
	static final String SAMPLE = "sample";
	static final String PATIENT = "patient";
	static final String TREATMENT = "treatment";

	// assignments
	static final Hashtable<String, String> COL_ASSIGNMENT = new Hashtable<String, String>();
	static {
		COL_ASSIGNMENT.put(Table.VARIANT, Port.COL_VARIANT);
		COL_ASSIGNMENT.put(Table.ALT, Port.COL_ALT);
		COL_ASSIGNMENT.put(Table.SAMPLE, Port.COL_SAMPLE);
		COL_ASSIGNMENT.put(Table.PATIENT, Port.COL_PATIENT);
		COL_ASSIGNMENT.put(Table.TREATMENT, Port.COL_TREATMENT);
	}

	static final Hashtable<String, String> ALL_COL_ASSIGNMENT = new Hashtable<String, String>();
	static {
		ALL_COL_ASSIGNMENT.put(Table.VARIANT, Port.VCF_ALL);
		ALL_COL_ASSIGNMENT.put(Table.ALT, Port.VCF_ALL);
		ALL_COL_ASSIGNMENT.put(Table.SAMPLE, Port.VCF_ALL);
		ALL_COL_ASSIGNMENT.put(Table.PATIENT, Port.PATIENT_ALL);
		ALL_COL_ASSIGNMENT.put(Table.TREATMENT, Port.MEDICAL_ALL);
	}

	static final Hashtable<String, List<String>> PORT_ASSIGNMENT = new Hashtable<String, List<String>>();
	static {
		List<String> list = new ArrayList<String>();
		list.add(Port.VCF_NORMAL);
		list.add(Port.VCF_TUMOR);
		PORT_ASSIGNMENT.put(Table.VARIANT, list);
		list = new ArrayList<String>();
		list.add(Port.VCF_NORMAL);
		list.add(Port.VCF_TUMOR);
		PORT_ASSIGNMENT.put(Table.ALT, list);
		list = new ArrayList<String>();
		list.add(Port.VCF_NORMAL);
		list.add(Port.VCF_TUMOR);
		PORT_ASSIGNMENT.put(Table.SAMPLE, list);
		list = new ArrayList<String>();
		list.add(Port.PATIENT);
		PORT_ASSIGNMENT.put(Table.PATIENT, list);
		list = new ArrayList<String>();
		list.add(Port.MEDICAL);
		PORT_ASSIGNMENT.put(Table.TREATMENT, list);
	}

	static final Hashtable<String, List<String>> PRIMARY_KEY_ASSIGNMENT = new Hashtable<String, List<String>>();
	static {
		List<String> list = new ArrayList<String>();
		list.add(DefaultColumns.CHROM);
		list.add(DefaultColumns.POS);
		list.add(DefaultColumns.REF);
		list.add(DefaultColumns.ALL_ALT);
		list.add(DefaultColumns.TISSUE);
		PRIMARY_KEY_ASSIGNMENT.put(Table.VARIANT, list);
		list = new ArrayList<String>();
		list.add(DefaultColumns.CHROM);
		list.add(DefaultColumns.POS);
		list.add(DefaultColumns.REF);
		list.add(DefaultColumns.ALT);
		list.add(DefaultColumns.ALL_ALT);
		list.add(DefaultColumns.TISSUE);
		PRIMARY_KEY_ASSIGNMENT.put(Table.ALT, list);
		list = new ArrayList<String>();
		list.add(DefaultColumns.PATIENT_ID);
		list.add(DefaultColumns.SAMPLE_TAG);
		list.add(DefaultColumns.TISSUE);
		list.add(DefaultColumns.ALT);
		list.add(DefaultColumns.CHROM);
		list.add(DefaultColumns.POS);
		PRIMARY_KEY_ASSIGNMENT.put(Table.SAMPLE, list);
		list = new ArrayList<String>();
		list.add(DefaultColumns.PATIENT_ID);
		PRIMARY_KEY_ASSIGNMENT.put(Table.PATIENT, list);
		list = new ArrayList<String>();
		list.add(DefaultColumns.PATIENT_ID);
		list.add(DefaultColumns.DRUG_ID);
		list.add(DefaultColumns.DRUG_NAME);
		PRIMARY_KEY_ASSIGNMENT.put(Table.TREATMENT, list);
	}

	static final Hashtable<String, String> FOREIGN_KEY_ASSIGNMENT = new Hashtable<String, String>();
	static {
		FOREIGN_KEY_ASSIGNMENT.put(Table.ALT, "FOREIGN KEY ("
				+ DefaultColumns.CHROM + "," + DefaultColumns.POS + ","
				+ DefaultColumns.ALL_ALT + "," + DefaultColumns.TISSUE + ") "
				+ "REFERENCES " + Table.VARIANT + "(" + DefaultColumns.CHROM
				+ "," + DefaultColumns.POS + "," + DefaultColumns.ALL_ALT + ","
				+ DefaultColumns.TISSUE + ")");
		FOREIGN_KEY_ASSIGNMENT.put(Table.SAMPLE, "FOREIGN KEY ("
				+ DefaultColumns.ALT + "," + DefaultColumns.CHROM + ","
				+ DefaultColumns.POS + "," + DefaultColumns.TISSUE + ") "
				+ "REFERENCES " + Table.ALT + "(" + DefaultColumns.ALT + ","
				+ DefaultColumns.CHROM + "," + DefaultColumns.POS + ","
				+ DefaultColumns.TISSUE + "), " + "FOREIGN KEY ("
				+ DefaultColumns.PATIENT_ID + ") REFERENCES " + Table.PATIENT
				+ "(" + DefaultColumns.PATIENT_ID + ")");
		FOREIGN_KEY_ASSIGNMENT.put(Table.TREATMENT, "FOREIGN KEY ("
				+ DefaultColumns.PATIENT_ID + ") REFERENCES " + Table.PATIENT
				+ "(" + DefaultColumns.PATIENT_ID + ")");
	}

	private String name;
	private Set<Column> columns;
	private String key;
	private String columnRequestInputName;
	private String inclAllCols;
	private List<String> dataports;
	private DefaultColumns defCols;
	private Set<String> infoField;
	private Set<String> formatField;

	public Table(String name) {
		this.name = name;
		this.defCols = new DefaultColumns(name);
		this.columns = new LinkedHashSet<Column>();
		this.addDefaultColumns();
		String s = String.format(PRIMARY_KEY,
				StringUtils.join(PRIMARY_KEY_ASSIGNMENT.get(name), ","));
		if (FOREIGN_KEY_ASSIGNMENT.get(name) != null) {
			s = s + " " + FOREIGN_KEY_ASSIGNMENT.get(name);
		}
		this.key = s.toString();
		this.columnRequestInputName = COL_ASSIGNMENT.get(name);
		this.inclAllCols = ALL_COL_ASSIGNMENT.get(name);
		this.dataports = PORT_ASSIGNMENT.get(name);
		this.infoField = new LinkedHashSet<String>();
		this.formatField = new LinkedHashSet<String>();
	}

	/**
	 * Adds default columns.
	 */
	private void addDefaultColumns() {
		for (String dc : defCols.getKeys()) {
			Column col = new Column(dc.toLowerCase(), defCols.getValue(dc));
			this.columns.add(col);
		}
	}

	/**
	 * Composes SQL-Create-Table statement.
	 * 
	 * @param cf
	 * @return String containing the SQL-statement
	 */
	public String buildCreateTableStmt(CommandFile cf) {
		StringBuffer sql = new StringBuffer();
		sql.append("CREATE TABLE " + this.name + "(");
		for (Column col : this.columns) {
			sql.append("\"" + col.getName() + "\"" + " " + col.getType() + ", ");
		}
		sql.append(this.key + ");");

		return sql.toString();
	}

	/**
	 * Adds requested columns by reading input port or parameter.
	 * 
	 * @param cf
	 */
	public void addRequestedColumns(CommandFile cf) {
		if (cf.inputDefined(columnRequestInputName)) {
			getPortInput(cf);
		} else if (cf.getParameter(columnRequestInputName) != null
				&& !cf.getParameter(columnRequestInputName).isEmpty()) {
			getParamInput(cf);
		}
	}

	/**
	 * Adds requested columns by reading input port.
	 * 
	 * @param cf
	 */
	private void getPortInput(CommandFile cf) {
		CollectionParser collectionParser = null;

		try {
			collectionParser = new CollectionParser(
					cf.getInput(columnRequestInputName));
		} catch (IOException e) {
			cf.writeError("Error reading " + columnRequestInputName
					+ " input: " + e);
		}

		// CSV contains only two lines, the requested column (header) and its
		// type OR only one line
		List<String> header = collectionParser.next(collectionParser
				.getColumnNames());
		List<String> values = new ArrayList<String>();
		if (collectionParser.hasNext()) {
			values = collectionParser.next(collectionParser.next());
			for (int i = 0; i < header.size(); i++) {
				Column col = new Column(header.get(i).toLowerCase(),
						values.get(i));
				this.columns.add(col);
			}
		} else {
			for (int i = 0; i < header.size(); i++) {
				Column col = new Column(header.get(i).toLowerCase(), null);
				this.columns.add(col);
			}
		}
	}

	/**
	 * Adds requested columns by reading input parameter.
	 * 
	 * @param cf
	 */
	private void getParamInput(CommandFile cf) {
		String[] input = cf.getParameter(columnRequestInputName).split(",");
		for (int i = 0; i < input.length; i++) {
			Column col = new Column(input[i].toLowerCase());
			this.columns.add(col);
		}
	}

	/**
	 * Adds all columns of one input file to the set of requested columns.
	 * 
	 * @param cf
	 * @param parser
	 */
	public void addAllColumns(CommandFile cf, CollectionParser parser) {
		if (name.equals(PATIENT) || name.equals(TREATMENT)) {
			addAllColumnsNonVCF(cf);
		} else {
			addAllColumnsVCF(cf, parser);
		}
	}

	/**
	 * Adds all columns of a non-VCF file to the set of requested columns.
	 * 
	 * @param cf
	 */
	private void addAllColumnsNonVCF(CommandFile cf) {
		CollectionParser collectionParser = null;
		for (String d : dataports) {
			try {
				collectionParser = new CollectionParser(cf.getInput(d));
			} catch (IOException e) {
				cf.writeError("Error reading " + d + " input: " + e);
			}
			String[] header = collectionParser.getColumnNames();
			for (int i = 0; i < header.length; i++) {
				columns.add(new Column(header[i]));
			}
		}
	}

	/**
	 * Adds all columns of a VCF file to the set of requested columns.
	 * 
	 * @param cf
	 * @param parser
	 */
	private void addAllColumnsVCF(CommandFile cf, CollectionParser parser) {

		if (name.equals(ALT)) {
			addAllAnnotationColumns(cf, Port.ANNOTATION_NORMAL);
			addAllAnnotationColumns(cf, Port.ANNOTATION_TUMOR);
		} else if (name.equals(VARIANT)) {
			getVCFFieldData(cf, parser);
			for (String s : infoField) {
				columns.add(new Column(s.toLowerCase()));
			}
		} else if (name.equals(SAMPLE)) {
			getVCFFieldData(cf, parser);
			for (String s : formatField) {
				columns.add(new Column(s.toLowerCase()));
			}
		}

	}

	/**
	 * Checks if annotation-input is defined. Adds columns to set of columns.
	 * 
	 * @param cf
	 * @param annotationPort
	 */
	private void addAllAnnotationColumns(CommandFile cf, String annotationPort) {
		if (cf.inputDefined(annotationPort)) {
			CollectionParser annotations = null;
			try {
				annotations = new CollectionParser(cf.getInput(annotationPort));
			} catch (IOException e) {
				cf.writeError("Error reading " + annotationPort + " input: "
						+ e);
			}
			String[] header = annotations.getColumnNames();
			for (int i = 0; i < header.length; i++) {
				columns.add(new Column("anno_" + header[i]));
			}
		}
	}

	/**
	 * Splits information of the VCF fields INFO and FORMAT.
	 * 
	 * @param cf
	 * @param parser
	 */
	private void getVCFFieldData(CommandFile cf, CollectionParser parser) {
		if (infoField.isEmpty() || formatField.isEmpty()) {
			Set<String> incl = new HashSet<String>();
			incl.add(DefaultColumns.FORMAT.toUpperCase());
			incl.add(DefaultColumns.INFO.toUpperCase());
			while (parser.hasNext()) {
				String[] next = parser.next();
				CaseInsensitiveMap line = parser.next(next,
						parser.getColumnNames());
				String format = line.get(DefaultColumns.FORMAT);
				String[] formatSplitRes = format.split(":");
				for (int i = 0; i < formatSplitRes.length; i++) {
					formatField.add(formatSplitRes[i]);
				}
				String info = line.get(DefaultColumns.INFO);
				String[] infoSplitRes = info.split(";");
				for (int i = 0; i < infoSplitRes.length; i++) {
					infoField.add(infoSplitRes[i].split("=", 2)[0]);
				}
			}
		}
	}

	/**
	 * Validates that there is input for the primary-key columns
	 * 
	 * @param cf
	 * @param parser
	 * @return true if there is input for the primary key columns
	 * @throws IllegalInputException
	 */
	public boolean validateColumnInput(CommandFile cf, CollectionParser parser)
			throws IllegalInputException {
		if (parser != null && (name.equals(PATIENT) || name.equals(TREATMENT))) {
			return validateNonVCFColumnInput(cf, parser);
		}
		if (name.equals(PATIENT) && columns.size() > 1) {
			throw new IllegalInputException(
					"Columns specified but no data-file provided: Table "
							+ name);
		}
		return true;
	}

	/**
	 * Validates that all user-requested columns can be created from the given
	 * non-VCF-file(s).
	 * 
	 * @param cf
	 * @param parser
	 * @return true if there is a data-column for all default and all
	 *         user-requested columns
	 * @throws IllegalInputException
	 *             if not all columns could be created from given input
	 */
	public boolean validateNonVCFColumnInput(CommandFile cf,
			CollectionParser parser) throws IllegalInputException {
		List<Column> editedColumns = new ArrayList<Column>();
		editedColumns.addAll(columns);

		CaseInsensitiveList header = new CaseInsensitiveList(
				parser.getColumnNames());
		if (name.equals(PATIENT)) {
			editedColumns = processColumnNameInput(cf, editedColumns,
					Port.PATIENT_PATIENT_ID, DefaultColumns.PATIENT_ID);
		} else {
			editedColumns = processColumnNameInput(cf, editedColumns,
					Port.MEDICAL_PATIENT_ID, DefaultColumns.PATIENT_ID);
			editedColumns = processColumnNameInput(cf, editedColumns,
					Port.MEDICAL_DRUG_ID, DefaultColumns.DRUG_ID);
			editedColumns = processColumnNameInput(cf, editedColumns,
					Port.MEDICAL_DRUG_NAME, DefaultColumns.DRUG_NAME);
		}

		for (Column col : editedColumns) {
			if (!header.contains(col.getName())) {
				throw new IllegalInputException("Column: " + col.getName());
			}
		}

		return true;
	}

	/**
	 * Manages primary-key column names of patient/treatment file if they differ
	 * from the one used in the database.
	 * 
	 * @param cf
	 * @param editedColumns
	 * @param port
	 * @param constant
	 * @return List of all requested columns, with column names as they appear
	 *         in the database
	 */
	private List<Column> processColumnNameInput(CommandFile cf,
			List<Column> editedColumns, String port, String constant) {
		String editedName = cf.getParameter(port);

		Column c = new Column(constant);
		if (editedName != null && !editedName.isEmpty()) {
			editedColumns.set(editedColumns.indexOf(c), new Column(editedName));
		}

		return editedColumns;
	}

	/**
	 * @return the name of the port where user requests columns
	 */
	public String getColRequestInputName() {
		return columnRequestInputName;
	}

	/**
	 * @return the name of the table
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return a list of all columns for the table
	 */
	public Set<Column> getColumns() {
		return columns;
	}

	/**
	 * @return the name of the port for the data-file
	 */
	public List<String> getDataports() {
		return dataports;
	}

	/**
	 * @return the name of the parameter to include all columns
	 */
	public String getInclAllCols() {
		return inclAllCols;
	}

	/**
	 * @return the name of the table
	 */
	public String toString() {
		return name;
	}

}