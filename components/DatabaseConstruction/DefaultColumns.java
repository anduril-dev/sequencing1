package dbConstruction;

import java.util.HashSet;
import java.util.Set;

public class DefaultColumns {

	// vcf headers, therefore column names
	static final String CHROM = "chrom";
	static final String POS = "pos";
	static final String ID = "id";
	static final String REF = "ref";
	static final String ALT = "alt";
	static final String QUAL = "qual";
	static final String FILTER = "filter";
	static final String INFO = "info";
	static final String FORMAT = "format";

	static final Set<String> VCF_HEADER = new HashSet<String>();
	static {
		VCF_HEADER.add(CHROM);
		VCF_HEADER.add(POS);
		VCF_HEADER.add(ID);
		VCF_HEADER.add(REF);
		VCF_HEADER.add(ALT);
		VCF_HEADER.add(QUAL);
		VCF_HEADER.add(FILTER);
		VCF_HEADER.add(INFO);
		VCF_HEADER.add(FORMAT);
	}

	// vcf column names that are no header
	static final String ALLELE_COUNT = "ac";
	static final String ALLELE_DEPTH = "ad";
	static final String GENOTYPE = "gt";
	static final String READ_DEPTH_SAMPLE = "dp";
	static final String COMBINED_READ_DEPTH = "dp";
	static final String ALLELE_NUMBER = "an";

	// other headers
	static final String PATIENT_ID = "patient_id";
	static final String DRUG_ID = "drug_id";
	static final String DRUG_NAME = "drug_name";

	// other column names
	static final String SAMPLE_TAG = "sample_tag";
	static final String ALL_ALT = "all_alt";
	static final String AD_REF = "ad_ref";
	static final String AD_ALT = "ad_alt";
	static final String TISSUE = "tissue";

	private CaseInsensitiveMap defCol;

	public DefaultColumns(String name) {
		defCol = new CaseInsensitiveMap();
		if (name.equals(Table.VARIANT)) {
			defCol.put(CHROM, "TEXT NOT NULL");
			defCol.put(POS, "INTEGER NOT NULL");
			defCol.put(ID, "TEXT");
			defCol.put(REF, "TEXT NOT NULL");
			defCol.put(ALL_ALT, "TEXT NOT NULL");
			defCol.put(TISSUE, "TEXT");
			defCol.put(QUAL, "REAL");
			defCol.put(FILTER, "TEXT");
			// defCol.put(ALLELE_NUMBER, "INTEGER NOT NULL");
			defCol.put(COMBINED_READ_DEPTH, "INTEGER NOT NULL");
		} else if (name.equals(Table.PATIENT)) {
			defCol.put(PATIENT_ID, "TEXT NOT NULL");
		} else if (name.equals(Table.ALT)) {
			defCol.put(CHROM, "TEXT NOT NULL");
			defCol.put(POS, "INTEGER NOT NULL");
			defCol.put(REF, "TEXT NOT NULL");
			defCol.put(ALT, "TEXT NOT NULL");
			defCol.put(ALL_ALT, "TEXT NOT NULL");
			defCol.put(TISSUE, "TEXT");
			defCol.put(ALLELE_COUNT, "INTEGER");
		} else if (name.equals(Table.SAMPLE)) {
			defCol.put(PATIENT_ID, "TEXT NOT NULL");
			defCol.put(SAMPLE_TAG, "TEXT");
			defCol.put(TISSUE, "TEXT");
			defCol.put(ALT, "TEXT NOT NULL");
			defCol.put(CHROM, "TEXT NOT NULL");
			defCol.put(POS, "INTEGER NOT NULL");
			defCol.put(GENOTYPE, "TEXT");
			defCol.put(AD_REF, "INTEGER");
			defCol.put(AD_ALT, "INTEGER");
			// defCol.put(READ_DEPTH_SAMPLE, "TEXT");
		} else if (name.equals(Table.TREATMENT)) {
			defCol.put(PATIENT_ID, "TEXT NOT NULL");
			defCol.put(DRUG_ID, "TEXT NOT NULL");
			defCol.put(DRUG_NAME, "TEXT NOT NULL");
		}
	}

	/**
	 * @return a set of the default column names
	 */
	public Set<String> getKeys() {
		return defCol.keySet();
	}

	/**
	 * @param key
	 *            name of default column for which to get the value
	 * @return the type for a default column (real, text, integer (not null))
	 */
	public String getValue(String key) {

		return defCol.get(key);
	}

}
