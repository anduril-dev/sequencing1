package dbConstruction;

public class Port {

	// port AND parameter names
	static final String COL_VARIANT = "columnsVariant";
	static final String COL_ALT = "columnsAlt";
	static final String COL_SAMPLE = "columnsSample";
	static final String COL_PATIENT = "columnsPatient";
	static final String COL_TREATMENT = "columnsTreatment";

	// port names
	static final String DATABASE = "database";
	static final String VCF_NORMAL = "normal";
	static final String VCF_TUMOR = "tumor";
	static final String PATIENT = "patient";
	static final String MEDICAL = "treatment";
	static final String ANNOTATION_NORMAL = "annotationsNormal";
	static final String ANNOTATION_TUMOR = "annotationsTumor";

	// parameter names
	static final String PATIENT_PATIENT_ID = "patientDefaultColumnPatientID";
	static final String MEDICAL_PATIENT_ID = "medicalDefaultColumnPatientID";
	static final String MEDICAL_DRUG_ID = "treatmentDefaultColumnDrugID";
	static final String MEDICAL_DRUG_NAME = "treatmentDefaultColumnDrugName";
	static final String VCF_ALL = "vcfAllColumns";
	static final String PATIENT_ALL = "patientAllColumns";
	static final String MEDICAL_ALL = "treatmentAllColumns";
	static final String ANNO_CHROM_NAME = "annotationChromName";
	static final String ANNO_POS_NAME = "annotationPosName";
	static final String ANNO_REF_NAME = "annotationRefName";
	static final String ANNO_ALT_NAME = "annotationAltName";
	static final String INCL_ALL_PAT_IDS = "inclAllPatientIDs";

}
