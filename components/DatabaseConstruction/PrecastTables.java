package dbConstruction;

import java.util.ArrayList;
import java.util.List;

public class PrecastTables {

	private List<String> tableNames;

	public PrecastTables(boolean extended) {

		this.tableNames = new ArrayList<String>();
		this.tableNames.add(Table.VARIANT);
		this.tableNames.add(Table.ALT);
		this.tableNames.add(Table.PATIENT);
		this.tableNames.add(Table.SAMPLE);
		// treatment table only if a treatment file is provided
		if (extended) {
			this.tableNames.add(Table.TREATMENT);
		}
	}

	/**
	 * @return a list with all the names of the table of the database
	 */
	public List<String> getTableNames() {
		return tableNames;
	}

}
