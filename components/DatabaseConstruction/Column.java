package dbConstruction;

public class Column {

	private String name;
	private String type;

	public Column(String name, String type) {
		String p = "(?i)(REAL|TEXT|INTEGER)( NOT NULL)?";
		if (type == null || !type.matches(p)) {
			type = "INTEGER";
		}
		this.name = name;
		this.type = type;
	}
	
	public Column (String name) {
		this(name, "INTEGER");
	}

	/**
	 * @return the name of the table
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the type of the column (real, text, integer (not null))
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the name of the table
	 */
	public String toString() {
		return name;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Column other = (Column) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
