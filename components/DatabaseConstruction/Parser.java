package dbConstruction;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fi.helsinki.ltdk.csbl.asser.io.CSVParser;

public class Parser extends CSVParser{
	
	public Parser(File file) throws IOException {
		// comment lines are not skipped
		super(file, false);
	}
	
	public Parser(Reader reader) throws IOException {
		super(reader, "\t", 0, "NA", false);
	}
	
	public Parser(File file, boolean comment) throws IOException {
		super(file, comment);
	}
	
	public CaseInsensitiveMap next(String[] line, String[] header) {
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < header.length; i++) {
			map.put(header[i], line[i]);
		}
		return new CaseInsensitiveMap(map);
	}

	public CaseInsensitiveMap next(String[] line, String[] header,
			Set<String> includeColumns) {
		Map<String, String> map = new HashMap<String, String>();
		for (int i = 0; i < header.length; i++) {
			if (includeColumns.contains(header[i])) {
				map.put(header[i], line[i]);
			}
		}		
		return new CaseInsensitiveMap(map);
	}

	public CaseInsensitiveList next(String[] line) {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < line.length; i++) {
			list.add(line[i]);
		}
		return new CaseInsensitiveList(list);
	}
	
	public List<String> nextCaseSensitive(String[] line) {
		
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < line.length; i++) {
			list.add(line[i]);
		}
		return list;
	}

}
