package dbConstruction;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;

public class Annotation {

	// annotation file header
	private String pos;
	private String chrom;
	private String ref;
	private String alt;

	public Annotation(CommandFile cf) {
		pos = cf.getParameter(Port.ANNO_POS_NAME);
		if (pos == null) {
			pos = "start";
		}

		chrom = cf.getParameter(Port.ANNO_CHROM_NAME);
		if (chrom == null) {
			chrom = "chrom";
		}

		ref = cf.getParameter(Port.ANNO_REF_NAME);
		if (ref == null) {
			ref = "ref";
		}

		alt = cf.getParameter(Port.ANNO_ALT_NAME);
		if (alt == null) {
			alt = "alt";
		}
	}

	public String getPos() {
		return pos;
	}

	public String getChrom() {
		return chrom;
	}

	public String getRef() {
		return ref;
	}

	public String getAlt() {
		return alt;
	}

}
