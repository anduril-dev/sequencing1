package dbConstruction;

import java.util.ArrayList;
import java.util.List;

public class CaseInsensitiveList extends ArrayList<String> {

	public CaseInsensitiveList() {
		super();
	}
	
	public CaseInsensitiveList(List<? extends String> list) {
		for (String s : list) {
			this.add(s);
		}
	}
	
	public CaseInsensitiveList(String[] stringArray) {
		for (int i = 0; i < stringArray.length; i++) {
			this.add(stringArray[i]);
		}
	}

	/**
	 * @param s
	 *            String to add to the list ignoring the case
	 * @return true if String is newly added
	 */
	public boolean add(String s) {
		return super.add(s == null ? null : s.toLowerCase());
	}

	/**
	 * @param s
	 *            String to check if it is contained in list
	 * @return true if list contains String
	 */
	public boolean contains(String s) {
		return super.contains(s == null ? null : s.toLowerCase());
	}

}
