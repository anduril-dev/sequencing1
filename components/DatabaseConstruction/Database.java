package dbConstruction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;

public class Database {

	private File database;
	private CommandFile cf;
	private Connection connection;
	private List<Table> tables;
	private PatientIDSet patientIDs;
	private AnnotationProperties annotationProperties;

	public Database(CommandFile cf) {
		this.database = cf.getOutput(Port.DATABASE);
		this.cf = cf;
		this.connection = null;
		this.tables = new ArrayList<Table>();
		this.patientIDs = new PatientIDSet();
		this.annotationProperties = new AnnotationProperties(cf);
	}

	/**
	 * Creates a database and connects to it.
	 */
	public boolean connect() {
		String dbURL;

		try {
			Class.forName("org.sqlite.JDBC");
		} catch (Exception e) {
			cf.writeError("Failed to load SQLite JDBC driver: " + e);
			return false;
		}

		dbURL = "jdbc:sqlite:" + this.database.toString();

		try {
			this.connection = DriverManager.getConnection(dbURL);
		} catch (SQLException e) {
			cf.writeError("Failed to start SQL server: " + e);
			return false;
		}
		return connection != null;
	}

	/**
	 * @param tableName
	 *            New table is added to the database
	 */
	public void addTable(String tableName) {
		Table table = new Table(tableName);
		tables.add(table);
	}

	/**
	 * Validates if input is correctly given and if the input-data is accurate.
	 * 
	 * @param table
	 *            Table-object that holds information about the SQl-table
	 * @return true if all columns for the table can be created from given input
	 * @throws IllegalInputException
	 *             if input ports and parameters are not correctly used or if
	 *             not all columns can be created from given input
	 */
	public boolean validateInput(Table table) throws IllegalInputException {
		String name = table.getName();
		List<String> dataports = table.getDataports();
		String colRequestInputName = table.getColRequestInputName();
		String colParamInput = cf.getParameter(colRequestInputName);
		boolean portInputDefined = cf.inputDefined(colRequestInputName);
		boolean paramDefined = colParamInput != null
				&& !colParamInput.isEmpty();
		boolean inclAllCols = cf.getBooleanParameter(table.getInclAllCols());

		if (portInputDefined && paramDefined) {
			throw new IllegalInputException(
					"Specify either input port or parameter. Table: " + name);
		}

		if ((portInputDefined || paramDefined) && inclAllCols) {
			throw new IllegalInputException(
					"Specify either certain columns or the allColumns-parameter."
							+ " Table: " + name);
		}

		if (inclAllCols) {
			boolean valid = false;
			for (String d : dataports) {
				if (cf.inputDefined(d) && !valid) {
					valid = true;
				}
			}
			if (!valid) {
				throw new IllegalInputException(
						"Cannot request all columns if input file is not given."
								+ " Table: " + name);
			}
		}

		Iterator<String> iterator = dataports.iterator();
		boolean ok = true;
		while (iterator.hasNext() && ok) {
			String d = iterator.next();
			CollectionParser collectionParser = null;
			if (cf.inputDefined(d)) {
				try {
					collectionParser = getInput(d);
				} catch (IOException e) {
					cf.writeError("Failed to read " + d + " input: " + e);
					return false;
				}

				if (portInputDefined || paramDefined) {
					table.addRequestedColumns(cf);
				} else if (inclAllCols) {
					table.addAllColumns(cf, collectionParser);
				}

				ok = table.validateColumnInput(cf, collectionParser);
			}
		}
		return ok;
	}

	/**
	 * Creates the actual table in the SQLite-database.
	 * 
	 * @param table
	 *            Table-object that holds information about the SQl-table
	 * @return true if table in database was successfully created
	 */
	public boolean createTable(Table table) {
		Statement stmt;
		String sql = table.buildCreateTableStmt(cf);
		try {
			stmt = connection.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			String msg = String
					.format("Table %s: error creating SQL table: %s; SQL statement is %s",
							table, e, sql);
			cf.writeError(msg);
			return false;
		}
		return true;
	}

	/**
	 * Inserts user-given input into the actual SQLite-database-table.
	 * 
	 * @param table
	 *            Table-object that holds information about the SQL-table
	 * @return true if insert was successful
	 */
	public boolean insertIntoTable(Table table) {
		CollectionParser collectionParser = null;
		boolean ok = true;
		String tableName = table.getName();

		List<String> dataport = table.getDataports();
		Iterator<String> iterator = dataport.iterator();
		while (iterator.hasNext() && ok) {
			String d = iterator.next();
			if (cf.inputDefined(d)) {
				try {
					collectionParser = getInput(d);
				} catch (IOException e) {
					cf.writeError("Failed to read " + d + " input: " + e);
					return false;
				} catch (IllegalInputException e) {
					cf.writeError("Failed to read " + d + " input: " + e);
					return false;
				}
			}
			StringBuffer insert = new StringBuffer();
			insert.append("INSERT INTO " + table.getName() + "(");
			String prefix = "";
			for (Column c : table.getColumns()) {
				insert.append(prefix + "\"" + c.getName() + "\"");
				prefix = ", ";
			}
			insert.append(") VALUES(");

			if (collectionParser == null && tableName.equals(Table.PATIENT)) {
				ok = insertNonVCFData(insert.toString());
			} else if (collectionParser != null) {
				if (tableName.equals(Table.PATIENT)
						|| tableName.equals(Table.TREATMENT)) {
					ok = insertNonVCFData(collectionParser, table,
							insert.toString());
				} else if (tableName.equals(Table.SAMPLE)) {
					ok = insertSampleData(collectionParser, table,
							insert.toString(), d);
				} else {
					ok = insertVCFData(collectionParser, table,
							insert.toString(), d);
				}
			}
		}
		return ok;
	}

	/**
	 * Fetches input from given dataport.
	 * 
	 * @param dataport
	 *            Name of the dataport from which the information is fetched.
	 * @return Parser of input from dataport
	 * @throws IOException
	 * @throws IllegalInputException
	 *             if file is given as VCF input, but does not contain the
	 *             default VCF-header
	 */
	private CollectionParser getInput(String dataport) throws IOException,
			IllegalInputException {
		File input;
		if (dataport.equals(Port.VCF_NORMAL) || dataport.equals(Port.VCF_TUMOR)) {
			if (cf.inputDefined(dataport)) {
				return readVCF(dataport);
			}
			return null;
		} else {
			input = cf.getInput(dataport);
			return new CollectionParser(input);
		}
	}

	/**
	 * Reads VCF file, omitting all lines starting with ##
	 * 
	 * @param port
	 *            Name of the port from which to get the VCF file
	 * @return Parser of input, first line is VCF-header
	 * @throws IOException
	 * @throws IllegalInputException
	 *             if file does not contain the default VCF-header
	 */
	private CollectionParser readVCF(String port) throws IOException,
			IllegalInputException {
		Reader r;
		BufferedReader br;
		File f = cf.getInput(port);
		r = new FileReader(f);
		br = new BufferedReader(r);
		int count = 0;
		String line = br.readLine();
		while (line != null) {
			while (Pattern.matches("^##.*", line)) {
				line = br.readLine();
				count++;
			}
			break;
		}

		r = new FileReader(f);
		br = new BufferedReader(r);
		for (int i = 0; i < count; i++) {
			br.readLine();
		}
		br.read();

		CollectionParser collectionParser = new CollectionParser(br);
		CaseInsensitiveList header = collectionParser.next(collectionParser
				.getColumnNames());
		for (String s : DefaultColumns.VCF_HEADER) {
			if (!header.contains(s)) {
				throw new IllegalInputException(
						"Given file in port vcf is not a VCF-file.");
			}
		}
		return collectionParser;
	}

	/**
	 * Completes insert-statement and inserts user-given input into table in
	 * SQLite-database. Input is not a VCF-File.
	 * 
	 * @param collectionParser
	 *            Parser from which input for insertion is read
	 * @param table
	 *            Table in which to insert
	 * @param insert
	 *            String which contains the uncompleted insert-statement
	 * @return true if insert-statement was successfully executed
	 */
	private boolean insertNonVCFData(CollectionParser collectionParser,
			Table table, String insert) {
		boolean ok = true;
		boolean tableIsPatient = table.getName().equals(Table.PATIENT);
		String patientId;
		String drugId = null;
		String drugName = null;

		if (tableIsPatient) {
			patientId = processColumnNameInput(Port.PATIENT_PATIENT_ID,
					DefaultColumns.PATIENT_ID);
		} else {
			patientId = processColumnNameInput(Port.MEDICAL_PATIENT_ID,
					DefaultColumns.PATIENT_ID);
			drugId = processColumnNameInput(Port.MEDICAL_DRUG_ID,
					DefaultColumns.DRUG_ID);
			drugName = processColumnNameInput(Port.MEDICAL_DRUG_NAME,
					DefaultColumns.DRUG_NAME);
		}

		while (collectionParser.hasNext() && ok) {
			CaseInsensitiveMap lineAsMap = collectionParser.next(
					collectionParser.next(), collectionParser.getColumnNames());
			lineAsMap.put(DefaultColumns.PATIENT_ID,
					lineAsMap.remove(patientId).toUpperCase());
			patientIDs.add(lineAsMap.get(DefaultColumns.PATIENT_ID));

			if (!tableIsPatient) {
				lineAsMap.put(DefaultColumns.DRUG_ID, lineAsMap.remove(drugId));
				lineAsMap.put(DefaultColumns.DRUG_NAME,
						lineAsMap.remove(drugName));
			}
			ok = executeStmt(table, lineAsMap, insert);
		}

		if (ok && tableIsPatient
				&& cf.getBooleanParameter(Port.INCL_ALL_PAT_IDS)) {
			insert = "INSERT INTO " + Table.PATIENT + " ("
					+ DefaultColumns.PATIENT_ID + ") VALUES (";
			return getPatientIdsFromVCF(insert);
		}
		return ok;
	}

	/**
	 * Completes insert-statement and inserts patientIDs into patient table, as
	 * no additional patient information was given.
	 * 
	 * @param insert
	 *            Incomplete insert-statement
	 * @return true if insert was successful
	 */
	private boolean insertNonVCFData(String insert) {
		if (cf.inputDefined(Port.MEDICAL)) {
			boolean ok = true;

			CollectionParser medical;
			try {
				medical = new CollectionParser(cf.getInput(Port.MEDICAL));
			} catch (IOException e) {
				cf.writeError("Failed to read " + Port.MEDICAL + " input: " + e);
				return false;
			}

			String patientId = processColumnNameInput(Port.MEDICAL_PATIENT_ID,
					DefaultColumns.PATIENT_ID);
			Set<String> incl = new HashSet<String>();
			incl.add(patientId);

			while (medical.hasNext()) {
				CaseInsensitiveMap lineAsMap = medical.next(medical.next(),
						medical.getColumnNames(), incl);
				// set avoids duplicates
				patientIDs.add(lineAsMap.get(patientId));
			}

			Iterator<String> iterator = patientIDs.iterator();
			while (iterator.hasNext() && ok) {
				String p = iterator.next();
				ok = insertPatientID(insert, p);
			}
			if (ok && cf.getBooleanParameter(Port.INCL_ALL_PAT_IDS)) {
				return getPatientIdsFromVCF(insert);
			}
			return ok;
		} else {
			return getPatientIdsFromVCF(insert);
		}
	}

	/**
	 * Processes given primary key column name.
	 * 
	 * @param parameter
	 *            Name of the parameter that stores the column name
	 * @param constant
	 *            Column name as used in the database
	 * @return Name of the column as used in the user-given file
	 */
	private String processColumnNameInput(String parameter, String constant) {
		String s = constant;
		String editedName = cf.getParameter(parameter);
		if (editedName != null && !editedName.isEmpty()) {
			s = editedName;
		}
		return s;
	}

	/**
	 * Gets patientIDs from VCF and inserts them into the patient table.
	 * 
	 * @param insert
	 *            Incomplete insert-statement
	 * @return true if all IDs were inserted successfully
	 */
	private boolean getPatientIdsFromVCF(String insert) {
		String prefix = "(?i)";
		StringBuffer buildPattern = new StringBuffer();
		for (String s : DefaultColumns.VCF_HEADER) {
			buildPattern.append(prefix + s);
			prefix = "|";
		}
		String vcfDefaultHeaderPattern = buildPattern.toString();
		boolean ok = false;

		List<String> dataports = new ArrayList<String>();
		dataports.add(Port.VCF_NORMAL);
		dataports.add(Port.VCF_TUMOR);
		for (String port : dataports) {
			if (cf.inputDefined(port)) {
				CollectionParser vcfFile;
				ok = true;
				try {
					vcfFile = this.getInput(port);
				} catch (IOException e) {
					cf.writeError("Failed to read " + port + " input: " + e);
					return false;
				} catch (IllegalInputException e) {
					cf.writeError("Failed to read " + port + " input: " + e);
					return false;
				}
				List<String> vcfHeader = vcfFile.nextCaseSensitive(vcfFile
						.getColumnNames());

				Iterator<String> iterator = vcfHeader.iterator();
				String h;
				while (iterator.hasNext() && ok) {
					h = iterator.next();
					for (String p : patientIDs) {
						if (Pattern.matches("(?i)" + p + "(.*)", h)) {
							h = p;
						}
					}
					if (!Pattern.matches(vcfDefaultHeaderPattern, h)
							&& patientIDs.add(h)) {
						ok = insertPatientID(insert, h);
					}
				}
			}
		}
		return ok;
	}

	/**
	 * Inserts patientID into patient table.
	 * 
	 * @param insert
	 *            Incomplete insert-statement
	 * @param patientID
	 *            patientID that is to be inserted
	 * @return true if patientID was inserted successfully
	 */
	private boolean insertPatientID(String insert, String patientID) {
		String values = "'" + patientID.toUpperCase() + "');";
		String sql = insert.toString() + values;
		return executeStmt(sql);
	}

	/**
	 * Completes insert-statement and executes insert into SQLite-database.
	 * Input is a VCF-file, and possibly an annotations file.
	 * 
	 * @param vcfParser
	 *            Parser from which input for insertion is read
	 * @param table
	 *            Table in which to insert
	 * @param insert
	 *            Incomplete insert-statement
	 * @return true if insert was successfully executed
	 */
	private boolean insertVCFData(CollectionParser vcfParser, Table table,
			String insert, String port) {
		CollectionParser annotation = null;
		CaseInsensitiveMap annoAsMap = null;
		String annotationPort = null;
		String tissue = null;

		if (port.equals(Port.VCF_NORMAL)) {
			annotationPort = Port.ANNOTATION_NORMAL;
			tissue = "normal";
		} else if (port.equals(Port.VCF_TUMOR)) {
			annotationPort = Port.ANNOTATION_TUMOR;
			tissue = "tumor";
		}
		if (annotationPort != null && cf.inputDefined(annotationPort)) {
			annotation = getAnnotationParser(annotationPort);
			if (annotation.hasNext()) {
				annoAsMap = annotation.next(annotation.next(),
						annotation.getColumnNames());
			} else {
				return false;
			}
		}

		boolean ok = true;
		while (vcfParser.hasNext() && ok) {
			CaseInsensitiveMap lineAsMap = vcfParser.next(vcfParser.next(),
					vcfParser.getColumnNames());
			lineAsMap.put(DefaultColumns.TISSUE, tissue);

			lineAsMap = splitVCFInfoField(lineAsMap);
			if (table.getName().equals(Table.ALT)) {
				ok = splitVCFAltInput(annotation, table, lineAsMap, annoAsMap,
						insert);
			} else {
				ok = executeStmt(table, lineAsMap, insert);
			}
		}
		return ok;
	}

	/**
	 * Creates a parser of given annotation data.
	 * 
	 * @return Parser of annotation or null if no annotation file is given
	 */
	private CollectionParser getAnnotationParser(String port) {
		CollectionParser annotation = null;
		try {
			annotation = this.getInput(port);
		} catch (IllegalInputException e) {
			cf.writeError("Failed to read " + port + " input: " + e);
		} catch (IOException e) {
			cf.writeError("Failed to read " + port + " input: " + e);
		}
		return annotation;
	}

	/**
	 * Completes insert-statement and executes it.
	 * 
	 * @param table
	 *            Table in which to insert
	 * @param lineAsMap
	 *            One input-line that is to be inserted
	 * @param insert
	 *            Incomplete insert-statement
	 * @return true if insert was successfully executed
	 */
	private boolean executeStmt(Table table, CaseInsensitiveMap lineAsMap,
			String insert) {
		String prefix = "'";
		StringBuffer values = new StringBuffer();
		for (Column c : table.getColumns()) {
			String colName = c.getName();
			if (table.getName().equals(Table.VARIANT)
					&& colName.equals(DefaultColumns.ALL_ALT)) {
				colName = DefaultColumns.ALT;
			}
			values.append(prefix + lineAsMap.get(colName));
			prefix = "','";
		}
		values.append("');");
		String sql = insert + values.toString();
		return executeStmt(sql);
	}

	/**
	 * Executes insert-statement.
	 * 
	 * @param sql
	 *            Complete insert-statement
	 * @return true if insert was executed successfully
	 */
	public boolean executeStmt(String sql) {
		sql = sql.replace("'null'", "NULL");
		Statement stmt;
		try {
			stmt = connection.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			String msg = String.format(
					"Error inserting into SQL table: %s; SQL statement is %s",
					e, sql);
			cf.writeError(msg);
			return false;
		}
		return true;
	}

	/**
	 * Splits Info-field from VCF-file into several key-value-pairs.
	 * 
	 * @param lineAsMap
	 *            Line from VCF-file as key-value pairs
	 * @return Inputted map, extended by split data from Info-field
	 */
	private CaseInsensitiveMap splitVCFInfoField(CaseInsensitiveMap lineAsMap) {
		String infoField = lineAsMap.get(DefaultColumns.INFO);
		lineAsMap.remove(DefaultColumns.INFO);
		String[] splitRes = infoField.split(";");
		for (int i = 0; i < splitRes.length; i++) {
			String[] fields = splitRes[i].split("=");
			if (fields.length == 1) {
				// special case for DB (dbSnp membership), where no value is
				// offered, so "1" means true in this case
				lineAsMap.put(fields[0], "1");
			} else {
				lineAsMap.put(fields[0], fields[1]);
			}
		}

		return lineAsMap;
	}

	/**
	 * Splits alternative allele field of VCF-file into separate
	 * key-value-pairs, if there is more than one alternative allele. Adds
	 * annotations if there are any. Completes and executes insert-statement.
	 * 
	 * @param annotation
	 *            Parser containing annotation-data
	 * @param table
	 *            Table in which to insert
	 * @param lineAsMap
	 *            Line from VCF-file as key-value pairs
	 * @param insert
	 *            Incomplete insert-statement
	 * @return true if insert was successful
	 */
	private boolean splitVCFAltInput(CollectionParser annotation, Table table,
			CaseInsensitiveMap lineAsMap, CaseInsensitiveMap annoAsMap,
			String insert) {
		boolean ok = true;
		String ac = DefaultColumns.ALLELE_COUNT;

		String altColInput = lineAsMap.get(DefaultColumns.ALT);
		String[] altColumnSplit = altColInput.split(",");
		lineAsMap.put(DefaultColumns.ALL_ALT, altColInput);
		String[] acArray = null;
		lineAsMap.remove(DefaultColumns.ALT);
		if (lineAsMap.get(ac) != null) {
			acArray = lineAsMap.get(ac).split(",");
			lineAsMap.remove(ac);
		}
		int i = 0;
		while (i < altColumnSplit.length && ok) {
			lineAsMap.put(DefaultColumns.ALT, altColumnSplit[i]);
			if (acArray != null) {
				lineAsMap.put(ac, acArray[i]);
			}
			if (annotation != null) {
				lineAsMap = getSpecificAnnotation(annotation, lineAsMap,
						annoAsMap);
			}
			ok = this.executeStmt(table, lineAsMap, insert);
			i++;
		}
		return ok;
	}

	/**
	 * Adds annotation to the variation site, if available.
	 * 
	 * @param annotation
	 *            Parser containing the annotation-data
	 * @param lineAsMap
	 *            Map containing the data from one line of the VCF-file
	 * @param annoAsMap
	 *            Current line of the annotation-file
	 * @return Map containing previous VCF-data and possibly annotation for this
	 *         position
	 */
	private CaseInsensitiveMap getSpecificAnnotation(
			CollectionParser annotation, CaseInsensitiveMap lineAsMap,
			CaseInsensitiveMap annoAsMap) {
		String chromSample = lineAsMap.get(DefaultColumns.CHROM);
		int posSample = getInt(lineAsMap, DefaultColumns.POS);
		String chromAnno = annoAsMap.get(annotationProperties.getChrom());
		int posAnno = getInt(annoAsMap, annotationProperties.getPos());

		while (annotation.hasNext() && chromAnno.equals(chromSample)
				&& posAnno < posSample) {
			// order of the chromosomes has to be identical in VCF and
			// annotations file
			annoAsMap = annotation.next(annotation.next(),
					annotation.getColumnNames());
			chromAnno = annoAsMap.get(annotationProperties.getChrom());
			posAnno = getInt(annoAsMap, annotationProperties.getPos());
		}

		String refSample = lineAsMap.get(DefaultColumns.REF);
		String altSample = lineAsMap.get(DefaultColumns.ALT);
		String refAnno = annoAsMap.get(annotationProperties.getRef());
		String altAnno = annoAsMap.get(annotationProperties.getAlt());
		// trying to fit the "-" values of the annotation file to the VCF-file
		if (refAnno.equals("-") && altSample.startsWith(refSample)) {
			altSample = altSample.substring(1);
		} else if (altAnno.equals("-") && refSample.startsWith(altSample)) {
			refSample = refSample.substring(1);
			posAnno -= 1;
		}

		if (posAnno == posSample
				&& chromAnno.equals(chromSample)
				&& (altAnno.equalsIgnoreCase(altSample) || refAnno
						.equalsIgnoreCase(refSample))) {
			// here, the order of the alternative alleles has to be identical in
			// the VCF- and the annotation file
			lineAsMap.putAll(annoAsMap);
		}

		return lineAsMap;
	}

	/**
	 * 
	 * @return parsed int of the value map.get(key)
	 */
	private int getInt(CaseInsensitiveMap map, String key) {
		return Integer.parseInt(map.get(key));
	}

	/**
	 * Completes and executes insert into sample-table.
	 * 
	 * @param vcfParser
	 *            Parser from which input for insertion is read
	 * @param table
	 *            Table in which to insert
	 * @param insert
	 *            Incomplete insert-statement
	 * @return true if insert was successful
	 */
	private boolean insertSampleData(CollectionParser vcfParser, Table table,
			String insert, String port) {

		String prefix = "(?i)";
		StringBuffer buildPattern = new StringBuffer();
		for (String s : DefaultColumns.VCF_HEADER) {
			buildPattern.append(prefix + s);
			prefix = "|";
		}
		String vcfDefaultHeaderPattern = buildPattern.toString();

		Map<String, String> lineAsMap = new HashMap<String, String>();
		CaseInsensitiveMap insertInput = new CaseInsensitiveMap();
		List<String> vcfHeader = vcfParser.next(vcfParser.getColumnNames());
		Set<String> noDuplicatePatientIDs = new LinkedHashSet<String>(vcfHeader);
		try {
			if (vcfHeader.size() > noDuplicatePatientIDs.size()) {
				throw new IllegalInputException(
						"VCF header contains duplicate patientIDs");
			}
		} catch (IllegalInputException e) {
			cf.writeError(e);
			return false;
		}
		boolean ok = true;

		while (vcfParser.hasNext()) {
			List<String> lineAsList = vcfParser.nextCaseSensitive(vcfParser
					.next());

			lineAsMap.clear();
			for (int i = 0; i < vcfHeader.size(); i++) {
				lineAsMap.put(vcfHeader.get(i), lineAsList.get(i));
			}

			String valueChrom = lineAsMap.get(DefaultColumns.CHROM);
			String valuePos = lineAsMap.get(DefaultColumns.POS);
			String tissue = null;
			if (port.equals(Port.VCF_NORMAL)) {
				tissue = "normal";
			} else if (port.equals(Port.VCF_TUMOR)) {
				tissue = "tumor";
			}

			insertInput.clear();
			insertInput.put(DefaultColumns.CHROM, valueChrom);
			insertInput.put(DefaultColumns.POS, valuePos);
			insertInput.put(DefaultColumns.TISSUE, tissue);
			boolean inclAll = cf.getBooleanParameter(Port.INCL_ALL_PAT_IDS);
			for (int i = 0; i < vcfHeader.size(); i++) {
				String h = vcfHeader.get(i);
				if (!Pattern.matches(vcfDefaultHeaderPattern, h)) {
					insertInput.put(DefaultColumns.PATIENT_ID, h);
					insertInput.put(DefaultColumns.SAMPLE_TAG, null);
					boolean patientInputAvailable = (cf
							.inputDefined(Port.PATIENT) || cf
							.inputDefined(Port.MEDICAL));
					Matcher matcher;
					String formattedPatientID = "";
					for (String p : patientIDs) {
						matcher = Pattern.compile("(?i)" + p + "(.*)").matcher(
								h);
						if (matcher.matches()) {
							insertInput.put(DefaultColumns.PATIENT_ID, p);
							formattedPatientID = p;
							if (patientInputAvailable && matcher.find(0)) {
								String matched = matcher.group(1).toUpperCase();
								if (!matched.isEmpty()) {
									insertInput.put(DefaultColumns.SAMPLE_TAG,
											matched);
								}
							}
						}
					}
					if (inclAll || patientIDs.contains(formattedPatientID)) {
						ok = insertOneSampleInput(table, lineAsMap,
								insertInput, h, insert);
					}
				}
			}
		}
		return ok;
	}

	/**
	 * Completes and executes insert for one sample-column and on variant site
	 * of the VCF-file.
	 * 
	 * @param table
	 *            Table in which to insert
	 * @param lineAsMap
	 * @param insertInput
	 * @param patientID
	 * @param insert
	 * @return
	 */
	private boolean insertOneSampleInput(Table table,
			Map<String, String> lineAsMap, CaseInsensitiveMap insertInput,
			String patientID, String insert) {
		boolean ok = true;
		String sampleData;
		String[] altColumnSplit;
		String[] adColumnSplit = null;
		String[] formatFieldSplit;

		sampleData = lineAsMap.get(patientID);
		if (!sampleData.startsWith("./.")) {
			formatFieldSplit = lineAsMap.get(DefaultColumns.FORMAT).split(":");
			altColumnSplit = lineAsMap.get(DefaultColumns.ALT).split(",");
			String[] sampleDataSplit;

			sampleDataSplit = sampleData.split(":");
			for (Column c : table.getColumns()) {
				for (int k = 0; k < formatFieldSplit.length; k++) {
					if (c.getName().equalsIgnoreCase(formatFieldSplit[k])
							|| formatFieldSplit[k]
									.equalsIgnoreCase(DefaultColumns.ALLELE_DEPTH)) {
						insertInput
								.put(formatFieldSplit[k], sampleDataSplit[k]);
					}
				}
			}
			if (insertInput.get(DefaultColumns.ALLELE_DEPTH) != null) {
				adColumnSplit = insertInput.get(DefaultColumns.ALLELE_DEPTH)
						.split(",");
			}
			int i = 0;
			while (i < altColumnSplit.length && ok) {
				insertInput.put(DefaultColumns.ALT, altColumnSplit[i]);
				if (adColumnSplit != null
						&& adColumnSplit.length == altColumnSplit.length + 1) {
					if (!(adColumnSplit[0].equals("0") && adColumnSplit[i + 1]
							.equals("0"))) {
						insertInput
								.put(DefaultColumns.AD_REF, adColumnSplit[0]);
						insertInput.put(DefaultColumns.AD_ALT,
								adColumnSplit[i + 1]);
						ok = this.executeStmt(table, insertInput,
								insert.toString());
					}
				} else {
					ok = this
							.executeStmt(table, insertInput, insert.toString());
				}
				i++;
			}
		}
		return ok;
	}

	/**
	 * Creates index on variant.tissue
	 * 
	 * @return true if successful
	 */
	public boolean createIndexOnTissueColumn() {
		String index;
		index = "CREATE INDEX tissue_idx ON " + Table.VARIANT + " ("
				+ DefaultColumns.TISSUE + ");";
		return executeStmt(index);
	}
	
	/**
	 * Creates index on variant.tissue
	 * 
	 * @return true if successful
	 */
	public boolean createIndexOnPosColumn() {
		String index;
		index = "CREATE INDEX pos_idx ON " + Table.VARIANT + " ("
				+ DefaultColumns.POS + ");";
		return executeStmt(index);
	}

	/**
	 * Turns auto-commit on or off.
	 * 
	 * @param bool
	 *            true (on) or false (off)
	 */
	public void setAutoCommit(boolean bool) {
		try {
			connection.setAutoCommit(bool);
		} catch (SQLException e) {
			cf.writeError(e);
		}
	}

	/**
	 * Commits to database
	 */
	public void commit() {
		try {
			connection.commit();
		} catch (SQLException e) {
			cf.writeError(e);
		}
	}

	/**
	 * @return List of Tables of the database
	 */
	public List<Table> getTables() {
		return this.tables;
	}
}