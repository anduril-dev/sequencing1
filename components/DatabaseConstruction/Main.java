package dbConstruction;

import java.io.File;
import java.util.List;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

public class Main extends SkeletonComponent {

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {

		// validate that VCF file is not only there, but not empty either
		File vcf_normal = cf.getInput(Port.VCF_NORMAL);
		File vcf_tumor = cf.getInput(Port.VCF_TUMOR);

		if (!cf.inputDefined(Port.VCF_NORMAL)
				&& !cf.inputDefined(Port.VCF_TUMOR)) {
			cf.writeError("No VCF-file. At least one VCF file has to be given.");
			return ErrorCode.ERROR;
		}
		if ((vcf_normal != null && vcf_normal.length() == 0)
				|| (vcf_tumor != null && vcf_tumor.length() == 0)) {
			cf.writeError("VCF-file is empty.");
			return ErrorCode.ERROR;
		}

		Database database = null;
		boolean ok;

		// establishing database
		database = new Database(cf);

		ok = database.connect();
		if (!ok) {
			return ErrorCode.ERROR;
		}

		// getting precast table names and adding them to the database

		List<String> tableNames;
		boolean extended = cf.inputDefined(Port.MEDICAL);
		PrecastTables precast = new PrecastTables(extended);

		tableNames = precast.getTableNames();

		for (String tn : tableNames) {
			database.addTable(tn);
		}

		List<Table> tables = database.getTables();

		// adding and checking requested columns

		for (Table tab : tables) {
			try {
				ok = database.validateInput(tab);
			} catch (IllegalInputException e) {
				cf.writeError("Table " + tab.getName() + ": " + e);
				return ErrorCode.ERROR;
			}

			if (!ok) {
				return ErrorCode.ERROR;
			}
		}

		// creating tables
		for (Table tab : tables) {
			ok = database.createTable(tab);
			if (!ok) {
				return ErrorCode.ERROR;
			}
		}

		// inserting information from ports
		database.executeStmt("PRAGMA synchronous = OFF");
		database.setAutoCommit(false);
		for (Table tab : tables) {
			ok = database.insertIntoTable(tab);
			if (!ok) {
				return ErrorCode.ERROR;
			}
		}
		database.commit();
		database.setAutoCommit(true);
		database.executeStmt("PRAGMA synchronous = FULL");

		ok = database.createIndexOnTissueColumn();
		if (!ok) {
			return ErrorCode.ERROR;
		}
		ok = database.createIndexOnPosColumn();
		if (!ok) {
			return ErrorCode.ERROR;
		}

		return ErrorCode.OK;
	}

	public static void main(String[] args) {
		new Main().run(args);
	}

}
