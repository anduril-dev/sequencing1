<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>DatabaseConstruction</name>
    <version>1.0</version>
    <doc>DatabaseConstruction will construct a database using informaion from VCF-files and files with patient-related data. The database can then be used for queries of specific research questions, for example to compare variations in a specific gene of a specific group of patients. Either normal or tumor VCF file has to be given. Columns to be included in the database can be given either as CSV-file (tab-separated) or as parameter-input (comma-separated list). VCF file containing the normal data can be used for filtering the tumor data.
    <br/>Database structure can be seen below. The table variant holds the information of the VCF-file concerning the whole row, whereas the table sample holds the information for that variation site for every patient. -- <b>Example:</b> If the VCF file was created using data of ten samples, for every entry in the variant table, there are ten entries in the sample-table. -- The table alt holds the information about the different alternative alleles at one position, so that they can be used for queries seperately. -- <b>Example:</b> If the VCF-column ALT shows two alternative alleles, there are two entries in the alt-table for one entry in the VCF table. --
    <br/><br/><b>Database structure:</b>
    <table border="3">
  <tr>
    <th>variant</th>
    <th>alt</th>
    <th>sample</th>
    <th>patient</th>
    <th>treatment</th>
  </tr>
  <tr>
    <td><font color="#8b0000">chrom</font></td>
    <td><font color="#8b0000">chrom</font></td>
    <td><font color="#8b0000">chrom</font></td>
    <td><font color="#8b0000">patient_id</font></td>
    <td><font color="#8b0000">patient_id</font></td>
  </tr>
  <tr>
    <td><font color="#8b0000">pos</font></td>
    <td><font color="#8b0000">pos</font></td>
    <td><font color="#8b0000">pos</font></td>
    <td></td>
    <td><font color="#8b0000">drug_id</font></td>
  </tr>
  <tr>
    <td><font color="#8b0000">ref</font></td>
    <td><font color="#8b0000">ref</font></td>
    <td><font color="#8b0000">patient_id</font></td>
    <td></td>
    <td><font color="#8b0000">drug_name</font></td>
  </tr>
  <tr>
    <td><font color="#8b0000">all_alt</font></td>
    <td><font color="#8b0000">alt</font></td>
    <td><font color="#8b0000">alt</font></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td><font color="#8b0000">tissue</font></td>
    <td><font color="#8b0000">all_alt</font></td>
    <td><font color="#8b0000">tissue</font></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>id</td>
    <td><font color="#8b0000">tissue</font></td>
    <td><font color="#8b0000">sample_tag</font></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>qual</td>
    <td>ac</td>
    <td>gt</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>filter</td>
    <td></td>
    <td>ad_ref</td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>dp</td>
    <td></td>
    <td>ad_alt</td>
    <td></td>
    <td></td>
  </tr>
</table>
<font color="#8b0000">column</font> = Primary Key column
<br/>column = default column
<br/><br/>Values for the primary key columns have to be there.
<br/><code><font size="4">sample_tag</font></code> is the column where additional information about the sample is stored, that the <code><font size="4">patient_id</font></code> was tagged with.
<br/><b>Example:</b> The <code><font size="4">patient_id</font></code> in your patient file is: 'ABCD-1234-56'. In your VCF-file, the sample of said patient is tagged with additional information, coded in numbers. So the VCF-file will actually list 'ABCD-1234-56-01-XY-2'. Accordingly, 'ABCD-1234-56' is stored as <code><font size="4">patient_id</font></code> and '-01-XY-2' as <code><font size="4">sample_tag</font></code>. If there are two samples of the same patient, then there is only one main entry in the patient table, but there can be numerous entries in the sample-table.</doc>
    <author email="anduril-dev@helsinki.fi">Insa Lietz</author>
    <category>Analysis</category>
    <launcher type="java"><argument name="class" value="dbConstruction.Main"/></launcher>
    <requires type="jar" URL="https://bitbucket.org/xerial/sqlite-jdbc">sqlite-jdbc-3.7.2.jar</requires>
    <inputs>
        <input name="normal" type="VCF" optional="true">
            <doc>File from which database will be created. If patient or treatment information is provided, samples should have the same ID as in that (those) file(s).</doc>
        </input>
        <input name="tumor" type="VCF" optional="true">
            <doc>File from which database will be created. If patient or treatment information is provided, samples should have the same ID as in that (those) file(s).</doc>
        </input>
        <input name="patient" type="CSV" optional="true">
            <doc>A CSV file which contains information about the patients (whose samples are used for variant calling in the VCF file). Headers have to be present. If the file is not provided patient table will only contain patientIDs either from medical data or inferred from variant file.</doc>
        </input>
        <input name="treatment" type="CSV" optional="true">
            <doc>A CSV file which contains information about the medical treatment of the patients. Headers should be present. Default columns: patientID, drugID, drugName.</doc>
        </input>
        <input name="annotationsNormal" type="CSV" optional="true">
            <doc>A CSV file which contains annotations for the given normal VCF file. Columns that should be included in the output can be specified in columnsAlt.
            <br/><b>Please note:</b> If no columns are specified, the annotations will not be used in the database - unless <code><font size="4">vcfAllColumns</font></code> is set to <code><font size="4">true</font></code>.</doc>
        </input>
        <input name="annotationsTumor" type="CSV" optional="true">
            <doc>A CSV file which contains annotations for the given tumor VCF file. Columns that should be included in the output can be specified in columnsAlt.
            <br/><b>Please note:</b> If no columns are specified, the annotations will not be used in the database - unless <code><font size="4">vcfAllColumns</font></code> is set to <code><font size="4">true</font></code>.</doc>
        </input>
        <input name="columnsVariant" type="CSV" optional="true">
            <doc>A CSV file which contains the information about which columns concerning the variant site in general (besides the default columns) should be included. The requested fields have to be present in the VCF file. First line should contain column information, second line can contain datatype (<code><font size="4">INTEGER</font></code>, <code><font size="4">REAL</font></code> or <code><font size="4">TEXT</font></code>  followed by <code><font size="4">NOT NULL</font></code> for those columns where every row should have an entry; default if second line is not or only partially provided is <code><font size="4">INTEGER</font></code>).</doc>
        </input>
        <input name="columnsAlt" type="CSV" optional="true">
            <doc>A CSV file which contains the information about which columns regarding the annotation file should be included. First line should contain column information, second line can contain datatype (<code><font size="4">INTEGER</font></code>, <code><font size="4">REAL</font></code> or <code><font size="4">TEXT</font></code>  followed by <code><font size="4">NOT NULL</font></code> for those columns where every row should have an entry; default if second line is not or only partially provided is <code><font size="4">INTEGER</font></code>).</doc>
        </input>
        <input name="columnsSample" type="CSV" optional="true">
            <doc>A CSV file which contains the information about which columns regarding the samples besides the default columns should be included. The available columns can be found in the <code><font size="4">FORMAT</font></code> field of the VCF file. First line should contain column information, second line can contain datatype (<code><font size="4">INTEGER</font></code>, <code><font size="4">REAL</font></code> or <code><font size="4">TEXT</font></code>  followed by <code><font size="4">NOT NULL</font></code> for those columns where every row should have an entry; default if second line is not or only partially provided is <code><font size="4">INTEGER</font></code>).</doc>
        </input>
        <input name="columnsPatient" type="CSV" optional="true">
            <doc>A CSV file which contains the information of which fields to include. The requested fields have to be present in the patient file(s). First line should contain column information, second line can contain datatype (<code><font size="4">INTEGER</font></code>, <code><font size="4">REAL</font></code> or <code><font size="4">TEXT</font></code>  followed by <code><font size="4">NOT NULL</font></code> for those columns where every row should have an entry; default if second line is not or only partially provided is <code><font size="4">INTEGER</font></code>).</doc>
        </input>
        <input name="columnsTreatment" type="CSV" optional="true">
            <doc>A CSV file which contains the information of which fields of the treatment file should be included. The requested fields have to be present in the given treatment file. First line should contain column information, second line can contain datatype (<code><font size="4">INTEGER</font></code>, <code><font size="4">REAL</font></code> or <code><font size="4">TEXT</font></code>  followed by <code><font size="4">NOT NULL</font></code> for those columns where every row should have an entry; default if second line is not or only partially provided is <code><font size="4">INTEGER</font></code>).</doc>
        </input>
    </inputs>
    <outputs>
        <output name="database" type="BinaryFile">
            <doc>The created database.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="columnsVariant" type="string" default="">
            <doc>A comma-separated list of columns concerning the variant table that should be included.</doc>
        </parameter>
        <parameter name="columnsAlt" type="string" default="">
            <doc>A comma-separated list of columns concerning the alternative-allele table that should be included.</doc>
        </parameter>
        <parameter name="columnsSample" type="string" default="">
            <doc>A comma-separated list of columns concerning the sample table that should be included.</doc>
        </parameter>
        <parameter name="columnsPatient" type="string" default="">
            <doc>A comma-separated list of columns concerning the patient table that should be included.</doc>
        </parameter>
        <parameter name="columnsTreatment" type="string" default="">
            <doc>A comma-separated list of columns concerning the variant table that should be included.</doc>
        </parameter>
        <parameter name="patientDefaultColumnPatientID" type="string" default="patient_id">
            <doc>The header for the patient_id default column as used in the given patient-related data file.</doc>
        </parameter>
        <parameter name="treatmentDefaultColumnPatientID" type="string" default="patient_id">
            <doc>The header for the patient_id default column as used in the given medical treatment data file.</doc>
        </parameter>
        <parameter name="treatmentDefaultColumnDrugID" type="string" default="drug_id">
            <doc>The header for the drug_id default column as used in the given medical treatment data file.</doc>
        </parameter>
        <parameter name="treatmentDefaultColumnDrugName" type="string" default="drug_name">
            <doc>The header for the drug_name default column as used in the given medical treatment data file.</doc>
        </parameter>
        <parameter name="vcfAllColumns" type="boolean" default="false">
            <doc>Set to <code><font size="4">true</font></code> if database should include all columns that can be created from the VCF and annotation files.</doc>
        </parameter>
        <parameter name="patientAllColumns" type="boolean" default="false">
            <doc>Set to <code><font size="4">true</font></code> if database should include all columns that can be created from the patient file.</doc>
        </parameter>
        <parameter name="treatmentAllColumns" type="boolean" default="false">
            <doc>Set to <code><font size="4">true</font></code> if database should include all columns that can be created from the medical treatment file.</doc>
        </parameter>
        <parameter name="inclAllPatientIDs" type="boolean" default="true">
            <doc>Set to <code><font size="4">false</font></code> if database should include only the information from the VCF file related to the patientIDs given in the patient or treatment file.</doc>
        </parameter>
        <parameter name="annotationChromName" type="string" default="chrom">
            <doc>Set to name of the corresponding column in the annotation file.</doc>
        </parameter>
        <parameter name="annotationPosName" type="string" default="start">
            <doc>Set to name of the corresponding column in the annotation file.</doc>
        </parameter>
        <parameter name="annotationRefName" type="string" default="ref">
            <doc>Set to name of the corresponding column in the annotation file.</doc>
        </parameter>
        <parameter name="annotationAltName" type="string" default="alt">
            <doc>Set to name of the corresponding column in the annotation file.</doc>
        </parameter>
    </parameters>
</component>

