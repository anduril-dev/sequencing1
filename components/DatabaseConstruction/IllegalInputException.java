package dbConstruction;

public class IllegalInputException extends Exception{
	
	IllegalInputException() {
		this("Requested columns can not be found in provided database file.");
	}
	
	IllegalInputException(String s) {
		super("Query can not be executed. " + s);
	}

}
