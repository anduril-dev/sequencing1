package dbConstruction;

import java.util.LinkedHashMap;
import java.util.Map;

public class CaseInsensitiveMap extends LinkedHashMap<String, String> {

	public CaseInsensitiveMap() {
		super();
	}

	public CaseInsensitiveMap(Map<? extends String, ? extends String> map) {
		super(map.size());
		putAll(map);
	}

	/**
	 * Puts key and value into map or replaces value previously associated with
	 * specified key.
	 * 
	 * @param key
	 *            key to insert into map
	 * @param value
	 *            value to insert into map, associated to key
	 * @return previous value associated with key or null
	 */
	public String put(String key, String value) {
		return super.put(key.toLowerCase(), value);
	}

	/**
	 * Gets value for specified key, where case of the key is not relevant.
	 * 
	 * @param key
	 *            key for which to get the value
	 * @return value of specified key
	 */
	public String get(String key) {
		return super.get(key == null ? null : key.toLowerCase());
	}

	/**
	 * @param key
	 *            key for which to check if it is contained in the map
	 * @return true if map contains key
	 */
	public boolean containsKey(String key) {
		return super.containsKey(key == null ? null : key.toLowerCase());
	}

	/**
	 * @param key
	 *            key to the key-value-pair that should be removed
	 */
	public String remove(String key) {
		return super.remove(key == null ? null : key.toLowerCase());
	}
	
	public void clear() {
		super.clear();
	}

}
