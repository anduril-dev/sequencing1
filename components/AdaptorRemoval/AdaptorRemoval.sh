#!/bin/bash -x

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"

#--Get all inputs, outputs and parameters
isFASTQ=$( getparameter fastq )
reads=$( getinput reads )
arrayOut=$( getoutput arrayOut )
stats=$( getparameter stats )
# Check if input tags was defined
input_tags=$( getinput tags )
over=$( getinput overSeqs )
param_tag3=$( getparameter tag3 )
param_tag5=$( getparameter tag5 )
mm3=$( getparameter mm3 )
mm5=$( getparameter mm5 )
trim=$( getparameter trim )
inputportindex=$( getinput "_index_arrayIn" )
portindex=$( getoutput "_index_arrayOut" )
predict=$( getparameter predict )
paired=$( getparameter paired )
predictLog=$( getoutput predictLog )
removalLog=$( getoutput removalLog )
verbose=$( getparameter verbose )
percentage=$( getparameter percentage )
extra=$( getparameter extra )
matrix=$( getparameter matrix )
tmpdir=$( gettempdir )

key="read"
myDir=$( pwd )

#TODO: Add exit if trim,stats,and predict are false

base_command=$( readlink -f tagcleaner.pl )

base_command="${base_command} -matrix $matrix"

if [ "$verbose" == "true" ]
then
    base_command="${base_command} -verbose"
fi

#Creating header of the index file
mkdir -p "$arrayOut"
echo -e '"Key"'"\t"'"File"' > "${portindex}"

tags_csv="$arrayOut"/tags.csv
touch $tags_csv

#--Building the command, defining if files are fastq or fasta
if [ "$isFASTQ" == "true" ]
then 
    base_command="${base_command} -fastq"
    ext=fastq
else 
	base_commnd="${base_command} -fasta"
    ext=fasta
fi

if [ "$reads" != "" ]
then
 	if [ ! -f "$reads" ]
   	then
       	writeerror "file does not exist: "$reads
		exit 1
   	fi
    inputDir=$( dirname $reads )
    newReads=$( basename $reads )
    cd $inputDir     
   	base_command="${base_command} $newReads"
    prefix=$( echo ${newReads} | cut -d'.' -f1 )
fi

#--Predict tags if predict option was selected
if [ "$predict" == "true" ]
then
	command="${base_command} -predict -log $predictLog"
    echo "command: "$command
    perl $command >> "$tags_csv" 2>> "$logfile"
    if [ $? != 0 ]
    then
        writeerror "It failed (error "$?"), check the log"
        exit 1
    fi
else
    echo "Predict was set to false" > $predictLog
fi

#--Trim sequences: check if tag is specified or if they are in the tags file recently created
if [ "$trim" == "true" -o "$stats" == "true" ]
then
    # if we have predicted tags then tags_csv is not empty
    if [ ! -z ${tags_csv} ]
    then
        a=$( wc -l ${tags_csv} ) 
        if [[ $a > 1 ]]
        then
            echo "We have predicted tags"
            #save tags that meet the percentage criteria in tag3 and tag5 files
            awk -v per=$percentage '/tag3/ {if ($4>=per) print substr($2,1,64)}' ${tags_csv} > "$tmpdir"/tag3
            awk -v per=$percentage '/tag5/ {if ($4>=per) print substr($2,1,64)}' ${tags_csv} > "$tmpdir"/tag5
        else
            echo "No tags were predicted"
        fi
    else 
        echo "No tags were predicted"        
    fi
    
    # check if we have tags in the input tags  
    if [ ! -z $input_tags -a -s $input_tags ]
    then
         echo "Tags found in tags input"
         awk '/tag3/ {print substr($2,1,64)}' $input_tags >> "$tmpdir"/tag3
         awk '/tag5/ {print substr($2,1,64)}' $input_tags >> "$tmpdir"/tag5
    else
        echo "No input tags supplied"
    fi
    
    # check if we have tags in the overrepresented sequences input; this sequences go both in tag3 and tag5 files
    if [ ! -z $over -a -s $over ]
    then
        echo "Tags found in overrepresented sequences input"
        awk -v per=$percentage '{if (NR>1) {if ($4 != "No Hit") print substr($1,1,64); else if ($3>=per) print substr($1,1,64)}}' $over >> "$tmpdir"/tag3
        awk -v per=$percentage '{if (NR>1) {if ($4 != "No Hit") print substr($1,1,64); else if ($3>=per) print substr($1,1,64)}}' $over >> "$tmpdir"/tag5
        #tail -n +2 $over | cat >> "$tmpdir"/tag3 
        #tail -n +2 $over | cat >> "$tmpdir"/tag5
    else
        echo "No overrepresented sequences to remove"
    fi
 
    if [ "$param_tag3"  == "true" -a -s "$tmpdir"/tag3 ]
    then
        awk '{print "-tag3 "$1}' "$tmpdir"/tag3 > "$tmpdir"/tag3_command
    else 
        writelog "tag3 is true but no tags were found"
        touch "$tmpdir"/tag3_command
    fi

    if [ "$param_tag5"  == "true" -a -s "$tmpdir"/tag5 ]
    then
        awk '{print "-tag5 "$1}' "$tmpdir"/tag5 > "$tmpdir"/tag5_command
    else
        writelog "tag5 is true but no tags were found"
        touch "$tmpdir"/tag5_command
    fi  
    
    #only continue if there are tags
    if [ -s "$tmpdir"/tag3 -o -s "$tmpdir"/tag5 ]
    then
        command="${base_command} -log $removalLog"
        #command="$command -matrix $matrix "

        if [ -s "$tmpdir"/tag3 -a -s "$tmpdir"/tag5 ]
        then
            paste -d' ' "$tmpdir"/tag3_command "$tmpdir"/tag5_command > "$tmpdir"/tags_command
        elif [ -s "$tmpdir"/tag3 ]
        then
            cp "$tmpdir"/tag3_command "$tmpdir"/tags_command
        else
            cp "$tmpdir"/tag5_command "$tmpdir"/tags_command
        fi

        out_format=$( getparameter out_format )
        if [ ${out_format} -gt 0 ] 
        then
            command="$command -out_format $out_format"
        fi  
    
        filename=$arrayOut/trimmed_$prefix
        command="$command -out $filename"
    
        if [ "$stats"  == "true" ]
        then
            command="$command -stats"
            addarray arrayOut stats stats.csv
        fi  
        if [ $mm3 -ne 0 ] 
        then
            command="$command -mm3 $mm3"
        fi  
        if [ "$mm5" -ne 0   ]   
        then
            command="$command -mm5 $mm5"
        fi  
        if [ "$extra"  != "" ]
        then
            command="$command $extra"
        fi  
    
        #echo "command so far "$command 
        awk -v comm="${command}" '{print comm" "$0}' "$tmpdir"/tags_command > "$tmpdir"/script
    	
        if [ -s "$tmpdir"/script ]
	    then
	        bash "$tmpdir"/script >> "$arrayOut"/stats.csv 2>> "$logfile"
            if [ $? != 0 ]
            then
                writeerror "It failed (error "$?"), check the log"
                exit 1
            fi 
	    fi
    fi
else
    echo "Trim was set to false" > $removalLog    
fi

if [ -s "$arrayOut"/trimmed_reads.${ext} ]
then
    addarray arrayOut ${key} trimmed_reads.${ext}
else
    addarray arrayOut ${key} ${reads}
fi

addarray arrayOut tags tags.csv

