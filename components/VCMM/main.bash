#!/bin/bash

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
export_command
tempdir=$( gettempdir ) # nice to have it always since it's the scratch disk
#!/usr/bin/env stage

# Build
#wget http://emu.src.riken.jp/VCMM/download_files/vcmm.zip
#unzip vcmm.zip
#cd VCMM
#make
#cd ..

# Build dep ( Needs samtools-0.1.8 )
#tar jxvf samtools-0.1.8.tar.bz2
#cd samtools-0.1.8/
#make
#cd ..

# Download sample
#wget http://emu.src.riken.jp/VCMM/download_files/NT_167207.1.pile.bz2
#bunzip2 NT_167207.1.pile.bz2

# Run

# -INF
#    Input pileup file path. VCMM accepts raw or bz2 compressed pileup files (need bzcat command)
# -INDEL
#    Output indel file path.
# -SNP
#    Output snp file path.
# -SUM
#    Output summary file path. 

#PATH=../samtools-0.1.8/:VCMM:$PATH

VCMM -INF $input_pileup -INDEL $output_indel -SNV $output_snp -SUM $output_summary $parameter_options

