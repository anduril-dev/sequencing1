PASS	Basic Statistics	read.fq.gz
FAIL	Per base sequence quality	read.fq.gz
PASS	Per sequence quality scores	read.fq.gz
FAIL	Per base sequence content	read.fq.gz
FAIL	Per base GC content	read.fq.gz
FAIL	Per sequence GC content	read.fq.gz
PASS	Per base N content	read.fq.gz
PASS	Sequence Length Distribution	read.fq.gz
PASS	Sequence Duplication Levels	read.fq.gz
FAIL	Overrepresented sequences	read.fq.gz
PASS	Kmer Content	read.fq.gz
