# running functions.sh 
set -e
source "$ANDURIL_HOME/bash/functions.sh"
export_command

# inputs
echo bam    				$input_bam
echo RefGenome 				$input_refGenome

# parameters
echo paired					$parameter_paired
echo phredScore				$parameter_phredScore
echo removeOverlap			$parameter_removeOverlap
echo firstIgnore			$parameter_firstIgnor
echo lastIgnore				$parameter_lastIgnore
echo minCoverage			$parameter_minCoverage
echo conversionFiltering	$parameter_conversionFiltering
echo threads				$parameter_threads
echo context				$parameter_context
echo destrand				$parameter_destrand
echo removeChr				$parameter_removeChr

if [ -n $(which samtools) ]
then
	export PATH=../../lib/samtools:$PATH
fi

mkdir "${output_analysis}"
temp_folder=$( gettempdir )

ln -s "${input_bam}" "${temp_folder}"

flagW="99,147"
flagC="83,163"

if [ "$parameter_paired" == "false" ]
then
	flagW="0"
	flagC="16"
fi

if [ "$parameter_removeOverlap" == "true" ]
then
	peOverlap="Y"
else
	peOverlap="N"
fi

if [ "$parameter_phredScore" != "phred33-quals" -a "$parameter_phredScore" != "phred64-quals" -a "$parameter_phredScore" != "solexa-quals" -a "$parameter_phredScore" != "solexa1.3-quals" ]
then
		writeerror "Unknown phredScore parameter"
		exit 1
fi

if [ "$parameter_context" != "CG" -a "$parameter_context" != "CHG" -a "$parameter_context" != "CHH" -a "$parameter_context" != "ALL" ]
then
		writeerror "Unknown context parameter"
		exit 1
fi

runCommand="perl  MethylExtract.pl seq=${input_refGenome} inDir=${temp_folder} flagW=${flagW} flagC=${flagC} \
			qscore=${parameter_phredScore} FirstIgnor=${parameter_firstIgnore} LastIgnor=${parameter_LastIgnore} \
			minDepthMeth=${parameter_minCoverage} peOverlap=${peOverlap} methNonCpGs=${parameter_conversionFiltering} \
			p=${parameter_threads} context=${parameter_context} outDir=${output_analysis}" 

echo "MethylExtract command is: " $runCommand
eval $runCommand

echo "formating output.."
if [ "$parameter_context" == "CG" ]
then
	CGoutput="${output_analysis}/CG.output"
	rscriptCommand="Rscript script.R ${CGoutput} ${output_CpGmeth} ${parameter_destrand} ${parameter_removeChr}"
	echo rscrip command is $rscriptCommand
	eval $rscriptCommand
	touch ${output_CHGmeth}
	touch ${output_CHHmeth}
fi

if [ "$parameter_context" == "CHG" ]
then
	CHGoutput="${output_analysis}/CHG.output"
	#rscriptCommand="Rscript script.R ${CHGoutput} ${output_CHGmeth} false ${parameter_removeChr}"
	#eval $rscriptCommand
	mv ${CHGoutput} ${output_CHGmeth}
	touch ${output_CpGmeth}
	touch ${output_CHHmeth}
fi

if [ "$parameter_context" == "CHH" ]
then
	CHHoutput="${output_analysis}/CHH.output"
	#rscriptCommand="Rscript script.R ${CHHoutput} ${output_CHHmeth} false ${parameter_removeChr}"
	#eval $rscriptCommand
	mv ${CHHoutput} ${output_CHHmeth}
	touch ${output_CpGmeth}
	touch ${output_CHGmeth}
fi

if [ "$parameter_context" == "ALL" ]
then
	CGoutput="${output_analysis}/CG.output"
	CHHoutput="${output_analysis}/CHH.output"
	CHGoutput="${output_analysis}/CHG.output"
	rscriptCommand1="Rscript script.R ${CGoutput} ${output_CpGmeth} ${parameter_destrand} ${parameter_removeChr}"
	#rscriptCommand2="Rscript script.R ${CHGoutput} ${output_CHGmeth} false ${parameter_removeChr}"
	#rscriptCommand3="Rscript script.R ${CHHoutput} ${output_CHHmeth} false ${parameter_removeChr}"
	eval $rscriptCommand1
	#eval $rscriptCommand2
	#eval $rscriptCommand3
	mv ${CHGoutput} ${output_CHGmeth}
	mv ${CHHoutput} ${output_CHHmeth}	
fi

mv "${output_analysis}"/SNVs.vcf "${output_SNVs}" 
