<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>MethylExtract</name>
    <version>1.0</version>
    <doc> Extracts methylation from a bam file simultaneously with variant calling. The component is a wrapper of the tool MethylExtract </doc>
    <author email="Amjad.Alkodsi@Helsinki.FI">Amjad Alkodsi</author>
    <category>Analysis</category>
    <launcher type="bash">
        <argument name="file" value="methylExtract.sh" />
    </launcher>
    <requires URL="http://bioinfo2.ugr.es/MethylExtract/">MethylExtract</requires>

    <inputs>
        <input name="bam" type="BAM" >
            <doc>Aligned Bisulfite sequencing bam file</doc>
        </input>
        <input name="refGenome" type="BinaryFolder">
            <doc>Reference genome used for alignment 
            </doc>
        </input>
    </inputs>
    <outputs>
        <output name="CpGmeth" type="CSV">
            <doc>Extracted methylation in CpG context.</doc>            
        </output>   
        <output name="CHGmeth" type="CSV">
            <doc>Extracted methylation in CHG context.</doc>
        </output>
         <output name="CHHmeth" type="CSV">
            <doc>Extracted methylation in CHH context.</doc>
        </output>       
         <output name="SNVs" type="VCF">
            <doc>Single nucleotide variations detected in VCF format.</doc>
        </output>  
        <output name="analysis" type="BinaryFolder">
            <doc>Folder containing all produced files other than outputs including logs. 
            </doc>
        </output>
     </outputs>
    <parameters>
        <parameter name="phredScore" type="string" default="phred33-quals">
            <doc>Available options: "phred33-quals", "phred64-quals", "solexa-quals" or "solexa1.3-quals".</doc>
        </parameter>
        <parameter name="destrand" type="boolean" default="false">
            <doc>Merge methylation calls from the two strands in CpG context.</doc>
        </parameter>
        <parameter name="paired" type="boolean" default="true">
            <doc>Set true for paired-end reads.</doc>
        </parameter>
        <parameter name="removeOverlap" type="boolean" default="true">
            <doc>If the read and mate overlaps, consider methylation only one time from the read and ignoring the mate.</doc>
        </parameter>
        <parameter name="context" type="string" default="CG">
            <doc>Contexts to be extracted. Options are "CG", "CHG", "CHH" or "ALL".</doc>
        </parameter>
        <parameter name="threads" type="int" default="1">
            <doc>The number of threads to be used.</doc>
        </parameter>
        <parameter name="conversionFiltering" type="float" default="0.9">
            <doc>The value can be the fraction of methylated non-CpG contexts within a read ( value between 0 and 1) or the absolute number of methylated
            non-CpG contexts (integers higher than 1). Default value is 0.9, i.e. a read is discarded when more than 90% of its non-CpG contexts are methylated. A value of 0
			will turn off the bisulfite check</doc>
        </parameter>
		<parameter name="firstIgnor" type="int" default="1">
            <doc>The number of bases to be ignored from the beginning of the reads (both read and mate) </doc>
        </parameter>
		<parameter name="lastIgnore" type="int" default="0">
            <doc>The number of bases to be ignored from the end of the reads (both read and mate). </doc>
        </parameter>
        <parameter name="minCoverage" type="int" default="1">
            <doc>Minimum coverage for a methylation call. </doc>
        </parameter>
        <parameter name="removeChr" type="boolean" default="true">
            <doc>Remove chromosomes other than autosomal, sex and mitochondrial chromosomes.</doc>
        </parameter>
    </parameters>
</component>
