library(componentSkeleton)

execute <- function(cf){

	# Inputs
	counts <- Matrix.read(get.input(cf, 'counts'))
	if (nrow(counts) < 2){
	  write.error(cf, c("Too few inputs."))
	  return(INVALID_INPUT)
	  }
	if (ncol(counts) < 1){
	  write.error(cf, c("Too few inputs."))
	  return(INVALID_INPUT)
	  }
	dat <- counts

	# Incidences
	incid <- dat[2,] / dat[1,]
	logvar <- 1/dat[2,]
	logsd <- sqrt(logvar)
	incid_lo <- incid*exp(-1.96*logsd)
	incid_hi <- incid*exp(1.96*logsd)

	# Hazard ratios
	hr <- incid / incid[1]
	logsd_hr <- sqrt( (1/dat[2,1]) + (1/dat[2,]) )
	hr_lo <- hr*exp(-1.96*logsd_hr)
	hr_hi <- hr*exp(1.96*logsd_hr)
	P_hr <- pnorm(0, log(hr), logsd_hr)
	
	# Joint P value
	like_1 <- sum(dpois(dat[2,], incid*dat[1,], log<-T))
	joint_incid <- sum(dat[2,]) / sum(dat[1,])
	like_0 <- sum(dpois(dat[2,], joint_incid*dat[1,], log<-T))
	chi2 <- -2*(like_0 - like_1)
	joint_P <- 1 - pchisq(chi2, ncol(dat)-1)

	# Output
	output <- rbind(incid, incid_lo, incid_hi, hr, hr_lo, hr_hi, P_hr)
	right <- c("joint P", joint_P, rep(NA, nrow(out)-2))
	output <- cbind(output, right)
	CSV.write(get.output(cf, 'hazards'), as.matrix(output), first.cell="rowName")
    
}

main(execute)
