#!/usr/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/functions.sh ]] && . $ANDURIL_HOME/bash/functions.sh $1
[[ -e $ANDURIL_HOME/lang/bash/functions.sh ]] && . $ANDURIL_HOME/lang/bash/functions.sh $1
export_command
export PATH=$PATH:$( readlink -f ../../lib/blast/bin )
querySeq="$input_query"
dbSeq="$input_dbSeq"
output="$output_result"
seqType="$parameter_sequenceType"
dbName="$parameter_dbName"
program="$parameter_program"
evalue="$parameter_evalue"
nprocessor="$parameter_nProcessor"
wordSize="$parameter_wordSize"
metaData=$( gettempdir )

iscmd formatdb || exit 1
[[ $program = "megablast" ]] && {
    iscmd megablast || exit 1
} || {
    iscmd blastall || exit 1
}

echo "Formating database sequences..."
cd "$metaData"
formatdb -i "$dbSeq" -p "$seqType" -n "$dbName"
echo "Sequence alignment...Please wait..."
[[ $program = "megablast" ]] && {
    megablast -i "$querySeq" -d "$dbName" -m 8 -W "$wordSize" -a "$inprocessor" -e "$evalue" -o "$metaData"/result.tmp
} || {
    blastall -p "$program" -i "$querySeq" -d "$dbName" -m 8 -W "$wordSize" -a "$nprocessor" -e "$evalue" -o "$metaData"/result.tmp
}
tsvecho Query_id Subject_id Identity Alignment_length Mismatches Gap_openings q_start q_end s_start s_end Evalue Bit_score > "$output"
cat result.tmp >> "$output"
