#!/bin/bash

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
export PATH=$PATH:$( readlink -f ../../lib/STAR/source/ )
export_command

set -ex

mkdir -p "${output_genome}"
cd "${output_genome}"

SPLICE_JUNCTIONS=""
if [[ "${input_spliceJunctions}" != "" ]]; then
    # Genome generation for second pss, adapted from https://code.google.com/p/rna-star/issues/detail?id=7
    # Sanity check on junctions should be done before.
    tail -n +2 ${input_spliceJunctions} | awk 'BEGIN {OFS="\t"; strChar[0]="."; strChar[1]="+"; strChar[2]="-";} {if($5>0){print $1,$2,$3,strChar[$4]}}' > SJ.out.tab.Pass1.sjdb
    SPLICE_JUNCTIONS="--sjdbFileChrStartEnd SJ.out.tab.Pass1.sjdb"
fi

ANNOTATION=""
if [[ "${input_annotation}" != "" ]]; then
    ANNOTATION="--sjdbGTFfile ${input_annotation}"
fi

# generate genome with junctions from the 1st pass
STAR --genomeDir ./ --runThreadN ${parameter_threads} --runMode genomeGenerate --genomeFastaFiles ${input_genomeFasta} ${SPLICE_JUNCTIONS} ${ANNOTATION} ${parameter_genomeParameters}

# Leave log files with the genome
rm -f SJ.out.tab.Pass1.sjdb

