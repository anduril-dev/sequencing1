# Extract a subset of interest from an expression matrix.
library(componentSkeleton)

execute <- function(cf) {

    # Inputs
    ref <- CSV.read(get.input(cf, "reference"))    
    expr <- CSV.read(get.input(cf, "exprMatrix"))
    subCol <- get.parameter(cf,"subsetColumn", "string")

    # Give expr rownames based on first column for easier reference
    rownames(expr) <- expr[,1]

    # If subCol is empty, take the first column
    if (subCol == "") {
        subCol <- colnames(ref)[1] 
    }

    # Verify subCol exists
    if (length(grep(paste("^",subCol,"$",sep=""),colnames(ref))) != 1) {   
        write.error(paste("ERROR with parameter subCol. ", subCol, " is not a unique column name of input reference.",sep=""))
        return(1)
    }

    # Retrieve subset
    overlap <- intersect(rownames(expr),ref[,subCol])
    subset <- expr[overlap,]

    # Write output
    CSV.write(get.output(cf,"subset"), subset) 
    return(0)
}

main(execute)
