import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fi.helsinki.ltdk.csbl.NGSVariation.Summarization;
import fi.helsinki.ltdk.csbl.NGSVariation.Summarization.LocationType;
import fi.helsinki.ltdk.csbl.NGSVariation.Summarization.SummarizationMethod;
import fi.helsinki.ltdk.csbl.NGSVariation.Summarization.SummarizationType;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.asser.io.CSVParser;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;


public class Summarize extends SkeletonComponent {


	
	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {
		
		File data 		= cf.getInput("data");
		String keys[] 	= cf.getParameter("keyCols").split(",");
		String values[] = cf.getParameter("valueCols").split(",");
		String method 	= cf.getParameter("method");
		String [] resCols = cf.getParameter("resultCol").split(",");
		
		int window 		= cf.getIntParameter("window");
		int windowStep 	= cf.getIntParameter("windowStep");
		int windowStart = cf.getIntParameter("windowStart");
		
		String locationCol = cf.getParameter("locationCol");
		
		if(!locationCol.equals("")){
			keys = new String[]{locationCol};
		}else if(cf.getParameter("keyCols").equals(""))
			keys = new String[0];
		
		CSVParser in = new CSVParser(data);
		
		List<List<Object>> vals = new ArrayList<List<Object>>();
		List<String> cols = new ArrayList<String>();
		
		while(in.hasNext()){
			
			String [] line = in.next();
			List<Object> tmp = new ArrayList<Object>();
			
			for(String key : keys){
				if(window > 0)
					tmp.add(Integer.parseInt(line[in.getColumnIndex(key)]));
				else
					tmp.add(line[in.getColumnIndex(key)]);
				
				cols.add(in.getColumnNames()[in.getColumnIndex(key)]);
			}
			for(String value : values){
				if(line[in.getColumnIndex(value)] != null){
					tmp.add(Double.parseDouble(line[in.getColumnIndex(value)]));
				}else{
					tmp.add(null);
				}	
				cols.add(in.getColumnNames()[in.getColumnIndex(value)]);
			}
			
			vals.add(tmp);
			
		}
		
		int[] valIndex = new int[values.length];
		
		for(int i = 0; i < values.length; i++){
			valIndex[i] = cols.indexOf(values[i]);
		}
		int[] keyIndex = new int[keys.length];
		for(int i = 0; i < keys.length; i++){
			keyIndex[i] = cols.indexOf(keys[i]);
		}
		
		if(window > 0){
			
			Summarization sum;
			
			if(!locationCol.equals("")){
				sum = new Summarization(SummarizationMethod.valueOf(method), SummarizationType.WINDOW, LocationType.LOCATION);
				
			}else
				sum = new Summarization(SummarizationMethod.valueOf(method), SummarizationType.WINDOW, LocationType.INDEX);

			sum.readDataset(vals, keyIndex, valIndex);
			
			sum.setWindow(window, windowStep);
			
			String [] heading = new String[1 + values.length];
			
			int i = 1;
			heading[0] = "Location";

			if(resCols[0].equals("")){
				for(String v : values){
					heading[i] = v;
					i ++;
				}
			}else{
				for(String v : resCols){
					heading[i] = v;
					i ++;
				}
			}
			
			CSVWriter out = new CSVWriter(heading, cf.getOutput("summarization"));
			
			
			Map<Integer, Double[]> result = sum.getSummarization(windowStart);
			
			for(int location : result.keySet()){
				out.write(location);
				
				for(int j = 0; j < result.get(location).length; j++){
					//result is float or integer
					if(cf.getParameter("resultType").equals("float")){
						out.write(result.get(location)[j]);
					}else{
						out.write(((int)result.get(location)[j].doubleValue()));
					}
				}
			}
			out.close();
			
		}else{

			Summarization sum = new Summarization(SummarizationMethod.valueOf(method), SummarizationType.ID, LocationType.ID);	
			
			sum.readDataset(vals, keyIndex, valIndex);
			
			String [] heading = new String[keys.length + values.length];
			
			int i = 0;
			for(String k : keys){
				heading[i] = k;
				i ++;
			}
			
			if(resCols[0].equals("")){
				for(String v : values){
					heading[i] = v;
					i ++;
				}
			}else{
				for(String v : resCols){
					heading[i] = v;
					i ++;
				}
			}
			 
			CSVWriter out = new CSVWriter(heading, cf.getOutput("summarization"));
			
			Map<String[], Double[]> result = sum.getIDSummarization();
			
			for(String[] key : result.keySet()){
				for(int j = 0; j < key.length; j++){
					if(key[j].equals("null"))
						out.write(null);
					else
						out.write(key[j]);
				}

				for(int j = 0; j < result.get(key).length; j++){
					//print  result in float or in integer
					if(cf.getParameter("resultType").equals("float")){
						if(result.get(key)[j] == null)
							out.write(null);
						else
							out.write(result.get(key)[j]);
					}else{
						if(result.get(key)[j] == null)
							out.write(null);
						else
							out.write(((int)result.get(key)[j].doubleValue()));
					}
				}
			}
			out.close();
		}
		

		
		
		return ErrorCode.OK;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Summarize().run(args);

	}



}
