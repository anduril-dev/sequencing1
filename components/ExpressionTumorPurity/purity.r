library(componentSkeleton)
library(estimate)

execute <- function(cf) {
    # Read input parameters from command file
    write.log(cf, "Reading input and parameters from command file ...")
    expr.path <- get.input(cf,'expr')
    sampleList <- get.parameter(cf, 'samples')
    id.symbol <- get.parameter(cf, 'idSymbol')    
    document.dir    <- get.output(cf,'report')
    out.dir <- dirname(document.dir)
    system(paste("mkdir ",out.dir,"/metaData",sep=""))    
    metaData.dir <- paste(out.dir,"/metaData",sep="")

    # Estimate the tumor purity
    write.log(cf, "Estimate tumor purity ...")
    expr <- LogMatrix.read(expr.path)    
    write.table(expr,paste(metaData.dir,"expr.txt",sep="/"),sep="\t",row.names=TRUE,quote=FALSE)
    samples <- strsplit(sampleList,",")[[1]]

    filterCommonGenes(input.f=paste(metaData.dir,"expr.txt",sep="/"), 
                      output.f=paste(metaData.dir,"genes.gct",sep="/"), 
                      id=id.symbol)

    estimateScore(paste(metaData.dir,"genes.gct",sep="/"), 
                  paste(metaData.dir,"estimate_score.gct",sep="/"))

    plotPurity(scores=paste(metaData.dir,"estimate_score.gct",sep="/"), 
               samples=samples,output.dir=document.dir)

    # report
    write.log(cf, "Write reports ...")
    scores <- CSV.read(paste(metaData.dir,"estimate_score.gct",sep="/"))
    tumor.purity.data <- scores[-1,]
    colnames(tumor.purity.data) <- scores[1,]
    rownames(tumor.purity.data) <- tumor.purity.data[,1]
    
    tex <- character()
    for(sample in colnames(expr)){
       print(sample)
       tex <- c(tex, latex.figure(paste(sample,".png",sep=""), caption=paste("Sample ", sample,". Estimated tumor purity is ",tumor.purity.data["TumorPurity",sample],".", sep=""), image.height=10), '\\clearpage')
    }

    CSV.write(get.output(cf, 'estimate'), tumor.purity.data)
    latex.write.main(cf, 'report', tex)
}
main(execute)
