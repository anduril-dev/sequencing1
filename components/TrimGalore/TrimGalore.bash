# running functions.sh 
set -e
source "$ANDURIL_HOME/bash/functions.sh"
export_command

# inputs
echo reads    				$input_reads
echo mates					$input_mates

# parameters
echo paired 				$parameter_paired
echo stringency				$parameter_stringency
echo minQuality				$parameter_minQuality
echo minLength				$parameter_minLength
echo clipR1					$parameter_clipR1
echo clipR2					$parameter_clipR2
echo extraArgs				$parameter_extraArgs
echo fastqcArgs				$parameter_fastqcArgs
echo qualityScore			$parameter_qualityScore
echo runFastqc				$parameter_runFastqc
echo adapter				$parameter_adapter
echo gzip					$parameter_gzip

mkdir $output_report
temp_folder=$( gettempdir )
mkdir $output_trimmed

### basic command ###
Command=$( readlink -f trim_galore )
fastqcArgs="--fastqc_args \"--outdir ${output_report} --extract ${fastqcArgs}\""
Command="${Command} --stringency ${parameter_stringency} --quality ${parameter_minQuality} --length ${parameter_minLength} --output_dir ${output_report} ${parameter_extraArgs}"

### print version ###
commandVersion="${Command} -v"
eval $commandVersion
cutadaptVersion=$( cutadapt --version )
echo "cutadapt version is ${cutadaptVersion}"

### runFastqc ###
if [ "${parameter_runFastqc}" == "true" ]
then
	Command="${Command} --fastqc ${fastqcArgs}"
fi

### adapter ###
if [ "${parameter_adapter}" != "" ]
then
	Command="${Command} -a '${parameter_adapter}'"
fi
### clipping ###
if [ "$parameter_clipR1" != "0" ]
then
	Command="${Command} --clip_R1 ${parameter_clipR1}"
fi

if [ "$parameter_clipR2" != "0" -a "${parameter_paired}" == "true" ]
then
	Command="${Command} --clip_R2 ${parameter_clipR2}"
fi

if [ "$parameter_qualityScore" == "phred33" ]
then
	Command="${Command} --phred33"
elif [ "$parameter_qualityScore" == "phred64" ]
then
	Command="${Command} --phred64"
else
	writeerror "unknown quality score"
	exit 1
fi

### gzip ###
if [ "$parameter_gzip" == "true" ]
then
	Command="${Command} --gzip"
else
	Command="${Command} --dont_gzip"
fi

### paired ###
if [ "$parameter_paired" == "true" ]
then
	if [ ! -e "${input_mates}" ]
	then
		writeerror "mates input does not exist"
		exit 1
	fi
	Command="${Command} --paired ${input_reads} ${input_mates}"
else
## single-end alignment
	Command="${Command} ${input_reads}"
fi

## running trim_galore
echo "trim_galore command is: "$Command
eval ${Command} 2>> "${logfile}"
if [ $? != 0 ]
then
	writeerror "trim_galore failed"
	exit 1
fi

### organising outputs
if [ "$parameter_paired" == "true" ]
then
	if [ "${parameter_runFastqc}" == "true" ]
	then
		rm "${output_report}"/*.zip
		rm "${output_report}"/*.html
		mv "${output_report}"/*val_1*fastqc "${output_report}"/reads_quality_report
		mv "${output_report}"/*val_2*fastqc "${output_report}"/mates_quality_report
	fi
	mv "${output_report}"/$(basename ${input_reads})_trimming_report.txt "${output_report}"/reads_trimming_report
	mv "${output_report}"/$(basename ${input_mates})_trimming_report.txt "${output_report}"/mates_trimming_report
	if [ "$parameter_gzip" == "true" ]
	then
		mv "${output_report}"/*val_1* "${output_trimmed}"/TrimmedReads.fq.gz
		mv "${output_report}"/*val_2* "${output_trimmed}"/TrimmedMates.fq.gz
		addarray trimmed Reads TrimmedReads.fq.gz
		addarray trimmed Mates TrimmedMates.fq.gz
        if [ -e "${output_report}"/*unpaired_1.fq.gz ]
        then 
            mv "${output_report}"/*unpaired_1.fq.gz "${output_trimmed}"/unpairedReads.fq.gz
            mv "${output_report}"/*unpaired_2.fq.gz "${output_trimmed}"/unpairedMates.fq.gz        
            addarray trimmed Ureads unpairedReads.fq.gz
            addarray trimmed Umates unpairedMates.fq.gz
        fi
	else
		mv "${output_report}"/*val_1* "${output_trimmed}"/TrimmedReads.fq
		mv "${output_report}"/*val_2* "${output_trimmed}"/TrimmedMates.fq
		addarray trimmed Reads TrimmedReads.fq
		addarray trimmed Mates TrimmedMates.fq
        if [ -e "${output_report}"/*unpaired_1.fq ]
        then 
            mv "${output_report}"/*unpaired_1.fq "${output_trimmed}"/unpairedReads.fq
            mv "${output_report}"/*unpaired_2.fq "${output_trimmed}"/unpairedMates.fq
            addarray trimmed Ureads unpairedReads.fq
            addarray trimmed Umates unpairedMates.fq
        fi  
	fi
else
	if [ "${parameter_runFastqc}" == "true" ]
	then
		rm "${output_report}"/*.html
		rm "${output_report}"/*.zip
		mv "${output_report}"/*fastqc "${output_report}"/reads_quality_report
	fi
	mv "${output_report}"/$(basename ${input_reads})_trimming_report.txt "${output_report}"/reads_trimming_report
	if [ "$parameter_gzip" == "true" ]
	then
		mv "${output_report}"/*trimmed* "${output_trimmed}"/TrimmedReads.fq.gz
		addarray trimmed Reads TrimmedReads.fq.gz
	else
		mv "${output_report}"/*trimmed* "${output_trimmed}"/TrimmedReads.fq
		addarray trimmed Reads TrimmedReads.fq
	fi
fi
