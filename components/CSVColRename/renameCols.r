# Rename columns of one table given a list of new names in another table.
library(componentSkeleton)

execute <- function(cf) {

    # Inputs
    Table <- CSV.read(get.input(cf, "csv"))
    ColNames <- CSV.read(get.input(cf, "reference"))
    oldNames <- get.parameter(cf,"oldNames", "string")
    newNames <- get.parameter(cf,"newNames", "string")

    # If no params given
    if (oldNames == "") { # take first column
        oldNames <- colnames(ColNames)[1] }
    if (newNames == "") { # take second column
        newNames <- colnames(ColNames)[2] }

    # Check params
    if (length(grep(oldNames, colnames(ColNames))) < 1) {
        write.error(paste(oldNames, "is not a column in input reference. Possible values are:",colnames(ColNames), sep=" "))
        return(1)
    } else if (length(grep(newNames, colnames(ColNames))) < 1) {
        write.error(paste(newNames, "is not a column in input reference. Possible values are:",colnames(ColNames), sep=" "))
        return(1)
    } else {

        toChange <- ColNames[,oldNames]
        ColNames <- ColNames[which(!is.na(toChange)),] # Remove NAs
        toChange <- toChange[which(!is.na(toChange))]

        # Find matches to old name
        for (n in 1:length(toChange)) {
            found <- grep(paste(toChange[n],"$|",gsub("-","_",toChange[n]),"$",sep=""),colnames(Table))
            if (length(found) == 1) { # One match found
                colnames(Table)[found] <- gsub(paste(toChange[n],gsub("-","_",toChange[n]),sep="|"),ColNames[n,newNames],colnames(Table)[found])
            }
            if (length(found) > 1) { # More than one match found
                found <- grep(paste("^",toChange[n],"$|^",gsub("-","_",toChange[n]),"$",sep=""),colnames(Table))
                if (length(found) > 1) {
                    write.error(paste(toChange[n], "matches multiple Table columns:", colnames(Table)[found],sep=" ", collapse=" "))
                    return(1)
                } else {
                    colnames(Table)[found] <- gsub(paste(toChange[n],gsub("-","_",toChange[n]),sep="|"),ColNames[n, newNames], colnames(Table)[found])
                }
            } 
        }

    }

    # Write output 
   CSV.write(get.output(cf,"csv"), Table) 
    return(0)
}

main(execute)
