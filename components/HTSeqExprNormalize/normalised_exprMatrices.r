library(componentSkeleton)

execute <- function(cf) {

   # Create array output dir.
    results <- get.output(cf, "array")
    dir.create(results)

    # Array object
    results.array.output <- Array.new()

    # Parameters + input
    options <- get.parameter(cf,"options", "string")
    expr <- CSV.read(get.input(cf, "expr"))

    # process options by removing spaces and using uppercase letters
    options <- unlist(strsplit(tolower(gsub("[[:blank:]]","",options)),","))

    # Calculate counts-per-million by default if no parameter given
    if (length(options) == 0) {
        options <- "edger"
    }

    # If required packages not installed, install them (if possible)
    is.installed <- function(pkg) {
                           is.element(pkg, installed.packages()[,1])
                           }

    if (!is.installed("DESeq")  && length(grep("deseq",options)) > 0) {
                        print("Downloading required R package DESeq")
                        update.packages(checkBuilt=TRUE, ask=FALSE, repos="http://bioconductor.statistik.tu-dortmund.de")
                        source("http://bioconductor.org/biocLite.R")
                        biocLite("DESeq")
                        }
    if (!is.installed("edgeR") && length(grep("edger",options)) > 0) {
                        print("Downloading required R package edgeR")
                        update.packages(checkBuilt=TRUE, ask=FALSE, repos="http://bioconductor.statistik.tu-dortmund.de")
                        source("http://bioconductor.org/biocLite.R")
                        biocLite("edgeR")
                        }

    # Move IDs to rownames
    rownames(expr) <- expr[,1]
    expr <- expr[,-1]
    for (option in options) {
		if (option == "deseq") {
			############  DESEQ  #####
			library(DESeq)
			cTable <- expr
			# make a dummy condition parameter
			conditions <- factor(c(1,rep(2,ncol(cTable)-1)))
			dataSet <- newCountDataSet(cTable,conditions)

			# Size factors to bring expression across samples to common scale
			dataSet <- estimateSizeFactors(dataSet)
			normalised <- counts(dataSet,normalized=TRUE)
            pseudoNormal <- normalised

			# Convert 0s to NA
            normalised[normalised == 0] <- NA
            normalised <- log2(normalised)
            idx_name <- paste(option,"Normlzd",sep="")

            # Create expr matrix with pseudocount so that log-transformed value of 0 is 0, not -Inf.
            pseudoNormal <- pseudoNormal+1
            pseudoNormal <- log2(pseudoNormal)
            idx_name2 <- paste(option,"PseudoNormlzd",sep="")

           # Output to array
            CSV.write(paste(results,"/",idx_name,".csv",sep=""), normalised)
            results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))
            CSV.write(paste(results,"/",idx_name2,".csv",sep=""), pseudoNormal)
            results.array.output <- Array.add(results.array.output,idx_name2,paste(idx_name2,".csv",sep=""))

		} else if (option == "edger") {
			############  EDGER  #####		
			library(edgeR)
			cTable <- expr
			normalised <- cpm(cTable)
            pseudoNormal <- normalised

            # Convert 0s to NA
            normalised[normalised == 0] <- NA
            normalised <- log2(normalised)
            idx_name <- paste(option,"Normlzd",sep="")

            # Create expr matrix with pseudocount so that log-transformed value of 0 is 0, not -Inf.
            pseudoNormal <- pseudoNormal+1
            pseudoNormal <- log2(pseudoNormal)
            idx_name2 <- paste(option,"PseudoNormlzd",sep="")

            # Output to array
            CSV.write(paste(results,"/",idx_name,".csv",sep=""), normalised)
            results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))
            CSV.write(paste(results,"/",idx_name2,".csv",sep=""), pseudoNormal)
            results.array.output <- Array.add(results.array.output,idx_name2,paste(idx_name2,".csv",sep=""))
		
		} else if (option == "uqnorm") {
    		############  UQNORM  #####
            cTable <- expr
				
			# Convert 0s to NA
			cTable[cTable == 0] <- NA
				
			# calculate library sizes
			laneSums <- colSums(cTable, na.rm=TRUE)

			# calculate the 75th quantile of each sample
			# MARGIN==2==apply to columns of table, 
			# exclude NAs from calculation
			upper.quartiles <- apply(cTable,2,function(x) quantile(x,0.75, na.rm=TRUE))

			# UQnorm of TPM counts is better at GLOBAL normalization between 
			# libraries than simply dividing by the total sums of each library.
			uq.scaled <- upper.quartiles/sum(upper.quartiles)*sum(laneSums)
			uq.norm <- sweep(cTable,MARGIN=2,uq.scaled,"/")*1000000	# count values divided by normalized total counts

			normalised <-  cbind(rownames(uq.norm),log2(uq.norm))
            colnames(normalised)[1] <- "RowName"
            idx_name <- paste(option,"Normlzd",sep="")

            # Create expr matrix with pseudocount so that log-transformed value of 0 is 0, not -Inf.
            pseudoNormal <- uq.norm
            pseudoNormal[is.na(pseudoNormal)] <- 0
            pseudoNormal <- pseudoNormal+1
            pseudoNormal <- log2(pseudoNormal)
            pseudoNormal <- cbind(rownames(uq.norm),pseudoNormal)
            colnames(pseudoNormal)[1] <- "RowName"
            idx_name2 <- paste(option,"PseudoNormlzd",sep="")

            # Output to array
            CSV.write(paste(results,"/",idx_name,".csv",sep=""), normalised)
            results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))
            CSV.write(paste(results,"/",idx_name2,".csv",sep=""), pseudoNormal)
            results.array.output <- Array.add(results.array.output,idx_name2,paste(idx_name2,".csv",sep=""))

		############  END OF OPTIONS #####
    	} else {
			print(paste("NORMALIZATION OPTION [",option,"] NOT RECOGNIZED.",sep=""))
			break
		}
	}

    Array.write(cf,results.array.output,"array")		
    return(0)
}

main(execute)
