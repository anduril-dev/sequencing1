#!/usr/bin/python

import anduril
from anduril.arrayio import *

import os, errno, sys
import subprocess, threading
import itertools
import shlex

class CommandRunner(threading.Thread):
    """ Runs a subprocess. """

    def __init__(self, args, communicate=True):
        """ args is passed to the subprocess as arguments.
        Disable communicate to avoid buffering stdin and stdout. """
        self.args = args
        self.communicate = communicate
        self.stdout = None
        self.stderr = None
        self.exception = None
        self.returncode = None
        threading.Thread.__init__(self)

    def run(self):
        """ Run command an store standard input and ouput to variables if
        communicate is set. """
        try:
            p = subprocess.Popen(self.args,
                                 shell=False,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            if self.communicate:
                self.stdout, self.stderr = p.communicate()
            self.returncode = p.returncode
        except:
            self.exception = sys.exc_info()

    def join(self):
        """ Join thread and throw possible exception again. """
        threading.Thread.join(self)
        if self.exception:
            msg = "Thread '%s' threw an exception: %s" % (self.getName(), self.exception[1])
            new_exc = Exception(msg)
            raise new_exc.__class__, new_exc, self.exception[2]

    def print_output(self):
        if self.stdout:
            print "STDOUT from thread running command: %s"%(" ".join(self.args))
            print self.stdout
        if self.stderr:
            print "STERR from thread running command: %s"%(" ".join(self.args))
            sys.stderr.write(self.stderr)

    def failed(self):
        return bool(self.exception or self.returncode)

class ThreadPool:
    """ Controls the executing thread amount. """
    def __init__(self, max_threads=1):
        self.threads=[]
        self.to_start=[]
        self.max_threads=max_threads

    def add(self, thread):
        """ Pick free thread from pool if available.
        Otherwise store thread for later running. """
        launch = len(self.threads) < self.max_threads
        if launch:
            thread.start()
            self.threads.append(thread)
        else:
            self.to_start.append(thread)

    def join(self):
        if not self.threads:
            return None

        thread=self.threads.pop()
        thread.join()

        if self.to_start:
            self.threads.append(self.to_start.pop())
            self.threads[-1].start()

        return thread

# Could be in anduril library, cause also used from pythonevaluate.py
def createFile(f,content=""):
    fid=open(f,'w')
    fid.write(content)
    fid.close()

# Could be in anduril library
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def start_grok(options, infile, outfile):
    """ Produce a thread running grok that may be added to the thread pool. """
    thread=CommandRunner(["python", "/usr/bin/grok", "convert",
        "--input-file=%s"%infile,
        "--output-file=%s"%outfile ]
            + options )
    return thread

def method_grok(cf, pool):
    # Split the option string but keep quoted spaces intact
    grok_options=shlex.split(cf.get_parameter("options"))

    # Determine input and output file type flags
    if cf.get_parameter("from"):
        grok_options+=["--input-type="+cf.get_parameter("from")]
    if cf.get_parameter("to"):
        grok_options+=["--output-type="+cf.get_parameter("to")]

    # Process input files

    if cf.get_input('file'):
        pool.add( start_grok(grok_options, cf.get_input('file'), cf.get_output('file')) )

    # For folders, make output filename same as original, except in the output folder
    if cf.get_input("folder"):
        for filename in os.listdir(cf.get_input("folder")):
            outfile = folder[filename]
            infile = os.path.join(cf.get_input("folder"), filename)
            pool.add( start_grok(grok_options, infile, outfile ) )
   
    # For arrays, retain keys and make filename similar to original
    if cf.get_input("array"):
        for key, infile in get_array("array"):
            outfile = os.path.join( array.get_folder(), os.path.basename(infile) )
            array.write(key, outfile)
            pool.add( start_grok(grok_options, infile, outfile ) )

def execute(cf):
    # Inputs and parameters - mainly here for copypaste.
    # FIXME: cf.get_param & friends could be shortened somehow in the anduril module?
    cf.get_input('file')

    cf.get_parameter("from")
    cf.get_parameter("to")
    cf.get_parameter("method")
    cf.get_parameter("id_column")
    cf.get_parameter("threads")
    cf.get_parameter("options")

    # Ensure outputs are present
    createFile(cf.get_output("file"))
    mkdir_p(cf.get_output('folder'))
    array = AndurilOutputArray(cf, 'array')

    # Object holding the subprocess threads
    pool=ThreadPool(cf.get_parameter("threads"))

    # Delegate conversion to the chosen method
    if cf.get_parameter("method")=="GROK":
        method_grok(cf, pool)
    else:
        return anduril.constants.PARAMETER_ERROR
    
    # Print thread output streams on completion.
    # Terminate on first error or if all threads complete.
    while True:
        thread=pool.join()
        if not thread:
            break
        thread.print_output()
        if thread.failed():
            sys.stderr.write("Execution of a conversion thread failed, stopping.\n")
            return anduril.constants.GENERIC_ERROR

    return anduril.constants.OK

# Stand-alone test case. Run with something like:
# PYTHONPATH=/opt/anduril/python python convert_file
if False:
    import time

    print "Testing."
    threads = []

    # Output to stderr
    threads.append( CommandRunner( ["/bin/ls", "Nonexistent file." ] ) )

    # Output to stdout
    threads.append( CommandRunner( ["/bin/echo", "Thread echoing 2." ] ) )
    threads.append( CommandRunner( ["/bin/echo", "Thread echoing 3." ] ) )

    for thread in threads:
        thread.start()
        time.sleep(1)
        print "Started thread."

    for thread in threads:
        thread.join()
        thread.print_output()
        print thread.failed()
else:    
    anduril.main(execute)

