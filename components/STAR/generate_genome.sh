#!/bin/bash

# Outputs an indexed genome that may be input to STAR runs.

# Run from a fresh directory with enough space (unknown how much)!
# The directory will end up containing around 50M of temp files and a Log.out file.

# Before running:

# Edit stuff under the INPUT section below

# Edit the GENOMEDIR variable to point to a target directory with a name
# such as "hg19_Ensemblv70.overhang74" - however that information
# is included in the produced genomeParameters.txt file.

# Target directory needs ~30G
# The index generation consumes a similar amount of memory.

# A successful run looks like this:

#/opt/STAR/STAR
#Aug 01 23:15:56 ..... Started STAR run
#Aug 01 23:15:56 ... Starting to generate Genome files
#Aug 01 23:18:08 ... finished processing splice junctions database ...
#Aug 01 23:19:20 ... starting to sort  Suffix Array. This may take a long time...
#Aug 01 23:19:49 ... sorting Suffix Array chunks and saving them to disk...
#Aug 01 23:46:28 ... loading chunks from disk, packing SA...
#Aug 02 00:04:34 ... writing Suffix Array to disk ...
#Aug 02 00:09:36 ... Finished generating suffix array
#Aug 02 00:09:36 ... starting to generate Suffix Array index...
#Aug 02 00:42:00 ... writing SAindex to disk
#Aug 02 00:42:21 ..... Finished successfully


# Actual script ensues

# Ensure STAR is on the path
set -e
PATH=$PATH:/opt/STAR
which STAR

# INPUT

# Point to space separated paths of FASTA files containing your chromosomes.
FASTAFILES="/mnt/csc-cloud/resources/references_annotations/Ensemblv70/ORIG/Sequence/WholeGenomeFasta/genome.fa"

# Annotation
GTF="--sjdbGTFfile /mnt/csc-cloud/resources/references_annotations/Ensemblv70/ORIG/Annotation/Genes/genes.gtf"

# Parameters
THREADS=24

OVERHANG="--sjdbOverhang 74" # Ideally it should be equal to (MateLength - 1).

# BIAS="--sjdbScore <N>" # (=2 by default) provides extra alignment score for alignments that cross database junctions. If this score is positive, it will bias the alignment toward annotated junctions.
# For GFF3 also specify GFF3="--sjdbGTFtagExonParentTranscript Parent"

ANNOTATION_PARAMETERS="$GTF $OVERHANG" # Leave empty for no annotation

# OUTPUT

# The only output is stored under, but the exact subdirectory name is based on parameters

# Has to be supplied to the alignment runs!

GENOMEDIR="/mnt/csc-cloud/resources/STAR/hg19_Ensemblv70.overhang${OVERHANG}"

STAR --runMode genomeGenerate --genomeDir $GENOMEDIR --genomeFastaFiles $FASTAFILES --runThreadN $THREADS $ANNOTATION_PARAMETERS


