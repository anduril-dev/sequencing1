library(componentSkeleton)

execute <- function(cf) {
 naF        <- get.parameter(cf, 'maxNA', 'float')
 use.counts <- get.parameter(cf, 'counts', 'boolean')
 use.norm   <- get.parameter(cf, 'normalized', 'boolean')
 add1       <- get.parameter(cf, 'add1', 'boolean');


 myArray <- Array.read(cf,"geneCounts")

 if (input.defined(cf, 'sampleNames')) {
   sampleAnnot <- CSV.read(get.input(cf, 'sampleNames'))
   colIn       <- get.parameter(cf, 'colIn');  if (colIn =="") colIn  <- 1
   colOut      <- get.parameter(cf, 'colOut'); if (colOut=="") colOut <- 2
   sOrder      <- match(myArray$Key, sampleAnnot[,colIn])
   sNames      <- sampleAnnot[sOrder, colOut]
 } else {
   sampleAnnot <- NULL
   sNames      <- myArray$Key
 }

 condition <- rep(NA, length(sNames))
 condition[seq(condition)] <- c('a','b') # pseudo conditions
 sampleTable <- data.frame(sampleName = sNames,
                           fileName   = myArray$File,
                           condition  = condition)
 if (!is.null(sampleAnnot)) { # Remove samples without a label
    sampleTable <- sampleTable[!is.na(sOrder),]
 }

 # DESeq
 library(DESeq2)
 ddsHTSeq  <- DESeqDataSetFromHTSeqCount(sampleTable = sampleTable,
                                         directory   = "/",
                                         design      = ~ condition)

 if (use.counts) {
    if (use.norm) {
       ddsHTSeq <- estimateSizeFactors(ddsHTSeq)
    }
    expr.out <- counts(ddsHTSeq, normalized=use.norm)
 } else {
    expr.out <- fpm(ddsHTSeq, robust=use.norm)
 }
 expr.out    <- expr.out[apply(expr.out, 1, function(x){ sum(x==0)<=length(x)*naF }),]
 CSV.write(get.output(cf, 'expr'), expr.out)
 if (add1) {
     expr1.out <- expr.out + 1
     expr1.out <- log2(expr1.out)
     mask <- apply(expr1.out,2,is.nan)
     expr1.out[mask] <- 0
     mask <- apply(expr1.out,2,is.na)
     expr1.out[mask] <- 0
     mask <- apply(expr1.out,2,is.infinite)
     expr1.out[mask] <- 0
 }
 else {
     expr1.out <- log2(expr.out)
 }
 CSV.write(get.output(cf, 'log2'), expr1.out)

 return(0)
}

main(execute)
