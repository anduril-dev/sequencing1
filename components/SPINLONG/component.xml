<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>SPINLONG</name>
    <version>1.0</version>
    <doc>
    Identify user-defined spatio-temporal patterns in ChIP-seq and other
    sequencing data using the SPINLONG method.
    SPINLONG stands for Spatial Pattern Identification using Non-Linear
    OptimizatioN with Global constraints. Its applications include analysis
    of time-series RNA polymerase and histone modification data.
    </doc>
    <author email="kristian.ovaska@helsinki.fi">Kristian Ovaska</author>
    <category>Short-read Sequencing</category>
    <launcher type="java">
        <argument name="class" value="fi.helsinki.ltdk.csbl.javatools.seq.pattern.anduril.SegmenterComponent" />
    </launcher>
    <requires type="jar">commons-primitives-1.0.jar</requires>
    <requires type="jar">commons-math-2.1.jar</requires>
    <requires type="jar">csbl-javatools.jar</requires>
    <requires type="jar">jahmm-0.6.1.jar</requires>
    <requires type="jar">jcommon-1.0.16.jar</requires>
    <requires type="jar">jfreechart-1.0.13.jar</requires>
    <requires type="jar">saxon9he.jar</requires>
    <inputs>
        <input name="bedFiles" type="BinaryFolder">
            <doc>Short reads in BED formats. The files may
            optionally by gzipped.</doc>
        </input>
        <input name="patterns" type="XML">
            <doc>XML file defining the patterns.</doc>
        </input>
        <input name="regions" type="CSV">
            <doc>Genomic regions.</doc>
        </input>
        <input name="chromosomes" type="CSV" optional="true">
            <doc>Metadata for chromosomes. Must contain the columns
            Chromosome and Size, where Size gives the length of the
            chromosome. If not present, the parameter chromosomePreset
            is used.</doc>
        </input>
        <input name="mappability" type="BinaryFile" optional="true">
            <doc>If given, contains genomic regions that are uniquely
            mappable in BED format. This is used to scale short read
            densities.</doc>
        </input>
        <input name="control" type="BinaryFile" optional="true">
            <doc>If given, contains a control track in BED format that
            is subtracted from primary tracks (bedFiles input).</doc>
        </input>
    </inputs>
    <outputs>
        <output name="scores" type="CSV">
            <doc>Segment scores.</doc>
        </output>
        <output name="plots" type="ImageList">
            <doc>Segment plots and optionally optimizer plots. If both
            plotSegments and plotOptimizer are false, this output is
            empty.</doc>
        </output>
        <output name="patternsDump" type="HTML">
            <doc>Patterns formatted as HTML.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="plotSegments" type="boolean" default="true">
            <doc>If true, visualize results of regions whose score is
            over the threshold. If false, omit plotting.</doc>
        </parameter>
        <parameter name="plotOptimizer" type="boolean" default="false">
            <doc>If true, visualize the progression of the optimizer.
            If false, omit optimizer plotting.</doc>
        </parameter>
        <parameter name="scoreThreshold" type="float" default="0.1">
            <doc>Minimum score for including a pattern match in the score and
            plot outputs. Notice that the score distribution depends on the
            scoring method.</doc>
        </parameter>
        <parameter name="chromosomePreset" type="string" default="hs37">
            <doc>If a custom chromosomes input is not given, this parameter
            is used to select a predefined chromosome set. Currently the
            legal values are hs36 (Homo sapiens, genome build 36) and
            hs37 (Homo sapiens, build 37).</doc>
        </parameter>
        <parameter name="chromosomePattern" type="string" default="">
            <doc>Java regular expression that selects the chromosomes to be
            used in analysis. The empty value selects all chromosomes.
            The pattern is matched against the first column of BED files.</doc>
        </parameter>
        <parameter name="fragmentSize" type="int" default="200">
            <doc>Size of sequences DNA fragments. The short reads are elongated so
            their final length matches this number. Setting this to 0 disables
            elongation.</doc>
        </parameter>
        <parameter name="maxDuplicateReads" type="int" default="0">
            <doc>The maximum number of duplicate short reads (same position and strand)
            that are utilized for each position. The rest are filtered out.
            This allows to renive reads that may be technical artefacts. If 0 or
            negative, all repeats are used.</doc>
        </parameter>
        <parameter name="threads" type="int" default="4">
            <doc>Maximum number of threads to use.</doc>
        </parameter>
        <parameter name="yLog" type="boolean" default="false">
            <doc>If true, the Y axis is plotted using logarithmic scale.</doc>
        </parameter>
        <parameter name="minRegionLength" type="int" default="0">
            <doc>Minimum length for input regions that are processed. If 0, all
            regions are processed. This allows to filter out very short genes.</doc>
        </parameter>
        <parameter name="seed" type="int" default="-1">
            <doc>Seed for random number generator. If negative, an automatically
            generated seed is used. Using a pre-defined seed ensures that results
            are deterministic.</doc>
        </parameter>
    </parameters>
</component>
