# Anduril Component to Analyze DNA Methylation data
# R Package MethylKit
# Author Chiara Facciotto

# Load packages
library(componentSkeleton)
library(GenomicRanges)
library(methylKit)

# Functions
getStats <- function(meth, dirStats, sample_id, state, both.strands, tex){
  
  # State for caption
  if (state=="beforeFilterNorm"){
    state_caption <- " before filtering and normalization."
  } else if (state=="afterFilterNorm"){
    state_caption <- " after filtering and normalization."
  }
  
  if (length(meth) == 1){
    
    # Print methylation statistics
    png.open(filename = paste(dirStats, "/", sample_id, "_", state, "_plot_methylation.png", sep=""), width = 1480, height = 780)
    getMethylationStats(meth, plot = T, both.strands = both.strands)
    png.close()
    
    tex_meth <- latex.figure(filename = paste(dirStats, "/", sample_id, "_", state, "_plot_coverage.png", sep=""),
                             caption   = paste("Methylation statistics of sample ", sample_id, state_caption, sep=""),
                             fig.label = sprintf('fig:%s_%s', sample_id, state))
    
    # Print coverage statistics
    png.open(filename = paste(dirStats, "/", sample_id, "_", state, "_plot_coverage.png", sep=""), width = 1480, height = 780)
    getCoverageStats(meth, plot = T, both.strands = both.strands, xlim=c(0,4))
    png.close()
    
    tex_cov <- latex.figure(filename = paste(dirStats, "/", sample_id, "_", state, "_plot_coverage.png", sep=""),
                            caption   = paste("Coverage statistics of sample ", sample_id, state_caption, sep=""),
                            fig.label = sprintf('fig:%s_%s', sample_id, state))
    
    # Produce one latex file with all figures
    tex <- c(tex, tex_meth, tex_cov)
    
  } else {
    for(m in 1:length(meth)){
      
      # Print methylation statistics
      png.open(filename = paste(dirStats, "/", sample_id[m], "_", state, "_plot_methylation.png", sep=""), width = 1480, height = 780)
      getMethylationStats(meth[[m]], plot = T, both.strands = both.strands)
      png.close()
      
      tex_meth <- latex.figure(filename = paste(dirStats, "/", sample_id[m], "_", state, "_plot_coverage.png", sep=""),
                               caption   = paste("Methylation statistics of sample ", sample_id[m], state_caption, sep=""),
                               fig.label = sprintf('fig:%s_%s', sample_id[m], state))
      
      # Print coverage statistics
      png.open(filename = paste(dirStats, "/", sample_id[m], "_", state, "_plot_coverage.png", sep=""), width = 1480, height = 780)
      getCoverageStats(meth[[m]], plot = T, both.strands = both.strands, xlim=c(0,4))
      png.close()
      
      tex_cov <- latex.figure(filename = paste(dirStats, "/", sample_id[m], "_", state, "_plot_coverage.png", sep=""),
                              caption   = paste("Coverage statistics of sample ", sample_id[m], state_caption, sep=""),
                              fig.label = sprintf('fig:%s_%s', sample_id[m], state))
      
      # Produce one latex file with all figures
      tex <- c(tex, tex_meth, tex_cov)  
    }
  }
  
  return(tex)
}

# Main program
execute <- function(cf) {
  
  # Get input files
  write.log(cf, "Reading input files from command file ...")
  
  # Get methylation call files as array
  array.methFiles  <- Array.read(cf, 'methCallArray')
  
  # Produce lists with keys and file locations
  sampleId <- list()
  location <- list()
  keys <- vector()
  
  for(i in 1:Array.size(array.methFiles)) {
    keys[i] <- as.character(Array.getKey(array.methFiles, i))
    sampleId[[keys[i]]] <- Array.getKey(array.methFiles, i)
    location[[keys[i]]] <- Array.getFile(array.methFiles, keys[i])
   
  }
  
  # Get group IDs as a csv file
  groupIDs <- get.input(cf, "groupIDs")
  
  # Read group IDs
  groups_csv <- read.table(groupIDs, header=TRUE)
  
  # Check that the IDs in the array and csv are the same
  g <- as.character(groups_csv$Key)
  
  if (length(g) != length(keys)){
    write.error(cf, 'Invalid key strings! The keys of the array needs to be the same as the "Key" column in the csv file containing the group IDs!')
    return(INVALID_INPUT)
  }else{
    if (sum(g[order(g)] != keys[order(keys)]) != 0){
      write.error(cf, 'Invalid key strings! The keys of the array needs to be the same as the "Key" column in the csv file containing the group IDs!')
      return(INVALID_INPUT)
    }
  }
  
  # Make a list of the group IDS
  groups <- list()
  for(i in 1:length(keys)[1]) {
    groups[[keys[i]]] <- as.numeric(as.character(groups_csv$Treatment[groups_csv$Key == keys[i]]))
  }
  
  # Get input parameters
  write.log(cf, "Reading input parameters from command file ...")
  
  assembly <- get.parameter(cf, "assembly", "string") # default "hg19"
  context <- get.parameter(cf, "context", "string") # default ""
  normalize <- get.parameter(cf, "normalize", "boolean")
  method <- get.parameter(cf, "method", "string") # default "median"
  loThr <- get.parameter(cf, "loThr", "float") # default 10
  hiThr <- get.parameter(cf, "hiThr", "float") # default 0.999 meaning 99.9%
  bedgraph <- get.parameter(cf, "bedgraph", "boolean")
  dataFrame <- get.parameter(cf, "dataFrame", "boolean")
  both.strands <- get.parameter(cf, "bothStrands", "boolean")
  
  # Get output directories and create them
  write.log(cf, "Create output folders ...")
  
  dirStats <- get.output(cf, "statistics")
  dir.create(dirStats)
  dirAnalysis <- get.output(cf, "analysis")
  dir.create(dirAnalysis)
  dirVisualization <- get.output(cf, "visualization")
  dir.create(dirVisualization)
  
  # Get output files
  rawFilteredNormMethyl <- get.output(cf, "rawFilteredNormMethyl")
  
  # Check parameteres
  write.log(cf, "Checking input parameters validity ...")
  
  if ((context != "CpG") && (context != "CHG") && (context != "CHH") && (context != "")) {
    write.error(cf, 'Invalid context string! Context must be either "CpG", "CHG", "CHH" or ""(empty string)!')
    return(INVALID_INPUT)
  }
  
  if ((method != "mean") && (method != "median")) {
    write.error(cf, 'Invalid method string! method must be either "mean" or "median!')
    return(INVALID_INPUT)
  }
  
  if ((loThr != NULL) && (loThr < 0)) {
    write.error(cf, 'Invalid loThr value! loThr must be positive! If loThr < 1 it is considered as a percentage, otherwise it is considered as a read number.')
    return(INVALID_INPUT)
  }
  
  if ((hiThr != NULL) && (hiThr <= 0)) {
    write.error(cf, 'Invalid hiThr value! hiThr must be positive! If hiThr < 1 it is considered as a percentage, otherwise it is considered as a read number.')
    return(INVALID_INPUT)
  }
  
  # Generate a MethylRawList object, containing the methylation profiles of all loaded datasets  
  write.log(cf, "Loading methylation calls into MethylRaw or MethylRawList object...")
  
  methC <- read(location  = location,
                sample.id = sampleId,
                assembly  = assembly,
                treatment = as.numeric(groups),
                pipeline  = list(fraction    = TRUE, # logical value denoting if the column frequency of Cs has a range from [0-1] (T) or [0-100] (F)
                                 chr.col      = 1, # number of the column that has chrosome string
                                 start.col    = 2, # number of the column that has start coordinate of the base/region of the methylation event
                                 end.col      = 2, # number of the column that has end coordinate of the base/region of the methylation event
                                 coverage.col = 4, # umber of the column that has read coverage values
                                 strand.col   = 3, # number of the column that has strand information, the strand information in the file has to be in the form of '+' or '-'
                                 freqC.col    = 5), # number of the column that has the frequency of Cs
                context   = context)
  
  # Initialize file name
  fileName <- "RawMethylation_"
  
  # Coverage and Methylation Statistics before Normalization and Filtering
  write.log(cf, "Computing coverage and methylation statistics before filtering and normalization...")
  tex <- "\\subsection{Coverage and Methylation Statistics}"
  tex <- getStats(methC, dirStats, keys, state="beforeFilterNorm", both.strands, tex)
  
  # Set loCount and loPerc values
  write.log(cf, "Set loCount and loPerc values")
  if (!is.null(loThr)){
    if (loThr > 1){
      loCount <- round(loThr)
      loPerc <- NULL
      write.log(cf, paste("Filtering using lo.count = ", loCount, "...", sep=""))
    } else {
      loCount <- NULL
      loPerc <- loThr*100
      write.log(cf, paste("Filtering using lo.perc = ", loPerc, "...", sep=""))
    }
  } else {
    loCount <- NULL
    loPerc <- NULL
  }
  
  # Set hiCount and hiPerc values
  write.log(cf, "Set hiCount and hiPerc values")
  if (!is.null(hiThr)){
    if (hiThr > 1){
      hiCount <- round(hiThr)
      hiPerc <- NULL
      write.log(cf, paste("Filtering using hi.count =  ", hiCount, "...", sep=""))
    } else {
      hiCount <- NULL
      hiPerc <- hiThr*100
      write.log(cf, paste("Filtering using hi.perc = ", hiPerc, "...", sep=""))
    }
  } else {
    hiCount <- NULL
    hiPerc <- NULL
  }
  
  # Filtering
  if ((!is.null(hiThr)) || (!is.null(loThr))){
    filtered_methC <- filterByCoverage(methC, lo.count = loCount, lo.perc = loPerc, hi.count =  hiCount, hi.perc = hiPerc)
    
    # Update file name
    fileName <- paste(fileName, "Filtered-loThr", loThr, "-hiThr", hiThr, "_", sep="")
  } else {
    write.log(cf, "Skip filtering...")
    filtered_methC <- methC
  }
  
  rm(methC)
  
  # Normalization
  if(normalize == TRUE){
    write.log(cf, paste("Normalization using method = ", method, "...", sep=""))
    filtered_norm_methC <- normalizeCoverage(filtered_methC,method=method)
    
    # Update file name
    fileName <- paste(fileName, "Normalized-", method, "_", sep="")
  } else {
    write.log(cf, "Skip normalization...")
    filtered_norm_methC <- filtered_methC
  }

  rm(filtered_methC)
 
  # Print the raw data for visualization if bedgraph==TRUE
  if (bedgraph==TRUE){
    write.log(cf, "Produce the bedgraph files to visualize the raw methylation after preprocessing...")
    bedgraph(filtered_norm_methC,file.name=paste(dirVisualization, "/", fileName, "numCs.bedgraph", sep=""),col.name="numCs",unmeth=TRUE,log.transform=FALSE,negative=FALSE, add.on=paste(" useScore=1 db=", assembly, sep=""))
    bedgraph(filtered_norm_methC,file.name=paste(dirVisualization, "/", fileName, "coverage.bedgraph", sep=""),col.name="coverage",unmeth=TRUE,log.transform=FALSE,negative=FALSE, add.on=paste(" useScore=1 db=", assembly, sep=""))
    bedgraph(filtered_norm_methC,file.name=paste(dirVisualization, "/", fileName, "percMeth.bedgraph", sep=""),col.name="perc.meth",unmeth=TRUE,log.transform=FALSE,negative=FALSE, add.on=paste(" useScore=1 db=", assembly, sep=""))
  }
  
  # Print the preprocessed data as a data frame if dataFrame==TRUE
  if (dataFrame==TRUE){
    write.log(cf, "Write CSV file containing raw methylation informations (i.e. coverage, numCs and perc.meth) after preprocessing...")
    
    filtered_norm_methC_numCs <- bedgraph(filtered_norm_methC,col.name="numCs",unmeth=TRUE,log.transform=FALSE,negative=FALSE)
    filtered_norm_methC_coverage <- bedgraph(filtered_norm_methC,col.name="coverage",unmeth=TRUE,log.transform=FALSE,negative=FALSE)
    filtered_norm_methC_perc <- bedgraph(filtered_norm_methC,col.name="perc.meth",unmeth=TRUE,log.transform=FALSE,negative=FALSE)
    
    if (length(keys) == 1){
      temp <- filtered_norm_methC_numCs
      temp$coverage <- filtered_norm_methC_coverage[,dim(filtered_norm_methC_coverage)[2]]
      temp$percMeth <- filtered_norm_methC_perc[,dim(filtered_norm_methC_perc)[2]]
      temp$start <- temp$start+1 # Convert to 1-based
      
      CSV.write(paste(dirAnalysis, "/", keys, "_numCs_coverage_percMeth.txt", sep=""), temp, append=FALSE)
      
    } else {
      for (s in 1:length(keys)){
        temp <- filtered_norm_methC_numCs[[keys[s]]]
        temp$coverage <- filtered_norm_methC_coverage[[keys[s]]][,dim(filtered_norm_methC_coverage[[keys[s]]])[2]]
        temp$percMeth <- filtered_norm_methC_perc[[keys[s]]][,dim(filtered_norm_methC_perc[[keys[s]]])[2]]
        temp$start <- temp$start+1 # Convert to 1-based
        
        CSV.write(paste(dirAnalysis, "/", fileName, keys[s], "_numCs_coverage_percMeth.txt", sep=""), temp, append=FALSE) 
      }
    }
    
    rm(filtered_norm_methC_numCs)
    rm(filtered_norm_methC_coverage)
    rm(filtered_norm_methC_perc)
  }
  
  # Coverage and Methylation Statistics after Normalization and Filtering
  if ((!is.null(hiThr)) || (!is.null(loThr)) || (normalize == TRUE)){
    write.log(cf, "Computing coverage and methylation statistics after normalization and filtering...")
    tex <- getStats(filtered_norm_methC, dirStats,  keys, state="afterFilterNorm", both.strands, tex)
  } else {
    write.log(cf, "Both normalization and filtering have been skipped. No changes in the initial methylation values...")
  }
  
  save(filtered_norm_methC, file=rawFilteredNormMethyl)
  
  # Print latex report  
  latex.write.main(cf, "statistics", tex)
  
  return(0)
}

main(execute)

