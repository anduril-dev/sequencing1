<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
  <name>MethFilterNorm</name>
  <version>1.0</version>
  <doc>
    Function loading the methylation calls data into a <code>MethylRaw</code> or <code>MethylRawList</code> object (<code>MethylKit</code> package data types) 
    and performing normalization and filtering of the data.
  </doc>
  <author email="chiara.facciotto@helsinki.fi">Chiara Facciotto</author>
  <category>Analysis</category>
  <launcher type="R">
    <argument name="file" value="MethFilterNorm.R"/>
  </launcher>
  <requires URL="http://www.r-project.org/" type="manual">R</requires>
  <requires type="R-bioconductor" URL="http://bioconductor.org/packages/release/bioc/html/GenomicRanges.html">GenomicRanges</requires>
  <requires type="R-package">data.table</requires>
  <requires name="download">
        <resource type="bash">install.sh</resource>
  </requires>
  <requires>methylKit</requires>
  <inputs>
    <input name="methCallArray" type="CSV" array="true" optional="false">
      <doc>
        An array pointing to CSV files containing the methylation calls information. Each of these files must include the following columns:
        <br/>
        - <code>chr</code>, the chromosome location in the format <code>chr1</code>
        <br/>
        - <code>pos</code>, the position (1-based) of the cytosine inside the chromosome
        <br/>
        - <code>strand</code>, the strand where the cytosine is located
        <br/>
        - <code>coverage</code>, how many reads are aligned at that specific position
        <br/>
        - <code>freqC</code>, the percentage of methylated cytosines aligned at that specific position.
      </doc>
    </input>
    <input name="groupIDs" type="CSV" optional="false">
		<doc>
        CSV file containg 2 columns:
        <br/>
        - <code>Key</code>, the same sample IDs associated to the <code>methCallArray</code> array elements
        <br/>
        - <code>Treatment</code>, a boolean value (0 or 1) denoting which samples belongs to one state (e.g. controls) which samples belongs to another (e.g. test).
        <br/>
      </doc>
    </input>
  </inputs>
  <outputs>
	<output name="rawFilteredNormMethyl" type="BinaryFile">
      <doc>
        RData file containing the methylRawList object to be used as input for DiffMeth component.
      </doc>
    </output>
    <output name="statistics" type="Latex">
      <doc>
        Directory containing plots related to coverage and methylation statistics before and after normalization and filtering.
      </doc>
    </output>
    <output name="analysis" type="CSVList">
      <doc>
        Directory containing CSV files with <code>numCs</code>, <code>coverage</code> and <code>perc.meth</code> information (one file per sample).
      </doc>
    </output>
    <output name="visualization" type="BinaryFolder">
      <doc>
        Directory containing the bedgraph files for the visualization. The positions are 0-based and every bedgraph file contains data for every sample but with different score value 
        (either <code>numCs</code>, <code>coverage</code> or <code>perc.meth</code>).
      </doc>
    </output>
  </outputs>
  <parameters>
     <parameter name="assembly" type="string" default="hg19">
      <doc>
        A string containing the assembly genome used during the alignment.
      </doc>
    </parameter>
    <parameter name="context" type="string" default="">
      <doc>
        A string stating the methylation context. Allowed values are <code>CpG</code>, <code>CHG</code>, <code>CHH</code> or just the empty string <code>""</code> 
        in case the context is not relevant in the analysis.
      </doc>
    </parameter>
    <parameter name="loThr" type="float" default="10">
      <doc>
        An value for filtering the read counts. When the value is between 0 and 1 it indicates the percentage of reads that are aligned, 
        while when it is higher than 1 it indicates the number of reads.
        <br/>
        Bases having lower coverage than this value are discarded.
        <br/>
        If the data don not have to be filtered, both <code>loThr</code> and <code>hiThr</code> must be set as <code>NULL</code>.
      </doc>
    </parameter>
    <parameter name="hiThr" type="float" default="0.999">
      <doc>
        An value for filtering the read counts. When the value is between 0 and 1 it indicates the percentage of reads that are aligned, 
        while when it is higher than 1 it indicates the number of reads.
        <br/>
        Bases having higher coverage than this value are discarded.
        <br/>
        If the data don not have to be filtered, both <code>loThr</code> and <code>hiThr</code> must be set as <code>NULL</code>.
      </doc>
    </parameter>
    <parameter name="normalize" type="boolean" default="true">
      <doc>
        Boolean values stating if the coverage has to be normalized or not.
      </doc>
    </parameter>
    <parameter name="method" type="string" default="median">
      <doc>
        String denoting the method used to calculate the scaling factor during normalization. Allowed values are <code>median</code> or <code>mean</code>.
      </doc>
    </parameter>
    <parameter name="bedgraph" type="boolean" default="false">
      <doc>
        Boolean value indicating if three bedgraph files should be printed in the <code>visualization</code> folder. Each of the three files contains a different parameter as score (respectivelly numCs, coverage and percMeth).
      </doc>
    </parameter>
    <parameter name="dataFrame" type="boolean" default="false">
      <doc>
        Boolean value indicating if CSV files summarizing the methylation information for every sample should be printed in the <code>analysis</code> folder. 
      </doc>
    </parameter>
    <parameter name="bothStrands" type="boolean" default="false">
      <doc>
        Boolean value indicating whether the plots of coverage and methylation statistics should summarize either both strands in the same plot (<code>FALSE</code>) or produce two strand-specific plots (<code>TRUE</code>).
      </doc>
    </parameter>
  </parameters>
</component>
