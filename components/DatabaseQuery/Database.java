package dbQuery;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class Database {

	static final String NAME = "name";
	static final String TABLE_NAME_QUERY = "SELECT " + NAME
			+ " FROM sqlite_master WHERE type = 'table';";

	private File database;
	private Connection conn;
	private List<Table> tables;
	private CommandFile cf;

	public Database(File database, CommandFile cf) {
		this.database = database;
		this.cf = cf;
		this.tables = new ArrayList<Table>();
		this.conn = null;
	}

	public Database(CommandFile cf) {
		this(cf.getInput(Port.DATABASE), cf);
	}

	/**
	 * Connects to the database.
	 */
	public void connect() {
		String dbURL;

		try {
			Class.forName("org.sqlite.JDBC");
		} catch (Exception e) {
			this.cf.writeError("Failed to load SQLite JDBC driver: " + e);
		}

		dbURL = "jdbc:sqlite:" + this.database.toString();

		try {
			this.conn = DriverManager.getConnection(dbURL);
		} catch (SQLException e) {
			cf.writeError("Failed to start SQL server: " + e);
		}
	}

	/**
	 * Gets all the tables and their respective columns from the connected
	 * database. Adds them to the database object and checks if all specified
	 * columns for the query are present in the database.
	 * 
	 * @return true if all columns requested for the query are present in the
	 *         database
	 */
	public boolean fetchTables() {
		Statement stmt;
		ResultSet rs;
		boolean ok = true;
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(TABLE_NAME_QUERY);
			while (rs.next() && ok) {
				ok = addTable(rs.getString(NAME));
			}
		} catch (SQLException e) {
			cf.writeError(e);
			return false;
		}
		return ok;
	}

	/**
	 * Adds tables from the given database to the database object. Checks if all
	 * requested columns are present in the database, if tables are patient or
	 * treatment.
	 * 
	 * @param name
	 *            Name of the table
	 * @return true if all requested query-columns are present in the database
	 *         tables
	 */
	private boolean addTable(String name) {
		Table table;
		try {
			table = new Table(cf, conn, name);
			this.tables.add(table);
		} catch (IllegalInputException e) {
			cf.writeError(e);
			return false;
		}
		return true;
	}

	/**
	 * Writes the structure of the database as a CSV. Headers are the table
	 * names and rows their respective columns.
	 * 
	 * @return
	 */
	public boolean getStructure() {
		String[] array = new String[tables.size()];
		int max = 0;
		for (int i = 0; i < tables.size(); i++) {
			Table t = tables.get(i);
			array[i] = t.toString();
			int size = t.getAllColumns().size();
			max = size > max ? size : max;
		}

		String[][] tablesAndColumns = new String[tables.size()][max];

		for (int i = 0; i < tablesAndColumns.length; i++) {
			List<Column> allColumns = tables.get(i).getAllColumns();
			for (int j = 0; j < allColumns.size(); j++) {
				tablesAndColumns[i][j] = allColumns.get(j).toString();
			}
		}

		CSVWriter writer = null;
		try {
			writer = new CSVWriter(array, cf.getOutput(Port.OUT));
		} catch (IOException e) {
			cf.writeError(e);
		}

		for (int j = 0; j < max; j++) {
			for (int i = 0; i < tablesAndColumns.length; i++) {
				String col = tablesAndColumns[i][j];
				if (col != null) {
					writer.write(col);
				} else {
					writer.skip();
				}
			}
		}

		writer.close();

		return true;
	}

	/**
	 * @return the Connection object of the database
	 */
	public Connection getConnection() {
		return this.conn;
	}

	/**
	 * 
	 * @return a list of the tables of the database.
	 */
	public List<Table> getTables() {
		return tables;
	}

}
