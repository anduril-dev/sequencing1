package dbQuery;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.asser.io.CSVWriter;

public class Query {

	static final String ADDITIONAL = "additional";

	private Map<Table, List<Column>> allColumns;
	private LinkedHashSet<Table> joinTables;
	private CommandFile cf;
	private Connection conn;
	private Set<Column> helpSet;
	private List<String> queryTableDotColumn;

	public Query(CommandFile cf, Database database) {
		this.cf = cf;
		this.conn = database.getConnection();
		allColumns = new HashMap<Table, List<Column>>();
		for (Table t : database.getTables()) {
			if (cf.getBooleanParameter(Port.FILTER_TUMOR)) {
				if (t.getName().equals(Table.VARIANT)
						|| t.getName().equals(Table.ALT)) {
					allColumns.put(t, t.getAllColumns());
				}
			} else if (!cf.getBooleanParameter(t.getOmitParameterName())) {
				allColumns.put(t, t.getAllColumns());
			}
		}
		joinTables = new LinkedHashSet<Table>();
		queryTableDotColumn = new ArrayList<String>();
		helpSet = new HashSet<Column>();
	}

	/**
	 * Checks if all requested columns are in the given database. If so,
	 * modifies requested columns and adds them to the query-column-list for
	 * creating the SQl-SELECT-statement.
	 * 
	 * @return true if all columns are feasible
	 * @throws IllegalInputException
	 *             if both, input-port and parameter are specified
	 */
	public boolean preprocessingQuery() throws IllegalInputException {
		String paramInput = cf.getParameter(Port.REQUESTED_COLUMNS);

		if (cf.inputDefined(Port.REQUESTED_COLUMNS) && paramInput != null
				&& !paramInput.isEmpty()) {
			throw new IllegalInputException(
					"Specify either input-port or parameter.");
		}

		if (cf.inputDefined(Port.REQUESTED_COLUMNS)) {
			return getPortInput(cf);
		} else if (paramInput != null && !paramInput.isEmpty()) {
			return getParamInput(paramInput);
		} else {
			for (Table t : allColumns.keySet()) {
				if (!cf.getBooleanParameter(t.getOmitParameterName())) {
					for (Column c : allColumns.get(t)) {
						if (!(cf.getBooleanParameter(Port.FILTER_TUMOR) && c
								.getName().equals(Column.TISSUE))) {
							if (t.getPrimaryKeyColumns().contains(c)) {
								if (helpSet.add(c)) {
									queryTableDotColumn.add(t.toString()
											+ ".\"" + c.toString() + "\"");
								}
							} else if (t.getAllColumns().contains(c)) {
								queryTableDotColumn.add(t.toString() + ".\""
										+ c.toString() + "\"");
							}
						}
						joinTables.add(t);
					}
				}
			}
			return true;
		}
	}

	/**
	 * Fetches requested columns from the input-port and validates the request.
	 * 
	 * @param cf
	 * @return
	 */
	private boolean getPortInput(CommandFile cf) {
		Parser parser = null;
		File input = cf.getInput(Port.REQUESTED_COLUMNS);

		if (input.length() > 0) {
			try {
				parser = new Parser(input);
				for (String request : parser.next(parser.getColumnNames())) {
					validateInput(request);
				}
			} catch (IllegalInputException e) {
				cf.writeError(e);
				return false;
			} catch (IOException e) {
				cf.writeError("Failed to read " + Port.REQUESTED_COLUMNS
						+ " input: " + e);
				return false;
			}
		}
		return true;
	}

	/**
	 * Fetches requested columns from the input-parameter and validates the
	 * request.
	 * 
	 * @param columnRequest
	 * @return
	 */
	private boolean getParamInput(String columnRequest) {
		String[] input = columnRequest.split(", ?");
		try {
			for (int i = 0; i < input.length; i++) {
				validateInput(input[i]);
			}
		} catch (IllegalInputException e) {
			cf.writeError(e);
			return false;
		}

		return true;
	}

	/**
	 * Validates if all requested query-columns can be found in the database.
	 * 
	 * @param input
	 * @throws IllegalInputException
	 */
	private void validateInput(String input) throws IllegalInputException {
		String[] mathOperation;
		String tableDotColumn;
		Pattern pattern = Pattern
				.compile("(?i)(COUNT|MAX|MIN|SUM|TOTAL|GROUP_CONCAT)\\((.+)\\) ?.*");
		Matcher matcher = pattern.matcher(input);
		if (matcher.matches()) {
			String matched = matcher.group(2);
			if (!matched.equals("*")) {
				tableDotColumn = validateExpression(matched, true);
				input = input.replace(matched, tableDotColumn);
			}
			queryTableDotColumn.add(input);
		} else {
			mathOperation = input.split(" ?[\\+\\*\\-/%\\(\\)] ?");
			if (mathOperation.length > 1) {
				Set<String> singleMathExpSet = new LinkedHashSet<String>();
				for (int i = 0; i < mathOperation.length; i++) {
					String mathOpString = mathOperation[i];
					if (!mathOpString.isEmpty()) {
						singleMathExpSet.add(mathOpString);
					}
				}
				for (String s : singleMathExpSet) {
					tableDotColumn = validateExpression(s, true);
					input = input.replace(s, tableDotColumn);

				}
				queryTableDotColumn.add(input);
			} else {
				tableDotColumn = validateExpression(input, false);
				if (tableDotColumn != null) {
					input = tableDotColumn;
					queryTableDotColumn.add(input);
				}
			}
		}
	}

	/**
	 * Validates if requested query-columns can be found in the database.
	 * 
	 * @param input
	 * @param mathExp
	 * @return
	 * @throws IllegalInputException
	 */
	private String validateExpression(String input, boolean mathExp)
			throws IllegalInputException {

		String[] splitCol = null;
		Column c = null;
		if (!input.startsWith("\"")) {
			splitCol = input.split("\\.", 2);
		}

		if (splitCol != null && splitCol.length == 2) {
			c = new Column(splitCol[1].replace("\"", ""));
			return validateColumn(c, splitCol[0], mathExp);
		} else {
			c = new Column(input.replace("\"", ""));
			return validateColumn(c, null, mathExp);
		}
	}

	/**
	 * Validates if the given column can be found in the table of which the name
	 * is given.
	 * 
	 * @param c
	 * @param table
	 * @param mathExp
	 * @return
	 * @throws IllegalInputException
	 */
	private String validateColumn(Column c, String table, boolean mathExp)
			throws IllegalInputException {
		String tableDotColumn;
		if (cf.getBooleanParameter(Port.FILTER_TUMOR)
				&& c.getName().equals(Column.TISSUE)) {
			return null;
		}
		for (Table t : allColumns.keySet()) {
			if ((table != null && t.getName().equalsIgnoreCase(table))
					|| table == null) {
				if (!mathExp && t.getPrimaryKeyColumns().contains(c)) {
					if (helpSet.add(c)) {
						tableDotColumn = t.toString() + ".\"" + c.toString()
								+ "\"";

						joinTables.add(t);
						return tableDotColumn;
					}
					return null;
				} else if (allColumns.get(t).contains(c)) {
					tableDotColumn = t.toString() + ".\"" + c.toString() + "\"";
					joinTables.add(t);
					return tableDotColumn;
				}

			}
		}
		if (table != null) {
			throw new IllegalInputException("Requested column " + c.getName()
					+ " is not located in specified table" + table + ".");
		} else {
			throw new IllegalInputException("Column " + c.getName()
					+ " can not be found in provided database.");
		}

	}

	/**
	 * Builds and executes the SQL-SELECT-statement.
	 * 
	 * @return true if query could be executed successfully
	 * @throws IllegalInputException
	 *             if parameter patientID is defined, but the column patient_id
	 *             does not exist
	 */
	public boolean runSelect() throws IllegalInputException {
		StringBuffer sql = new StringBuffer();
		String s;
		String prefix = "";
		String where = " WHERE ";

		try {
			s = addSelectFrom(prefix);
		} catch (IllegalJoinException e) {
			cf.writeError(e);
			return false;
		}
		sql.append(s);

		String selectStmtForFiltering = sql.toString();

		prefix = "";
		s = addConstraint(prefix, where);
		if (!s.isEmpty()) {
			sql.append(s);
			where = "";
			prefix = " AND ";
		}

		if (cf.getBooleanParameter(Port.FILTER_TUMOR)) {
			sql.append(where + prefix + Table.VARIANT + "." + Column.TISSUE
					+ "='tumor'");
		} else {
			s = addPatientIDs(prefix, where);
			if (!s.isEmpty()) {
				sql.append(s);
				where = "";
				prefix = " AND ";
			}
		}

		String additional = cf.getParameter(ADDITIONAL);
		if (additional != null && !additional.isEmpty()) {
			sql.append(" " + additional);
		}

		if (cf.getBooleanParameter(Port.FILTER_TUMOR)) {
			String filterConstraints;
			sql.append(" EXCEPT " + selectStmtForFiltering);
			sql.append(" WHERE " + Table.VARIANT + "." + Column.TISSUE
					+ "='normal'");
			filterConstraints = cf.getParameter(Port.FILTER_CONSTRAINTS);
			String[] filterConstraintsSplit = filterConstraints.split(",");
			prefix = " AND ";
			for (int i = 0; i < filterConstraintsSplit.length; i++) {
				sql.append(prefix + filterConstraintsSplit[i]);
			}
		}

		sql.append(";");
		return executeStmt(sql.toString());
	}

	/**
	 * Adds the first part of the SELECT-statement.
	 * 
	 * @param prefix
	 * @return
	 * @throws IllegalJoinException
	 */
	private String addSelectFrom(String prefix) throws IllegalJoinException {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT ");
		for (int i = 0; i < queryTableDotColumn.size(); i++) {
			sql.append(prefix + queryTableDotColumn.get(i) + " AS \""
					+ queryTableDotColumn.get(i).replace("\"", "") + "\"");
			prefix = ", ";
		}
		sql.append(" FROM ");
		if (joinTables.isEmpty()) {
			for (Table t : allColumns.keySet()) {
				joinTables.add(t);
			}
		}
		if (joinTables.size() > 1) {
			Join join = null;
			join = new Join(joinTables);
			sql.append(join.toString());
		} else {
			for (Table t : joinTables) {
				if (t.getName().equals(Table.VARIANT)
						|| t.getName().equals(Table.ALT)
						|| t.getName().equals(Table.SAMPLE)) {
					Join join = null;
					join = new Join(joinTables);
					sql.append(join.toString());
				} else {
					sql.append(t.toString());
				}
			}
		}
		return sql.toString();
	}

	/**
	 * Adds the WHERE-clause to the SELECT-statement.
	 * 
	 * @param prefix
	 * @param where
	 * @return
	 */
	private String addConstraint(String prefix, String where) {

		String parameter;
		StringBuffer sql = new StringBuffer();
		String pattern_alt = ".*[ (]+alt[ =<>!].*|^alt[ =<>!].*";
		String pattern_chrom = ".*[ (]+chrom[ =<>!].*|^chrom[ =<>!].*";
		String pattern_pos = ".*[ (]+pos[ =<>!].*|^pos[ =<>!].*";
		String pattern_ref = ".*[ (]+ref[ =<>!].*|^ref[ =<>!].*";

		parameter = cf.getParameter(Port.CONSTRAINT);
		if (parameter != null && !parameter.isEmpty()) {
			sql.append(where);
			where = "";
			String[] splitRes = parameter.split(", ?");
			for (int i = 0; i < splitRes.length; i++) {
				if (Pattern.matches(pattern_chrom, splitRes[i])) {
					splitRes[i] = splitRes[i].replace("chrom", "variant.chrom");
				}
				if (Pattern.matches(pattern_pos, splitRes[i])) {
					splitRes[i] = splitRes[i].replace("pos", "variant.pos");
				}
				if (Pattern.matches(pattern_ref, splitRes[i])) {
					splitRes[i] = splitRes[i].replace("ref", "variant.ref");
				}
				if (Pattern.matches(pattern_alt, splitRes[i])) {
					splitRes[i] = splitRes[i].replace("alt", "alt.alt");
				}
				sql.append(prefix + splitRes[i]);
				prefix = " AND ";
			}
		}
		return sql.toString();
	}

	private List<String> getPatientIDInput() throws IllegalInputException {
		Parser parser = null;
		try {
			parser = new Parser(cf.getInput(Port.PATIENT_ID));
		} catch (IOException e) {
			cf.writeError("Failed to read " + Port.PATIENT_ID + " input: " + e);
			return null;
		}

		return parser.next(parser.getColumnNames());
	}

	private String addPatientIDs(String prefix, String where)
			throws IllegalInputException {
		String patientIDParamInput = cf.getParameter(Port.PATIENT_ID);
		StringBuffer sql = new StringBuffer();

		if (cf.inputDefined(Port.PATIENT_ID) && patientIDParamInput != null
				&& !patientIDParamInput.isEmpty()) {
			throw new IllegalInputException(
					"Specify either input-port or parameter: "
							+ Port.PATIENT_ID);
		} else {

			List<String> patientIDs = null;
			if (cf.inputDefined(Port.PATIENT_ID)) {
				patientIDs = getPatientIDInput();
				if (patientIDs == null) {
					throw new IllegalInputException(
							"patient_id file exists, but no patient_ids are specified.");
				}
			} else if (patientIDParamInput != null
					&& !patientIDParamInput.isEmpty()) {
				patientIDs = getPatientIDInput(patientIDParamInput);

			}

			if (patientIDs != null) {
				List<String> tableNames = new ArrayList<String>();
				for (Table t : joinTables) {
					tableNames.add(t.toString());
				}
				String tableDotPatientID;
				if (tableNames.contains(Table.PATIENT)) {
					tableDotPatientID = Table.PATIENT + "." + Column.PATIENT_ID;
				} else if (tableNames.contains(Table.MEDICAL_TREATMENT)) {
					tableDotPatientID = Table.MEDICAL_TREATMENT + "."
							+ Column.PATIENT_ID;
				} else if (tableNames.contains(Table.SAMPLE)
						|| tableNames.contains(Table.ALT)
						|| tableNames.contains(Table.VARIANT)) {
					tableDotPatientID = Table.SAMPLE + "." + Column.PATIENT_ID;
				} else {
					throw new IllegalInputException(
							"patient_id column does not exist. Can not include given patient_ids.");
				}

				sql.append(where);
				prefix = prefix + " (";
				for (String s : patientIDs) {
					sql.append(prefix + tableDotPatientID + " = '" + s + "'");
					prefix = " OR ";
				}
				if (prefix.equals(" OR ")) {
					sql.append(")");
				}
			}

		}

		return sql.toString();
	}

	private List<String> getPatientIDInput(String paramInput) {
		String[] array = paramInput.split(",");
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < array.length; i++) {
			list.add(array[i]);
		}
		return list;
	}

	/**
	 * Executes SQL-SELECT-statement.
	 * 
	 * @param sql
	 *            The completed SQL-statement
	 * @return true if statement could be executed successfully
	 */
	private boolean executeStmt(String sql) {
		Statement stmt;
		ResultSet resultSet;
		ResultSetMetaData metadata;
		try {
			stmt = conn.createStatement();
			resultSet = stmt.executeQuery(sql);
			metadata = resultSet.getMetaData();
		} catch (SQLException e) {
			String msg = String
					.format("Error selecting columns from database: %s; SQL statement is %s",
							e, sql);
			cf.writeError(msg);
			return false;
		}
		return writeOutput(resultSet, metadata);
	}

	private boolean writeOutput(ResultSet resultSet, ResultSetMetaData metadata) {
		CSVWriter writer = null;
		String[] stringArray = null;
		try {
			stringArray = new String[metadata.getColumnCount()];
			for (int i = 0; i < metadata.getColumnCount(); i++) {
				stringArray[i] = metadata.getColumnName(i + 1);
			}
		} catch (SQLException e) {
			cf.writeError(e);
			return false;
		}
		try {
			writer = new CSVWriter(stringArray, cf.getOutput(Port.OUT));
		} catch (IOException e) {
			cf.writeError(e);
		}
		try {
			while (resultSet.next()) {
				for (int i = 0; i < metadata.getColumnCount(); i++) {
					String value = resultSet.getString(i + 1);
					writer.write(value);
				}
			}
		} catch (SQLException e) {
			String msg = String.format(
					"Error adding row data into result CSV table: %s", e);
			cf.writeError(msg);
			return false;
		} finally {
			writer.close();
		}

		return true;
	}
}