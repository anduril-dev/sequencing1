package dbQuery;

public class Port {

	// port AND parameter names
	static final String REQUESTED_COLUMNS = "requestedColumns";
	static final String PATIENT_ID = "patientID";

	// port names
	static final String DATABASE = "database";
	static final String OUT = "queryResults";

	// parameter names for constraints, requesting columns or omitting tables in
	// the output
	static final String CONSTRAINT = "constraints";
	static final String OMIT_VCF = "omitVCFTables";
	static final String OMIT_PATIENT = "omitPatientTable";
	static final String OMIT_MEDICAL = "omitTreatmentTable";
	static final String SELECT = "additionalSelect";
	static final String FILTER_TUMOR = "filterTumor";
	static final String FILTER_CONSTRAINTS = "filterConstraints";

}
