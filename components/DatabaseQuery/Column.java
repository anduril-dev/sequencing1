package dbQuery;

public class Column {

	static final String PATIENT_ID = "patient_id";
	static final String TISSUE = "tissue";
	static final String DEPTH = "dp";
	static final String QUAL = "qual";
	static final String FILTER = "filter";

	private String name;

	public Column(String name) {
		this.name = name.toLowerCase();
	}

	/**
	 * @return the name of the column
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return the name of the column
	 */
	public String toString() {
		return this.name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Column other = (Column) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
