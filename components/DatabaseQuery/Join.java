package dbQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Join {

	static final String CHROM = "chrom";
	static final String POS = "pos";
	static final String ALT = "alt";
	static final String ALL_ALT = "all_alt";
	static final String TISSUE = "tissue";
	static final String PATIENT_ID = "patient_id";

	static final String VAR_ALT = Table.VARIANT + " INNER JOIN "
			+ Table.ALT + " ON " + Table.VARIANT + "." + CHROM + " = "
			+ Table.ALT + "." + CHROM + " AND " + Table.VARIANT + "." + POS
			+ " = " + Table.ALT + "." + POS + " AND " + Table.VARIANT + "." + ALL_ALT
			+ " = " + Table.ALT + "." + ALL_ALT + " AND " + Table.VARIANT + "." + TISSUE
			+ " = " + Table.ALT + "." + TISSUE;
	static final String VAR_ALT_SAMP = VAR_ALT + " INNER JOIN " + Table.SAMPLE
			+ " ON " + Table.ALT + "." + CHROM + " = " + Table.SAMPLE + "."
			+ CHROM + " AND " + Table.ALT + "." + POS + " = " + Table.SAMPLE
			+ "." + POS + " AND " + Table.ALT + "." + ALT + " = "
			+ Table.SAMPLE + "." + Table.ALT + " AND " + Table.ALT + "." + TISSUE
			+ " = " + Table.SAMPLE + "." + TISSUE;
	static final String VAR_ALT_SAMP_PAT = VAR_ALT_SAMP + " INNER JOIN "
			+ Table.PATIENT + " ON " + Table.SAMPLE + "." + PATIENT_ID + " = "
			+ Table.PATIENT + "." + PATIENT_ID;
	static final String VAR_ALT_SAMP_MED = VAR_ALT_SAMP + " INNER JOIN "
			+ Table.MEDICAL_TREATMENT + " ON " + Table.SAMPLE + "."
			+ PATIENT_ID + " = " + Table.MEDICAL_TREATMENT + "." + PATIENT_ID;
	static final String VAR_ALT_SAMP_PAT_MED = VAR_ALT_SAMP_PAT
			+ " INNER JOIN " + Table.MEDICAL_TREATMENT + " ON " + Table.PATIENT
			+ "." + PATIENT_ID + " = " + Table.MEDICAL_TREATMENT + "."
			+ PATIENT_ID;
	static final String PAT_MED = Table.PATIENT + " INNER JOIN "
			+ Table.MEDICAL_TREATMENT + " ON " + Table.PATIENT + "."
			+ PATIENT_ID + " = " + Table.MEDICAL_TREATMENT + "." + PATIENT_ID;

	private String onClause;

	public Join(Set<Table> tables) throws IllegalJoinException {
		List<String> strings = new ArrayList<String>();
		for (Table t : tables) {
		    strings.add(t.toString());
		}
		if (strings.contains(Table.VARIANT)
				|| strings.contains(Table.ALT)
				|| strings.contains(Table.SAMPLE)) {
			onClause = VAR_ALT_SAMP;
			if (strings.contains(Table.PATIENT)) {
				onClause = VAR_ALT_SAMP_PAT;
				if (strings.contains(Table.MEDICAL_TREATMENT)) {
					onClause = VAR_ALT_SAMP_PAT_MED;
				}
			} else if (strings.contains(Table.MEDICAL_TREATMENT)) {
				onClause = VAR_ALT_SAMP_MED;
			}
		} else if (strings.contains(Table.PATIENT)
				&& strings.contains(Table.MEDICAL_TREATMENT)
				&& strings.size() == 2) {
			onClause = PAT_MED;
		} else {
			throw new IllegalJoinException("Tables: " + strings.toString());
		}
	}

	/**
	 * @return the statement that is used as Join-clause in the SQL-statement
	 */
	public String toString() {
		return this.onClause;
	}
}
