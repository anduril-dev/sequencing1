package dbQuery;

public class IllegalJoinException extends Exception{
	
	IllegalJoinException() {
		super("Choice of tables is not reasonably joinable.");
	}
	
	IllegalJoinException(String s) {
		super("Choice of tables is not reasonably joinable. " + s);
	}

}
