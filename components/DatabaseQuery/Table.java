package dbQuery;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;

public class Table {

	// SQL-statement pieces for query to get all column names of the table
	static final String COLUMN_NAME_QUERY = "PRAGMA table_info(%s)";
	static final String CNQ_NAME = "name";
	static final String CNQ_PK = "pk";

	static final String VARIANT = "variant";
	static final String ALT = "alt";
	static final String SAMPLE = "sample";
	static final String PATIENT = "patient";
	static final String MEDICAL_TREATMENT = "treatment";

	static final Hashtable<String, String> OMIT_ASSIGNMENT = new Hashtable<String, String>();
	static {
		OMIT_ASSIGNMENT.put(Table.VARIANT, Port.OMIT_VCF);
		OMIT_ASSIGNMENT.put(Table.ALT, Port.OMIT_VCF);
		OMIT_ASSIGNMENT.put(Table.SAMPLE, Port.OMIT_VCF);
		OMIT_ASSIGNMENT.put(Table.PATIENT, Port.OMIT_PATIENT);
		OMIT_ASSIGNMENT.put(Table.MEDICAL_TREATMENT, Port.OMIT_MEDICAL);
	}

	private CommandFile cf;
	private String name;
	private List<Column> allColumns;
	private List<Column> primaryKeyColumns;
	private String omit_parameter;

	public Table(CommandFile cf, Connection conn, String name)
			throws IllegalInputException {
		this.cf = cf;
		this.name = name;
		this.omit_parameter = OMIT_ASSIGNMENT.get(name);
		this.allColumns = new ArrayList<Column>();
		this.primaryKeyColumns = new ArrayList<Column>();
		fetchColumns(conn);
	}

	/**
	 * Fetches all columns from the table by executing a PRAGMA-SQL-statement.
	 * 
	 * @param conn
	 *            Connection to the database
	 */
	private void fetchColumns(Connection conn) {
		Statement stmt;
		ResultSet rs;
		String result;
		try {
			stmt = conn.createStatement();
			String sql = String.format(COLUMN_NAME_QUERY, name);
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				result = rs.getString(CNQ_NAME);
				Column col = new Column(result);
				allColumns.add(col);
				if (rs.getInt(CNQ_PK) == 1) {
					primaryKeyColumns.add(col);
				}
			}
		} catch (SQLException e) {
			cf.writeError(e);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Table other = (Table) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * @return the name of the table
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the name of the table
	 */
	public String toString() {
		return name;
	}

	/**
	 * @return a list of all columns that are primary keys
	 */
	public List<Column> getPrimaryKeyColumns() {
		return primaryKeyColumns;
	}

	/**
	 * @return all columns of this table
	 */
	public List<Column> getAllColumns() {
		return allColumns;
	}

	/**
	 * @return name of parameter for user-requested columns
	 */
	public String getOmitParameterName() {
		return omit_parameter;
	}

}
