package dbQuery;

import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;

public class Main extends SkeletonComponent {

	static final String STRUCTURE = "databaseStructure";

	@Override
	protected ErrorCode runImpl(CommandFile cf) throws Exception {

		boolean ok;

		// creating new database object
		Database database = new Database(cf);

		// connecting to database
		database.connect();

		if (database.getConnection() == null) {
			return ErrorCode.ERROR;
		}

		// adding tables including columns to database
		ok = database.fetchTables();
		if (!ok) {
			return ErrorCode.ERROR;
		}

		if (cf.getBooleanParameter(STRUCTURE)) {
			ok = database.getStructure();
		} else {
			// constructing select-query, checking if all inputs are correct
			Query query = new Query(cf, database);
			ok = query.preprocessingQuery();
			try {
				if (!ok) {
					throw new IllegalInputException();
				}
			} catch (IllegalInputException e) {
				cf.writeError(e);
				return ErrorCode.ERROR;
			}

			// executing the SQL-query, composing the CSV-output
			try {
				ok = query.runSelect();
			} catch (IllegalInputException e) {
				cf.writeError(e);
				return ErrorCode.ERROR;
			}
		}

		if (!ok) {
			return ErrorCode.ERROR;
		}

		return ErrorCode.OK;
	}

	public static void main(String[] args) {
		new Main().run(args);
	}

}
