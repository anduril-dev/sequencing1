#!usr/bin/perl

use strict;
use componentSkeleton;
use Encode::Unicode;
use SeedMatchLib;

sub execute {
    my ($cf_ref) = @_;

    # Read mature sequences
    my %hash_mature_seq;
    open(FILEIN,get_input($cf_ref,"matureSeq"));
    my $i=0;
    while(<FILEIN>){
	if($i==0){
		$i++;
		next;
	}
	chomp($_);
	my @tmp = split(/\t/,$_);
        $tmp[0] =~ s/"//g;
	$tmp[1] =~ s/"//g;
	$hash_mature_seq{$tmp[0]} = $tmp[1];
	$i++;
    }
    close(FILEIN);

    # Read pairs
    my %hash_pair;
    open(FILEPAIR,get_input($cf_ref,"pairs"));
    $i=0;
    while(<FILEPAIR>){
	if($i==0){
		$i++;
		next;
	}
	chomp($_);
	$_=~s/"//g;
	my @tmp = split(/\t/,$_);
	if(exists $hash_pair{$tmp[1]}){
		push(@{$hash_pair{$tmp[1]}},$tmp[0]);
	}else{
		@{$hash_pair{$tmp[1]}} = ($tmp[0]);
	}
    }
    close(FILEPAIR);

    # Seed match
    open(FILESEED,get_input($cf_ref,"utrSeeds"));
    $i=0;
    my $str="SeedID\tTranscriptID\tmiRNA\tSiteType\n";
    while(<FILESEED>){
	if($i==0){
		$i++;
		next;
	}
	chomp($_);
	my @tmp=split(/\t/,$_);
	my $id = $tmp[0];
	my $transSeedSeq = $tmp[1];
	@tmp = split(/\|/,$id);
	my $transcript = $tmp[0];
	my $ref_allele = $tmp[2];
	my $mutant_allele = $tmp[3];
	my @this_pair = @{$hash_pair{$transcript}};
	my $res = SeedMatch($id,$transcript,$transSeedSeq,\@this_pair,\%hash_mature_seq);
	$str = $str.$res;
	$i++;
    }
    close(FILESEED);

    # Output
    open(FILEOUT,">".get_output($cf_ref,"matched"));
    print FILEOUT $str;
    close(FILEOUT);
}

if(scalar @ARGV == 0){
    print "NO_COMMAND_FILE";
    exit;
}
my %cf = parse_command_file($ARGV[0]);
execute(\%cf);
