#!/bin/bash -x

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"

# bsmap and samtools Command Locations
BSMAPPATH=$( readlink -f ../../lib/bsmap/bsmap )
SAMTOOLSPATH=$( readlink -f ../../lib/bsmap/samtools/samtools )

# Get inputs
reference=$( getinput reference ) 
reads=$( getinput reads )
mate=$( getinput mate ) 

# Get parameters
restrictionSite=$( getparameter restrictionSite | tr [:lower:] [:upper:] )
mismatches=$( getparameter mismatches ) 
gapSize=$( getparameter gapSize ) 
threads=$( getparameter threads ) 
optionsAlignment=$( getparameter optionsAlignment ) 

# Get outputs
alignedReads=$( getoutput alignedReads ) 

# Get the file extension from reference, reads and mate
suffix_reference=$(echo ${reference} | awk -F\. '{print $NF}' | tr [:upper:] [:lower:] )
suffix_reads=$(echo ${reads} | awk -F\. '{print $NF}' | tr [:upper:] [:lower:] )
suffix_mate=$(echo ${mate} | awk -F\. '{print $NF}' | tr [:upper:] [:lower:] )

# Check reference file extension
if [ "${suffix_reference}" != "fasta" ] && [ "${suffix_reference}" != "fa" ] 
then
    writeerror "Format \"${suffix_reference}\" not recognized. Reference file must be in FASTA format!"
    exit 1
fi

# Check reads file extension
if [ "${suffix_reads}" != "fastq" ] && [ "${suffix_reads}" != "fasta" ] && [ "${suffix_reads}" != "fa" ] && [ "${suffix_reads}" != "bam" ]
then
    writeerror "Format \"${suffix_reads}\" not recognized. Reads file must  be in either FASTQ, FASTA or BAM format!"
    exit 1
fi

paired=0

if [ -f "${mate}" ] # If Mate exists
then
	paired=1
	
	# Check mate file extension
    if [ "${suffix_mate}" != "fastq" ] && [ "${suffix_mate}" != "fasta" ] && [ "${suffix_mate}" != "fa" ] && [ "${suffix_mate}" != "bam" ]
	then
		writeerror "Format \"${suffix_mate}\" not recognized. Mate file must  be in either FASTQ, FASTA or BAM format!"
		exit 1
	fi
	
	# Check that reads and mate files have the same extension
	if [ "${suffix_reads}" !=  "${suffix_mate}" ]
	then
		writeerror "Format error. Reads and Mate must have the same file format!"
		exit 1
	fi
	
	# Check that if the extension is "BAM" then reads and mate are the same file
	if [ "${suffix_reads}" ==  "bam" ] && [ "${reads}" != "${mate}" ]
	then
		writeerror "Input error. When Reads and Mate are in \"bam\" format, they must be the same file!"
		exit 1
	fi	
fi

# Check parameters
# restrictionSite
if [ "${restrictionSite}" != "" ]
then
	# Check that restrictionSite contains only the character "-ACTG"
	string=$( echo ${restrictionSite} | tr -d [\\-ATCG] )
	if [ ${#string} -ne 0 ] 
	then
		writeerror "restrictionSite \"${restrictionSite}\" not recognized. The Restriction Site string must be composed of only the following character \"ATCG-\"!"
		exit 1
	fi  
    
    # Check that restrictionSite contains only and only one hypen symbol "-"
    string=$( echo ${restrictionSite} | tr -d [ATCG] )
	if [ ${#string} -ne 1 ] 
	then
		writeerror "restrictionSite \"${restrictionSite}\" not recognized. The digestion position must be marker by one and only one hypen symbol \"-\"!"
		exit 1
	fi
fi

# mismatches
lower=$( echo "${mismatches}<0" | bc -l )
upper=$( echo "${mismatches}>15" | bc -l )

if [ "${lower}" -eq 1 ] || [ "${upper}" -eq 1 ]
then
	writeerror "Incorrect mismatches value \"${mismatches}\". The number of mismatches must be a positive number <=15!"
	exit 1
fi

# gapSize
if [ "${gapSize}" -lt 0 ] || [ "${gapSize}" -gt 3 ]
then
	writeerror "Incorrect gapSize value \"${gapSize}\". The gap size must be a positive number between 0 and 3!"
	exit 1
fi

# threads
if [ "${threads}" -le 0 ]
then
	writeerror "Incorrect threads value \"${threads}\". The number of threads must be a positive number!"
	exit 1
fi
if [ "${threads}" -gt 12 ]
then
	writelog "[WARNING]: Selected threads value \"${threads}\". No significant speed gain for >12 threads!"
	exit 1
fi

# Command strings initialization
bsmapSwitches=""

# Construction of bsmap alignment command 
bsmapSwitches="-d ${reference} -a ${reads}"

# Single-end or paired-end reads
if [ "${paired}" -eq 1 ]
then
	bsmapSwitches="$bsmapSwitches -b ${mate}" 
fi

# Choose WGBS or RRBS. In the second case select also the restriction site recognized in the experimental protocol 
if [ "${restrictionSite}" != "" ]
then
	bsmapSwitches="$bsmapSwitches -D ${restrictionSite}"
fi 

# Mismatches 
if [ "${mismatches}" != "0.08" ]
then
	bsmapSwitches="$bsmapSwitches -v ${mismatches}"
fi

# Gaps sets
if [ "${gapSize}" -ne 0 ]
then
	bsmapSwitches="$bsmapSwitches -g ${gapSize}"
fi

# Threads
bsmapSwitches="$bsmapSwitches -p ${threads}"

# Extra options
if [ "${optionsAlignment}" != "" ]
then
	bsmapSwitches="$bsmapSwitches ${optionsAlignment}"
fi

# Run Alignment + Conversion
echo $BSMAPPATH ${bsmapSwitches}
$BSMAPPATH ${bsmapSwitches} | $SAMTOOLSPATH view -Sb - -o ${alignedReads} 2>&1 >> "$logfile"
exitcode=$?

if [ ! -f "${alignedReads}" ]
then
	writeerror "Missing output file!"
	exit 1
fi

exit $exitcode
