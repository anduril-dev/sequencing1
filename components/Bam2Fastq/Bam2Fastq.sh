#!/bin/bash -x

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
export PATH=$PATH:../../lib/samtools/

# use these functions to read command file
bam=$( getinput bam ) 
fq=$( getoutput fq )
picard=$( getparameter picard )

picardJAR="${picard}/SamToFastq.jar"
writelog "Path used: ${picardJAR}"
[[ -e "$picardJAR" ]] || {
      echo cannot find picard at "$picardJAR"
      exit 1
}
# Create a temporary directory
tempdir=$( gettempdir )
iscmd samtools || exit 1
# Extract just the mapped reads as a sub-BAM file
samtools view -b -F 4 $bam > "$tempdir"/subBam

# Convert the subBam file to a Fastq file
java -Xmx4g -jar "$picardJAR" VALIDATION_STRINGENCY=SILENT INPUT="$tempdir"/subBam FASTQ="$fq" || writelog "Error in picard.jar"

if [ -f $fq ]
then 
        writelog "Successful conversion."
else  
        writeerror "Missing output file!"
        exit 1
fi
