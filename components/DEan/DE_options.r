library(componentSkeleton)

execute <- function(cf) {

   # Create array output dir.
    results <- get.output(cf, "array")
    if (!file.exists(results)) {
        dir.create(results, recursive=TRUE)
    }

    # Array object
    results.array.output <- Array.new()

    # Parameters + input
    DE_pairwise <- get.parameter(cf,"DE_pairwise", "boolean")
    options <- get.parameter(cf,"options", "string")
    pval <- get.parameter(cf, "pval", "float")
    expr <- CSV.read(get.input(cf, "expr"))
    treatment <- CSV.read(get.input(cf, "treatment"))
	allsGood <- TRUE

    # process options by removing spaces and using uppercase letters
    options <- unlist(strsplit(tolower(gsub("[[:blank:]]","",options)),","))

    # If required packages not installed, install them (if possible)
    is.installed <- function(pkg) {
                           is.element(pkg, installed.packages()[,1])
                           }

    if (!is.installed("DESeq")  && length(grep("deseq",options)) > 0) {
                        print("Downloading required R package DESeq")
                        update.packages(checkBuilt=TRUE, ask=FALSE)
                        source("http://bioconductor.org/biocLite.R")
                        biocLite("DESeq")
                        }

    if (!is.installed("DESeq2")  && length(grep("deseq2",options)) > 0) {
                        print("Downloading required R package DESeq")
                        update.packages(checkBuilt=TRUE, ask=FALSE)
                        source("http://bioconductor.org/biocLite.R")
                        biocLite("DESeq2")
                        }

    if (!is.installed("edgeR") && length(grep("edger",options)) > 0) {
                        print("Downloading required R package edgeR")
                        update.packages(checkBuilt=TRUE, ask=FALSE)
                        source("http://bioconductor.org/biocLite.R")
                        biocLite("edgeR")
                        }

    if (!is.installed("gtools")  && length(grep("uqnorm",options)) > 0) {
                        print("Downloading required R package gtools")
                        update.packages(checkBuilt=TRUE, ask=FALSE)
                        install.packages("gtools",repos="http://cran.r-project.org", dependencies=TRUE)
                        }

    # Check that pval = [0,1]
    if (pval > 1 | pval < 0) {   
        write.error("ERROR WITH INPUT pval. Value must be between 0 and 1.")
        return(1)
    }

    # Check options. If no options input, default deseq
    if (length(options) == 0) {
        options <- "deseq"
    }

    # Check input CSV treatment has a "treatment" column
    treatCol <- grep("treatment", colnames(treatment), ignore.case=TRUE)
    if (length(treatCol) != 1) {
        write.error("Input 'treatment' file does not contain a single column labelled 'Treatment'. DE analysis cannot be performed without sample treatment groups identified.")
        return(1)
    }

    # Conduct paired sample DE?
    if (DE_pairwise) {
	    paired <- TRUE
    } else {
	    paired <- FALSE
    }

    if (paired) {
        # If paired, check input CSV treatment has a "sample" column
        sampleCol <- grep("sample", colnames(treatment), ignore.case=TRUE)
        if (length(sampleCol) != 1) {
            write.error("Input 'treatment' file does not contain a single column labelled 'Sample' or 'Key'. Pairwise analysis cannot be performed without sample treatment groups identified.")
            return(1)
        }

        # treatment file must have a minimum 3 columns: 
        # 1 - unique sample IDs, 2 - path to sample count files, and a column labelled "treatment"
        ref_conditions <-treatment[,c(treatCol, sampleCol)]
        colnames(ref_conditions) <- c("Treatment","Sample")
    } else {
        ref_conditions <- matrix(treatment[,treatCol],ncol=1)
        colnames(ref_conditions) <- "Treatment"
    }
    rownames(ref_conditions) <- gsub("-","_",treatment[,1])

    # For non-pairwise analysis. remove treatment group(s) if only represented by 1 sample
    if  (length(which(table(ref_conditions) == 1)) && !paired) {
        ref_conditions <- matrix(treatment[-grep(names(table(treatment[,treatCol]))[which(table(treatment[,treatCol]) == 1)],treatment[,treatCol]),treatCol],ncol=1)
	    rownames(ref_conditions) <- treatment[-grep(names(table(treatment[,treatCol]))[which(table(treatment[,treatCol]) == 1)],treatment[,treatCol]),1]
	    colnames(ref_conditions) <- "Treatment"
    }

    # Check that ref_conditions and expr contain the same samples
    overlap <- intersect(rownames(ref_conditions),colnames(expr))
    if (paired) {
        ref_conditions <- ref_conditions[overlap,]
        dimnames(ref_conditions) <- list(overlap,c("Treatment","Sample"))
    } else {    
        ref_conditions <- matrix(ref_conditions[overlap,],ncol=1)
        dimnames(ref_conditions) <- list(overlap,"Treatment")
    }
        rownames(expr) <- expr[,1]
        expr <- expr[,overlap]

    if (length(unique(ref_conditions[,"Treatment"])) < 2) {
        write.error(">> DE: DESeq requires at least two distinct treatment groups, each represented by more than 1 sample!")
        return(1)
    }

	# Calculate all possible pairs of conditions to analyse
   	paired_cond <- combn(as.character(unique(ref_conditions[,"Treatment"])),2)
				
	for (option in options) {

        if (option == "deseq2") {
            library(DESeq2)
            cTable <- expr
            cTable <- cTable[,grep(paste(rownames(ref_conditions),collapse="|",sep=""),colnames(cTable))]
            colnames(cTable) <- gsub("^X","",colnames(cTable))
            sampleRefs <- colnames(cTable)
            conditioned <- ref_conditions            

            # Remove rows without expression after filtering to avoid errors
             if (length(which(rowSums(cTable) == 0)) != 0) {
                cTable <- cTable[-which(rowSums(cTable) == 0),]
            }

            if (paired) {
                # Reduce to only those with paired samples before analysis
                if (length(grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"$"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)) != 0) {					
                    cTable <- as.matrix(cTable[,-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)])
     				conditioned <- as.matrix(conditioned[-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),rownames(conditioned)),],ncol=1,dimnames=list(rownames(conditioned),colnames(conditioned)))
      			 	sampleRefs <- sampleRefs[-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)]
      			 	}
      			 	           
			dataSet <- DESeqDataSetFromMatrix(countData = cTable, colData = data.frame(conditioned), design = ~Sample+Treatment)
	        dataSet <- DESeq(dataSet)
	        result <- results(dataSet)
	        result <- result[order(result$pvalue),]
	        result <- data.frame(id=rownames(result), result)

            # Save to arrays
            idx_name <- paste(option,"paired",paste(sort(unique(conditioned[,"Treatment"])),collapse="vs"),sep="_")
            CSV.write(paste(results,"/",idx_name,".csv",sep=""), result[which(result$pval <= pval),])
            results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))
				

        } else {

            dataSet <- DESeqDataSetFromMatrix(countData = cTable, colData = data.frame(conditioned), design = ~Treatment)
	        dataSet <- DESeq(dataSet)

            # Perform nbinomTest on all pairs of conditions
            for (c in 1:ncol(paired_cond)) {
                cond1 <- paired_cond[1,c]
                cond2 <- paired_cond[2,c]				
                result <- results(dataSet)
                result <- result[order(result$pvalue),]
       	        result <- data.frame(id=rownames(result), result)

				
                idx_name <- paste(option,paste(sort(unlist(as.character(paired_cond[,c]))),collapse="vs"),sep="_")
                CSV.write(paste(results,"/",idx_name,".csv",sep=""), result[which(result$pval <= pval),])
                results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))
                }
            }

        } else if (option == "deseq") {
			############  DESEQ  #####	
            library(DESeq)
            cTable <- expr
            cTable <- cTable[,grep(paste(rownames(ref_conditions),collapse="|",sep=""),colnames(cTable))]
            colnames(cTable) <- gsub("^X","",colnames(cTable))
            sampleRefs <- colnames(cTable)
            conditioned <- ref_conditions

            # Remove rows without expression after filtering to avoid errors
             if (length(which(rowSums(cTable) == 0)) != 0) {
                cTable <- cTable[-which(rowSums(cTable) == 0),]
            }

            if (paired) {
                # Reduce to only those with paired samples before analysis
                if (length(grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)) != 0) {					
                    cTable <- as.matrix(cTable[,-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)])
     				conditioned <- as.matrix(conditioned[-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),rownames(conditioned)),],ncol=1,dimnames=list(rownames(conditioned),colnames(conditioned)))
      			 	sampleRefs <- sampleRefs[-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)]
    			}
      		
			dataSet <- newCountDataSet(cTable,conditioned)
	        dataSet <- estimateSizeFactors(dataSet)
			dataSet <- estimateDispersions(dataSet, method="pooled-CR",modelFormula=count~Sample+Treatment)
					
			fit1<- fitNbinomGLMs(dataSet,count~Sample+Treatment)
			fit0 <- fitNbinomGLMs(dataSet,count~Sample)
			pvalue <- nbinomGLMTest( fit1, fit0 )
			padj <- p.adjust(pvalue, method="BH" )
			result <- cbind(rownames(cTable),fit1,pvalue,padj)
			result <- result[order(result$pvalue),]
			colnames(result)[1] <- "id"

            # Save to arrays
			idx_name <- paste(option,"paired",paste(unique(conditioned[,"Treatment"]),collapse="vs"),sep="_")
            CSV.write(paste(results,"/",idx_name,".csv",sep=""), result[which(result$pval <= pval),])
            results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))
				

        } else {

            dataSet <- newCountDataSet(cTable,as.vector(conditioned))
	        dataSet <- estimateSizeFactors(dataSet)
            dataSet <- estimateDispersions(dataSet)

            # Perform nbinomTest on all pairs of conditions
            for (c in 1:ncol(paired_cond)) {
                cond1 <- paired_cond[1,c]
                cond2 <- paired_cond[2,c]				
                result <- nbinomTest(dataSet,cond1,cond2)
                result <- result[order(result$pval),]
				
                idx_name <- paste(option,paste(unlist(as.character(paired_cond[,c])),collapse="vs"),sep="_")
                CSV.write(paste(results,"/",idx_name,".csv",sep=""), result[which(result$pval <= pval),])
                results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))
            }
        }

    } else if (option == "edger") {
			############  EDGER  #####		
			library(edgeR)
			cTable <- expr
			colnames(cTable) <- gsub("^X","",colnames(cTable))
			sampleRefs <- colnames(cTable)
			sampleRefs <- sampleRefs[grep(paste(rownames(ref_conditions),collapse="|",sep=""),sampleRefs)] 	
            conditioned <- ref_conditions

            # Analyse only those with paired samples
			if (paired) {
                if (length(grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)) != 0) {					
                    cTable <- as.matrix(cTable[,-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)])
                    conditioned <- conditioned[-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),rownames(conditioned)),]
                    sampleRefs <- sampleRefs[-grep(paste(paste("^",names(which(table(do.call(rbind,strsplit(sampleRefs,"_"))[,1])==1)),"_", sep=""),collapse="|"),sampleRefs)]
                }
					
                # Assuming conditions correspond to column names,
                # extract just the conditions to be analysed
                dge <- DGEList(counts=cTable,group=conditioned$Treatment, remove.zeros=TRUE)
			    dge <- calcNormFactors(dge)
			    dge <- estimateCommonDisp(dge)
			    dge <- estimateTagwiseDisp(dge)		
			    Subject <- factor(conditioned$Sample)
			    Treatment <- factor(conditioned$Treatment, levels=unique(conditioned$Treatment))
			    design <- model.matrix(~Subject+Treatment)

			    # This test detects genes that are differentially expressed in response 
			    # to the active treatment compared to the control, adjusting for baseline 
			    # differences between the patients. This test
			    # can be viewed as a generalization of a paired t-test.
			    y <- estimateGLMCommonDisp(dge,design)
			    y <- estimateGLMTrendedDisp(y,design)
			    y <- estimateGLMTagwiseDisp(y,design)
			    fit <- glmFit(y, design)
			    lrt <- glmLRT(fit)

			    # Save only those DEGs below a given p-value cutoff				
			    idx_name <- paste(option,"paired",paste(unique(conditioned$Treatment),collapse="vs"),sep="_")
			    result <- topTags(lrt, n=length(which(lrt$table[,"PValue"] <= pval)))$table
			    result <- cbind(rownames(result),result)
			    colnames(result)[1] <- "id"
                CSV.write(paste(results,"/",idx_name,".csv",sep=""), result)
                results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))
				
            } else {
				# Assuming conditions correspond to column names,
				# extract just the conditions to be analysed
				dge <- DGEList(counts=cTable,group=conditioned, remove.zeros=TRUE)
				dge <- calcNormFactors(dge)
				dge <- estimateCommonDisp(dge)
				dge <- estimateTagwiseDisp(dge)		

				# Perform nbinomTest on all pairs of conditions
				for (c in 1:ncol(paired_cond)) {
					cond1 <- paired_cond[1,c]
					cond2 <- paired_cond[2,c]
					result <- exactTest(dge, pair=c(cond1,cond2))	

					idx_name <- paste(option,paste(unlist(as.character(paired_cond[,c])),collapse="vs"),sep="_")
						
					# Save only those DEGs below a given p-value cutoff
					result <- topTags(result, n=length(which(result$table[,"PValue"] <= pval)))$table
					result <- cbind(rownames(result),result)
					colnames(result)[1] <- "id"
                    CSV.write(paste(results,"/",idx_name,".csv",sep=""), result)
                    results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))			
				}
			}
				
			if (!allsGood) { 
    			write.error(paste("Number of conditions do not match number of samples in ", file))
				return(1)
			}


		
		############  UQNORM  #####
		} else if (option == "uqnorm") {
			library(gtools)
			skipDE <- FALSE
			paired_condUQ <- paired_cond
			cTable <- expr
    		colnames(cTable) <- gsub("^X","",colnames(cTable))
			sampleRefs <- colnames(cTable)
			sampleRefs <- sampleRefs[grep(paste(rownames(ref_conditions),collapse="|",sep=""),sampleRefs)] # Analyse only those represented in more than 1 treatment group
			conditionedUQ <- ref_conditions[sampleRefs,]
		
			# Convert all 0 counts to NA
			cTable[cTable == 0] <- NA

			# Analyse only those with paired samples
			if (paired) {				
				if (length(grep(paste(paste("^",names(which(table(conditionedUQ$Sample) == 1)),"$", sep=""),collapse="|"),as.character(conditionedUQ$Sample))) != 0) {					
					cTable <- as.matrix(cTable[,-grep(paste(paste("^",names(which(table(conditionedUQ$Sample) == 1)),"$", sep=""),collapse="|"),as.character(conditionedUQ$Sample))])
					sampleRefs <- sampleRefs[-grep(paste(paste("^",names(which(table(conditionedUQ$Sample) == 1)),"$", sep=""),collapse="|"),as.character(conditionedUQ$Sample))]
					conditionedUQ <- conditionedUQ[-grep(paste(paste("^",names(which(table(conditionedUQ$Sample) == 1)),"$", sep=""),collapse="|"),as.character(conditionedUQ$Sample)),]
				}
            # Reduce to just treatment samples
            conditionedUQ <- conditionedUQ[,"Treatment"]
			}

			# calculate library sizes
			laneSums <- colSums(cTable, na.rm=TRUE)
	
			# calculate the 75th quantile of each sample
			# MARGIN==2==apply to columns of table, 
			# exclude NAs from calculation
			upper.quartiles <- apply(cTable,2,function(x) quantile(x,0.75, na.rm=TRUE))

			# UQnorm of TPM counts is better at GLOBAL normalization between 
			# libraries than simply dividing by the total sums of each library.
			uq.scaled <- upper.quartiles/sum(upper.quartiles)*sum(laneSums)
			uq.norm <- sweep(cTable,MARGIN=2,uq.scaled,"/")*100000	# count values divided by normalized total counts


			for (c in 1:ncol(paired_condUQ)) {
				cond1 <- paired_condUQ[1,c]
				cond2 <- paired_condUQ[2,c]
					
				avgCond1 <- rowSums(uq.norm[,grep(cond1,as.character(conditionedUQ))],na.rm=TRUE)
				avgCond2 <- rowSums(uq.norm[,grep(cond2,as.character(conditionedUQ))],na.rm=TRUE)
				FC <- foldchange(avgCond1,avgCond2)

				# Calculate t-test pvalue for each gene
				Ttest <- c()
				for (gene in 1:nrow(cTable)) {
					x <- uq.norm[gene,grep(paste("^",cond1,"$",sep=""),as.character(conditionedUQ))]
					y <- uq.norm[gene,grep(paste("^",cond2,"$",sep=""),as.character(conditionedUQ))]
					skipTest <- FALSE
						
					# If a condition group has 1 or 0 non-NA values, skip test
					if (sum(is.na(x)) >= length(x)-1) {
						skipTest <- TRUE
					}
					if (sum(is.na(y)) >= length(y)-1) {
						skipTest <- TRUE
					}
					# If both condition groups are low expressed, skip test
					if ((sum(x,na.rm=TRUE) < 1)&&(sum(y,na.rm=TRUE) < 1)) {
						skipTest <- TRUE
					}
					# If exactly same value is in both condition groups, skip test
					tried <- try(t.test(x,y),TRUE)
					if (class(tried) == "try-error") {
						skipTest <- TRUE
					} 
					if (!skipTest) {
						if (paired) {
							# If not enough paired samples, skip test
							if (sum(!is.na(x+y)) <=1) {
								Ttest <- c(Ttest,NA)
							} else {
                                # t.test takes vectors as input, not dataframe
								T <- t.test(unlist(x),unlist(y), paired=TRUE)
								Ttest <- c(Ttest,T$p.value)
							}
						} else {
							T <- t.test(x,y)
							Ttest <- c(Ttest,T$p.value)
						}
					} else {
						Ttest <- c(Ttest,NA)
					}
				}
					
				# Calculate FDR
				FDR <- p.adjust(Ttest,method="fdr")
					
				# Table of DEG based on t-test p-value < pval
				result <- cbind(FC,Ttest,FDR)
				result <- result[!is.na(result[,"FDR"]),]
				idx_name <- paste(option,paste(unlist(as.character(paired_cond[,c])),collapse="vs"),sep="_")
				if (paired) {
					idx_name <- paste(option,"paired",paste(unlist(as.character(paired_cond[,c])),collapse="vs"),sep="_")
				}
                result <- result[order(result[,"Ttest"]),]
				resulting <- result[which(result[,"Ttest"] <= pval),]
				if (is.null(colnames(resulting))) { # Only one result!
					resulting <- matrix(resulting, nrow=1)
					colnames(resulting) <- colnames(result)
					rownames(resulting) <- rownames(result)[which(result[,"Ttest"] <= pval)]
				}

				# Save only those DEGs below a given p-value cutoff
                CSV.write(paste(results,"/",idx_name,".csv",sep=""), resulting)
                results.array.output <- Array.add(results.array.output,idx_name,paste(idx_name,".csv",sep=""))			
			}
					
		############  END OF OPTIONS #####
		} else {
			write.error(paste("NORMALIZATION OPTION [",option,"] NOT RECOGNIZED.",sep=""))
			return(1)
		}	
	}	


    Array.write(cf,results.array.output,"array")
    return(0)
}

main(execute)
