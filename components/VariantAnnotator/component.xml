<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>VariantAnnotator</name>
    <version>0.4</version>
    <deprecated>Use the VCF2AnnotatedCSV (for ANNOVAR) or AnnTools component instead.</deprecated>
    <doc>This component is not being actively maintained currently, since Annovar is updated to frequently. A simpler, more maintainable interface is available at in the component VCF2AnnotatedCSV. Another alternative is the AnnTools component, which is even easier to maintain, but so far largely unused.
        <p>This variant annotation function uses ANNOVAR which isn't allowed to be redistributed but must be downloaded directly from the author for each user ( http://www.openbioinformatics.org/annovar/ ). Installation however is as easy as uncompressing. You do need to have the appropriate database files in the reference directory, otherwise nothing will work.</p>
        <p><a href="http://www.openbioinformatics.org/annovar/annovar_startup.html">The quick start guide of ANNOVAR</a> may prove helpful.</p>
    <p> ANNOVAR uses three main methods:
    <ul>
        <li>gene-based annotation: Produces two files:<ul><li>Location with respect to genes (exonic, intronic, intergenic, splicing site, 5'/3'-UTR, upstream/downstream of genes, invalid input</li><li>Amino acid changes caused by the mutation</li></ul></li>
        <li>region-based annotation: Annotates by regions in an annotation database or in GFF3 files. Produces region annotations for each variant.</li>
        <li>filter-based annotation: Filter out those found in another database to find new variants</li>
    </ul>
    </p>
    <p>This component offers four different ways to call ANNOVAR, the first of which just calls it directly. The others use a configurable, automated sequence of filtering steps. You can select the method by specifying the annotator parameter. The scripts used by each of the extra methods are explained in <a target="_blank" href="http://www.openbioinformatics.org/annovar/annovar_accessary.html">ANNOVAR Accessory Programs</a>
    </p>
    <ul>
        <li>The default ("annovar"): annotate_variation.pl</li>
        <li>Variant reduction ("variant_reduction"): variants_reduction.pl.</li>
        <li>Summarize ("summarize"): summarize_annovar.pl</li>
        <li>Table ("table"): table_annovar.pl</li>
    </ul>

    <p>ANNOVAR only outputs a CSV file when using the summarize or table method. For other methods it produces non-standard VCF files that have extra columns before the standard VCF columns. It doesn't output a header. This component adds a CSV header with anonymous field names.</p>
    <p>The first fields are called "Annotation1", "Annotation2" etc. and the rest are taken from the VCF specification, i.e. they are called CHROM, POS, ID, REF, ALT, QUAL, FILTER and INFO.</p>
    <p>You should check ANNOVAR website to find out what the column headers mean in each case. The meaning of the extra fields might also depend on the database that is used so result interpretation must be done carefully.</p>
    <p>Documentation for ANNOVAR itself can be mainly found on its website. Each ANNOVAR command's help file itself can be viewed with "perldoc annotate_variation.pl" (substitute the script name), for those who don't want to read the perl files directly.</p>
    <p>ANNOVAR provides a script called "variant_reduction.pl" which can be perform multiple variant reduction steps (as does the table_annovar.pl script). It needs a two special options: the protocols to apply, and how they are applied. The exact format of these is documented in the script's own help or explained more verbosely in <a target="_blank" href="http://www.openbioinformatics.org/annovar/annovar_accessary.html">Variant reduction</a></p>
        <p>Explanations from the ANNOVAR documentation for some gene-based annotations:</p>
        <table>
            <tr><td>exonic</td><td>variant overlaps a coding exon</td></tr>
            <tr><td>splicing</td><td>variant is within 2-bp of a splicing junction</td></tr>
            <tr><td>ncRNA</td><td>variant overlaps a transcript without coding annotation in the gene definition</td></tr>
            <tr><td>UTR5</td><td>variant overlaps a 5' untranslated region</td></tr>
            <tr><td>UTR3</td><td>variant overlaps a 3' untranslated region</td></tr>
            <tr><td>intronic</td><td>variant overlaps an intron</td></tr>
            <tr><td>upstream</td><td>variant overlaps 1-kb region upstream of transcription start site</td></tr>
            <tr><td>downstream</td><td>variant overlaps 1-kb region downtream of transcription end site</td></tr>
            <tr><td>intergenic</td><td>variant is in intergenic region</td></tr>
        </table>
    </doc>
    <author email="lauri.lyly@helsinki.fi">Lauri Lyly</author>
    <author email="miko.valori@helsinki.fi">Miko Valori</author>
    <category>VariationAnalysis</category>
    <launcher type="bash">
        <argument name="file" value="main.sh" />
    </launcher>
    <type-parameters>
        <type-parameter name="T"/> 
    </type-parameters> 
    <inputs>
        <input name="variantQuery" type="VCF" optional="false">
            <doc>Variants to annotate which are first converted to ANNOVAR's own format for convenience. All standards fields are retained even in the output. The additional FORMAT fields are discarded by ANNOVAR.
            </doc>
        </input>
        <input name="reference" type="BinaryFolder" optional="true">
            <doc>Path to directory containing reference variant and other databases. The location should be kept up-to-date by running various download commands as documented on the ANNOVAR website. For SBL, the default should be /mnt/csc-gc/resources/annovar/hg19_db. May optionally be specified as a string parameter.
            </doc>
        </input>
    </inputs>
    <outputs>
        <output name="calls" type="CSV" array="true">
            <doc>Annotation method (parameter "caller") specific files that are contained in an Anduril array directory.</doc>
        </output>
        <output name="log" type="TextFile" array="true">
            <doc>Log files produced by the run i.e. those ending in .log.</doc>
        </output>
        <output name="raw" type="TextFile" array="true">
            <doc>This is only used for the summarize annotator. Contains the raw outputs that are used to form the summary. Specify the --remove option to avoid producing them.
            </doc>
    	</output>
    </outputs>
    <parameters>
        <parameter name="annotator" type="string" default="annovar">
            <doc>Software/algorithm to use to call variants. Values: {annovar, variant_reductor, summarize, table}. Notice that for the variant_reductor and table methods you practically always want to pass an option string containing the protocol and operation, as documented in ANNOVAR itself. See details above.
            </doc>
        </parameter>
        <parameter name="annovar" type="string" default="">
            <doc>Path to the ANNOVAR installation directory. If empty string is given (default), ANNOVAR_HOME environment variable is assumed to point to the ANNOVAR directory, where all the associated scripts are assumed to reside.
            </doc>
        </parameter>
        <parameter name="index" type="string" default="hg19">
            <doc>Basename of the genome build, e.g. <code>"hg19"</code> for <code>ucsc.hg19</code>. The relevant files are assumed to reside in the directory pointed to by the "reference" input. Note that support for different builds may vary.
            </doc>
        </parameter>
        <parameter name="options" type="string" default="defaults">
            <doc>This string will be added to the command and can include any number of options in the software specific format. The default value of "defaults" chooses method specific defaults, which are not necessarily same as method's own defaults in ANNOVAR. 
            <br/><br/>
            For the summarize annotator the default is: "--verdbsnp 135"<br/>
            This is because otherwise ANNOVAR will use a non-existent database. For the "annovar" annotator, the options string is empty.
            <br/>Notice that for the variant_reductor method you practically always want to pass an option string containing the protocol and operation, as documented in ANNOVAR itself. See details above.
            </doc>
        </parameter>
        <parameter name="referenceDir" type="string" default="">
            <doc>Alternative to specifying the same as an input.
            </doc>
        </parameter>
        <parameter name="convertFromType" type="string" default="vcf4old">
            <doc>The first step is conversion from various formats to ANNOVAR's internal format. Specify "none" if you want to skip the conversion. Otherwise consult the convert2annovar.pl script or ANNOVAR documentation for the available formats. vcf4 and vcf4old are the most obvious ones.</doc>
        </parameter>
        <parameter name="countAlts" type="boolean" default="true">
            <doc><p>(FIXME: Not implemented yet!) Enable to calculate four additional columns: alt_samples, ref_samples, alt_alleles and called_samples.</p>
                <p>These indicate the number of samples presenting a non-reference allele, number of samples homozygous for the reference allele, total number of alternative alleles and number of samples for which a call was present for this variant, respectively.</p></doc>
        </parameter>
    </parameters>
</component>
