# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"

# Get inputs
methCall=$( getinput methCall ) 

# Get parameters
coverage=$( getparameter coverage ) 
methPerc=$( getparameter methPerc)

# Get outputs
cpg=$( getoutput cpg ) 
chg=$( getoutput chg ) 
chh=$( getoutput chh ) 
statistics=$( getoutput statistics ) 

#Check parameters
# coverage
if [ $coverage -lt 0 ]
then
	writeerror "Incorrect coverage value  \"${coverage}\". The coverage must be a positive number!"
	exit 1
fi

# methPerc
lower=$( echo "${methPerc}<0" | bc -l )
upper=$( echo "${methPerc}>1" | bc -l )
if [ $lower -eq 1 ] || [ $upper -eq 1 ]
then
	writeerror "Incorrect methPerc value  \"${methPerc}\". The methPerc must be a positive number between 0 and 1!"
	exit 1
fi

# Methylation statistics
writelog "Computing methylation context statistics..."
awk -v cov="$coverage" -v meth="$methPerc" '
  BEGIN{   	cpg = 0;
			chg = 0;
			chh = 0;
			OFS = "\t";
  }{ if ((NR != 1) && ($0 !~ /NA/) && ($6 >= cov) && ($5 > meth)){
		if((($3 == "-") && ($4 ~ /^.GC/)) || (($3 == "+" ) &&  ($4 ~ /^..CG/))) cpg = cpg+1
		else if ((($3 == "-") && ($4 ~ /^G[ATC]C/)) || (($3 == "+") &&  ($4 ~ /^..C[ATC]G/))) chg = chg+1
		else chh = chh+1;
	 }
  }END{		total = cpg+chh+chg;
			printf "Context\tPercentage\n";
			if (total != 0){
				printf "mCpG\t%.2f\n", (cpg*100/total);
				printf "mCHG\t%.2f\n", (chg*100/total);
				printf "mCHH\t%.2f\n", (chh*100/total);
			} else {
				printf "mCpG\t0\n";
				printf "mCHG\t0\n";
				printf "mCHH\t0\n";
			}
  }' $methCall > $statistics || {
	  writelog "Error in computing methylation statistics" 
	  writeerror "Unable to compute methylation statistics"
	  exit 1
  }
  
if [ -f $statistics ]
then writelog "Methylation context statistics completed successfully."
fi

# Context separation
writelog "Separating cytosines based on context..."

# CpG
writelog "CpG context..."
awk '
  { if ($0 !~ /NA/){
		if (NR != 1){
			if((($3 == "-") && ($4 ~ /^.GC/)) || (($3 == "+" ) &&  ($4 ~ /^..CG/))){
				print $0;
			}
		} else {
			print $0;
		}
    }
  }' $methCall > $cpg || {
	  writelog "Error in selecting CpG cytosines" 
	  writeerror "Unable to select CpG cytosines"
	  exit 1
  }

if [ -f $statistics ]
then writelog "CpG cytosines selection completed successfully."
fi

# CHG
writelog "CHG context..."
awk '
  { if ($0 !~ /NA/) {
		if (NR != 1){ 
			if ((($3 == "-") && ($4 ~ /^G[ATC]C/)) || (($3 == "+") &&  ($4 ~ /^..C[ATC]G/))){ 
				print $0;
			}
		} else { 
			print $0;
		}
	}
  }' $methCall > $chg || {
	  writelog "selecting CHG cytosines" 
	  writeerror "Unable to select CHG cytosines"
	  exit 1
  }

if [ -f $statistics ]
then writelog "CHG cytosines selection completed successfully."
fi

# CHH
writelog "CHH context..."
awk '
  { if ($0 !~ /NA/) {
		if (NR != 1){ 
			if ((($3 == "-") && ($4 !~ /^.GC/) && ($4 !~ /^G[ATC]C/)) || (($3 == "+") &&  ($4 !~ /^..CG/) &&  ($4 !~ /^..C[ATC]G/))){ 
				print $0;
			}
		} else { 
			print $0;
		}
    }
  }' $methCall > $chh || {
	  writelog "Error in selecting CHH cytosines" 
	  writeerror "Unable to select CHH cytosines"
	  exit 1
  }

if [ -f $statistics ]
then writelog "CHH cytosines selection completed successfully."
fi
