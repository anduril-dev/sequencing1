#!/bin/bash
set -e

source "$ANDURIL_HOME/bash/functions.sh"

VCF=$(getinput vcf)
BUILDVER=$(getparameter buildver)
ANNOVAR_HOME=$(getparameter annovar_bin)
DB_DIR=$(getparameter annovar_db)
PROTOCOL=$(getparameter protocol)
OPERATION=$(getparameter operation)
OUT=$(getoutput annotated)

DIR="$PWD"
cd "$(gettempdir)"

"$ANNOVAR_HOME"/convert2annovar.pl -format vcf4old -includeinfo "$VCF" > annovarized_info.txt
cut -f 1-5 annovarized_info.txt > annovarized.txt

"$ANNOVAR_HOME"/table_annovar.pl -nastring 0 \
	-buildver "$BUILDVER" annovarized.txt "$DB_DIR" \
	-protocol "$PROTOCOL" -operation "$OPERATION"

python "$DIR"/count_alts.py annovarized_info.txt > alts.txt
paste annovarized.txt.${BUILDVER}_multianno.txt alts.txt > "$OUT"
