<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>VCF2AnnotatedCSV</name>
    <version>1</version>
    <doc>
        <p>A quick and dirty wrapper for ANNOVAR. Annotates variants from a VCF file using the chosen databases and outputs CSV.</p>
        <p>Explanations from the ANNOVAR documentation for some gene-based annotations:</p>
        <table>
            <tr><td>exonic</td><td>variant overlaps a coding exon</td></tr>
            <tr><td>splicing</td><td>variant is within 2-bp of a splicing junction</td></tr>
            <tr><td>ncRNA</td><td>variant overlaps a transcript without coding annotation in the gene definition</td></tr>
            <tr><td>UTR5</td><td>variant overlaps a 5' untranslated region</td></tr>
            <tr><td>UTR3</td><td>variant overlaps a 3' untranslated region</td></tr>
            <tr><td>intronic</td><td>variant overlaps an intron</td></tr>
            <tr><td>upstream</td><td>variant overlaps 1-kb region upstream of transcription start site</td></tr>
            <tr><td>downstream</td><td>variant overlaps 1-kb region downtream of transcription end site</td></tr>
            <tr><td>intergenic</td><td>variant is in intergenic region</td></tr>
        </table>
        <p>This component will add four additional columns: alt_samples, ref_samples, alt_alleles and called_samples.
        These indicate the number of samples presenting a non-reference allele, number of samples homozygous for the reference allele, total number of alternative alleles and number of samples for which a call was present for this variant, respectively.</p>
    </doc>
    <author email="miko.valori@helsinki.fi">Miko Valori</author>
    <launcher type="bash">
        <argument name="file" value="vcf2annotatedcsv.sh"/>
    </launcher>             
    <inputs>
        <input name="vcf" type="VCF" optional="false">
            <doc>VCF file to be annotated.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="annotated" type="CSV">
            <doc>CSV file containing the annotated variants.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="annovar_bin" type="string" default="/opt/annovar">
            <doc>Path to the ANNOVAR home directory.</doc>
        </parameter>
        <parameter name="annovar_db" type="string" default="/opt/annovar/humandb">
            <doc>Path to the ANNOVAR database directory.</doc>
        </parameter>
        <parameter name="buildver" type="string" default="hg18">
            <doc>Either hg19 or hg18.</doc>
        </parameter>
        <parameter name="protocol" type="string" default="ensGene">
            <doc>Comma separated list of databases to use, e.g. "ensGene,1000g2012apr_all,snp137,cytoBand" for annotating the variants with Ensembl gene definitions, 1000 Genomes allele frequencies, dbSNP identifiers and cytogenetic band locations.</doc>
        </parameter>
        <parameter name="operation" type="string" default="g">
            <doc>Comma separated list of annotation operations corresponding to the databases listed in the protocol parameter.
            For the protocol example we would use "g,f,f,r".
            Use "g" for gene-based annotations, "f" for filter-based and "r" for region-based.
            Gene-based annotations are for gene definition files, filter-based for specific variant information containing files and region-based for files than contain genomic regions.
            ANNOVAR doesn't automatically know what to do with the databases defined in the protocol parameter so you need to use this parameter to guide it.</doc>
        </parameter>
    </parameters>
</component>
