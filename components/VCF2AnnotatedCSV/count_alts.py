#!/usr/bin/python
import re
import fileinput

print "alt_samples\tref_samples\talt_alleles\tcalled_samples"
for line in fileinput.input():
    line = line.strip()
    fields = line.split("\t")[14:]
    called_sample_count = 0
    alt_sample_count = 0
    alt_allele_count = 0
    for field in fields:
        field = re.match(r'[^:]*', field).group()
        alleles = field.split('/')
        altcount = 0
        called = 0
        for allele in alleles:
            if allele != ".":
                called = 1
            if allele != "0" and allele != ".":
                altcount += 1
        called_sample_count += called
        if altcount > 0:
            alt_sample_count +=1
        alt_allele_count += altcount
    print "%s\t%s\t%s\t%s" % (alt_sample_count, called_sample_count - alt_sample_count, alt_allele_count, called_sample_count)
