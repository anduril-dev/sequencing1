library(componentSkeleton)
library(edgeR)
require(limma)

execute <- function(cf){

    # Params
    norm.method <- get.parameter(cf, 'normMethod',     type = 'string')
    p.corr      <- get.parameter(cf, 'correction',     type = 'string')
    norm.quant  <- get.parameter(cf, 'normQuartile',   type = 'float')
    disp.method <- get.parameter(cf, 'dispersion',     type = 'string')
    ref.group   <- get.parameter(cf, 'referenceGroup', type = 'string')
    smp.group   <- get.parameter(cf, 'targetGroup',    type = 'string')
    deg.limit   <- get.parameter(cf, 'pLimitPlot',     type = 'float')
    only.norm   <- get.parameter(cf, 'onlyNorm',       type = 'boolean')
    out.dir     <- get.output(cf, 'plot')
    docName     <- file.path(out.dir, LATEX.DOCUMENT.FILE)
    dir.create(out.dir, recursive=TRUE)
    # Inputs
    groups <- SampleGroupTable.read(get.input(cf, 'groups'))
    counts <- Matrix.read(get.input(cf, 'counts'))

    sampleCols <- unlist(strsplit(groups[groups[,'ID'] %in% c(ref.group,smp.group),'Members'], ","))
    counts <- counts[,sampleCols]
    if (input.defined(cf, 'reads')) {
        reads <- Matrix.read(get.input(cf, 'reads'))
        reads <- as.vector(reads[match(rownames(reads), colnames(counts))])
    } else {
        reads <- colSums(counts, na.rm = TRUE)
    }

    if (nrow(counts) < 1){
        write.error(cf, c("Counts input is empty."))
        return(INVALID_INPUT)
    }

    # Drop tags with NAs
    for (i in 1:ncol(counts)){
        nulls <- which(is.na(counts[,i]))
        if (length(nulls) > 0){
            counts <- counts[-nulls, , drop=FALSE]
        }
    }
    # Create suitable data object
    if (ref.group != ""){
        medianRef <- counts[,SampleGroupTable.get.source.groups(groups, ref.group), drop=FALSE]
        medianRef <- apply(medianRef, 1, median, na.rm = TRUE)
        input <- cbind(counts, medianRef)
        ref.column = "medianRef"
    } else {
        ref.column = NULL
    }
    # Normalize
    data.norm <- calcNormFactors(input,
                                 method    = norm.method,
                                 refColumn = ref.column,
                                 p         = norm.quant)
    data.norm <- data.norm[-length(data.norm)]

    # Create DGEList object
    sampleGroups <- rbind(cbind(SampleGroupTable.get.source.groups(groups, smp.group), smp.group),
                          cbind(SampleGroupTable.get.source.groups(groups, ref.group), ref.group))
    sampleGroups <- sampleGroups[match(sampleGroups[,1], colnames(counts)),2]
    data.groups  <- as.vector(sampleGroups)
    data.list    <- DGEList(counts       = counts,
                            lib.size     = reads,
                            norm.factors = data.norm,
                            group        = data.groups)
    # Estimate dispersion
    if (disp.method == "tag"){
        data.com  <- estimateCommonDisp(data.list)
        if (is.na(data.com$common.dispersion)){
            data.com$common.dispersion <- 0.4 # Default when no replicates exist
            write.log(cf, "Cannot estimate dispersion: no biological replicates. Proceeding with common dispersion.")
            data.disp <- data.com
        } else {
            data.disp <- estimateTagwiseDisp(data.com)
        }
    } else if (disp.method == "common"){
        data.disp <- estimateCommonDisp(data.list)
        if (is.na(data.disp$common.dispersion)){
            data.disp$common.dispersion <- 0.4 # Default when no replicates exist
            write.log(cf, "Cannot estimate dispersion: no biological replicates. Proceeding with common dispersion.")
        }
    } else {
        write.error(cf, c("Value ",disp.method," for parameter dispersion is not defined. Must be either tag or common."))
        return(INVALID_INPUT)
    }

    if (only.norm){
        output <- matrix(nrow=0, ncol=ncol(counts))
        colnames(output) <- colnames(counts)
        cat( "\\clearpage",
             sep="\n", file=docName, append=TRUE)
    } else {
        # Do test for fc and p-value
        degs <- exactTest(data.disp)
        # Plot pvalue dispersion
        fig.name <- sprintf('%s-pvalue-dispersion.png', get.metadata(cf, 'instanceName'))
        fig.path <- file.path(out.dir, fig.name)
        png(fig.path, height=1000, width=1500, res=150)
        tCat <- tryCatch({
            de.tags <- rownames(topTags(degs, adjust.method = p.corr, n=length(which(p.adjust(degs$table[,'PValue']) <= deg.limit)))$table)
            plotSmear(data.disp, de.tags=de.tags, yLab = colnames(degs$table)[1], xLab = colnames(degs$table)[2])
        }, error = function(err){
            write.log(cf, (paste('Caught error when plotting: ', err, 'No plots were created. Continuing execution...')))
        }, finally = {
           foo <- dev.off()
        })
        cat( latex.figure(fig.name, quote.capt = FALSE, image.width=12),
             "\\clearpage",
             sep="\n", file=docName, append=TRUE)
        # Output
        if (p.corr == "none"){
            output <- degs$table
            colnames(output)[3] <- "pvalue"
        } else {
            output <- topTags(degs, adjust.method = p.corr, n = nrow(degs$table))$table
            colnames(output)[3:4] <- c("pvalue",p.corr)
        }
    }
    first.cell <- CSV.read.first.cell(get.input(cf, 'counts'))
    CSV.write(get.output(cf, 'normalized'), as.matrix(data.disp$counts), first.cell = first.cell)
    CSV.write(get.output(cf, 'logratio'),   as.matrix(output), first.cell = first.cell)
}

main(execute)
