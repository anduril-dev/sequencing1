<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
  <name>MethylCall</name>
  <version>1.0</version>
  <doc>
    Perform Methylation Calling of aligned RRBS and WGBS data obtained from the component BSalign.
    <br/>
    WARNING: It requires quite a big amount of CPU (For human genome, needs ~26GB memory or more). For systems with limited memory, user can set the -c/--chr option to process specified chromosomes only,
and combine results for all chromosomes afterwards.
  </doc>
  <author email="chiara.facciotto@helsinki.fi">Chiara Facciotto</author>
  <category>Alignment</category>
  <launcher type="bash">
	<argument name="file" value="MethylCall.sh"/>
  </launcher>
  <requires>python</requires>
  <requires>samtools</requires>
  <inputs>
    <input name="reference" type="FASTA" optional="false">
      <doc>
        The reference genome file in fasta format. It supports also gzipped fasta format.
      </doc>
    </input>
    <input name="alignment" type="BAM" optional="false">
      <doc>
        Aligned reads in bam format.
      </doc>
    </input>
  </inputs>
  <outputs>
    <output name="methylationCalling" type="CSV">
      <doc>
        CSV file with the following columns:
        <br/>
        <br/>
        1) chromorome
        <br/>
        2) coordinate (1-based)
        <br/>
        3) strand
        <br/>
        4) sequence context (2nt upstream to 2nt downstream in Watson strand direction)
        <br/>
        5) methylation ratio, calculated as #C_counts / #eff_CT_counts
        <br/>
        6) number of effective total C+T counts on this locus (#eff_CT_counts) 
        <br/>
            <code>ctSNP</code>
            ="no action", #eff_CT_counts = #CT_counts
        <br/>
            <code>ctSNP</code>
            ="correct", #eff_CT_counts = #CT_counts * (#rev_G_counts / #rev_GA_counts)
        <br/>
        7) number of total C counts on this locus (#C_counts)
        <br/>
        8) number of total C+T counts on this locuso (#CT_counts)
        <br/>
        9) number of total G counts on this locus of reverse strand (#rev_G_counts)
        <br/>
        10) number of total G+A counts on this locus of reverse strand (#rev_GA_counts)
        <br/>
        11) lower bound of 95% confidence interval of methylation ratio, calculated by Wilson score interval for binomial proportion.
        <br/>
        12) upper bound of 95% confidence interval of methylation ratio, calculated by Wilson score interval for binomial proportion.
      </doc>
    </output>
  </outputs>
  <parameters>
    <parameter name="trim" type="int" default="2">
      <doc>
        Defines the number of fill-in nucleotides to be trimmed in DNA fragment end-repairing.
        <br/>
        <br/>
        This option is only for pair-end mapping. For RRBS, 
        <code>trim</code>
        could be detetmined by the distance between
        cuttings sites on forward and reverse strands.
        <br/>
        <br/>
        For WGBS, 
        <code>trim</code>
        is usually between 0~3.
      </doc>
    </parameter>
    <parameter name="ctSNP" type="string" default="correct">
      <doc>
        How to handle CT SNP when performing the methylation calling. Three possible modes of use: "no-action", "correct", "skip":
        <br/>
        - "correct": correct the methylation ratio according to the C/T SNP information estimated by the G/A counts on reverse strand
        <br/>
        -"skip": do not report loci with C/T SNP detected (i.e. detected A on reverse strand)
        <br/>
        - "no-action": do not consider C/T SNP.
      </doc>
    </parameter>
    <parameter name="optionsMethylCall" type="string" default="">
      <doc>
        Other options for methylation calling. This parameter is given as written to the aligner execution command. Example: "-g true" combines CpG methylaion ratio from both strands.
      </doc>
    </parameter>
    <parameter name="zeroMeth" type="boolean" default="true">
      <doc>
        Option to report loci with zero methylation ratios (i.e true -> report loci with zero methylation ratios, false -> report only loci with non-zero methylation ratios).
      </doc>
    </parameter>
    <parameter name="unique" type="boolean" default="false">
      <doc>
        Option to process only unique mappings/pairs (i.e true -> process only unique mappings/pairs, false -> process all aligned reads).
      </doc>
    </parameter>
    <parameter name="pair" type="boolean" default="true">
      <doc>
        Option to process only properly paired mappings (i.e true -> process only properly paired reads, false -> process all aligned reads).
      </doc>
    </parameter>
    <parameter name="removeDuplicate" type="boolean" default="false">
      <doc>
        Option to remove duplicated mappings to reduce PCR bias (i.e true -> remove duplicated mappings, false -> process all mappings). 
      <br/>
      <br/>
        This option should not be used on RRBS data. For WGBS, sometimes it's hard to tell if duplicates are caused by PCR due to high seqeuncing depth.
      </doc>
    </parameter>
    <parameter name="chr" type="string" default="all">
      <doc>
        Option to process only specified chromosomes. Chromosomes must be listed as comma separated values without spaces and in the form chr1,chrX and not only the chromosome number or identifier (X, Y or MT).
      <br/>
      <br/>
        example: 
        <code>chr</code>
        ="chr1,chr2" uses ~4.5GB compared with ~26GB for the whole genome.
      </doc>
    </parameter>
  </parameters>
</component>
