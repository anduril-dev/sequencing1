# running functions.sh 
set -e
source "$ANDURIL_HOME/bash/functions.sh"
export_command
export PATH=$PATH:../../lib/samtools:../../lib/bowtie:../../lib/bowtie2:

# inputs
echo reads    				$input_reads
echo Reference Genome 		$input_refGenome
echo mates					$input_mates

# parameters
echo paired 				$parameter_paired
echo aligner 				$parameter_aligner
echo directional			$parameter_directional
echo dupRemoval				$parameter_dupRemoval
echo deleteNonCpG			$parameter_deleteNonCpG
echo ignoreR1				$parameter_ignoreR1
echo ignoreR2				$parameter_ignoreR2
echo alignExtra				$parameter_alignExtra
echo extractExtra			$parameter_extractExtra
echo inputFormat			$parameter_inputFormat
echo extract				$parameter_extract


mkdir $output_analysis
temp_folder=$( gettempdir )

## check required binaries
if which samtools >/dev/null
then
	:   
else
    writeerror "samtools was not found. samtools should be in PATH"
    exit 1
fi

###### alignment step ######
alignCommand=$( readlink -f bismark )

# print version

alignCommandVersion="${alignCommand} --version"
$alignCommandVersion 2>> "${logfile}"

## aligner
if [ "$parameter_aligner" == "bowtie2" ] 
then
	alignCommand="${alignCommand} --bowtie2"
	if which bowtie2 >/dev/null
	then
		:   
	else
		writeerror "bowtie2 was not found. bowtie2 should be in PATH"
		exit 1
	fi
elif [ "$parameter_aligner" == "bowtie" ]
then
	if which bowtie >/dev/null
	then
		:   
	else
		writeerror "bowtie was not found. bowtie should be in PATH"
		exit 1
	fi
else
	writeerror "unknown aligner"
	exit 1
fi



## directional alignment
if [ "$parameter_directional" == "false" ]
then
	alignCommand="${alignCommand} --non_directional"
fi

## input format
if [ "$parameter_inputFormat" == "fastq" ]
then
	alignCommand="${alignCommand} --fastq"
elif [ "$parameter_inputFormat" == "fasta" ]
then
	alignCommand="${alignCommand} --fasta"
else
	writeerror "unknown input format"
	exit 1
fi

## optional switches
alignCommand="${alignCommand} ${parameter_alignExtra}"

## paired alignment
if [ "$parameter_paired" == "true" ]
then
	if [ ! -e "${input_mates}" ]
	then
		writeerror "mates input does not exist"
		exit 1
	fi
	alignCommand="${alignCommand} --bam --temp_dir ${temp_folder} -o ${output_analysis} ${input_refGenome} -1 ${input_reads} -2 ${input_mates}"
else
## single-end alignment
	alignCommand="${alignCommand} --bam --temp_dir ${temp_folder} -o ${output_analysis} ${input_refGenome} ${input_reads}"
fi

echo "Align command is: "$alignCommand
$alignCommand 2>> "${logfile}"
if [ $? != 0 ]
then
	writeerror "alignment failed"
	exit 1
fi

alignedFileName="$(ls ${output_analysis}/*.bam)"

echo "Alignment finished."


###### deduplication ######
if [ "$parameter_dupRemoval" == "true" ]
then
	dedupCommand=$( readlink -f deduplicate_bismark )
	
	# print version
	dedupCommandVersion="${alignCommand} --version"
	$dedupCommandVersion 2>> "${logfile}"

	if [ "$parameter_paired" == "true" ]
	then
		dedupCommand="${dedupCommand} -p"
	else
		dedupCommand="${dedupCommand} -s"
	fi
	dedupCommand="${dedupCommand} --bam ${alignedFileName}"
	
	echo "Deduplication command is: "$dedupCommand
	$dedupCommand 2>> "${logfile}"
	if [ $? != 0 ]
	then
		writeerror "Deduplication failed"
		exit 1
	fi
	alignedFileName="$(ls ${output_analysis}/*.deduplicated.bam)"
	echo "Deduplication Finished"
fi


###### Methylation extraction ######
extractCommand=$( readlink -f bismark_methylation_extractor )

# print version
extractCommandVersion="${alignCommand} --version"
$extractCommandVersion 2>> "${logfile}"

## paired
if [ "$parameter_paired" == "true" ]
then
	extractCommand="${extractCommand} -p"
else
	extractCommand="${extractCommand} -s"
fi
if [ "$parameter_extract" == "true" ] 
then
	## ignore
	if [ "$parameter_ignoreR1" != "0" ]
	then
		extractCommand="${extractCommand} --ignore ${parameter_ignoreR1}"
	fi

	if [ "$parameter_ignoreR2" != "0" -a "${parameter_paired}" == "true" ]
	then
		extractCommand="${extractCommand} --ignore_r2 ${parameter_ignoreR2}"
	fi

	##extra options
	extractCommand="${extractCommand} ${parameter_extractExtra}"
	extractCommand="${extractCommand} -o ${output_analysis} --report --counts --comprehensive --bedGraph ${alignedFileName}"
fi

if [ "$parameter_extract" == "false" ] 
then
	extractCommand="${extractCommand} -o ${output_analysis} --mbias_only ${alignedFileName}"
fi


echo "Methylation extraction command is: "$extractCommand
$extractCommand 2>> "${logfile}"
if [ $? != 0 ]
then
	writeerror "Methylation Extraction failed"
	exit 1
fi
	
echo "Methylation Extraction finished"

###### Cytosine report generation ######
if [ "$parameter_extract" == "true" ] 
then
	
	covFile=$(ls ${output_analysis}/*.cov)
	CreportCommand=$( readlink -f coverage2cytosine )
	CreportCommand="${CreportCommand} -o CReport.csv --dir ${output_analysis} --genome_folder ${input_refGenome} ${covFile}"

	echo "Cytosine report command is:" $CreportCommand
	$CreportCommand 2>> "${logfile}"
	if [ $? != 0 ]
	then
		writeerror "Cytosine report generation failed"
		exit 1
	fi
	echo "Generating Cytosine report has finished."
fi
 
###### report generation ######
reportCommand=$( readlink -f bismark2report )
reportCommand="${reportCommand} -o ${output_report}"
cd "${output_analysis}"
$reportCommand 2>> "${logfile}"

###### Generating outputs ######
if [ "$parameter_extract" == "true" ] 
then
	mv $(ls ${output_analysis}/*.bedGraph) $output_bedGraph
	cat ${output_analysis}/CReport.csv | awk '{if ($4+$5>0) print $1"\t"$2"\t"$3"\t"$4+$5"\t"$4/($4+$5)}' > $output_methylationCalls
else
	touch $output_bedGraph
	touch $output_methylationCalls
fi
mv "$alignedFileName" $output_alignment
mv $(ls ${output_analysis}/*.M-bias.txt) $output_Mbias

if [ "$parameter_extract" == "true" ] 
then
	if [ $parameter_deleteNonCpG == "true" ]
	then
		rm "${output_analysis}"/CH*
	fi


	if [ "$parameter_dupRemoval" == "true" ]
	then
		rm "${output_analysis}"/*.bam
	fi
fi
