library(componentSkeleton)

execute <- function(cf) {

    # Parameters + input
    param5 <- get.parameter(cf,"useMart_host", "string")    
    param4 <- get.parameter(cf,"keyType", "string")    
    param3 <- get.parameter(cf,"keyCol", "string")
    param2 <- get.parameter(cf,"attrTypes", "string")
    param1 <- get.parameter(cf, "biomart_dataset", "string")
    table1 <- CSV.read(get.input(cf, "geneList"))

    # If required package not installed, install it
    is.installed <- function(pkg) {
                           is.element(pkg, installed.packages()[,1])
                           }

    if (!is.installed("biomaRt")) {
                        print("Downloading required R package biomaRt")
                        update.packages(checkBuilt=TRUE, ask=FALSE)
                        source("http://bioconductor.org/biocLite.R")
                        biocLite("biomaRt")
                        }

    library(biomaRt)

    if (param4 == "") {
        param4 <- colnames(table1)[1] # If key column not provided, use the first column
    }

    if (param5 != "") {
        ensembl <- useMart(host=param5, biomart='ENSEMBL_MART_ENSEMBL', dataset=param1)
    } else {
        ensembl <- useMart("ensembl",dataset=param1) # Uses current version of Ensembl!
    }
    stm <- "SELECT * FROM"

    # Some IDs have decimal places and/or underscores. Remove them as they hinder sql look up.
    table1[,grep(param3,colnames(table1), ignore.case=TRUE)[1]] <-  gsub("\\.[0-9]|_[0-9]","",table1[,grep(param3,colnames(table1), ignore.case=TRUE)[1]])

    # Extract list of the DE miRNAs and the genes to look up
    genes <- unique(table1[,grep(param3,colnames(table1), ignore.case=TRUE)[1]])
    if (length(which(is.na(genes))) > 0) {
        genes <- genes[-which(is.na(genes))]
    }

    # Look up EnsemblIDs and ensure reference type is included in the output. Required for merging step (mergedDB)
    attributeType <- unique(c(unlist(strsplit(gsub("[[:blank:]]","",param2),",")),param4))
    filterType <-  param4 # Reference type
    ensemblIDs <- getBM(attributes=attributeType, filters=filterType,values=genes, mart=ensembl)

    # Attach IDs to reference table
    table1 <- merge(table1,ensemblIDs,by.x=param3,by.y=param4, all.x=TRUE,all.y=FALSE, suffixes = c("","IDs"))

    # Write outputs
    CSV.write(get.output(cf,"wIDs"),table1) 
    return(0)
}

main(execute)
