bash ../../lib/install-scripts/fastx_install.sh
bash ../../lib/install-scripts/wigToBigWig_install.sh
bash ../../lib/install-scripts/picard_install.sh
bash ../../lib/install-scripts/bwa_install.sh
bash ../../lib/install-scripts/tophat_install.sh
bash ../../lib/install-scripts/rna_seqc_install.sh
bash ../../lib/install-scripts/rseqc_install.sh
bash ../../lib/install-scripts/bedtools_install.sh
