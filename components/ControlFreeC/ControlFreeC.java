import fi.helsinki.ltdk.csbl.anduril.component.SkeletonComponent;
import fi.helsinki.ltdk.csbl.anduril.component.ErrorCode;
import fi.helsinki.ltdk.csbl.anduril.component.CommandFile;
import fi.helsinki.ltdk.csbl.anduril.core.utils.IOTools;
import fi.helsinki.ltdk.csbl.anduril.component.LatexTools;

import java.io.IOException;
import java.io.File;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * ControlFreeC
 *
 * @author Riku Louhimo
 *
 */
public class ControlFreeC extends SkeletonComponent {
    /* Command  */
    private String  COMMAND;
    private String  PLOT_COM;
    private String  CONF_FILE;
    private String  EXEC_DIR;

    /* Input files */
    private File reference;  // Input reference fasta file
    private File input;      // Input file
    private File lengthFile; // Chromosome length file
    private File mappability;
    private File SNPFile;
    private File GCcontentProfile;
    private File chrFiles; //
    private File cRegions; // Capture regions for exome input data
    
    /* Output files */
    private File    calls; //
    private File    windowRatios; //
    private File    rawSampleNProfile; //
    private File    rawControlNProfile; //
    private File    BAF; //
    private File    GCProfile; //
    private File    callsTmp; //
    private File    windowRatiosTmp; //
    private File    rawSampleNProfileTmp; //
    private File    rawControlNProfileTmp; //
    private File    BAFTmp; //
    private File    GCProfileTmp; //
    private File    plot; //

    /* Parameters */
    private String  freecCommand; // FREEC system command
    private String  memory; // Memory allocation 
    private String  inputFormat; //
    private String  gender;
    private String  mateOrientation;
    private String  uniqueMatch;
    private String  contaminationAdjustment;
    
    private int     ploidy; // Ploidy of case sample
    private int     breakPointType; // Type of break point
    private int     window;
    private int     degree;
    private int     minCNALength;
    private int     step;
    private int     forceGCcontentNormalization;
    private int     telocentromeric;
    private int     intercept;
    private int     readCountThreshold;
    
    private double  breakPointThreshold; /// threshold for calling a break point
    private double  coefficientOfVariation;
    private double  contamination;
    private double  minMappabilityPerWindow;
    
    private boolean verbose;
    private boolean noisyData;
    
    
    protected ErrorCode runImpl(CommandFile cf) throws Exception {
        getParameters(cf);
        // Create the command for FREEC
        CONF_FILE = cf.getTempDir().getAbsolutePath()+"/freecConf.tmp";
        COMMAND   = freecCommand + " -conf " + CONF_FILE;
        // Get inputs, outputs and parameters from command file
        getInsOuts(cf);
        // Create configuration file for FREEC (located in component temporary folder)
        createConfig(cf);
        // Execute FREEC
        System.out.println("Execution command is:\n" + COMMAND);
        int status;
        if (verbose){
            status = IOTools.launch(COMMAND, null, null, "", "");
        } else {
            status = IOTools.launch(COMMAND, null, null, null, "");
        }
        if (status > 0) return ErrorCode.ERROR;
        // Move freec output files to output ports.
        try {
            checkOutput(cf);
        } catch (IOException e){
            cf.writeError(e);
            return ErrorCode.OUTPUT_IO_ERROR;
        }
        // Do some plotting.
        ArrayList<String> latexDoc = (ArrayList<String>)LatexTools.formatFigure(
                                                        cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME)+"-copyNumberProfile.png",
                                                        null,
                                                        "Copy number profile of sample",
                                                        "!ht", 9.0);
        if (cf.inputDefined("SNPs")){
            PLOT_COM = "Rscript plot.R "+ploidy+" "+
                        cf.getOutput("plot")+" "+
                        //cf.getOutput("windowRatios").getName() +" "+
                        cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME)+"-copyNumberProfile "+ 
                        cf.getOutput("windowRatios")+" "+
                        cf.getOutput("BAF");
            latexDoc.addAll((ArrayList<String>)LatexTools.formatFigure(
                                               cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME)+"-copyNumberProfile-BAF.png",
                                               null,
                                               "B-allele frequency profile of sample",
                                               "!ht", 9.0));
        } else {
            PLOT_COM = "Rscript plot.R "+ploidy+" "+
                        cf.getOutput("plot")+" "+
                        //cf.getOutput("windowRatios").getName()+" "+
                        cf.getMetadata(CommandFile.METADATA_INSTANCE_NAME)+"-copyNumberProfile "+
                        cf.getOutput("windowRatios")+" "+
                        "FALSE";
        }
        status = IOTools.launch(PLOT_COM, null, null, null, "");
        if (status > 0) cf.writeLog("WARNING: Plotting failed. Continuing despite errors...");
        // LaTeX document
        LatexTools.writeDocument(plot, latexDoc);
        return ErrorCode.OK;
	}
	
	/**
	 * Checks the output
	 *
	 */
	private void checkOutput(CommandFile cf) throws IOException {
        try {
            if (!calls.exists())              calls.createNewFile();
            if (!BAF.exists())                BAF.createNewFile();
            if (!GCProfile.exists())          GCProfile.createNewFile();
            if (!windowRatios.exists())       windowRatios.createNewFile();
            if (!rawSampleNProfile.exists())  rawSampleNProfile.createNewFile();
            if (!rawControlNProfile.exists()) rawControlNProfile.createNewFile();
            move(callsTmp, calls);
            move(windowRatiosTmp, windowRatios);
            move(rawSampleNProfileTmp, rawSampleNProfile);
            if (cf.inputDefined("reference")) 
				move(rawControlNProfileTmp, rawControlNProfile);
            if (BAFTmp.exists())       move(BAFTmp, BAF);
            if (GCProfileTmp.exists()) move(GCProfileTmp, GCProfile);
        } catch (IOException exception) {
            throw exception;
        }
	}
	
	private void move(File fileIn, File fileOut) throws IOException {
        InputStream  in  = new FileInputStream(fileIn);
        OutputStream out = new FileOutputStream(fileOut);
	    try {
	        byte[] buf = new byte[1024];
	        int len;
	        while ((len = in.read(buf)) > 0) {
	            if (len > 0) out.write(buf, 0, len);
	        }
	    } catch (IOException e){
	        throw e;
	    } finally {
	        out.flush();
	        out.close();
	        in.close();
	        fileIn.delete();
	    }
	}

    /**
     * Gets inputs and outputs from the command file
     *
     */
    private void getInsOuts(CommandFile cf) throws IOException {
        if (cf.inputDefined("input")){
            input = cf.getInput("input");
        } else {
            cf.writeError("Input file is not present.");
        }
        if (cf.inputDefined("reference"))        reference        = cf.getInput("reference");
        if (cf.inputDefined("mappability"))      mappability      = cf.getInput("mappability");
        if (cf.inputDefined("SNPs"))             SNPFile          = cf.getInput("SNPs");
        if (cf.inputDefined("GCcontentProfile")) GCcontentProfile = cf.getInput("GCcontentProfile");
        if (cf.inputDefined("chrFiles"))         chrFiles         = cf.getInput("chrFiles");
        if (cf.inputDefined("captureRegions"))   cRegions         = cf.getInput("captureRegions");
        if (cf.inputDefined("lengthFile")){
            lengthFile = cf.getInput("lengthFile");
        } else {
            lengthFile = new File(cf.getParameter("lengthFile"));
        }
        
        calls              = cf.getOutput("calls");
        BAF                = cf.getOutput("BAF");
        GCProfile          = cf.getOutput("GCProfile");
        windowRatios       = cf.getOutput("windowRatios");
        rawSampleNProfile  = cf.getOutput("rawSampleNProfile");
        rawControlNProfile = cf.getOutput("rawControlNProfile");
        plot               = cf.getOutput("plot");
        EXEC_DIR = calls.getParent();
        
        if (!plot.exists() && !plot.mkdirs())
            throw new IOException("Cannot create output folder: "+plot.getCanonicalPath());
        
        callsTmp              = new File(EXEC_DIR+"/"+cf.getInput("input").getName()+"_CNVs");
        BAFTmp                = new File(EXEC_DIR+"/"+cf.getInput("input").getName()+"_BAF.txt");
        GCProfileTmp          = new File(EXEC_DIR+"/GC_profile.cpn");
        windowRatiosTmp       = new File(EXEC_DIR+"/"+cf.getInput("input").getName()+"_ratio.txt");
        rawSampleNProfileTmp  = new File(EXEC_DIR+"/"+cf.getInput("input").getName()+"_sample.cpn");
        if (cf.inputDefined("reference")) 
			rawControlNProfileTmp = new File(EXEC_DIR+"/"+cf.getInput("reference").getName()+"_control.cpn");
    }

    /**
     * Gets parameters from the command file
     *
     */
    private void getParameters(CommandFile cf) {
        freecCommand     = cf.getParameter("freecCommand");
        if (freecCommand.equals("")) freecCommand = "freec";
        memory           = cf.getParameter("memory");
        inputFormat      = cf.getParameter("inputFormat");
        ploidy           = cf.getIntParameter("ploidy");
        window           = cf.getIntParameter("window");
        noisyData		 = cf.getBooleanParameter("noisyData");
        intercept        = cf.getIntParameter("intercept");
        degree           = cf.getIntParameter("degree");
        gender           = cf.getParameter("gender");
        step             = cf.getIntParameter("step");
        verbose          = cf.getBooleanParameter("verbose");
        if (cf.getBooleanParameter("uniqueMatch")){ uniqueMatch = "TRUE"; } else { uniqueMatch = "FALSE";}
        contamination    = cf.getDoubleParameter("contamination");
        minCNALength     = cf.getIntParameter("minCNAlength");
        coefficientOfVariation      = cf.getDoubleParameter("coefficientOfVariation");
        forceGCcontentNormalization = cf.getIntParameter("forceGCcontentNormalization");
        minMappabilityPerWindow     = cf.getDoubleParameter("minMappabilityPerWindow");
        telocentromeric             = cf.getIntParameter("telocentromeric");
        breakPointThreshold         = cf.getDoubleParameter("breakPointThreshold");
        breakPointType              = cf.getIntParameter("breakPointType");
        mateOrientation             = cf.getParameter("mateOrientation");
        readCountThreshold          = cf.getIntParameter("readCountThreshold");
        if (cf.getBooleanParameter("contaminationAdjustment")){ 
            contaminationAdjustment = "TRUE";
        } else {
            contaminationAdjustment = "FALSE";
        }
    }

   
    /**
     * Creates the configuration file.
     */
    private void createConfig(CommandFile cf){
        try {
            OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(CONF_FILE));
            // Parameters
            out.write("[general]");
            out.append('\n').write("chrLenFile = "); out.write(lengthFile.getAbsolutePath());
            out.append('\n').write("ploidy = ");     out.write(Integer.toString(ploidy));
            if (cf.inputDefined("GCcontentProfile")){
                out.append('\n').write("GCcontentProfile = "); out.write(GCcontentProfile.getAbsolutePath());
                out.append('\n').write("intercept = 1");
            } else if (cf.inputDefined("chrFiles")){
                out.append('\n').write("chrFiles = "); out.write(chrFiles.getAbsolutePath());
                out.append('\n').write("intercept = 1");
            } else if (cf.inputDefined("reference")){
                out.append('\n').write("intercept = 0");
            } 
            out.append('\n').write("degree = ");     out.write(Integer.toString(degree));
            out.append('\n').write("gender = ");     out.write(gender);
            out.append('\n').write("uniqueMatch= "); out.write(uniqueMatch);
            out.append('\n').write("minCNAlength ="); out.write(Integer.toString(minCNALength));
            out.append('\n').write("forceGCcontentNormalization = "); out.write(Integer.toString(forceGCcontentNormalization));
            out.append('\n').write("minMappabilityPerWindow = "); out.write(Double.toString(minMappabilityPerWindow));
            out.append('\n').write("telocentromeric = ");         out.write(Integer.toString(telocentromeric));
            out.append('\n').write("breakPointThreshold = ");     out.write(Double.toString(breakPointThreshold));
            out.append('\n').write("breakPointType = ");          out.write(Integer.toString(breakPointType));
            out.append('\n').write("readCountThreshold = ");      out.write(Integer.toString(readCountThreshold));
            out.append('\n').write("memory = ");                  out.write(memory);
            out.append('\n').write("noisyData = ");				  out.write(Boolean.toString(noisyData).toUpperCase());
            if (noisyData) out.append('\n').write("printNA = FALSE");
            out.append('\n').write("intercept = ");               out.write(Integer.toString(intercept));
            
            if (contamination >= 0){
                out.append('\n').write("contamination = "); out.write(Double.toString(contamination));
                out.append('\n').write("contaminationAdjustment = TRUE");
            } else {
                out.append('\n').write("contaminationAdjustment = "); out.write(contaminationAdjustment);
            }
            if (window < 0){
                out.append('\n').write("coefficientOfVariation = "); out.write(Double.toString(coefficientOfVariation));
            } else {
                out.append('\n').write("window = ");  out.write(Integer.toString(window));
                if (step >= 0){
                    out.append('\n').write("step = ");    out.write(Integer.toString(step));
                }
            }
            if (cf.inputDefined("mappability")){
                out.append('\n').write("gemMappabilityFile = ");
                out.write(mappability.getAbsolutePath());
            }
            out.append('\n').write("outputDir = "); out.write(EXEC_DIR);
            
            // Sample
            out.append('\n').write("[sample]");
            out.append('\n').write("mateFile = ");        out.write(input.getAbsolutePath());
            out.append('\n').write("inputFormat = ");     out.write(inputFormat);
            out.append('\n').write("mateOrientation = "); out.write(mateOrientation);
            // Control
            out.append('\n').write("[control]");
            if (cf.inputDefined("reference")){
                out.append('\n').write("mateFile = ");        out.write(reference.getAbsolutePath());
                out.append('\n').write("inputFormat = ");     out.write(inputFormat);
                out.append('\n').write("mateOrientation = "); out.write(mateOrientation);
            }
            // BAF
            if (cf.inputDefined("SNPs")){
                out.append('\n').write("[BAF]");
                out.append('\n').write("SNPfile = "); out.write(SNPFile.getAbsolutePath());
                out.append('\n').write("minimalCoveragePerPosition = 0");
            }
            // capture regions
            if (cf.inputDefined("captureRegions")){
                out.append('\n').write("[target]");
                out.append('\n').write("captureRegions = "); out.write(cRegions.getAbsolutePath());
            }
            out.close();
        } catch (IOException e){
            cf.writeError(e);
        }
    }
    

    public static void main(String[] args) {
        new ControlFreeC().run(args);
    }
}
