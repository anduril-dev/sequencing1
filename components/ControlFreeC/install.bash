#!/bin/bash

. $ANDURIL_HOME/bash/functions.sh no_command

NAME=ControlFreeC
URL="http://bioinfo-out.curie.fr/projects/freec/src/FREEC_Linux64.tar.gz"
TGTDIR=../../lib/ControlFreeC
TMPFILE=../../lib/ControlFreeC/FREEC_Linux64.tar.gz

[[ -d "$TGTDIR" ]] && {
    echo $NAME already installed
    echo $( readlink -f "$TGTDIR" ) exists.
    exit 0
}

iscmd tar || exit 1
iscmd wget || exit 1

mkdir -p "$TGTDIR" || {
    echo Cannot create directory 
    exit 1
}

echo Downloading...
wget -nc -O "$TMPFILE" "$URL"
echo Unarchiving...

tar -zxvf "$TMPFILE" -C "$TGTDIR"

echo Change access rights...
chmod -R ugo+rX "$TGTDIR" 
rm "$TMPFILE"
