#!/bin/bash
set -ex

# running functions.sh also sets logfile and errorfile.
source "$ANDURIL_HOME/bash/functions.sh"
export_command

# You can use a special temporary directory:
# Temporary folder is not automatically deleted if component exits with error.
#tempdir=$( gettempdir )
#echo "Hello" > "$tempdir"/tempfile

# Check that programs exist and are functional

CGQUERY=$( which cgquery )
if ( $CGQUERY --version | grep -i cgquery )
    then echo "cgquery found."
else
    msg="cgquery not found, or perhaps not properly installed ( $CGQUERY )"
    writeerror "$msg"
    exit 1
fi

GTDOWNLOAD=$( which gtdownload )
if ( $GTDOWNLOAD --version | grep -i gtdownload )
    then echo "gtdownload found."
else
    msg="gtdownload not found, or perhaps not properly installed ( $GTDOWNLOAD )"
    writeerror "$msg"
    exit 1
fi

# errors may be handled with the || operator
$CGQUERY "${parameter_cgquery}" "${parameter_cgqueryOptions}" "$parameter" "$infile" -o ${output_queryResults} || writelog "Error in querying." 

mkdir -p "${output_data}"
if [ "$parameter_queryOnly" == "false" ] ; then
    cd "${output_data}"
    $GTDOWNLOAD  -c "${input_credentials}" ${parameter_gtdownloadOptions} 
fi

