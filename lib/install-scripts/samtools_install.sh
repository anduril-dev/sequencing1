#!/bin/bash

set -e

. $ANDURIL_HOME/bash/generic.sh
NAME=samtools
VERSION=1.2
URL="http://downloads.sourceforge.net/project/samtools/samtools/${VERSION}/samtools-${VERSION}.tar.bz2"
cd "$( dirname $0 )"
if [ -f "../${NAME}"/samtools ]
then
    echo "${NAME} already installed"
else
    echo "${NAME} not found"
    cd ..
    wget -N $URL
    tar xvjf ${NAME}-${VERSION}.tar.bz2
    rm -f ${NAME}-${VERSION}.tar.bz2
    ln -Tfs ${NAME}-${VERSION} ${NAME}
    cd ${NAME}
    make
fi

     
