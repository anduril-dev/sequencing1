#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=bwa
VERSION="0.7.12"
URL="http://sourceforge.net/projects/bio-bwa/files/bwa-0.7.12.tar.bz2/download"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/bwa" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O - $URL | tar xj
ln -Tsf "${NAME}-${VERSION}" $NAME
cd "$NAME"
make
