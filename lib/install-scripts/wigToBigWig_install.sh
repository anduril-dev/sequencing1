#!/bin/bash

set -e
P=$( readlink -f $( dirname $0 ) )
NAME=wigToBigWig
URL="http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/wigToBigWig"

cd "$P"
if [ ! -e ../$NAME ]
then 
    cd ..
    wget -N $URL
    chmod 755 $NAME
fi
     
 

