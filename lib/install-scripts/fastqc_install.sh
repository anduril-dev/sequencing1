#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=fastqc
VERSION="FastQC v0.11.3"
URL="http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.3.zip"
cd "$( dirname $0 )"
iscmd "${NAME}" && {
    echo "${NAME} already installed"
    exit 0
}
echo "${NAME} not found in PATH"
cd ..
[[ $( FastQC/fastqc -v ) = "$VERSION" ]] && {
        echo "${VERSION} already installed"
        exit 0
}
rm -rf FastQC

wget -O $NAME.zip $URL
unzip $NAME.zip
chmod 755 FastQC/fastqc
rm $NAME.zip     
