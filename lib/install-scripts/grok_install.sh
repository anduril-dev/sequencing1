#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=grok
VERSION="1.1.1"
URL="http://csbi.ltdk.helsinki.fi/grok/files/${NAME}-${VERSION}.tar.gz"
cd "$( dirname $0 )"
[[ -d ../"${NAME}-${VERSION}/python" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O - $URL | tar xz
ln -Tsf "${NAME}-${VERSION}" $NAME
cd $NAME
./configure PYTHON_INCLUDE_DIR=/usr/include/python2.7
make python
