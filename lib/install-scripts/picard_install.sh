#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=picard
VERSION="1.141"
URL="https://github.com/broadinstitute/picard/releases/download/${VERSION}/picard-tools-${VERSION}.zip"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-tools-${VERSION}/SamToFastq.jar" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-tools-${VERSION}"
wget -O $NAME.zip $URL
unzip $NAME.zip
rm $NAME.zip     
ln -Tsf "${NAME}-tools-${VERSION}" $NAME
