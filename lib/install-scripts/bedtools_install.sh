#!/bin/bash

set -e

. $ANDURIL_HOME/bash/generic.sh
NAME=bedtools
VERSION=2.25.0
URL=" https://github.com/arq5x/bedtools2/releases/download/v${VERSION}/bedtools-${VERSION}.tar.gz"
cd "$( dirname $0 )"
if iscmd "${NAME}"
then
    echo "${NAME} already installed"
else
    echo "${NAME} not found in PATH"
    cd ..
    [ -d ${NAME}-${VERSION} ] && {
        echo "${NAME}-${VERSION} already installed"
        exit 0
    }

    wget -N $URL
    tar -zxvf ${NAME}-${VERSION}.tar.gz
    rm -f ${NAME}-${VERSION}.tar.gz
    ln -Tfs ${NAME}2 ${NAME}
    cd ${NAME}
    make
fi

     
