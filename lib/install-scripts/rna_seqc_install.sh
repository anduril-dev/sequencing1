#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=RNA-SeQC
VERSION="1.1.8"
URL="http://www.broadinstitute.org/cancer/cga/tools/rnaseqc/RNA-SeQC_v${VERSION}.jar"
cd "$( dirname $0 )"
unzip -t ../java/"${NAME}_v${VERSION}.jar" > /dev/null && {
    echo "${NAME} already installed"
    exit 0
}
cd ../java
wget "$URL"
ln -sfT RNA-SeQC_v${VERSION}.jar RNA-SeQC.jar
