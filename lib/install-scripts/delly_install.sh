#!/bin/bash

set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=delly
VERSION=0.6.7
URL="https://github.com/tobiasrausch/delly/releases/download/v0.6.7/delly_v0.6.7_linux_x86_64bit"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..

echo Downloading...
wget -O "${NAME}-${VERSION}" "$URL"
echo Change access rights...
chmod ugo+rx "${NAME}-${VERSION}"
ln -Tsf "${NAME}-${VERSION}" $NAME

