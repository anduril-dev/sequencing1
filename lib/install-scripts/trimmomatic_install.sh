#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=Trimmomatic
VERSION="0.33"
URL="http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-$VERSION.zip"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/trimmomatic-${VERSION}.jar" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O $NAME.zip $URL
unzip $NAME.zip
rm $NAME.zip
ln -Tsf "${NAME}-${VERSION}" $NAME
ln -Tsf ../"${NAME}-${VERSION}"/trimmomatic-${VERSION}.jar java/$NAME.jar
