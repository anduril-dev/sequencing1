#!/bin/bash
set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh
NAME=RSeQC
VERSION="2.6.2"
URL="http://sourceforge.net/projects/rseqc/files/RSeQC-2.6.2.tar.gz/download"
cd "$( dirname $0 )"
[[ -e ../"${NAME}/bin/inner_distance.py" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}" "${NAME}"
wget -O - $URL | tar xz
cd "${NAME}-${VERSION}"
python setup.py install --root=../"${NAME}"
cd ../"${NAME}"
ln -sf usr/local/bin/
ln -sfT usr/local/lib/*/dist-packages/ lib
echo "$VERSION" > VERSION
rm -r ../"${NAME}-${VERSION}"

echo Add to PATH:  $( readlink -f . )/bin 
echo Add to PYTHONPATH: $( readlink -f . )/lib

