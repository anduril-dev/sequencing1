#!/bin/bash

set -e
[[ -e $ANDURIL_HOME/bash/generic.sh ]] && . $ANDURIL_HOME/bash/generic.sh
[[ -e $ANDURIL_HOME/lang/bash/generic.sh ]] && . $ANDURIL_HOME/lang/bash/generic.sh

NAME=tophat
VERSION=2.1.0.Linux_x86_64
URL="http://ccb.jhu.edu/software/tophat/downloads/tophat-2.1.0.Linux_x86_64.tar.gz"
cd "$( dirname $0 )"
[[ -e ../"${NAME}-${VERSION}/tophat" ]] && {
    echo "${NAME} already installed"
    exit 0
}
cd ..
rm -rf "${NAME}-${VERSION}"
wget -O $NAME.tgz $URL
tar xf $NAME.tgz
rm $NAME.tgz
ln -Tsf "${NAME}-${VERSION}" $NAME

