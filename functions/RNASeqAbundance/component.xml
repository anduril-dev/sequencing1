<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>RNASeqAbundance</name>
    <version>1.0</version>
    <doc>
        Performs RNA-Seq transcript assembly, quantification and/or differential expression analysis.
        It is based on Cufflinks and combines the usage of cufflinks, cuffmerge, cuffcompare and cuffdiff.
        
        How to use it (refer to the flowchart if needed):
        
        Discovery mode:
        - For finding new transcripts (not in the annotation file) from the RNA-Seq data set, then the deNovo parameter should be set to true, 
        and cufflinks_options parameter should include the parameters that you want to give to cufflinks. If the parameter requires an input 
        file, then use the designated input and it will be added to the command for cufflinks. Uses cufflinks for transcript assembly and quantification.

        - If you have only one sample, then cuffcompare is needed for comparing the new transcripts to an annotation file. 
        Use cuffcompare_options to provide parameters for that.

        - If you have more than one sample then you need cuffmerge. Use cuffmerge_options for setting parameters if needed.
        
        - If you have more than one sample and a time series or different conditions and you want differential expression analysis, then cuffmerge and 
        cuffdiff are needed. Provide parameters for them if you do not want the default ones using cuffmerge_options and cuffdiff_options. 

        No discovery mode:
        - If you just want to quantify the known transcripts you only need cufflinks and you can set the parameters using cufflinks_options.

        - If you want differential expression analysis then you need cuffdiff and you can set the parameters using cuffdiff_options.
         
        <p>
            <img src="flowchart.jpg"/>
        </p>
    </doc>
    <author email="alejandra.cervera@helsinki.fi">Alejandra Cervera</author>
    <author email="lauri.lyly@helsinki.fi">Lauri Lyly</author>

    <inputs>
    	<input name="files" type="BinaryFile" array="true">
            <doc>
                FIXME: Explain key naming scheme
            </doc>
        </input>
        <input name="annotation" type="GTF" optional="true">
            <doc>
                A gtf file for annotating the expressed genes/transcripts.
            </doc>
        </input>
        <input name="reference" type="FASTA" optional="true">
            <doc>
                A reference genome for frag bias correction. When the file is supplied the corresponding parameter is automatically
                included in the command.
            </doc>
        </input>
    </inputs>
    <outputs>
        <output name="array" type="BinaryFile" array="true">
            <doc>
                All output files.
        	</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="deNovo" type="boolean" default="false">
            <doc>
                Possible new isoforms (not in the annotation file) can be identified when set to true, otherwise only the transcripts
                in the annotation file are quantified.
            </doc>
        </parameter>
        <parameter name="diffExp" type="boolean" default="false">
            <doc>
                When false transcripts will only be quantified, when true differential expression is performed on samples using conditions 
                specified in the files input array.
            </doc>
        </parameter>
        <parameter name="options_cufflinks" type="string" default="--quiet --multi-read-correct --num-threads 1">
            <doc>
                Parameters for cufflinks, it should not include parameters that require an input file (such as frag bias correction).
            </doc>
        </parameter>
        <parameter name="options_cuffdiff" type="string" default="">
            <doc>
                Parameters for cuffdiff, it should not include parameters that require an input file (such as frag bias correction).
            </doc>
        </parameter>
        <parameter name="options_cuffcompare" type="string" default="">
            <doc>
                Parameters for cuffcompare.
            </doc>
        </parameter>
        <parameter name="options_cuffmerge" type="string" default="">
            <doc>
                Parameters for cuffmerge.
            </doc>
        </parameter>
    </parameters>
</component>

