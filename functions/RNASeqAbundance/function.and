#!/usr/bin/env anduril-runner
//$ -b /opt/anduril/sequencing

/*
  RNA-seq expression function 
  To make this script more legible, put this in your $HOME/.vimrc file:

au BufRead,BufNewFile *.and set filetype=anduril
au! Syntax anduril source /opt/anduril/doc/anduril.vim
source /opt/anduril/doc/anduril_tweaks.vim

Remember to replace /opt/anduril with your ANDURIL_HOME

The overall logic of this script is documented as a flowchart.
Please see it before attempting to comprehend.

*/

function RNASeqAbundance {
	std.echo(std.strReplace("""Starting RNASeqAbundance pipeline with
\tdeNovo = @deNovo
\tdiffExp = @diffExp
\toptions_cufflinks = @options_cufflinks
\toptions_cuffdiff = @options_cuffdiff
\toptions_cuffmerge = @options_cuffmerge
\toptions_cuffcompare = @options_cuffcompare

Reference will be passed to Cuffdiff and Cufflinks.
""",
    "@deNovo", deNovo,
    "@diffExp", diffExp,
    "@options_cufflinks", options_cufflinks,
    "@options_cuffdiff", options_cuffdiff,
    "@options_cuffmerge", options_cuffmerge,
    "@options_cuffcompare", options_cuffcompare ))

	cufflinks=false
	if (deNovo) {
		cufflinks=true
	} else {
		if (diffExp==false) {
			cufflinks=true
		}
	}

    // These variables are passed to BashEvaluates
    vars = { "annotation"=annotation, "dummy"=annotation } // Dummy is to make Anduril believe it should be converted into an array

    if ( reference != null ) {
        options_cuffdiff = options_cuffdiff + " -b " 
        options_cufflinks = options_cufflinks + " -b "

        vars["reference"]=reference
    }
   

	// Create alphabetically sorted CSV of array keys classified by a prefix before the underscore.
	// Keys that do not have an underscore have as their class the full key.
	// Also the number of keys with the same prefix is reported on each line.
	// CSV has header: "prefix" "key" "replicates"
	validate_py=StringInput(content='''if True:
        from anduril import *
        files_by_prefix={}
        for key in sorted(array):
            try:
                prefix = key[0 : key.index("_")] # Throws if _ not found.
                if not prefix in files_by_prefix:
                    files_by_prefix[prefix]=[]
                files_by_prefix[prefix].append(key)
            except ValueError:
                files_by_prefix[key]=[key]

        table=TableWriter(table, ["prefix", "key", "replicates"])
        for prefix in files_by_prefix:
            file_list = files_by_prefix[prefix]
            for file in file_list:
                table.writerow({"prefix": prefix, "key": array[file], "replicates":len(file_list)})
	''')

	validated=PythonEvaluate(script=validate_py, array=files)

	// Cufflinks is always the first step, if needed at all
	cufflinks_sh=StringInput(content='''
        # Run cufflinks.
        # param1: Cufflinks options
        set -e
        cd @folder1@ 

        old_files="$(ls -1)"

        command="cufflinks "${aligned_reads}" $(eval echo @param1@) @var1@" # Eval to unquote option string. Guess we could just std.strReplace it but...
        echo "Command is:\n" $command
        $command

        # Produce output array with only the new files
        new_files=$(comm -2 -3 <(ls -1) <(echo "$old_files"))
        for f in $new_files; do
            echo -e ${f}"\t"$( readlink -f ${f} ) >> @arrayIndex1@ 
        done

        mv transcripts.gtf @optOut1@; ln -s $(basename @optOut1@) transcripts.gtf
	''')

	cufflinks_results = record()
	if ( cufflinks ) {
		for rec: std.iterArray(files) {
			std.echo("Using file " + rec.file)
            vars["aligned_reads"]=files[rec.key]
			cufflinks_results[rec.key] = BashEvaluate(script=cufflinks_sh, var1=reference, param1=options_cufflinks, vararray=vars  ) // transcripts: GTF, isoforms: FPKM_tracking, genes: FPKM_tracking
		}
	}

	if ( deNovo == false && diffExp == false ) {
		// Return just transcript from cufflinks
		// FIXME: Or should they be merged instead anyway
		// in order to return them?
		cufflinks_transcripts=record()
		for key, value: cufflinks_results {
			cufflinks_transcripts[key]=value.optOut1
		}

		return std.makeArray(cufflinks_transcripts) // Exit NO-NO
	}

	cuffdiff_sh=StringInput(content='''
        # Run cuffdiff. Has loads of outputs, so they should be arrayed?
        # var1: Annotation GTF file
        # param1: Cuffdiff options
        # param2: String in form: A,A B,B C  for replicates of A, B and C etc.
        set -e
        cd @folder1@ 

        old_files="$(ls -1)"

        command="cuffdiff ${annotation} $(eval echo @param2@) $(eval echo @param1@) @var1@"
        echo -e "Command is:\n" $command
        $command

        # Produce output array with only the new files
        new_files=$(comm -2 -3 <(ls -1) <(echo "$old_files"))
        for f in $new_files; do
            echo -e ${f}"\t"$( readlink -f ${f} ) >> @arrayIndex1@ 
        done
	''')

	if ( deNovo == false && diffExp == true ) {
		// String for cuffDiff
		rep_string="" // String that has all the samples/conditions/replicates
		last_cond=""
		for row: std.itercsv(validated.table) {
			// CSV has header: "prefix" "key" "replicates"
			if ( row.prefix != last_cond ) {
				// New condition, append
				// CSV has header: "prefix" "key" "replicates"
				rep_string=rep_string+" "+row.key
				last_cond=row.prefix
			} else {
				rep_string=rep_string+","+row.key
			}
		}

		cuffdiff_results = BashEvaluate(script=cuffdiff_sh, var1=reference, vararray=vars, param1=options_cuffdiff, param2=rep_string )
		return cuffdiff_results.array1 // EXIT NO-YES
	}
	
	// From here on, deNovo is always true. Cufflinks has already been run.
	// So now we want to know whether we have replicates OR diffExp

	have_replicates=false
	for row: std.itercsv(validated.table) {
		// CSV has header: "prefix" "key" "replicates"
		if ( row.replicates != 1 ) {
			have_replicates=true
		}
	}

	// Needed for Cuffmerge or Cuffcompare
	cufflinks_transcripts=record()
	for key, value: cufflinks_results {
		cufflinks_transcripts[key]=value.optOut1
	}

	// YES-YES-NO/YES
	if ( have_replicates==true || diffExp==true ) {
		cuffmerge_sh=StringInput(content='''
            # Run cuffmerge. Has essentially same outputs as Cufflinks.
            # var2: SequenceSet. FIXME: Missing currently. This argument should point to the genomic DNA sequences for the reference. If a directory, it should contain one fasta file per contig. If a multifasta file, all contigs should be present.
            # param1: Cuffmerge options
            # param2: String in form: A,A B,B C  for replicates of A, B and C etc.
            set -e
            cd @folder1@
            keys=( $( getarraykeys array1 ) )
            files=( $( getarrayfiles array1 ) )

            for (( i=0; i<${#keys[@]};i++ ))
            do
                echo ${files[$i]} >> manifest
            done
            #cuffmerge -g @var1@ -s /opt/anduril/sequencing/components/Cuffmerge/testcases/case1/input/ref_seq manifest

            old_files="$(ls -1)"
            command="cuffmerge -o $(dirname @optOut1@) -g $annotation manifest $(eval echo @param1@)"
            echo "Command is:\n" $command
            $command

            # Produce output array with only the new files
            new_files=$(comm -2 -3 <(ls -1) <(echo "$old_files"))
            for f in $new_files; do
                echo -e ${f}"\t"$( readlink -f ${f} ) >> @arrayIndex1@ 
            done

            mv transcripts.gtf @optOut1@; ln -s $(basename @optOut1@) transcripts.gtf
		''')
		cuffmerge_results = BashEvaluate(array1=cufflinks_transcripts, script=cuffmerge_sh, vararray=vars, param1=options_cuffmerge)
		if ( diffExp==false ) {
			return cuffmerge_results.array1 // EXIT YES-YES-NO
		}

		// String for cuffDiff
		rep_string="" // String that has all the samples/conditions/replicates
		last_cond=""
		for row: std.itercsv(validated.table) {
			// CSV has header: "prefix" "key" "replicates"
			if ( row.prefix != last_cond ) {
				// New condition, append
				// CSV has header: "prefix" "key" "replicates"
				rep_string=rep_string+" "+row.key
				last_cond=row.prefix
			} else {
				rep_string=rep_string+","+row.key
			}
		}

        vars["annotation"]=cuffmerge_results.optOut1
		cuffdiff_results = BashEvaluate(script=cuffdiff_sh, vararray=vars, param1=options_cuffdiff, param2=rep_string )
		return cuffdiff_results.array1 // EXIT YES-YES-YES 

	} else {
		// YES-NO
		cuffcompare_sh=StringInput(content='''
            # Run cuffcompare.
            # param1: Cuffcompare options
            set -e
            cd @folder1@
            keys=( $( getarraykeys array1 ) )
            files=( $( getarrayfiles array1 ) )

            str="" # Filenames space-separated
            for (( i=0; i<${#keys[@]};i++ )); do
                str=${str}" "${files[$i]}
            done

            old_files="$(ls -1)"
            command="cuffcompare -r $annotation $(eval echo @param1@) ${str}"
            echo "Command is:\n" $command
            $command

            # Produce output array with only the new files
            new_files=$(comm -2 -3 <(ls -1) <(echo "$old_files"))
            for f in $new_files; do
                echo -e ${f}"\t"$( readlink -f ${f} ) >> @arrayIndex1@ 
            done
		''')

		cuffcompare_results = BashEvaluate(array1=cufflinks_transcripts, script=cuffcompare_sh, vararray=vars, param1=options_cuffcompare)
		return cuffcompare_results.array1
	}
}

/*
A1=INPUT(path="/opt/anduril/sequencing/components/Cufflinks/testcases/case1/input/alignment.sam")
A2=INPUT(path="/opt/anduril/sequencing/components/Cuffdiff/testcases/case2/input/array/some.sam")
B1=INPUT(path="/opt/anduril/sequencing/components/Cuffdiff/testcases/case2/input/array/some.sam")
B2=INPUT(path="/opt/anduril/sequencing/components/Cuffdiff/testcases/case2/input/array/some1.sam")
C=INPUT(path="/opt/anduril/sequencing/components/Cuffdiff/testcases/case2/input/array/some1.sam")
D=INPUT(path="/opt/anduril/sequencing/components/Cuffdiff/testcases/case2/input/array/some1.sam")

annotation=INPUT(path="/opt/anduril/sequencing/components/Cuffdiff/testcases/case2/input/transcripts.gtf")

testArray = record(CONDA_1=A1, CONDC=C)
testArray2 = record(CONDA_2=A2, CONDB_1=B1, CONDB_2=B2)
r = record( CONDE_=D )
combined=std.makeArray(testArray, testArray2, r)

norepsArray = record(CONDA_1=A1, CONDB=C)
norepsArray2 = record(CONDC_2=A2, CONDD_1=B1, CONDE_2=B2)
noreps = record( CONDF_=D )

combinedNoReps=std.makeArray(norepsArray, norepsArray2, noreps)

test1=RNASeqAbundance(files=combined, annotation=annotation, reference=null, deNovo=false, diffExp=false)
test2=RNASeqAbundance(files=combined, annotation=annotation, reference=null, deNovo=false, diffExp=true)
test3=RNASeqAbundance(files=combined, annotation=annotation, reference=null, deNovo=true, diffExp=false)
test4=RNASeqAbundance(files=combined, annotation=annotation, reference=null, deNovo=true, diffExp=true)
test5=RNASeqAbundance(files=combinedNoReps, annotation=annotation, reference=null, deNovo=true, diffExp=false)

OUTPUT(test1)
OUTPUT(test2)
OUTPUT(test3)
OUTPUT(test4)
OUTPUT(test5)
*/

