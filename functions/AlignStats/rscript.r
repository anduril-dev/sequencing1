# parse table2
tmpList <- list()
for(i in 1:nrow(table2)){
	tmpList[[i]] <- strsplit(table2[i,1]," +")[[1]]
}
readDist <- do.call(rbind,tmpList)
colnames(readDist) <- c("Group","Total_bases","Tag_count","Tags/Kb")
rownames(readDist) <- readDist[,1]

# ratio
m3vs5 <- as.numeric(readDist["3'UTR_Exons","Tags/Kb"])/as.numeric(readDist["5'UTR_Exons","Tags/Kb"])
m3vsCDS <- as.numeric(readDist["3'UTR_Exons","Tags/Kb"])/as.numeric(readDist["CDS_Exons","Tags/Kb"])
mCDSvs5 <- as.numeric(readDist["CDS_Exons","Tags/Kb"])/as.numeric(readDist["5'UTR_Exons","Tags/Kb"])

# combine
selected <- c("Mapped","Mapped Unique","Mapped Unique Rate of Total","Mapped Pairs","Unpaired Reads","Duplication Rate of Mapped","rRNA","rRNA rate","Intragenic Rate","Exonic Rate","Intronic Rate","Intergenic Rate","Genes Detected")

table.out <- cbind(SampleID=param1,table1[,selected],End3vsEnd5=m3vs5,End3vsCDS=m3vsCDS,mCDSvsEnd5=mCDSvs5)

