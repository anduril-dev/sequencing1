myArray <- Array.read(cf,"var1")
instanceName <- param1

size <- Array.size(myArray)

document.out <- ""

for (i in 1:size) { 
    key <- Array.getKey(myArray,i)
    if (length(i <- grep("pdf", key))) {
        s <- unlist(strsplit(key,"\\."))
        if (length(grep("_", s[2]))) {
            t <- gsub("_", "", s[2])
        }
        else {
            t <- s[2]
        }
        new_name=paste(t, "_", instanceName, ".pdf",sep="")

        file <- Array.getFile(myArray,key)
        n <- basename(file)
        
        b <- latex.figure(filename=new_name)
        myDir = get.output(cf,'document')
        a <- paste(myDir,"/",new_name,sep="")
        file.copy(file,a)
        document.out <- c(document.out,b)
    }
}

table.out <- data.frame(c())

