declare -A @param2@

# Complete path to folder1/unmapped.fa
fastq_to_fasta -i @param1@ -v -o @folder1@/unmapped.fasta ${mirD[Qscore]}
fastx_collapser -i @folder1@/unmapped.fasta -o @folder1@/unmapped.fa ${mirD[Qscore]}
echo "NovelMirnas: Conversion to FASTA complete."

# mirDeep2 requires a minimum read length of 17
fastx_clipper -a AAAAAAAAAAAAAAAAAAAA -M 20 -l 17 -n -i @folder1@/unmapped.fa -o @folder1@/unmapped_trimmed.fa ${mirD[Qscore]}
echo "NovelMirnas: Preprocessing of FASTA complete."

# To ensure mapper.pl runs with correct format
sed -i 's/-/_x/g' @folder1@/unmapped_trimmed.fa
sed -i 's/^>/>hsa_/g' @folder1@/unmapped_trimmed.fa

# Run mirdeep2 in folder
cd @folder1@
genomePath=@var1@
mapper.pl unmapped_trimmed.fa -c -p ${mirD[genomePATH]} -t reads_collapsed.arf
echo "MIRDEEP2: mapper.pl finished."

# Extract if mapped to Mitochondria
grep MT -v reads_collapsed.arf > reads_collapsedMT.arf
grep Y -v reads_collapsedMT.arf > reads_collapsed_vs_genome.arf

echo "MIRDEEP2: miRDeep2.pl executing..."
miRDeep2.pl unmapped_trimmed.fa $genome reads_collapsed_vs_genome.arf $mature $other_mature $precursor -v -t ${mirD[species]} -a 15 -P 2> report.log
echo "MIRDEEP2: analysis finished."

#rm reads_collapsed.arf
#rm reads_collapsedMT.arf
#rm unmapped.fa
#rm unmapped.fasta
#rm -rf dir_prepare*
#rm -rf expression_analyses
#rm error*
#rm -rf mirdeep_runs
# Delete known miRNA pdfs
#for f in $(find . -name hsa* -type f); do echo $f | xargs rm; done
