/* Execute novel miRNA discovery tools miRanalyzer or mirDeep2 */
function NovelMirnas {
    fasta_files = {}

    // Check input variables
    if (Qscore == "") {
        Qscore = "-Q64"
    }
    if (Qscore != "-Q33" && Qscore != "-Q64") {
       std.fail(std.concat("miRanalyzer component parameter Q = '",Qscore, "' is invalid. Valid options are '-Q33' or '-Q64'"))
    }

// *****    miRanalyzer    *****//
    if (method == "miranalyzer" || method == "") {       
        if (!std.exists(miranalyzer, type="file")) {
            std.fail(std.concat("miRanalyzer jar file ", miranalyzer, " not found."))
        }
        if (!std.exists(miranalyzerDB, type="file")) {
            std.fail(std.concat("miRanalyzer absolute path ", miranalyzerDB, " not found."))
        }
        if (!std.exists(bowtiePath, type="file")) {
            std.fail(std.concat("bowtie path with binaries ", bowtiePath, " not found."))
        }
        if (kingdom == "") { // use default
            kingdom = "animal"
        }
        if (species == "") { // use default
            species = "hsa"
        }

            
        for row: std.itercsv(fqFiles) {

            mirA = BashEvaluate(
                script = INPUT(path="mirAn.sh"),
                var1 = INPUT(path=row.File),
                param2 = "mirA=([Qscore]=" + Qscore + " [species]=" + species + " [minReadLength]=" + minReadLength + " [mmToUse]=" + mmToUse + " [kingdom]=" + kingdom + ")",
                param3 = miranalyzer,
                param4 = miranalyzerDB,
                param1 = bowtiePath,
                @name = row.Key
                )

            fasta_files[row.Key] = mirA.folder1
        }

// *****    miRdeep2    ***** //
    } else if (method == "mirdeep2" || method == "miRDeep2") {
        
        if (mirDP_genome == null)  {
            std.fail("Input file mirDP_genome is required for mirdeep2.")
        }
        if (mirDP_mature == null)  {
            std.fail("Input file mirDP_mature is required for mirdeep2.")
        }
        if (mirDP_related_mature == null)  {
            std.fail("Input file mirDP_related_mature is required for mirdeep2.")
        }
        if (mirDP_precursor == null)  {
            std.fail("Input file mirDP_precursor is required for mirdeep2.")
        }
        if (species == "") { // use default
            species = "Human"
        }


        mirDprep = BashEvaluate(
                var1 = mirDP_genome,
                var2 = mirDP_mature,
                var3 = mirDP_related_mature,
                var4 = mirDP_precursor,
                script = """awk '{print $1}' @var2@ > @folder1@/mature.fa
                                awk '{print $1}' @var3@ > @folder1@/other_mature.fa
                                awk '{print $1}' @var4@ > @folder1@/precursor.fa
                                awk '{print $1}' @var1@ > @folder1@/genome.fa

                                # Convert U to T
                                sed -i 's/U/T/g' @folder1@/mature.fa
                                sed -i 's/U/T/g' @folder1@/other_mature.fa
                                sed -i 's/U/T/g' @folder1@/precursor.fa

                                # Change all other valid IUPAC letters to N to avoid errors
                                sed -i 's/[RYMSWBDHVN]/N/g' @folder1@/genome.fa
                                sed -i 's/[RYMSWBDHVN]/N/g' @folder1@/mature.fa
                                sed -i 's/[RYMSWBDHVN]/N/g' @folder1@/other_mature.fa
                                sed -i 's/[RYMSWBDHVN]/N/g' @folder1@/precursor.fa"""
            )

        mirDPfiles = Folder2Array(folder1 = mirDprep.folder1, filePattern="(.*)[.]fa")

        mirDPgenome = BashEvaluate(
            var1 = mirDP_genome,
            script = """echo @var1@ | sed 's/.fa//g'"""
            )

        genomePATH = std.fRead(mirDPgenome.stdOut)
        for row: std.itercsv(fqFiles) {
            // Error 127 = no results found
            mirD = BashEvaluate(
                script = INPUT(path="mirDp.sh"),
                param1 = row.File,
                param2 = "mirD=([Qscore]=" + Qscore + " [species]=" + species + 
                " [genomePATH]=" + genomePATH + ")",
                var1 = mirDP_genome,
                vararray=mirDPfiles.array,
                @name = row.Key
                )

            fasta_files[row.Key] = mirD.folder1		
        }

// *****    FAIL    ***** //
    } else {
        std.fail("INVALID INPUT: " + method + ". Parameter 'method' must be either 'miranalyzer' or 'mirdeep2'.")
    }

    novelMiRNA = Array2CSV(
        array = std.makeArray(fasta_files)
        )

    // Extract count expression per sample by chr positions
    novelExpr = REvaluate(
        script = INPUT(path="mirExpr.r"),
        table1 = novelMiRNA.csv,
        param1 = method
        )

    return record(array = novelExpr.array)

}
