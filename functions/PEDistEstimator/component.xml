<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>PEDistEstimator</name>
    <version>1.0</version>
    <doc> Estimate pair-end reads inner distance.
        
        Uses a script from MISO, called pe_utils.py. The estimation is done by aligning reads to a set of constitutive exons, i.e. ones that do not undergo alternative splicing and should thus produce a more reliable empirical value.

        The script depends on bedtools.

        If you get an error such as "tagBam call failed", that likely means the constitutive exons use different chromosome prefix than the reads.
    </doc>
    <author email="ping.chen@helsinki.fi">Ping Chen</author>
    <author email="lauri.lyly@helsinki.fi">Lauri Lyly</author>
    <author email="alejandra.cervera@helsinki.fi">Alejandra Cervera</author>
    <category>Quality control</category>
    <inputs>
        <input name="read" type="FASTQ" >
            <doc>Trimmed reads are recommended here. </doc>
        </input>
        <input name="mate" type="FASTQ" >
            <doc>Mates from paired-end sequencing. </doc>
        </input>
        <input name="long3UTR" type="GFF">
            <doc>This should be a file containing constitutive exons, generated e.g. by MISO's exon_utils.py script. The contents of this file depend on the reference annotation. See. http://genes.mit.edu/burgelab/miso/docs/#getting-a-set-of-long-constitutive-exons</doc>
        </input>
	    <input name="reference" type="FASTA">
            <doc>The reference genome along with aligner specific index files. 
                 The fasta file should have the extension .fasta (BWA and Bowtie)
                 or .fa (Tophat). See 'Align' documentation.
            </doc>
        </input>
    </inputs>
    <outputs>
        <output name="innerDist" type="CSV">
            <doc>CSV with fields: Mean_insert_size	Median_insert_size	Standard_deviation
            The mean and the standard deviation can be passed as parameters to tophat.</doc>
        </output>
        <output name="insertSize" type="CSV">
            <doc>CSV with fields: Key     Read    Mate    LenMean LenDev
             The length of the fragment calculated from the start position of the read to the end alignment position of the mate.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="randomN" type="int" default="10000000">
            <doc> The number of random reads/mates to be picked for estimation.</doc>
        </parameter>
        <parameter name="picard" type="string" default="/opt/picard">
            <doc>Path to Picard directory, e.g. "/opt/picard", which containg the Picard-tools .jar files. If empty 
            string is given (default), PICARD_HOME environment variable is assumed to point to the Picard directory.</doc>
        </parameter>
	<parameter name="aligner" type="string" default="bowtie">
            <doc>Defines the aligner to be used. Executable(s) for the aligner used must be set in PATH. 
                 Possible values: {bowtie and bowtie2} 
            </doc>
        </parameter>
        <parameter name="options" type="string" default="">
            <doc> Other options for alignment. This parameter is given as written to the aligner execution command. See 'Align'.</doc>
        </parameter>
        <parameter name="threads" type="int" default="4">
            <doc> The number of threads for alignment.</doc>
	</parameter>
    </parameters>
</component>
