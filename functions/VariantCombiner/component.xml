<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>VariantCombiner</name>
    <version>1.0</version>
    <doc>
    This function combines all input variant files into one file using the Genome Analysis Toolkit (GATK).
    
    Complete documentation:
    <ul>
    <li><a href="http://www.broadinstitute.org/gatk/guide/" 
    target="_BLANK">GATK guide</a></li>
    <li><a href="http://www.broadinstitute.org/gatk/gatkdocs/org_broadinstitute_sting_gatk_walkers_variantutils_CombineVariants.html" 
    target="_BLANK">CombineVariants</a></li>
    </ul>
    </doc>
    <author email="rony.lindell@helsinki.fi">Rony Lindell</author>
    <category>VariationAnalysis</category>
    <inputs>
        <input name="reference" type="FASTA" optional="false">
            <doc>Reference fasta file.</doc>
        </input>
        <input name="variants" type="VCF" optional="true" array="true">
            <doc>Input VCF files</doc>
        </input>
        <input name="variants1" type="VCF" optional="true">
            <doc>Input VCF file. For more than 5 files you can use the 'files' parameter for paths or use the combiner multiple times.</doc>
        </input>
        <input name="variants2" type="VCF" optional="true">
            <doc>Input VCF file.</doc>
        </input>
        <input name="variants3" type="VCF" optional="true">
            <doc>Input VCF file.</doc>
        </input>
        <input name="variants4" type="VCF" optional="true">
            <doc>Input VCF file.</doc>
        </input>
        <input name="variants5" type="VCF" optional="true">
            <doc>Input VCF file.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="calls" type="VCF">
            <doc>Merged vcf file.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="gatk" type="string" default="">
            <doc>Path to GATK directory containing the 'GenomeAnalysisTK.jar' file. If empty string is given (default), GATK_HOME environment variable is assumed to point to the GATK directory where GenomeAnalysisTK.jar is located.</doc>
        </parameter>
        <parameter name="files" type="string" default="">
            <doc>A "-V"-tag separated list of paths to multiple variant vcf files (single- or multi-sample), e.g. files="-V FILE1.vcf -V FILE2.vcf, ... -V FILEN.vcf".</doc>
        </parameter>
        <parameter name="memory" type="string" default="4g">
            <doc>The amount of java-heap memory being allocated to the GATK thread, given in the format "4g" for 4 gigabytes or "2560m" for 2560 megabytes (2,5g) etc.</doc>
        </parameter>
        <parameter name="disjoint" type="boolean" default="false">
            <doc>This will allow simple concatenation and considerably reduce the runtime for files with identical sample sets and disjoint calls (e.g. split chromosomes from the same patient).</doc>
        </parameter>
        <parameter name="genotype" type="string" default="UNSORTED">
            <doc>Defines how colliding variants are handled. Allowed values: {UNIQUIFY, PRIORITIZE, UNSORTED, REQUIRE_UNIQUE}</doc>
        </parameter>
        <parameter name="options" type="string" default="">
            <doc>This string will be added to the command and can include any number of options in the software specific format.</doc>
        </parameter>
    </parameters>
</component>
