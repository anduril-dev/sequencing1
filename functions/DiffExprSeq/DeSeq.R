library(DESeq2)
library(gplots)
library(RColorBrewer)

countData = table1[,-1]
rownames(countData) <- table1[,1]

colData = table2[,-1,drop=FALSE]
rownames(colData) <- table2[,1]

dds <-  DESeqDataSetFromMatrix(countData = countData,
                               colData  = colData,
                               design = as.formula(param1))

print(head(dds))
#some filtering to speed up stuff
dds <- dds[rowSums(counts(dds)) > 1, ]

dds <- DESeq(dds)
res <- results(dds)

table.out <- data.frame(ids=rownames(res), as.data.frame(res))
print(head(table.out))
print(is.data.frame(table.out))
optOut1 <- mcols(res)$description[2]

save(dds, file=get.output(cf,'optOut2'))
save(res, file=get.output(cf,'optOut3'))

rm(optOut2)
rm(optOut3)

setwd(document.dir)
pdf("plotMA.pdf")
plotMA(res,ylim=c(-2,2))
resMLE <- results(dds, addMLE=TRUE)
plotMA(resMLE,ylim=c(-2,2))
dev.off()

#rld <- rlog(dds)
#distsRL <- dist(t(assay(rld)))
#mat <- as.matrix(distsRL)
#rownames(mat) <- colnames(mat) <- with(colData(dds),condition)
#hc <- hclust(distsRL)
#hmcol <- colorRampPalette(brewer.pal(9,"GnBu"))(100)
#pdf("heatmap.pdf")
#heatmap.2(mat,Rowv=as.dendrogram(hc),symm=TRUE,trace="none", col=rev(hmcol), margin=c(13,13))
#dev.off()
