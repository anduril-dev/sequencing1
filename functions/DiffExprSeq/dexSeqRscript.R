# If required packages not installed, install them (if possible)
is.installed <- function(pkg) {
                    is.element(pkg, installed.packages()[,1])
                    }

if (!is.installed("DEXSeq")) {
    print("Downloading required R package DESeq")
    update.packages(repos="http://cran.r-project.org", checkBuilt=TRUE, ask=FALSE)
    source("http://bioconductor.org/biocLite.R")
    biocLite("DEXSeq")
}

library("DEXSeq")
library(BiocParallel)

print(sessionInfo())

setwd(document.dir)

annotationfile = file.path(var1)
print(paste("Annotation file ",annotationfile))

cores <- as.integer(param1)

samples = data.frame(condition = table1[,3], row.names=table1$Key, stringsAsFactors = TRUE, check.names = FALSE)
print (samples)

print ("Getting exon counts")
dxd = DEXSeqDataSetFromHTSeq(   countfiles      = table1$File,
                                sampleData      = samples,
                                design          = ~ sample + exon + condition:exon,
                                flattenedfile   = annotationfile)

print (head(rowData(dxd),3))
print (sampleAnnotation(dxd))



print("Estimating size factors")
dxd <- estimateSizeFactors(dxd)
#print (sizeFactors(ecs))

print("Estimating dispersions")
dxd <- estimateDispersions(dxd, BPPARAM=MulticoreParam(workers=cores))

pdf("plotDispEst.pdf")
plotDispEsts(dxd)
dev.off()

print("Testing for differential expression")
dxd <- testForDEU(dxd,BPPARAM=MulticoreParam(workers=cores))
meanvalues <- rowMeans(counts(dxd))

print("Estimating log2foldChanges")
dxd <- estimateExonFoldChanges(dxd,fitExpToVar="condition",BPPARAM=MulticoreParam(workers=cores))

res1 <- DEXSeqResults(dxd)

pdf("plotMA.pdf")
plotMA(res1,cex=0.8)
dev.off()

print (table(res1$padjust<0.1))
print (table(res1$pvalue<0.05))
print (head(dxd))
print (head(rowData(dxd)))

print("Saving")
save(dxd, file=get.output(cf,'optOut1'))
save(res1, file=get.output(cf,'optOut2'))
table.out <- as.data.frame(res1)

rm(optOut1)
rm(optOut2)
