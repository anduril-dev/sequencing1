function VariantRecalibrator {

    // ------------------------------------------------------------
    // Recalibrate VCF file by region or as a whole.
    // - Step 1: Calculate model
    // - Step 2: Apply recalibration
    // ------------------------------------------------------------
    
    // ------------------------------------------------------------
    // Create general settings.
    // ------------------------------------------------------------

    if (gatk == "") gatk='${GATK_HOME}'
    if (variants != null) inputStr="-input @var1@ " else inputStr=""
    if (files != "") inputStr=inputStr + files + " "
    if (hapmap != null) hapmapStr="-resource:hapmap,known=false,training=true,truth=true,prior=15.0 @var4@ " else hapmapStr=""
    if (omni != null) omniStr="-resource:omni,known=false,training=true,truth=false,prior=12.0 @var5@ " else omniStr=""
    if (dbsnp != null) dbsnpStr="-resource:dbsnp,known=true,training=false,truth=false,prior=2.0 @var6@ " else dbsnpStr=""
    if (hcsnp != null) hcsnpStr="-resource:1000G,known=false,training=true,truth=false,prior=10.0 @var3@ " else hcsnpStr=""
    if (mills != null) millsStr="-resource:mills,known=true,training=true,truth=true,prior=12.0 @var3@ " else millsStr=""

    // ------------------------------------------------------------
    // Step 1: Run GATK to calculate gaussian mixture model.
    // 1.1 First we calculate the SNP model
    // 1.2 Then we calculate the INDEL model
    // ------------------------------------------------------------
 
    // The core script for running GATK
    gatkStr='java -Xmx' + memory + ' -jar ' + gatk +
            '/GenomeAnalysisTK.jar -T VariantRecalibrator -R @var2@ ' + inputStr +
            '-recalFile @arrayOut1@/recal.txt -nt ' + threads + ' ' +
            '-tranchesFile @arrayOut1@/tranches.txt ' +
            '-rscriptFile @arrayOut1@/plots.r '
            
    // Script for adding files to the array
    indexStr='; echo -e recal"\t"recal.txt >> @arrayIndex1@; ' +
            'echo -e tranches"\t"tranches.txt >> @arrayIndex1@; '  +
            'echo -e rscript"\t"rscript.r >> @arrayIndex1@'

    // Add parameters and files for the snp model
    modeStr="-mode SNP "
    trainingStr=hapmapStr + omniStr + dbsnpStr + hcsnpStr
    paramStr="--numBadVariants 1000 "
    annoStr=" -an " + snpAnno
    annoStr=std.strReplace(annoStr,","," -an ")
    if (!capture) annoStr=annoStr+" -an DP "
    else annoStr=annoStr + " "
 
    // Run GATK: 1.1 Calculate SNP model
    @out.array1.filename="model_snp"
    gatk_model_snp = BashEvaluate(
        var1=variants,
        var2=reference, 
        var3=hcsnp,
        var4=hapmap,
        var5=omni,
        var6=dbsnp,
        script=gatkStr + paramStr + trainingStr + annoStr + modeStr + indexStr
        )

    // Add parameters and files for the indel model
    modeStr="-mode INDEL "
    trainingStr=dbsnpStr + millsStr
    paramStr="--maxGaussians 4 --numBadVariants 1000 "
    annoStr=" -an " + indelAnno
    annoStr=std.strReplace(annoStr,","," -an ")
    annoStr=annoStr + " "

    // Run GATK: 1.2 Calculate INDEL model
    @out.array1.filename="model_indel"
    gatk_model_indel = BashEvaluate(
        var1=variants,
        var2=reference,
        var3=mills,
        var6=dbsnp,
        script=gatkStr + paramStr + trainingStr + annoStr + modeStr + indexStr
        )

    // ------------------------------------------------------------
    // Step 2: Run GATK to apply the calculated models.
    // 2.1 First apply the INDEL model
    // 2.2 Then apply the SNP model
    // ------------------------------------------------------------

    // Create the core script for running GATK
    gatkStr='java -Xmx' + memory + ' -jar ' + gatk +
            '/GenomeAnalysisTK.jar -T ApplyRecalibration -R @var2@ ' + inputStr +
            '-tranchesFile @var4@ ' +
            '-recalFile @var5@ ' +
            '-o @optOut1@ '
    
    // Set the parameters
    modeStr="-mode INDEL "
    paramStr="--ts_filter_level " + truth + " "

    // Run GATK: 2.1 Apply INDEL model
    @out.optOut1.filename="recal_indel.vcf"
    gatk_calibrated_indel = BashEvaluate( 
        var1=variants,
        var2=reference,
        var4=gatk_model_indel.array1["tranches"],
        var5=gatk_model_indel.array1["recal"],
        script=gatkStr + paramStr + modeStr + indexStr
        )

    variants=gatk_calibrated_indel.optOut1
    modeStr="-mode SNP "

    // Run GATK: 2.2 Apply SNP model
    @out.optOut1.filename="recal_snp.vcf"
    gatk_calibrated_snp = BashEvaluate(
        var1=variants,
        var2=reference,
        var4=gatk_model_snp.array1["tranches"],
        var5=gatk_model_snp.array1["recal"],
        script=gatkStr + paramStr + modeStr + indexStr
        )

     return gatk_calibrated_snp.optOut1
    
}
