/*
table1 = variants (Chromosome, Start, End)
var1 = reference genome directory
var/chrN.txt = nucleotides for chromosome N without newlines or metadata
*/

import fi.helsinki.ltdk.csbl.asser.io.CSVWriter
import java.io.{File,FileOutputStream,RandomAccessFile}
import scala.collection.mutable.Map

val chromCol = getColumn("Chromosome")
val startCol = getColumn("Start")
val endCol = getColumn("End")

val chromFileMap = Map[String,RandomAccessFile]()

def getReferenceSequence(chromosome: String, start: Int, end: Int): String = {
    val chrPattern = "chr(.*)".r
    val normChromosome = chromosome match {
        case chrPattern(x) => x
        case "MT" => "M"
        case x => x
    }
    if (!chromFileMap.contains(normChromosome)) {
        val path = new File(var1File, "chr%s.txt".format(normChromosome))
        chromFileMap(normChromosome) = new RandomAccessFile(path, "r")
    }
    val file = chromFileMap(normChromosome)
    file.seek(start-1)
    val buffer = new Array[Byte](end-start+1)
    file.read(buffer)
    new String(buffer).toUpperCase()
}

val VCF_COLUMNS = Array[String]("#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT")
val vcfStream = new FileOutputStream(tableOutFile)
vcfStream.write("##fileformat=VCFv4.1\n".getBytes())
val vcf = new CSVWriter(VCF_COLUMNS, vcfStream, "\t", false)

for (row <- table1) {
    val chr = row(chromCol)
    val start = row.int(startCol)
    val end = row.int(endCol)
    val refSequence = getReferenceSequence(chr, start, end)

    for (pos <- start.to(end)) {
        for (altAllele <- "ACGT") {
            val refAllele = refSequence(pos-start)
            if (refAllele != altAllele) {
                vcf.write(chr, false)
                vcf.write(pos)
                vcf.write(".", false)
                vcf.write(refAllele)
                vcf.write(altAllele)
                vcf.write(40)
                vcf.write("PASS", false)
                vcf.write(".", false)
                vcf.write(".", false)
            }
        }
    }
}
vcf.close()
