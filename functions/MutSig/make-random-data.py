import csv
import random
import sys

# [(name, chromosome, start, end)]
ALL_GENES = [
    ('A1BG', 19, 58862757,  58863053),  # ENSG00000121410
    ('A1CF', 10, 52595834,  52596072),  # ENSG00000148584
    ('A2LD1',13, 101185817, 101186028), # ENSG00000134864
    ('A2M',  12, 9264973,   9265132),   # ENSG00000175899
]

PATIENT_PATTERN = 'P%d'

# Minimal valid test data: 4 genes, 50+50 variants
# ~10 patients

num_genes = 4
num_patients = 100
num_silent = 100
num_nonsilent = 100

genes = ALL_GENES[:num_genes]
patients = [PATIENT_PATTERN % i for i in range(1, num_patients+1)]

gene_names = [g[0] for g in genes]
sys.stderr.write('# Command for grepping selected genes\n')
sys.stderr.write("grep -E '^(%s)[[:space:]]'\n" % ('|'.join(gene_names)))

sys.stderr.write("\n")
sys.stderr.write("# Coverage\n")
coverage_writer = csv.writer(sys.stderr, 'excel-tab', lineterminator='\n')
coverage_writer.writerow(["Chromosome", "Start", "End"])
for gene in genes:
    coverage_writer.writerow(gene[1:4])

writer = csv.writer(sys.stdout, 'excel-tab', lineterminator='\n')
writer.writerow(['gene', 'CHROM', 'POS', 'ALT', 'patient', 'effect'])

for i in range(num_silent+num_nonsilent):
    gene_rec = random.choice(genes)
    name = gene_rec[0]
    chrom = gene_rec[1]
    pos = random.randint(gene_rec[2], gene_rec[3])
    alt_allele = random.choice("ACGT")
    patient = random.choice(patients)
    if i < num_silent:
        effect = 'silent'
    else:
        effect = 'nonsilent'
    writer.writerow([name, chrom, pos, alt_allele, patient, effect])
