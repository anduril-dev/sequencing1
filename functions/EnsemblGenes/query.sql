SELECT gene_stable_id.stable_id AS ID, gene.seq_region_strand AS strand, seq_region.name AS chromosome, gene.seq_region_start AS start, gene.seq_region_end AS end, gene.biotype
FROM gene
JOIN gene_stable_id ON gene.gene_id=gene_stable_id.gene_id
JOIN seq_region ON seq_region.seq_region_id=gene.seq_region_id
JOIN coord_system ON coord_system.coord_system_id=seq_region.coord_system_id
WHERE coord_system.name='chromosome'
ORDER BY seq_region.name
