<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>Sam2Fastq</name>
    <version>1.0</version>
    <doc>This component uses the Picard java library to perform a conversion from 
    SAM/BAM alignment format back to FASTQ sequence format. The resulting raw reads 
    with included quality values can then be re-aligned using an alignment component. 
    The procedure SAM->FASTQ->SAM can be useful when re-processing
    old alignments.

    Normally single-end reads will be output to 'reads' and
    pair-end reads to 'reads' and 'mate'. If 'perReadgroup' is set to 'true', all 
    read-group specific output files are written to 'folder'. Use 'options' to 
    add any number of additional picard-tools options to the run command. See the 
    <a href="http://picard.sourceforge.net/command-line-overview.shtml#SamToFastq" target="_BLANK">manual</a>
    for more information about picard-tools SamToFastq.
    </doc>
    <author email="rony.lindell@helsinki.fi">Rony Lindell</author>
    <category>Alignment</category>
    <requires URL="http://picard.sourceforge.net/">picard-tools</requires>
    <inputs>
        <input name="alignment" type="AlignedReadSet" optional="false">
            <doc>Input SAM/BAM alignment file to convert. The file should have correct extension (.sam or .bam) in order for Picard to be able to determine the file type.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="folder" type="BinaryFolder">
            <doc>Folder in which to output fastq files when 'perReadgroup' is set to true.</doc>
        </output>
        <output name="reads" type="FASTQ">
            <doc>Output FASTQ file containing single-end reads or reads of the first pair in pair-end data.</doc>
        </output>
        <output name="mate" type="FASTQ">
            <doc>Output FASTQ file containing reads of the second pair in pair-end data.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="picard" type="string" default="">
            <doc>Path to Picard directory, e.g. "/opt/picard", which containg the Picard-tools .jar files. If empty string is given (default), PICARD_HOME environment variable is assumed to point to the Picard directory.</doc>
        </parameter>
        <parameter name="memory" type="string" default="2g">
            <doc>A non-default value appends -XmxVALUE to the java command to specify the maximum size, in bytes, of the memory allocation pool. This value must a multiple of 1024 greater than 2MB. Append the letter k or K to indicate kilobytes, m or M to indicate megabytes or g or G to indicate gigabytes. The default value is chosen at runtime based on system configuration. For example a value of "4g" would allocate 4 gigabytes of memory and a value of "512m" would allocate 512 megabytes of memory. Note: a value of at least '2g' is recommended by Picard developers, therefore this is the default value.
            </doc>
        </parameter>
        <parameter name="perReadgroup" type="boolean" default="false">
            <doc>Output a FASTQ file per read group (two fastq files per read group if the group is paired). All FASTQ files will be written to 'folder' output.</doc>
        </parameter>
        <parameter name="options" type="string" default="">
            <doc>Any additional picard-tools options can be added here. This string is added to the run command as written. See the picard-tools manual part 'SamToFastq' for more information. Some useful options are e.g. clipping and trimming.
            <br /><br />
            <code>Example: options="CLIPPING_ACTION=X READ1_TRIM=5 READ2_TRIM=5 RE_REVERSE=false"</code>
            </doc>
        </parameter>
    </parameters>
</component>
