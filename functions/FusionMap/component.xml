<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>FusionMap</name>
    <version>1.0</version>
    <doc>
        Detect genomic fusions from paired or single ended DNA-seq or RNA-seq data using
        <a href="http://bioinformatics.oxfordjournals.org/content/27/14/1922.full">FusionMap</a>.
        FusionMap works by splitting reads into three parts, aligning the prefix and suffix
        reads, and detecting fusions by discordant alignment of the prefix and suffix.
        The most important parameters are minSeedLength (alpha) and maxAlignments (beta).

        Notes: FusionMap 7.0.1.25 does not appear to work with Mono 3+,
        so Mono 2.10.9+ should be used.
        On the first run, FusionMap may take a long time at "Thread number=N"; this
        is due to downloading and processing of the genome index.
    </doc>
    <author email="kristian.ovaska@helsinki.fi">Kristian Ovaska</author>
    <category>Short-read Sequencing</category>
    <requires URL="http://www.arrayserver.com/wiki/index.php?title=FusionMap">FusionMap</requires>
    <requires URL="http://www.mono-project.com">Mono (2.10.9+ or 2.11)</requires>
    <inputs>
        <input name="reads" type="RegionSet">
            <doc>Sequencing reads in FASTQ/FASTA/BAM format (optionally gzipped).
                Data can either include all reads, or only unmapped reads after
                an initial alignment; the latter reduces FusionMap run time.</doc>
        </input>
        <input name="mates" type="RegionSet" optional="true">
            <doc>For paired-end sequencing, these are the mate reads in the same
            format as primary reads.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="fusions" type="CSV">
            <doc>Fusion table. See
                <a href="http://www.arrayserver.com/wiki/index.php?title=Fusion_SE_report">column descriptions</a>.
            </doc>
        </output>
        <output name="fusionReads" type="BAM">
            <doc>Fusion reads. Each read has been cut into two parts, which align
            to different locations.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="fusionMapPath" type="string" default="">
            <doc>Path to FusionMap installation, i.e., the directory that contains bin
                as a subdirectory. This directory must be writable by the user. If the parameter
                is empty, the environment variable FUSIONMAP_HOME is used.</doc>
        </parameter>
        <parameter name="rna" type="boolean" default="true">
            <doc>If true, data are from RNA-seq. If false,
            data are from genomic DNA-seq.</doc>
        </parameter>
        <parameter name="referenceName" type="string" default="Human.B37.3">
            <doc>Code of the reference genome in
                <a href="http://www.arrayserver.com/wiki/index.php?title=A_list_of_compiled_genome_and_gene_model_from_OmicSoft">Array Suite nomenclature</a>.
            </doc>
        </parameter>
        <parameter name="geneModelName" type="string" default="Ensembl.R73">
            <doc>
                Code of the gene model in
                <a href="http://www.arrayserver.com/wiki/index.php?title=A_list_of_compiled_genome_and_gene_model_from_OmicSoft">Array Suite nomenclature</a>.
                Only used for RNA-seq.
            </doc>
        </parameter>
        <parameter name="minSeedLength" type="int" default="25">
            <doc>Minimum length of a seed read (alpha in the FusionMap article).
                If 0, the length is computed using Min(25, Max(17,floor(ReadLength/3))).</doc>
        </parameter>
        <parameter name="maxAlignments" type="int" default="1">
            <doc>Maximum number of read end alignments (beta in the FusionMap article).
            Range: 1 to 5.</doc>
        </parameter>
        <parameter name="mismatchPercentage" type="int" default="8">
            <doc>Percentage (0 to 100) of allowed mismatches in alignment of reads.</doc>
        </parameter>
        <parameter name="monoExec" type="string" default="">
            <doc>Path to Mono executable, if not on path. Example: /opt/mono/bin/mono.</doc>
        </parameter>
        <parameter name="threads" type="int" default="4">
            <doc>Number of threads used by FusionMap.</doc>
        </parameter>
        <parameter name="memoryGB" type="int" default="8">
            <doc>An estimate for memory usage by FusionMap. Only used in a cluster
                environment. For the human genome, 7-8 GB are needed.</doc>
        </parameter>
    </parameters>
</component>
