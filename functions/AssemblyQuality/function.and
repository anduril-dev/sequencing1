function AssemblyQuality {
    threads = 4
    minAlignQuality = 30
    fastaStatsScript = INPUT(path="fastaStats.scala")
    referenceStatsScript = INPUT(path="referenceStats.scala")
    nPlotAnnotator = StringInput(content="""
        index.50 <- which(x.column == 50)
        if (length(index.50) > 0) {
            n50 <- y.column[index.50]
            lines(list(x=c(0, 50), y=c(n50, n50)), col="blue")
            legend("topright", legend=sprintf("N50 (%.0f bp)", n50), col="blue", lwd=1)
        }
    """)
    coverageScript = """\
import grok
reader = grok.reader(var1, "sam").flip(0)
result = grok.unionL(reader)
writer = result.writer(optOut1File, "bed")
writer.add(result)"""

    contigStats = ScalaEvaluate(fastaStatsScript, var1=contigs)
    contigNPlot = Plot2D(y=contigStats.optOut1, x=contigStats.optOut1,
        plotAnnotator=nPlotAnnotator,
        xColumns="Quantile", yColumns="Length", plotType="l", pngImage=false,
        xLabel="Assembly coverage %%", yLabel="Contig length",
        title="Minimum contig length (Y) to cover X%% of assembly")

    scaffoldStats = ScalaEvaluate(fastaStatsScript, var1=scaffolds)
    scaffoldNPlot = Plot2D(y=scaffoldStats.optOut1, x=scaffoldStats.optOut1,
        plotAnnotator=nPlotAnnotator,
        xColumns="Quantile", yColumns="Length", plotType="l", pngImage=false,
        xLabel="Assembly coverage %%", yLabel="Scaffold length",
        title="Minimum scaffold length (Y) to cover X%% of assembly")

    report = LatexCombiner(contigNPlot.plot, scaffoldNPlot.plot)

    if (reference == null) {
        empty = StringInput(content="")
        contigReferenceStatsValue = empty
        scaffoldReferenceStatsValue = empty
    } else {
        alignedContigs = BashEvaluate(var1=reference, var2=contigs,
            script=std.strReplace("bwa mem @var1@ @var2@ -t @THREADS@", "@THREADS@", threads))
        alignedScaffolds = BashEvaluate(var1=reference, var2=scaffolds,
            script=std.strReplace("bwa mem @var1@ @var2@ -t @THREADS@", "@THREADS@", threads))

        contigCoverageIslands   = PythonEvaluate(var1=alignedContigs.stdOut,   script=coverageScript)
        scaffoldCoverageIslands = PythonEvaluate(var1=alignedScaffolds.stdOut, script=coverageScript)

        contigReferenceStats = ScalaEvaluate(referenceStatsScript,
            force table1=contigs, force table2=reference, force table3=alignedContigs.stdOut, force table4=contigCoverageIslands.optOut1,
            format="table1=fasta,table2=fasta,table3=fasta,table4=bed", param1=minAlignQuality)
        scaffoldReferenceStats = ScalaEvaluate(referenceStatsScript,
            force table1=scaffolds, force table2=reference, force table3=alignedScaffolds.stdOut, force table4=scaffoldCoverageIslands.optOut1,
            format="table1=fasta,table2=fasta,table3=fasta,table4=bed", param1=minAlignQuality)

        contigReferenceStatsValue = contigReferenceStats.table
        scaffoldReferenceStatsValue = scaffoldReferenceStats.table
    }

    return record(
        report=report,
        contigStats=contigStats.table,
        scaffoldStats=scaffoldStats.table,
        contigReferenceStats=contigReferenceStatsValue,
        scaffoldReferenceStats=scaffoldReferenceStatsValue)
}
