// table1=contigs/scaffolds (FASTA)
// table2=reference (FASTA)
// table3=aligned scaffolds (SAM)
// table4=coverage islands (BED)
// param1=minimum align quality

import org.anduril.runtime.table.io.CSVTable
import org.anduril.runtime.table.io.FASTAGenome
import org.anduril.runtime.table.io.Genome
import scala.collection.mutable.Map

val SamChromosomeCol = 2
val SamMapQualCol = 4
val SamCigarCol = 5
val SamSequenceCol = 9
val MinAlignScore = param1.toInt

/* Constant to denote all chromosomes. */
val ChromosomeAll = "ALL"

def table2Genome(inTable: Table, chromosomeFunc: (Row => String), lengthFunc: (Row => Long)): Genome = {
    val genome = new Genome()
    for (row <- inTable) {
        val chr = chromosomeFunc(row)
        val regionLength = lengthFunc(row)
        genome.incChromosomeLength(chr, regionLength)
    }
    genome
}

/*
Cigar format:
M: alignment match
I: insertion
D: deletion
N: skipped from reference
S: soft clipping
H: hard clipping
P: padding
=: sequence match
X: sequence mismatch
MIS=X equals the length of sequence
*/
def samRecordLength(row: Row): Long = {
    if (row.int(SamMapQualCol) < MinAlignScore) {
        return(0L)
    }

    val cigar = row(SamCigarCol)

    val pattern = "([0-9]+)([MIDNSHP=X])".r
    var seqLength = 0L
    for (element <- pattern.findAllIn(cigar).matchData) {
        val numBases = element.group(1)
        val operator = element.group(2)
        seqLength += (operator match {
            case "M" | "I" | "D" | "=" => numBases.toInt
            case _ => 0
        })
    }
    seqLength
}

def getLength(genome: Genome, chromosome: String): Double = {
    if (chromosome == ChromosomeAll) {
        genome.totalLength()
    } else {
        Math.max(genome.chromosomeLength(chromosome), 0L)
    }
}

val alignedTable = new CSVTable(table3File, hasHeader=false, skipRegexp="^@.*", rectangular=false)
val coverageTable = table4

val scaffoldLength = new FASTAGenome(table1File, _ => ChromosomeAll)
val reference = new FASTAGenome(table2File)
val aligned = table2Genome(alignedTable, r => r(SamChromosomeCol), samRecordLength)
val coverage = table2Genome(coverageTable, r => r(0), r=> r.int(2)-r.int(1))

val scaffoldTotalLength = scaffoldLength.totalLength()

val statsWriter = Table.writer(
    Seq("Statistic", "Chromosome", "Value"),
    tableOutFile, Table.FormatCSV)

statsWriter.write(("AssemblyLength", ChromosomeAll, scaffoldTotalLength))

for (chr <- ChromosomeAll +: reference.chromosomes) {
    val referenceLength = getLength(reference, chr)
    val alignedLength = getLength(aligned, chr)
    val coverageLength = getLength(coverage, chr)

    statsWriter.write(("ReferenceLength", chr, referenceLength))
    statsWriter.write(("AlignedLength",   chr, alignedLength))
    statsWriter.write(("IslandLength",    chr, coverageLength))
    statsWriter.write(("Coverage",        chr, coverageLength/referenceLength))
    statsWriter.write(("Multiplicity",    chr, alignedLength/coverageLength))
    if (chr == ChromosomeAll) {
        statsWriter.write(("Validity",        chr, alignedLength/scaffoldTotalLength))
        statsWriter.write(("Parsimony",       chr, scaffoldTotalLength/coverageLength))
    }
}

statsWriter.close()
