// var1: FASTA

val LengthCol = "length"
val CountNCol = "countN"
val OutputNX = Seq(10, 25, 50, 75, 90)

val fasta = Table.reader(var1File, Table.FormatFASTA)
fasta.addColumnD(LengthCol, r => r(1).size)
fasta.addColumnD(CountNCol, r => r(1).count(_ == 'N'))
val lengthsCallback = fasta.collectD(LengthCol)
val lengthStats = fasta.statistics(LengthCol)
val countNStats = fasta.statistics(CountNCol)
fasta.readAll()

val lengthsArray: Array[Double] = lengthsCallback()
scala.util.Sorting.quickSort(lengthsArray)
val lengths = lengthsArray.view.reverse

val statsWriter = Table.writer(Seq("Statistic", "Value"), tableOutFile, Table.FormatCSV)
val statsList = Seq[(String,Double)](
    ("NumSequences", lengthStats.n()),
    ("TotalLength",  lengthStats.sum()),
    ("MinLength",    lengthStats.min()),
    ("MaxLength",    lengthStats.max()),
    ("MeanLength",   lengthStats.mean()),
    ("MedianLength", lengthStats.median()),
    ("LengthSD",     lengthStats.sd()),
    ("MissingBases", countNStats.sum()) )
for ((k, v) <- statsList) statsWriter.write2(k, v)

/** Maximum observed percentile (p). */
var maxP = 0
val cumulativeSum = lengths.scan(0.0)(_ + _)
val totalSum = lengthStats.sum()
/** Length = shortest length of contigs/scaffolds
  * that cover Quantile% of total assembly. */
val nWriter = Table.writer(Seq("Quantile", "Length"), optOut1File, Table.FormatCSV)
for ((length, curSum) <- lengths.zip(cumulativeSum)) {
    val curP = ((curSum / totalSum)*100).floor.toInt
    if (curP > maxP) {
        ((maxP+1) to curP).foreach(p => {
            nWriter.write2(p, length)
            if (OutputNX.contains(p)) {
                statsWriter.write2("N%d".format(p), length)
            }
        })
        maxP = curP
    }
}

statsWriter.close()
nWriter.close()
