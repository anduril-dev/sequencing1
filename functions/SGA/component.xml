<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>SGA</name>
    <version>1.0</version>
    <doc>
        Perform de novo assembly for sequencing reads using
        <a href="https://github.com/jts/sga">String Graph Assembler</a> (SGA).
        SGA is designed for experiments using a read length of 100 base pairs
        or more. Minimum recommended coverage is 20-30x.

        The following SGA executables must be on path: sga, sga-align,
        sga-bam2de.pl and sga-astat.py. Also, abyss-fixmate, bwa and samtools need
        to be on path. Please follow SGA documentation for installation.

        SGA parameters may need to be tuned for both performance and assembly
        quality reasons. The most important parameters are correctionK,
        minOverlapAssemble and minOverlapMerge. For machines with limited
        memory, indexBatchSize may need to be set.
        SGA is parallelized at two levels: assembly of a single genome is
        multithreaded, and multiple genomes can be assembled using a cluster.

        Below are benchmarks for SGA 0.10.12 that provide hints on CPU and
        memory requirements:
        <ul>
            <li>E. coli, 1.7 Gbp sequence: 1.0 GB memory, 7 CPU hours</li>
            <li>C. elegans, 6.7 Gbp sequence: 8.7 GB memory, 49 CPU hours</li>
            <li>Human, 148 Gbp sequence: 70 GB memory, 1900 CPU hours</li>
        </ul>
    </doc>
    <author email="kristian.ovaska@helsinki.fi">Kristian Ovaska</author>
    <category>Assembly</category>
    <requires URL="https://github.com/jts/sga">SGA</requires>
    <requires URL="http://bio-bwa.sourceforge.net/">BWA</requires>
    <requires URL="http://samtools.sourceforge.net/">SAMtools</requires>
    <requires URL="http://www.bcgsc.ca/platform/bioinfo/software/abyss">ABySS</requires>
    <requires URL="http://code.google.com/p/pysam/">pysam</requires>
    <requires URL="http://www.ruffus.org.uk/">Ruffus</requires>
    <requires URL="http://www.scala-lang.org/">Scala</requires>
    <inputs>
        <input name="reads" type="SequenceSet" array="true">
            <doc>Sequencing reads. SGA accepts input as FASTA, FASTQ and
            their gzipped variants. When paired-end sequencing is used,
            either these files contain the paired reads after one another,
            or paired reads are in the second input.</doc>
        </input>
        <input name="mates" type="SequenceSet" array="true" optional="true">
            <doc>Paired sequencing reads. When paired-end sequencing
            is used and paired reads and not interlaced in the primary
            input, these files contain the paired reads in the same
            order as primary reads. The length of the mates array must
            be equal to the reads array and read-mate files must be in
            the same order.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="contigs" type="FASTA">
            <doc>Assembled contigs, i.e., contiguous sequences constructed
            from the reads.</doc>
        </output>
        <output name="scaffolds" type="FASTA">
            <doc>Assembled scaffolds, i.e., concatenated contigs. In non-paired
            end mode (pairedEnd=false), this file is empty.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="threads" type="int" default="8">
            <doc>Maximum number of threads used by one process.</doc>
        </parameter>
        <parameter name="pairedEnd" type="boolean" default="true">
            <doc>If false, paired-end sequencing is not used. If
            true, paired reads are either contained in the primary
            input, or split between the primary and secondary inputs.</doc>
        </parameter>
        <parameter name="minOverlap" type="int" default="45">
            <doc>Minimum length of read overlap in the overlap computation step.
            Corresponds to the -m argument of "sga overlap". The value
            must be smaller than the read length.</doc>
        </parameter>
        <parameter name="minOverlapAssemble" type="int" default="75">
            <doc>Minimum length of read overlap in the assembly step.
            Corresponds to the -m argument of "sga assemble". The value
            must be smaller than the read length. Too small values increase
            memory and CPU usage. SGA recommends a value of 75 for 100 bp
            reads.</doc>
        </parameter>
        <parameter name="minOverlapMerge" type="int" default="65">
            <doc>Minimum length of read overlap in the FM index merge step.
            Corresponds to the -m argument of "sga fm-merge". The value
            must be smaller than the read length and minOverlapAssemble.
            Too small values increase memory and CPU usage. SGA recommends
            a value of 65 for 100 base pair reads.</doc>
        </parameter>
        <parameter name="minContigLength" type="int" default="200">
            <doc>Minimum contig length in scaffolding. Corresponds
            to the -m argument in sga-bam2de.pl, sga-astat.py,
            "sga scaffold" and "sga scaffold2fasta".</doc>
        </parameter>
        <parameter name="correctionK" type="int" default="41">
            <doc>Error correction k-mer size. Corresponds to the
            -k argument of "sga correct".</doc>
        </parameter>
        <parameter name="indexBatchSize" type="int" default="0">
            <doc>When constructing BWT indexes, this many reads are
            indexed in one batch, using disk-based BWT construction. If
            the value is 0, an efficient in-memory "ropebwt" algorithm is used;
            this is suitable for reads up to 200 bp and can index 1.5 billion
            reads using 64 GB memory. If the value is negative, an in-memory
            "sais" algorithm is used (suitable for long reads). For very large
            data sets, use a value in the range of 2-10 million.</doc>
        </parameter>
        <parameter name="minBranchLength" type="int" default="150">
            <doc>In the assembly step, branches shorter than this number
            of base pairs are removed. Corresponds to the -l argument in
            "sga assembly". The default is tuned of 100 bp reads; experiments
            using longer reads should increase this value.</doc>
        </parameter>
        <parameter name="quality" type="string" default="phred33">
            <doc>For FASTQ input files, type of base quality values present.
            Legal values are phred33 and phred64.</doc>
        </parameter>
        <parameter name="memoryGB" type="int" default="16">
            <doc>An estimate on the maximum amount of memory in gigabytes that
            an individual SGA process will need. This is only used in a cluster
            environment.</doc>
        </parameter>
    </parameters>
</component>
