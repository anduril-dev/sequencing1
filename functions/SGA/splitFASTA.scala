// var1: work dir
// param1: FASTA filename in work dir

val MaxLength = 65536 - 10

val inputFile = new File(var1File, param1)
val reader = Table.reader(inputFile, Table.FormatFASTA)

val outputFile = new File(var1File, "tmp-split.fa")
val writer = Table.writer(reader, outputFile, Table.FormatFASTA)

for (row <- reader) {
    val sequence = row(1)
    val length = sequence.length
    if (length < MaxLength) {
        writer.write(row)
    } else {
        val originalID = row(0)
        var pos = 0
        while (pos < length) {
            val subSequence = sequence.substring(pos, Math.min(length, pos+MaxLength))
            row(0) = "%s_%d".format(originalID, pos)
            row(1) = subSequence
            writer.write(row)
            pos += MaxLength
        }
    }
}

writer.close()
outputFile.renameTo(inputFile)
