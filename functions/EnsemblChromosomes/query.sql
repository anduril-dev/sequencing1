SELECT seq_region_id,
       seq_region.name AS chromosome,
       length,
       coord_system.name AS coord_name,
       version,
       attrib
FROM  seq_region, coord_system
WHERE seq_region.coord_system_id = coord_system.coord_system_id
ORDER BY length DESC
