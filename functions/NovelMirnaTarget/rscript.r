table.out <- table1
if(param2=="reverse_comp"){
	    table.out[,2] <- reverse(table.out[,2])
            table.out[,2] <- gsub("A","t",table.out[,2])
	    table.out[,2] <- gsub("T","a",table.out[,2])
	    table.out[,2] <- gsub("G","c",table.out[,2])
            table.out[,2] <- gsub("C","g",table.out[,2])
	    table.out[,2] <- toupper(table.out[,2])
}else if(param2=="reverse"){
	    table.out[,2] <- reverse(table.out[,2])
}else if(param2=="comp"){
	    table.out[,2] <- gsub("A","t",table.out[,2])
	    table.out[,2] <- gsub("T","a",table.out[,2])
	    table.out[,2] <- gsub("G","c",table.out[,2])
            table.out[,2] <- gsub("C","g",table.out[,2])
	    table.out[,2] <- toupper(table.out[,2])
}