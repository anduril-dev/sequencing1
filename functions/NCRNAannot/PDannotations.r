novel <- table1
known <- table2

# Reformat potentially novel miRNAs
novel_clean <- novel[,1:3]
novelID <- paste(as.character(novel_clean[,1]),as.character(novel_clean[,2]),as.character(novel_clean[,3]),sep="_")
novel_clean <- cbind(novelID,novel_clean)

if (!is.null(known)) { # known has not been formatted
    expanded <- strsplit(as.character(known[,9]),"; ")

	    # extract only needed info
	    biotype <- c()
	    geneID <- c()
	    geneName <- c()
	    for (n in 1:length(expanded)) {
		    biotype <- c(biotype,strsplit(expanded[[n]][grep("biotype",expanded[[n]])]," ")[[1]][2])
		    geneID <- c(geneID,strsplit(expanded[[n]][grep("gene_id",expanded[[n]])]," ")[[1]][2])
		    geneName <- c(geneName,strsplit(expanded[[n]][grep("gene_name",expanded[[n]])]," ")[[1]][2])
	    }

	    # Keep only those with unique gene ID
	    biotypeFltrd <- biotype[which(duplicated(geneID)==FALSE)]
	    geneIDFltrd <- geneID[which(duplicated(geneID)==FALSE)]
	    geneNameFltrd <- geneName[which(duplicated(geneID)==FALSE)]
	    knownFltrd <- known[which(duplicated(geneID)==FALSE),]

	    # Position information in columns 1,4,5 and 7
	    knownFltrd <- knownFltrd[,c(1,4,5,7)]
	    known_clean <- cbind(knownFltrd,biotypeFltrd,geneIDFltrd,geneNameFltrd)
	    colnames(known_clean) <- c("chr","start","end","strand","biotype","geneID","Name")
	    knownID <- paste(as.character(known_clean[,"geneID"]),as.character(known_clean[,"biotype"]),as.character(known_clean[,"Name"]),sep="_")
	    known_annot <- cbind(knownID,known_clean[,1:3])

} else { # known has been formatted prior.
	known_clean <- read.table(param1, header=TRUE, sep="\t")

    # Check that all columns are correct and included
    if (length(grep(paste(c("chr","start","end","strand","biotype","geneID","Name"),collapse="|"), colnames(known_clean))) != 7) {
        stop(paste("INPUT ", param1," does not have the correct columns. Must include: chr, start, end, biotype, geneID, Name",sep=""))
    }

	knownID <- do.call("paste",known_clean[,c("geneID","biotype","Name")])
    knownID <- gsub(" ","_",knownID)
    known_annot <- cbind(knownID,known_clean[,c("chr","start","end")])
}

chr <- known_annot[,"chr"]
chr <- gsub("X","23",chr)
chr <- gsub("Y","24",chr)
chr <- gsub("MT","25",chr)
known_annot[,"chr"] <- chr

toKeep <- names(table(known_clean[,"biotype"]))[grep("rna|RNA|non_coding",names(table(known_clean[,"biotype"])))]

# Filter out all non-ncRNA biotypes
clean <- known_annot[grep(paste(toKeep,collapse="|"),known_clean[,"biotype"]),]
chr <- chr[grep(paste(toKeep,collapse="|"),known_clean[,"biotype"])]

# Only take annotation info from valid chromosomal locations
table.out <- clean[which(is.na(as.numeric(chr))==FALSE),]

array.out <- list(known=known_annot,novel=novel_clean, known_annotated=known_clean)
