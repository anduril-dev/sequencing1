<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>BamStats</name>
    <version>1.0</version>
    <doc> The function collects alignment and coverage statistics from a bam file. It uses picard CollectMultipleMetrics, bedtools genomecov/coverage and some in-house plotting and reporting scripts
    to produce the output. The function accept sorted bam files produced by either whole-genome or targeted (exome) sequencing experiments. 
    </doc>
    <author email="amjad.alkodsi@helsinki.fi">Amjad Alkodsi</author>
    <category>Alignment</category>
    <requires URL="http://picard.sourceforge.net/">picard-tools</requires>
    <requires URL="http://bedtools.readthedocs.org/en/latest/">bedtools</requires>
    <requires type="R-package" URL="http://cran.r-project.org/web/packages/ggplot2/index.html">ggplot2</requires>
    <requires type="R-package" URL="http://cran.r-project.org/web/packages/hwriter/index.html">hwriter</requires>

    <inputs>
		<input name="bam" type="BAM" optional="false">
            <doc>Input bam file, should be sorted.</doc>
        </input>
		<input name="refGenome" type="FASTA" optional="false">
            <doc>The reference genome used to produce the alignment.</doc>
        </input>

        <input name="targets" type="BED" optional="true">
            <doc>Targets file if experiment is targeted.</doc>
        </input>
		<input name="chrLength" type="CSV" optional="true">
            <doc>Chromosomes lengths formatted as a headerless CSV with three columns: chr,start,end. Required only if the experiment is not targeted. Only chromosomes specified by this file will be analyzed.</doc>
        </input>
        		<input name="markStats" type="BinaryFile" optional="true">
            <doc>The statistics file reported by Picard MarkDuplicates (Anduril function DuplicateMarker). If specified, the statistics will be included in the report.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="report" type="BinaryFolder">
            <doc>Binary folder containing index.html and plotted images.</doc>
        </output>
        <output name="summary" type="CSV">
            <doc>Two-column CSV file with metrics in first column and measured statistics in the second column which named as the <code>sampleName</code> parameter. This file can be easily combined with other files
            when iterating over large number of samples.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="picard" type="string" default="">
            <doc>Path to Picard directory, e.g. "/mnt/csc-gc5/opt/picard-tools-1.113", which containg the Picard-tools .jar files. If empty 
            string is given (default), PICARD_HOME environment variable is assumed to point to the Picard directory. Note that some older versions of picard have bugs in the CollectMultipleMetrics module.</doc>
        </parameter>
        <parameter name="bedtools" type="string" default="">
            <doc>Path to bedtools binary directory,If empty string is given (default), BEDTOOLS_HOME environment variable is assumed to point to the bedtools directory.</doc>
        </parameter>
        <parameter name="memory" type="string" default="4g">
            <doc>This value is used with Picard. e.g. "4g" or "8g".
            </doc>
        </parameter>
        <parameter name="targeted" type="boolean" default="false">
            <doc>Whether the sequencing experiment is targeted or not. If true, the <code>targets</code> input should be specified, and if false, the <code>chrLength</code> input should be specified.
           </doc>
        </parameter>
        <parameter name="paired" type="boolean" default="true">
            <doc>Whether the bam file is paired-end or single-end.
           </doc>
        </parameter> 
        <parameter name="stopAfter" type="int" default="0">
	        <doc>Number of reads that picard will use to report the statistics. The default value "0" will use all reads in the input file.</doc>
		</parameter>
		<parameter name="maxCoverage" type="int" default="150">
            <doc>Coverage higher than this value will be truncated from the histogram. Negative or zero value will suppress truncation.
	        </doc>
        </parameter>
		<parameter name="sampleName" type="string" default="Sample">
            <doc> Sample name or key to be used in the report and the output summary.             
	   		</doc>
        </parameter>
    </parameters>
</component>
