# Final collapse of chromosomal regions in full expression matrix
# Give each row a unique ID based on Point position information
clusterID <- paste(as.character(table1[,"chr"]),as.character(table1[,"start"]),as.character(table1[,"end"]),as.character(table1[,"strand"]), sep="_")
rownames(table1) <- clusterID

# Merge ID with point distance info
collapsable <- 0
IDs <- cbind(clusterID,collapsable)

# Positions are sorted, so should only have to compare to neighbour
for (N in 2:nrow(IDs)) {
	refStart <- table1[N,"start"]
	refEnd <- table1[N,"end"]
	refChr <- table1[N,"chr"]
	refStrd <- table1[N,"strand"]
	if ((table1[N-1,"chr"] == refChr)
		& (table1[N-1,"strand"] == refStrd)
		& (min(abs(table1[N-1,"start"] - refStart),
				abs(table1[N-1,"end"] - refEnd)) < 10)) {
		IDs[N,"collapsable"] <- "1"
		IDs[N-1,"collapsable"] <- "1"
		}				
	}

# Find the clusterID of the ones to merge
collapsable <- IDs[which(IDs[,"collapsable"] == 1),1]


# Make duplicate copy to work with
table1_collapsed <- table1

if (length(collapsable) != 0) {
	# Evaluate each paired distance of a given id value
	for (R in 2:length(collapsable)) {
		if ((table1[collapsable[R],"chr"] == table1[collapsable[R-1],"chr"])
			& (table1[collapsable[R],"strand"] == table1[collapsable[R-1],"strand"])
			& (min(abs(table1[collapsable[R-1],"start"] - table1[collapsable[R],"start"]),
					abs(table1[collapsable[R-1],"end"] - table1[collapsable[R],"end"])) < 10)) {

			# Calculate union of values by clusterID
			present <- collapsable[R]
			previous <- collapsable[R-1]
			newStart <- min(table1_collapsed[c(present,previous),"start"])
			newEnd <- max(table1_collapsed[c(present,previous),"end"])
			newSum <- colSums(table1_collapsed[c(present,previous),5:ncol(table1_collapsed)],na.rm=TRUE)
		
			# Replace info for present entry
			table1_collapsed[present,"start"] <- newStart
			table1_collapsed[present,"end"] <- newEnd
			table1_collapsed[present,5:ncol(table1_collapsed)] <- newSum
	
			# Delete previous entry
			table1_collapsed <- table1_collapsed[-which(rownames(table1_collapsed) == previous),]
			}
	}
}

table.out <-table1_collapsed
