# Create unique cluster IDs based on Distance and Point_chr.
# If Distance is NAN, use a default ID number
# Assumed both table1 and table2 outputs are IN THE SAME ORDER and OF THE SAME DIMENSIONS

# Change reference names for miranalyzer to match mirdeep2
if (length(grep("chrom",colnames(table1))) != 0) {
	colnames(table1)[3:5] <- c("Point chr","Point start","Point end")
	colnames(table2)[1:4] <- c("chr","start","end","strand","total.read.count")
}

# Format tables for easier reference
colnames(table1) <- gsub("\\."," ",colnames(table1))
clusterID <- paste(as.character(table2[,"chr"]),as.character(table2[,"start"]),as.character(table2[,"end"]),as.character(table2[,"strand"]), sep="_")

# Collapse duplicate rows --- means alignment of reads was a little different
if (length(which(duplicated(clusterID) | (table1[,"Distance"] == 0))) > 0) {
	duplicates <- as.character(which(duplicated(clusterID) | (table1[,"Distance"] == 0)))
	for (D in 2:length(duplicates)) {
		present <- which(rownames(table2) == duplicates[D])
		previous <- which(rownames(table2) == duplicates[D-1])
		if ((table2[present,"chr"] == table2[previous,"chr"])
			& (table2[present,"start"] == table2[previous,"start"])
			& (table2[present,"end"] == table2[previous,"end"])
			& (table2[present,"strand"] == table2[previous,"strand"])) {
			
			table2[present,"total.read.count"] <- sum(table2[c(present,previous),"total.read.count"])
			table1 <- table1[-previous,]
			table2 <- table2[-previous,]
			clusterID <- clusterID[-previous]
		}
	}
}

# Finish formatting	
rownames(table2) <- clusterID
rownames(table1) <- clusterID
readCount <- table2[,"total.read.count"]

# Find the clusterID of those with short enough distances (< param1) to merge
clusterIDs <- rownames(table1)[which(table1[,"Distance"] < as.numeric(param1))]

# Make copy of count matrix
table2_collapsed <- table2

# Positions are sorted, so only need to compare present row with previous row
if (length(clusterIDs) > 1) {
	# Retrieve chromosomal positions and union of positions
	for (R in 2:length(clusterIDs)) {
		present <- clusterIDs[R]
		previous <- clusterIDs[R-1]
		if ((table2[present,"chr"] == table2[previous,"chr"])
		& (table2[present,"strand"] == table2[previous,"strand"])
		& (min(abs(table2[present,"start"] - table2[previous,"start"]),
				abs(table2[present,"end"] - table2[previous,"end"])) < 10)) {

		# Calculate union of values by clusterID
		newStart <- min(table2_collapsed[c(present,previous),"start"])
		newEnd <- max(table2_collapsed[c(present,previous),"end"])
		newSum <- sum(table2_collapsed[c(present,previous),"total.read.count"],na.rm=TRUE)
		
		# Replace info for present entry
		table2_collapsed[present,"start"] <- newStart
		table2_collapsed[present,"end"] <- newEnd
		table2_collapsed[present,"total.read.count"] <- newSum
	
		# Delete previous entry
		table2_collapsed <- table2_collapsed[-which(rownames(table2_collapsed) == previous),]
		}
	}
}
	
# Trim to just the required information
combin <- table2_collapsed[,c("chr","start","end","strand","total.read.count")]
colnames(combin) <- c("chr","start","end","strand",param2)

table.out <- combin
