# The last row in table1 contains the combined expression matrix of ALL samples
novelExpr <- table1

# Check percentage value
allsWell <- TRUE
if (is.na(as.numeric(param1))) {
	stop(">> Percent parameter is not a positive numeric value.")
	allsWell <- FALSE
} else {
	percent <- as.numeric(param1)
	if (percent < 0) {
		stop(">> Percent parameter is not a positive numeric value.")
		allsWell <- FALSE
	} else if (percent < 1) {
		print("WARNING: percent parameter is < 1%")
	} else if ( percent > 100) {
		stop(">> Percent parameter is greater than 100%.")
		allsWell <- FALSE
	} else {
		print("Percent parameter is OK.")
	}
}

if (allsWell) {
	toRemove <- c()
	totalSample <- ncol(novelExpr[-c(1:4)])
	for (r in 1:nrow(novelExpr)) {
		summary <- table(unlist(novelExpr[r,-c(1:4)]),useNA="ifany")
		rowPercent <- sum(summary[which(names(summary) != "0" | names(summary) != NA)]) / totalSample * 100
		if (rowPercent < percent) {
			toRemove <- c(toRemove,r)
		}
	}
	
	if (length(toRemove) > 0) {
		table.out <- novelExpr[-toRemove,]
	} else {
		table.out <- novelExpr
	}
	
} else {
	table.out <- data.frame()
	stop("SOMETHING WENT WRONG. SEE ERRORS.")
}

# Replace NAs with 0s
convert <- table.out
convert[is.na(convert)] <- 0

table.out <- convert

ID <- paste(as.character(convert[,"chr"]),as.character(convert[,"start"]),as.character(convert[,"end"]),as.character(convert[,"strand"]),sep="_")
convert <- convert[,-c(1:4)]
ID <- gsub(" ","",ID) # remove spaces
convert <- cbind(ID,convert)

array.out <- list(coordinatesID = convert)
