<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>QCFasta</name>
    <version>5.0</version>
    <doc>
        Quality control function for RNA-Seq data. 
        It takes an array of fastq paired or single-end reads and performs and filters the low quality sequences from it.
        The function carries out the following steps:
        <ul>
            <li> Obtains statistics on sequences with FastQC, if input fastQCfolders is present this step is skipped. </li>
            <li> Filter out files that do not have at least a certain <i>percentage</i> of sequences with a minimum quality of <i>minQuality</i>.</li>
            <li> Adaptor removal and quality trimming with either Trimmomatic or trimGalore.</li>
            <li> Filter out files that do not have enough remaining sequences after the trimming step.</li>
            <li> Obtain statistics on the preprocessed sequences with FastQC and create html report. </li>
        </ul>
        Three tools are available for quality trimming: TrimGalore, Trimmomatic, and FastX.
        All tools remove adapters, trim low quality bases at both ends, and discard too short sequences.
        FastX only works with single-end data; TrimGalore and Trimmomatic can be used for both single and paired end and in the latter case remove unpaired reads.
        TrimGalore has options for RRBS libraries.
        Trimmomatic is faster than trimGalore and has a sliding window that is useful to trim sequences that have low quality bases that are not at the extremes of the sequence.
        FastX seems to be better at removing known adaptors.

        Most parameters work with all tools in the same way, but some have slightly different behavior (ex. stringency) or are specific to one of the tools.
        
        QCFasta may have issues creating the quality report if the keys for the samples are single numbers from 1-10 (it is recommended to use keys that are clearly a string, i.e. not just numbers).
        If the fastQCfolders input is used then the parameters readkey and mateKey need to be set. This parameters are a substring of the names FastQC gave to the folders containing the reads and 
        mates statistics. Usually reads have "_1.fq" ending while mates end in "_2.fq". In that case readKey="_1" and mateKey="_2". If you used SeqQC then the word "read" and "mate" were added to the 
        folder names and in that case you do not need to modify readKey and mateKey since "read" and "mate" are the default values for them.
   </doc>
    <author email="alejandra.cervera@helsinki.fi">Alejandra Cervera</author>
    <author email="erkka.valo@helsinki.fi">Erkka Valo</author>

    <inputs>
    	<input name="reads" type="FASTQ" array="true">
            <doc>An array of reads.</doc>
        </input>
        <input name="mates" type="FASTQ" array="true" optional="true">
            <doc>An array of mates for the reads if the data is paired-end. 
                 The matching of reads to mates is done by the array keys.</doc>
        </input>
        <input name="fastQCfolders" type="BinaryFolder" array="true" optional="true">
            <doc>If FastQC output is available you can skip running it again by providing the output folders as an array.</doc>
        </input>
        <input name="adapterSinglecell" type="FASTA" optional="true">
            <doc>Adapter file for single cell RNA-seq data. Only for using Trommomatic to trim single cell reads.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="reads" type="FASTQ" array="true">
            <doc>An array of sequences that passed the quality control step</doc>
        </output>
        <output name="mates" type="FASTQ" array="true">
            <doc>An array sequences that passed the quality control step</doc>
        </output>
        <output name="report" type="HTML">
            <doc>Quality control report</doc>
        </output>
        <output name="images" type="BinaryFile" array="true">
            <doc>Quality control images</doc>
        </output>
        <output name="surviving" type="CSV">
            <doc>Samples that survived the sequence filtering step in CSV format.</doc>
        </output>
         <output name="unpairedReads" type="FASTQ" array="true">
            <doc>For paired end data only, reads that passed quality trimming but the mate did not.</doc>
        </output>
        <output name="unpairedMates" type="FASTQ" array="true">
            <doc>For paired end data only, mates that passed quality trimming but the read did not.</doc>
        </output>

    </outputs>
    <parameters>
        <parameter name="tool" type="string" default="trimmomatic">
            <doc>Choose trimmomatic, trimGalore or fastx for adapter removal and quality trimming</doc>
        </parameter>
        <parameter name="percentage" type="float" default="0.3">
            <doc>Minimum percentage of sequences in each file that should have at least <code>minQuality</code> average quality.
            </doc>
        </parameter>
        <parameter name="qual" type="string" default="">
            <doc>Encoding used by the sequencer: phred33 or phred64, if empty then we try to guess it.</doc>
        </parameter>
        <parameter name="minQuality" type="int" default="20">
            <doc>Bases below this quality threshold will be trimmed from the 5' end of the sequence.</doc>
        </parameter>
        <parameter name="minLength" type="int" default="20">
            <doc>Reads shorter than minLength will be removed. In paired-end sequencing also the corresponding mate is removed. No trimming is done if the value is set to 0.</doc>
        </parameter>
        <parameter name="minSequences" type="float" default="0.3">
            <doc>Files for which at least <code>minSequences</code> 
                 fraction of reads pass the second filtering step are kept.
            </doc>
        </parameter>
        <parameter name="stringency" type="int" default="2">
            <doc>Minimum overlap of sequence with the adapter for the bases to be trimmed (TrimGalore and FastX); allowed mismatches with the adaptor (Trimmomatic).
            </doc>
        </parameter>
        <parameter name="headcrop" type="int" default="0">
            <doc>The number of bases to remove from the start of the read.
                 No trimming is done if the value is set to 0.
            </doc>
        </parameter>
        <parameter name="extra" type="string" default="">
            <doc>Extra parameters for trim Galore! or for FastX.</doc>
        </parameter>
        <parameter name="leading" type="int" default="30">
            <doc> Remove bases from the start of the read, if quality value is
                  below the given threshold. Only Trimmomatic.
            </doc>
        </parameter>
        <parameter name="trailing" type="int" default="30">
            <doc> Remove bases from the end of the read, if quality value is
                  below the given threshold. Only Trimmomatic.
            </doc>
        </parameter>
        <parameter name="slidingWindow" type="string" default="null">
            <doc> Sliding window trimming where the sequence is cut if the
                  average quality of the bases within the sliding window falls
                  below the defined threshold. A string specifies the window
                  size and the average required quality in the sliding window.
                  The format is windowSize:requiredQuality. For example, 4:15
                  (window size = 4; required quality = 15). No trimming is done
                  if value is set to 'null'. Only Trimmomatic.
            </doc>
        </parameter>
        <parameter name="adapter" type="string" default="">
            <doc>
              Adapter specified directly as a string for all tools; for Trimmomatic you can also specify the Illumina adapter file to use instead: TruSeq2-SE.fa, TruSeq2-PE.fa, TruSeq3-SE.fa, or
              TruSeq3-PE.fa.
            </doc>
        </parameter>
        <parameter name="gzip" type="boolean" default="false">
            <doc>Defines if the output sequences should be gzipped or not.</doc>
        </parameter>
        <parameter name="readKey" type="string" default="read">
            <doc>Key to identify reads from mates in the FastQCfolders, it is part of the filename usually "read" or "_1". It has to be immediately before the extension in the filename.</doc>
        </parameter>
        <parameter name="mateKey" type="string" default="mate">
            <doc>Key to identify mates from reads in the FastQCfolders, it is part of the filename usually "mate" or "_2". It has to be immediately before the extension in the filename.</doc>
        </parameter>    
        <parameter name="simpleClip" type="int" default="12">
            <doc>A threshold specifies how accurate the match between any adapter must be against a read. Each matching base adds just over 0.6. Only Trimmomatic.</doc>
        </parameter>    
        <parameter name="crop" type="int" default="-1">
            <doc>Crop bases at the end of the read so it maximally has the crop size, only for Trimmomatic and FastX.</doc>
        </parameter>
        <parameter name="minPercent" type="int" default="20">
            <doc>Minimum percentage of bases that must have at least <code>minQuality</code> for a read to be kept. Only FastX.</doc>
        </parameter>
        <parameter name="isSinglecell" type="boolean" default="false">
            <doc>Define if the read files are from Linnarson's single cell RNA-seq protocol (STRT). </doc>
        </parameter>     
        <parameter name="UMIlen" type="int" default="6">
            <doc>The length of unique molecular identifiers (UMIs), only used when isSingleCell is true.</doc>
        </parameter>
        <parameter name="UMIsliding" type="string" default="1:17">
            <doc>slidingWindow for UMIs. For example, when UMIsliding = 1:17, any UMI bases with a quality lower than 17 will be removed. Only used when isSingleCell is true. </doc>
        </parameter>
    </parameters>
</component>
