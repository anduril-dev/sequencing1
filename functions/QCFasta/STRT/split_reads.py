#!/usr/bin/python
import os
import sys
import gzip

def assure_path_exists(path):
        tar = os.path.dirname(path)
        if not os.path.exists(tar):
            os.makedirs(tar)

def myopen(myfile, mode):
    if myfile.endswith('.gz'):
        return gzip.open(myfile, mode)
    else: 
        return open(myfile, mode)

filein = sys.argv[1]
UMIs_out= sys.argv[2]
Tseqs_out = sys.argv[3]
UMIlen = int(sys.argv[4])
assure_path_exists(UMIs_out)
assure_path_exists(Tseqs_out)
readfile = myopen(filein, 'r')
UMIs = myopen(UMIs_out,'w')
Tseqs = myopen(Tseqs_out, 'w')

i = 0
for line in readfile:
    i = i + 1
    if i==1:
        ID = line
    elif i==2:
        UMI = line[0:UMIlen]
        Tseq = line[UMIlen:]
    elif i==3:
        plus = line
    elif i==4:
        UMI_Q = line[0:UMIlen]
        Tseq_Q = line[UMIlen:].strip()
        i=0
    if i==0:
        print >> UMIs, ID+UMI+'\n'+plus+UMI_Q
        print >> Tseqs, ID+Tseq+plus+Tseq_Q
UMIs.close()
Tseqs.close()
readfile.close()
