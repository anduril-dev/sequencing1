## Quality control figures for reads.
## before filtering.
while read -r line; do
    a=$( echo $line | cut -d' ' -f2 )
    n=$( echo $line | cut -d' ' -f1 )
    name=$( basename $a .png )
    cp $a @folder1@/@param1@_$name.png
#    cp $a @arrayOut1@/@param1@_$name.png
    echo -e "@param1@_$n\t@folder1@/@param1@_$name.png" >> @arrayIndex1@
    ## Annotation for figures
    echo -e "File\tAnnotation" >> @arrayOut2@/@param1@_${name}_annotation.csv
    echo -e "@param1@_$name.png\tThe ${name} for @param1@." >> @arrayOut2@/@param1@_${name}_annotation.csv
    echo -e "@param1@_$n\t@param1@_${name}_annotation.csv" >> @arrayIndex2@
done < @var1@
