## Combine quality control figures for reads and mates
## before and after filtering.

while read -r line; do
    a=$( echo $line | cut -d' ' -f2 )
    b=$( echo $line | cut -d' ' -f3 )
    c=$( echo $line | cut -d' ' -f4 )
    d=$( echo $line | cut -d' ' -f5 )
    n=$( echo $line | cut -d' ' -f1 )
    name=$( basename $a .png )
    echo $name
    montage $a $b $c $d -tile 2x2 -geometry +0+0 @folder1@/@param1@_$name.png
    addarray array1 @param1@_"$n" @folder1@/@param1@_"$name.png"
done < @var1@

