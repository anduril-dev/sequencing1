pwd
cp -vr @param1@/*@param2@_fastqc/Images @folder1@/preStats_read
if [ ! -e @folder1@/preStats_read/kmer_profiles.png ]
then
    cp -v @var1@/kmer_profiles.png @folder1@/preStats_read/
fi
if [ ! -e @folder1@/preStats_read/per_tile_quality.png ]
then
    cp -v @var1@/per_tile_quality.png @folder1@/preStats_read/
fi
addarray array1 "read" @param1@/*@param2@_fastqc/fastqc_data.txt
addarray array1 "read_warnings" @param1@/*@param2@_fastqc/summary.txt

if [ -d @param1@/*@param3@_fastqc ]
then 
    cp -vr @param1@/*@param3@_fastqc/Images @folder1@/preStats_mate
    if [ ! -e @folder1@/preStats_mate/kmer_profiles.png ]
    then
        cp -v @var1@/kmer_profiles.png @folder1@/preStats_mate/
    fi
    if [ ! -e @folder1@/preStats_mate/per_tile_quality.png ]
    then
        cp -v @var1@/per_tile_quality.png @folder1@/preStats_mate/
    fi
    addarray array1 "mate" @param1@/*@param3@_fastqc/fastqc_data.txt
    addarray array1 "mate_warnings" @param1@/*@param3@_fastqc/summary.txt
fi
