## Combine quality control figures for reads
## before and after filtering.

figCaption=$"Read before (top) and after (bottom) quality control."

while read -r line; do
    a=$( echo $line | cut -d' ' -f2 )
    b=$( echo $line | cut -d' ' -f3 )
    n=$( echo $line | cut -d' ' -f1 )
    name=$( basename $a .png )
    convert $a $b -append @folder1@/@param1@_$name.png
#    convert $a $b -append @arrayOut1@/@param1@_$name.png
    echo -e "@param1@_$n\t@folder1@/@param1@_$name.png" >> @arrayIndex1@
    ## Annotation for figures
    echo -e "File\tAnnotation" >> @arrayOut2@/@param1@_${name}_annotation.csv
    echo -e "@param1@_$name.png\tThe ${name} for @param1@. ${figCaption}" >> @arrayOut2@/@param1@_${name}_annotation.csv
    echo -e "@param1@_$n\t@param1@_${name}_annotation.csv" >> @arrayIndex2@
done < @var1@
