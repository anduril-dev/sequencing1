## Get file sizes of reads and mates before and after filtering.

## Returns a the file size in human readable format.
get.file.size <- function(file) {
    if(file.exists(file)) {
        command <- paste("ls -lh", file)
        strsplit(system(command, intern=T), " ")[[1]][5]
    }else {
        return(NA)
    }
}
table.out <- data.frame(Sample=character(), 
                        stringsAsFactors=FALSE)

for(i in 1:nrow(table1)) {
    sample <- table1[i, "Key"]
    print(sample)
    table.out[i, "Sample"] <- sample
    table.out[i, "File Size Read"] <- get.file.size(table1[i, "File"])
    ## If the sample survived filtering.
    if(sample %in% table2[,"Key"]) {
        table.out[i, "File Size Read After Filtering"] <- get.file.size(table2[table2[,"Key"] == sample, "File"])
    }else {
        table.out[i, "File Size Read After Filtering"] <- NA
    }
    ## Check if the data was pair ended and the mates exists.    
    if(!is.null(table3) & !is.null(table4)) { 
        table.out[i, "File Size Mate"] <- get.file.size(table3[table3[,"Key"] == sample, "File"])
        ## If the sample survived filtering.
        if(sample %in% table4[,"Key"]) {
            table.out[i, "File Size Mate After Filtering"] <- get.file.size(table4[table4[,"Key"] == sample, "File"])
        }else {
            table.out[i, "File Size Mate After Filtering"] <- NA
        }
    }
}
table.out
