
library(RColorBrewer)



#setwd(document.dir)
data <- data.frame(table1)
cols1 <- colorRampPalette(brewer.pal(8,"Accent"))(5)
cols2 <- rev(colorRampPalette(brewer.pal(8,"Accent"))(5))
cols=c("#7FC97F","#EDBB99","#9BB5A4","#E31864","#666666")

df<-data[,-1]
print (head(df))
percentages<-apply(df,2,function(x) (x*100) / data$Input_Sequences)
print (head(percentages))
max<-apply(percentages,2,max)
min<-apply(percentages,2,min)
l<-paste(round(min[-1]),round(max[-1]),sep="-")
labels<-paste(l,"%",sep="")
print(labels)
print(cols1)

png(get.output(cf,"optOut1"), width = 1100, height = 480, units = "px")

par(mfrow=c(1,2), oma=c(0,0,2,0),mar=c(4,4,4,2),xpd=NA)
boxplot(data[,2:3],col=cols[1:2], ylab="million reads", main="Overall number of sequences in all samples")

par(mar=c(4,4,4,10))
boxplot(data[,4:ncol(data)],col=cols[-c(1,2)],ylab="million reads", main=param1)

legend("topright", inset=c(-0.3,0),legend=labels,bty="n",fill=cols[-1],title="Min-Max %")

title("Read Counts Before and After Trimming", outer = TRUE, line=-1)
dev.off()

table.out <- table1
rm (optOut1)

