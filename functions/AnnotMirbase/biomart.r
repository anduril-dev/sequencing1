# Run biomaRt to extract transcript IDs for all know miRNAs
# BiomartAnnotator cannot take "values" parameter as a boolean (string only)

# If required package not installed, install it
is.installed <- function(pkg) {
                       is.element(pkg, installed.packages()[,1])
                       }

if (!is.installed("biomaRt")) {
                    print("Downloading required R package biomaRt")
                    update.packages(checkBuilt=TRUE, ask=FALSE)
                    source("http://bioconductor.org/biocLite.R")
                    biocLite("biomaRt")
                    }
library(biomaRt)
ensembl <- useMart(host=param2, biomart="ENSEMBL_MART_ENSEMBL", dataset=param3)

attributes <- unlist(strsplit(tolower(gsub("[[:blank:]]","",param1)),","))
attributes <- c(attributes,"chromosome_name","transcript_start","transcript_end")
table.out <- getBM(attributes=attributes, filters="with_mirbase",values=TRUE, mart=ensembl)
