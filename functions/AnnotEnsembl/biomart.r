# Run biomaRt to extract transcript IDs for all know miRNAs
# BiomartAnnotator cannot take "values" parameter as a boolean (string only)

# If required package not installed, install it
is.installed <- function(pkg) {
                       is.element(pkg, installed.packages()[,1])
                       }

if (!is.installed("biomaRt")) {
                    print("Downloading required R package biomaRt")
                    update.packages(checkBuilt=TRUE, ask=FALSE)
                    source("http://bioconductor.org/biocLite.R")
                    biocLite("biomaRt")
                    }

if (!is.installed("Biostrings")) {
                    print("Downloading required R package biomaRt")
                    update.packages(checkBuilt=TRUE, ask=FALSE)
                    source("http://bioconductor.org/biocLite.R")
                    biocLite("Biostrings")
                    }

library(biomaRt)
library(Biostrings)

ncRNA <- readDNAStringSet(param4, format="fasta")
ncRNA_ref <- do.call(rbind,strsplit(names(ncRNA)," "))

ensembl <- useMart(host=param2, biomart="ENSEMBL_MART_ENSEMBL", dataset=param3)

attributes <- unlist(strsplit(tolower(gsub("[[:blank:]]","",param1)),","))
attributes <- c(attributes,"chromosome_name","transcript_start","transcript_end","strand")
table.out <- getBM(attributes=attributes, filters="ensembl_transcript_id",values=ncRNA_ref[,1], mart=ensembl)

# Create a pseudo-GFF file
pseudoGFF <- cbind(table.out[,1:2],rep("exon",nrow(table.out)),table.out[,c("transcript_start", "transcript_end")],rep(".",nrow(table.out)), table.out[,"strand"],rep(".", nrow(table.out)), paste("ensembl_transcript_id ", table.out[,1], "; external_transcript_name ", table.out[,3], "; chr ", table.out[,"chromosome_name"], sep=""))

#Convert strands to +/-
pseudoGFF[,7] <- gsub(-1,"-",pseudoGFF[,7])
pseudoGFF[,7] <- gsub(1,"+",pseudoGFF[,7])

# Convert transcript start and end positions to be relative to the transcript itself (i.e. start is always 1)
relativeEnd <- pseudoGFF[,5] - pseudoGFF[,4]
relativeStart <- rep(1,nrow(pseudoGFF))
pseudoGFF[,5] <- relativeEnd
pseudoGFF[,4] <- relativeStart

# Create output directory for GFF
table.out <- data.frame()
outdir=get.output(cf,'optOut1')
dir.create(outdir)
write.table(pseudoGFF, paste(outdir,"/ncrna.gff",sep=""),row.names=FALSE, col.names=FALSE, sep="\t",quote=FALSE)
rm(optOut1)
