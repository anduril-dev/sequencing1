import sys
import csv
import numpy as np
import matplotlib
matplotlib.use('PDF')
import matplotlib.pyplot as plt
from matplotlib import cm as cm

data = np.loadtxt(sys.argv[1],skiprows=1,delimiter='\t')
b = np.corrcoef(data,rowvar=0)
c = np.around(b,2)
np.savetxt(sys.argv[2],c,delimiter='\t',fmt="%.2f")

with open(sys.argv[1]) as f:
    
    reader = csv.reader(f,delimiter='\t')
    header = reader.next()

mask =  np.tri(c.shape[0], k=-1)
c = np.ma.array(c, mask=mask)

column_labels = header
row_labels = header

fig, ax = plt.subplots()
cmap =cm.get_cmap('Blues')
cmap.set_bad('w')
heatmap = ax.pcolor(c, cmap=cmap)
ax.set_xticks(np.arange(c.shape[0])+0.5, minor=False)
ax.set_yticks(np.arange(c.shape[1])+0.5, minor=False)
ax.invert_yaxis()
ax.xaxis.tick_top()
ax.set_xticklabels(row_labels, minor=False,fontsize=4)
ax.set_yticklabels(column_labels, minor=False,fontsize=4)
plt.xticks(rotation=90)
plt.colorbar(heatmap)
plt.axis('tight')
plt.savefig(sys.argv[3])
