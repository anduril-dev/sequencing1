<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>VariantSelector</name>
    <version>1.0</version>
    <doc>
    Select variants from a VCF file using specific criteria, e.g. type, annotations or genomic intervals.

    Complete documentation:
    <ul>
    <li><a href="http://www.broadinstitute.org/gsa/wiki/index.php/Introduction_to_the_GATK" 
    target="_BLANK">Introduction to GATK</a></li>
    <li><a href="http://www.broadinstitute.org/gsa/gatkdocs/release/org_broadinstitute_sting_gatk_walkers_variantutils_SelectVariants.html" 
    target="_BLANK">SelectVariants (gatk)</a></li>
    </ul>
    </doc>
    <author email="rony.lindell@helsinki.fi">Rony Lindell</author>
    <category>VariationAnalysis</category>
    <inputs>
        <input name="reference" type="FASTA" optional="false">
            <doc>The reference fasta file.</doc>
        </input>
        <input name="variants" type ="VCF" optional="false">
            <doc>Input variants.</doc>
        </input>
        <input name="intervals" type="BED" optional="true">
            <doc>Select only variants in the genomic regions specified in this file. Other than BED type files can be
            forced. Explore the allowed formats in the GATK wiki.</doc>
        </input>
        <input name="conc" type="VCF" optional="true">
            <doc>Concordance variants. Output only the variants that were also called in this file.</doc>
        </input>
        <input name="disc" type="VCF" optional="true">
            <doc>Discordance variants. Output only the variants that were not called in this file.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="calls" type="VCF">
            <doc>The selected variants.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="gatk" type="string" default="">
            <doc>Path to GATK jar file, e.g. "/opt/gatk/GenomeAnalysisTK.jar". If empty string is given (default), GATK_HOME environment variable is assumed to point to the GATK directory where GenomeAnalysisTK.jar is located.</doc>
        </parameter>
        <parameter name="type" type="string" default="">
            <doc>Type of variants to select. A comma separated list will choose multiple types. Values: {INDEL, SNP, MIXED, MNP, SYMBOLIC, NO_VARIATION}
            <br /><br />
            Example: type="INDEL,MNP" selects all indel and phased variants.
            </doc>
        </parameter>
        <parameter name="select" type="string" default="">
            <doc>Selection criteria as a JEXL expression. See the
            <a href="http://www.broadinstitute.org/gsa/wiki/index.php/Using_JEXL_expressions" 
            target="_BLANK">GATK wiki</a> more information about how to construct the expression. Note the case-sensitivity of the annotations.
            <br /><br />
            Example: select="QUAL > 30.0 &amp;&amp; DP == 10"
            </doc>
        </parameter>
        <parameter name="region" type="string" default="">
            <doc>Genomic region from which to select, e.g. "chr1" or "chr2:1-20000". The region can also be a comma separated list, e.g. "chr1,chr2:1-20000,chrX:1-4000". A more extensive list of regions can be defined as a file using the 'intervals' input.</doc>
        </parameter>
        <parameter name="memory" type="string" default="4g">
            <doc>The amount of java-heap memory being allocated to GATK, given in the format "4g" for 4 gigabytes or "2560m" for 2560 megabytes (2,5g) etc. For huge data sets when using a memory-draining selection this may require an increase.</doc>
        </parameter>
        <parameter name="options" type="string" default="">
            <doc>This string will be added to the command and can include any number of options in the software specific format.
            <br /><br />
            Example: options="-sn SAMPLE1 -sn SAMPLE2 -fraction 0.05"</doc>
        </parameter>
    </parameters>
</component>
