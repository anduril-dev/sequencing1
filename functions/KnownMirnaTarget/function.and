function KnownMirnaTarget {
  
  if (matureSeq == null) {
    findNovel = false
  }else {
    findNovel = true
  }

  if (geneExpr_type == "transcript"||geneExpr_type == "Transcript") {
    TranscLevel = true
  } else if (geneExpr_type == "gene"||geneExpr_type == "Gene") {
    TranscLevel = false
  } else { std.fail("Unknown geneExpr_type: ", geneExpr_type, ". Must be 'transcript' or 'gene' ") }

  script = StringInput(content="echo -e 'database.url = jdbc:mysql://@param1@:@param4@/@param2@\n"+
                               "database.user = @param3@\n"+
                               "database.password =@param5@\n"+
                               "database.timeout = 20\ndatabase.recycle = true\ndatabase.driver = com.mysql.jdbc.Driver' > @folder1@/conn1;"+
                               "echo -e 'host = @param1@\n"+"database = @param2@\nuser = @param3@\ndriver = mysql\nport = @param4@' > @folder1@/conn2;"+
                               "sed 's/\"//g' @folder1@/conn1 > @optOut1@; sed 's/\"//g' @folder1@/conn2 > @optOut2@;")
  
  connection = BashEvaluate(script=script.in,
                            param1=dbHost,
                            param2=dbDatabase,
                            param3=dbUser,
                            param4=dbPort,
                            param5=dbPassword)
                                                      
  // correlate miRNA with mRNA //
  if(miRNAList == null){
  
    rscript = StringInput(content="samples <- intersect(colnames(table1)[-1],colnames(table2)[-1]); "+
                                  "gexpr <- cbind(ID=table1[,1],table1[,samples]); "+
                                  "miexpr <- cbind(ID=table2[,1],table2[,samples]); "+
                                  "array[[\"gene_expr\"]] <- gexpr; array[[\"mirna_expr\"]] <- miexpr; "+
                                  "array.out <- array; table.out <- data.frame()")
                          
    expr = REvaluate(table1=geneExpr,
                     table2=mirnaExpr,
                     script=rscript.in)
  
    all_correlated_pairs = GlobalCorrelation(force gene=expr.array["gene_expr"],
                                             force mirna=expr.array["mirna_expr"],
                                             cutoff=cor)
  
  }else{
  
    rscript = StringInput(content="samples <- intersect(colnames(table1)[-1],colnames(table2)[-1]); "+
                                  "gexpr <- cbind(ID=table1[,1],table1[,samples]); "+
                                  "miexpr <- cbind(ID=table2[,1],table2[,samples]); "+
                                  "rownames(miexpr) <- miexpr[,1]; "+
                                  "array[[\"gene_expr\"]] <- gexpr; array[[\"mirna_expr\"]] <- miexpr[table3[,1],]; "+
                                  "array.out <- array; table.out <- data.frame()")
                               
    expr = REvaluate(table1=geneExpr,
                     table2=mirnaExpr,
                     table3=miRNAList,
                     script=rscript.in)
                                    
    all_correlated_pairs = GlobalCorrelation(force gene=expr.array["gene_expr"],
                                             force mirna=expr.array["mirna_expr"],
                                             cutoff=cor)
  }
  
  correlated_pairs = TableQuery(table1=all_correlated_pairs.correlated_all,
                                memoryDB=false,engine="h2",
                                query="SELECT * FROM table1 WHERE table1.\"Correlation\"< -"+cor+" AND table1.\"Pvalue\" < "+pvalue+" AND table1.\"Pvalue\" IS NOT NULL")
  
  transcripts = TableQuery(table1=correlated_pairs.table,
                           query="SELECT DISTINCT table1.\"TargetGene\" FROM table1")

  if (TranscLevel) {
      annot_refseq_tmp = KorvasieniAnnotator(sourceKeys=transcripts.table,
                                         connection=connection.optOut1,
                                         echoColumns="*",
                                         indicator=false,
                                         inputDB=".TranscriptId",
                                         inputType="Transcript",
                                         keyColumn="TargetGene",
                                         targetDB="_RefSeq_mRNA",
                                         rename="RefSeq_mRNA=refseq")
  } else {
      annot_refseq_tmp = KorvasieniAnnotator(sourceKeys=transcripts.table,
                                         connection=connection.optOut1,
                                         echoColumns="*",
                                         indicator=false,
                                         inputDB=".GeneId",
                                         inputType="Gene",
                                         keyColumn="TargetGene",
                                         targetDB=".TranscriptId, _RefSeq_mRNA",
                                         rename="TargetGene=GeneId, .TranscriptId=TargetGene, RefSeq_mRNA=refseq")
  }
                                     
  annot_refseq = ExpandCollapse(relation=annot_refseq_tmp.bioAnnotation,
                                listCols="refseq")        

  if (TranscLevel) {                              
      correlated_pairs_refseq = TableQuery(table1=correlated_pairs.table,
                                           table2=annot_refseq.relation,
                                           query="SELECT DISTINCT table1.\"miRNA\",table1.\"TargetGene\",table1.\"Correlation\",table1.\"Pvalue\",table2.\"refseq\" FROM table1 JOIN table2 ON table1.\"TargetGene\"=table2.\"TargetGene\" WHERE table2.\"refseq\" LIKE 'NM_%'")

      annot_entrz_tmp = KorvasieniAnnotator(sourceKeys=transcripts.table,
                                         connection=connection.optOut1,
                                         echoColumns="*",
                                         indicator=false,
                                         inputDB=".TranscriptId",
                                         inputType="Transcript",
                                         keyColumn="TargetGene",
                                         targetDB="_EntrezGene")
  } else {
      correlated_pairs_refseq = TableQuery(table1=correlated_pairs.table,
                                           table2=annot_refseq.relation,
                                           query="SELECT DISTINCT table1.\"miRNA\",table1.\"TargetGene\",table1.\"Correlation\",table1.\"Pvalue\",table2.\"refseq\" FROM table1 JOIN table2 ON table1.\"TargetGene\"=table2.\"GeneId\" WHERE table2.\"refseq\" LIKE 'NM_%'")

      annot_entrz_tmp = KorvasieniAnnotator(sourceKeys=transcripts.table,
                                         connection=connection.optOut1,
                                         echoColumns="*",
                                         indicator=false,
                                         inputDB=".GeneId",
                                         inputType="Gene",
                                         keyColumn="TargetGene",
                                         targetDB="_EntrezGene")
  }
                 
  annot_entrz = ExpandCollapse(relation=annot_entrz_tmp.bioAnnotation,
                               listCols="EntrezGene")   
                 
  correlated_pairs_entrz = TableQuery(table1=correlated_pairs.table,
                                       table2=annot_entrz.relation,
                                       query="SELECT DISTINCT table1.\"miRNA\",table1.\"TargetGene\",table1.\"Correlation\",table1.\"Pvalue\",table2.\"EntrezGene\" FROM table1 JOIN table2 ON table1.\"TargetGene\"=table2.\"TargetGene\" WHERE table2.\"EntrezGene\" IS NOT NULL")
                                  
  // known targets of query miRNAs by target database //

if (sqlDB == null) { // Deprecate this option in next big update of RNA-seq pipeline
  known_targets = REvaluate(table1=correlated_pairs_refseq.table,
                            table2=correlated_pairs_entrz.table,
                            script=INPUT(path="known_mirna_targets.r"),
                            param1="mirna_targets",
                            param2=TPdb,
                            param3=TPtables,
                            param4=TVtables)
} else {
  known_targets = REvaluate(table1=correlated_pairs_refseq.table,
                            table2=correlated_pairs_entrz.table,
                            script=INPUT(path="known_mirna_targets.r"),
                            param1="mirna_targets",
                            var1=sqlDB,
                            param3=TPtables,
                            param4=TVtables)
}
// Find novel miRNA targets for known miRNAs
if (findNovel) {

  // unannotated pairs in target database //
  unannotated_candidates  = TableQuery(table1=correlated_pairs.table,
                                       table2=known_targets.table,
                                       query="SELECT DISTINCT table1.\"miRNA\" AS \"mirna\", table1.\"TargetGene\" AS \"ensemblID\", table2.\"miRNA\", table2.\"TargetGene\"  FROM table1 LEFT JOIN table2 ON table1.\"miRNA\"=table2.\"miRNA\" AND table1.\"TargetGene\"=table2.\"TargetGene\" WHERE table2.\"miRNA\" IS NULL")
                                       
  // Fetch candidate UTR sequences
  candiTrans = TableQuery(table1=unannotated_candidates.table,
                          query="SELECT DISTINCT table1.\"ensemblID\" FROM table1")

  utrSeq = GetUtrSeq(transcripts=candiTrans.table,
                     connection=connection.optOut2,
                     type="3prime")

  // unannotated miRNA-microRNA pairs
  pairs = TableQuery(table1=unannotated_candidates.table,
                     query="SELECT DISTINCT table1.\"mirna\", table1.\"ensemblID\" from table1")

  csvseq = FASTA2CSV(fasta=utrSeq.UTR3)
                   
  UTRseedsArray = UtrSeq2Seeds(utrseq=csvseq.csv)

  bsiteArray = {}
  for rec: std.iterArray(UTRseedsArray.seeds){

    bsiteArray[rec.key] = MirMatch(utrSeeds=INPUT(path=rec.file),
                                   matureSeq=matureSeq,
                                   pairs=pairs.table)
  }

  bsiteCSV = CSVJoin(array=bsiteArray, useKeys=false)

  result = TableQuery(table1=bsiteCSV.csv,
                      table2=pairs.table,
                      table3=correlated_pairs.table,
                      query="SELECT DISTINCT table3.\"miRNA\",table3.\"TargetGene\",table1.\"SiteType\",table3.\"Correlation\", table3.\"Pvalue\" FROM table1 "+
                          "JOIN table2 ON table1.\"miRNA\"=table2.\"mirna\" AND table1.\"TranscriptID\"=table2.\"ensemblID\" "+
                          "JOIN table3 ON table2.\"ensemblID\"=table3.\"TargetGene\" AND table2.\"mirna\"=table3.\"miRNA\"")

  if (TranscLevel) {      
      annotation = KorvasieniAnnotator(sourceKeys=result.table,
                                       connection=connection.optOut1,
                                       echoColumns="*",
                                       indicator=false,
                                       inputDB=".TranscriptId",
                                       inputType="Transcript",
                                       keyColumn="TargetGene",
                                       targetDB=".GeneId,.GeneName,.GeneDesc")
  } else {
      annotation = KorvasieniAnnotator(sourceKeys=result.table,
                                       connection=connection.optOut1,
                                       echoColumns="*",
                                       indicator=false,
                                       inputDB=".GeneId",
                                       inputType="Gene",
                                       keyColumn="TargetGene",
                                       targetDB=".GeneId,.GeneName,.GeneDesc")
  }  

  return record(AnnotatedTargets=known_targets.table,NovelTargets=annotation.bioAnnotation)
} else {
  return record(AnnotatedTargets=known_targets.table, force NovelTargets=known_targets.optOut1)
}
  
}
