library(sqldf)

if(param1=="predicted_targets"){
  
  pred.table.names <- strsplit(param3,",")[[1]]
  if (param2 == "") {
    db <- var1
  } else { # when TPdb is no longer a parameter for this function, remove this part
    db <- param2
  }
  pairs <- table1
  results <- c()
  print("fetching predited targets from target database...")
  print(paste("--- there are ",nrow(table1)," miRNA-transcript pairs",sep=""))
  for(i in 1:nrow(table1)){
    
    curr.mirna <- table1[i,"miRNA"]
    curr.target <- table1[i,"RefSeq_mRNA"]
    
    marks <- c()
    # microcosm
    if("microcosm" %in% pred.table.names){
      stm <- paste("SELECT * FROM microcosm WHERE mirna_name='",curr.mirna,"' AND refseq='",curr.target,"'",sep="")
      res <- sqldf(stm,dbname=db)
    }
    if(nrow(res)>0){marks <- c(marks,"microcosm")}
    
    # targetscan
    if("targetscan" %in% pred.table.names){
      stm <- paste("SELECT * FROM targetscan WHERE mirna_name='",curr.mirna,"' AND refseq='",curr.target,"'",sep="")
      res <- sqldf(stm,dbname=db)
    }
    if(nrow(res)>0){marks <- c(marks,"targetscan")}
    
    # pita
    if("pita" %in% pred.table.names){
      stm <- paste("SELECT * FROM pita WHERE mirna_name='",curr.mirna,"' AND refseq='",curr.target,"'",sep="")
      res <- sqldf(stm,dbname=db)
    }
    if(nrow(res)>0){marks <- c(marks,"pita")}
    
    # microt
    if("microt" %in% pred.table.names){
      stm <- paste("SELECT * FROM microt WHERE mirna_name_v18='",curr.mirna,"' AND ensemblID='",table1[i,"TargetGene"],"'",sep="")
      res <- sqldf(stm,dbname=db)
    }
    if(nrow(res)>0){marks <- c(marks,"microt")}
    
    marks <- paste(marks,collapse=",")
    results <- c(results,marks)
  }
  table.out <- cbind(table1,PredictTargetSource=results)
  table.out <- table.out[table.out[,"PredictTargetSource"]!="",]
  
}else if(param1=="validated_targets"){

  validated.table.names <- strsplit(param3,",")[[1]]
  if (param2 == "") {
    db <- var1
  } else { # when TPdb is no longer a parameter for this function, remove this part
    db <- param2
  }
  
  pairs <- table1
  results <- c()
  print("fetching validated targets from target database...")
  print(paste("--- there are ",nrow(table1)," miRNA-transcript pairs",sep=""))
  for(i in 1:nrow(table1)){
    
    curr.mirna <- table1[i,"miRNA"]
    curr.target <- strsplit(table1[i,"EntrezGene"],",")[[1]][1]
    
    stm <- paste("SELECT * FROM mirtarbase WHERE mirna_name='",curr.mirna,"' AND entrezID='",curr.target,"'",sep="")
    res <- sqldf(stm,dbname=db)
    if(nrow(res)>0){
      results <- c(results,"validated")
    }else{
      results <- c(results,NA)
    }
  }
  table.out <- cbind(table1,ValidatedTargetSource=results)
  table.out <- table.out[!is.na(table.out[,"ValidatedTargetSource"]),]
}else if(param1=="merge_targets"){
  
  predicted <- table1
  validated <- table2
  
  combined <- unique(rbind(predicted[,1:4],validated[,1:4]))
  
  stm <- "SELECT * FROM combined LEFT JOIN predicted ON predicted.miRNA=combined.miRNA AND predicted.TargetGene=combined.TargetGene LEFT JOIN validated ON combined.miRNA=validated.miRNA AND combined.TargetGene=validated.TargetGene"
  res <- sqldf(stm)
  table.out <- res[,c(1:4,10,16)]
}else if(param1=="mirna_targets"){
  
  inRefseq <- table1
  inEntrz <- table2
  if (param2 == "") {
    TPdb <- var1
  } else { # when TPdb is no longer a parameter for this function, remove this part
    TPdb <- param2
  }

  predTables <- strsplit(param3,",")[[1]]
  validatedTables <- strsplit(param4,",")[[1]]
  
  # predicted targets
  mirna <- as.character(unique(inRefseq[,"miRNA"]))
  pred.res <- list()
  for(i in 1:length(predTables)){
    print(predTables[i])
    stm <- paste("SELECT * FROM ",predTables[i]," WHERE mirna_name IN ('")
    stm <- paste(stm,paste(mirna,collapse="','"),"')",sep="")
    res <-sqldf(stm,dbname=TPdb)
    
    if(predTables[i] %in% c("microcosm","targetscan","pita")){
      stm <- "SELECT * FROM inRefseq JOIN res ON inRefseq.miRNA=res.mirna_name AND inRefseq.refseq=res.refseq"
    }else{
      stm <- "SELECT * FROM inRefseq JOIN res ON inRefseq.miRNA=res.mirna_name AND inRefseq.TargetGene=res.ensemblID"
    }
    tmp <- sqldf(stm)
    if(nrow(tmp)>0){
      pred.res[[i]] <- unique(cbind(tmp,TargetSource=predTables[i])[,c("miRNA","TargetGene","Correlation","Pvalue","TargetSource")])
    }else{
      pred.res[[i]] <- NULL
    }
  }
  predictedRes <- do.call(rbind,pred.res)
  
  # validated targets
  validated.res <- list()
  if (length(validatedTables) > 0) {
  for(i in 1:length(validatedTables)){
    stm <- paste("SELECT * FROM ",validatedTables[i]," WHERE mirna_name IN ('")
    stm <- paste(stm,paste(mirna,collapse="','"),"')",sep="")
    res <-sqldf(stm,dbname=TPdb)
    
    stm <- "SELECT * FROM inEntrz JOIN res ON inEntrz.miRNA=res.mirna_name AND inEntrz.EntrezGene=res.entrezID"
    tmp <- sqldf(stm)
    if(nrow(tmp)>0){
      validated.res[[i]] <- unique(cbind(tmp,TargetSource=validatedTables[i])[,c("miRNA","TargetGene","Correlation","Pvalue","TargetSource")])
    }else{
      validated.res[[i]] <- NULL
    }
  }
  validatedRes <- do.call(rbind,validated.res)
  # summary
  result <- rbind(predictedRes,validatedRes)
  } else {
  result <- predictedRes
  }
  
  pairs <- unique(result[,1:4])
  out.list <- list()
  for(i in 1:nrow(pairs)){
    mirna <- as.character(pairs[i,"miRNA"])
    target <- as.character(pairs[i,"TargetGene"])
    corr <- pairs[i,"Correlation"]
    pvalue <- pairs[i,"Pvalue"]
    source <- paste(unique(result[as.character(result[,"miRNA"])==mirna & as.character(result[,"TargetGene"])==target,"TargetSource"]),collapse=", ")
    sourceN <- length(unique(result[as.character(result[,"miRNA"])==mirna & as.character(result[,"TargetGene"])==target,"TargetSource"]))
    out.list[[i]] <- c(mirna,target,corr,pvalue,source,sourceN)
  }
  table.out <- do.call(rbind,out.list)
  colnames(table.out) <- c("miRNA","TargetGene","Correlation","Pvalue","TargetSource","TargetSourceCounts")
}

