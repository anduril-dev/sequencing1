<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<component>
    <name>Align</name>
    <version>2.0</version>
    <doc><p>Generates sequence alignments from short- or long-reads using selected aligner. 
    The following aligners are supported:</p>
        <ul>
            <li><b>BWA for whole-genome/exome alignment</b>
                <p>You can align short- or long-reads using 'bwa' aligner from fastq or bam formats. For
                short-reads BWA first finds the suffix array coordinates for the reads and then 
                generates the alignments. Execution steps are:</p>
            	<ul>
            		<li>Short-reads: 'aln' followed by 'samse/sampe'</li>
            		<li>Long-reads: 'bwasw' for single-end or 'aln' followed by 'samse/sampe' for paired-end</li>
            	</ul>
            	<p>Multi-threading is only possible for the 'aln' step.</p>
	            <p>Complete documentation:
	            <a href="http://bio-bwa.sourceforge.net/bwa.shtml" target="_BLANK">
	            BWA manual</a></p>
        	</li>
    	    <li><b>Bowtie/Bowtie2 for whole-genome/exome alignment</b>
        	    <p>You can align short- or long-reads using 'bowtie' or 'bowtie2' aligner from fastq or fasta files.
        	    Bowtie/Bowtie2 alignment can be fully multi-threaded. Bowtie can align using 'seed' or 
        	    the whole-read when comparing mismatches.</p>
        	    <p>Bowtie2 uses the whole read by default
        	    using a mismatch function. By issuing '--local' into the 'options' parameter Bowtie2 splits and 
        	    soft-clips the reads for local mismatch "optimization". Note that Bowtie2 has numerous
        	    presets that can be set using 'options', e.g. '--very-sensitive-local' or '--very-fast'.</p>
	            <p>Complete documentation:
	            <a href="http://bowtie-bio.sourceforge.net/manual.shtml" target="_BLANK">
	            Bowtie manual</a>, 
	            <a href="http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml" target="_BLANK">
	            Bowtie2 manual</a></p>
    	    </li>
    	    <li><b>Tophat for RNA-seq alignment</b>
        	    <p>RNA short-reads are aligned using 'tophat' from fastq or fasta formats. Any number
        	    of additional options can be set using the 'options' parameter.</p>
        	    <p>Tophat is fully multi-threadable and enables "seeding" by default by cutting reads into segments. Using 
        	    the 'options' parameter is advisable, for example setting '--mate-inner-dist XXX --mate-std-dev XX' is 
        	    required for pair-end mapping.</p>
        	    <p>Complete documentation:
        	    <a href="http://tophat.cbcb.umd.edu/manual.html" target="_BLANK">
        	    Tophat manual</a></p>
    	    </li>
            <li><b>STAR for RNA-seq alignment</b>
                More flexibility for using STAR aligner can be obtained using the standalone STAR component, but most options can be customized here as well.
                To configure STAR behavior use only the parameter file, and the threads and options parameters below. The rest of the parameters will be ignored by STAR.
                Do not forget to unload genomes from the shared memory in case of crashes, using ipcs -m and ipcrm -m unix commands.
                Two pass mode is possible by setting it in the parameters or by running the Align function twice using the splice junctions to make a new genome for the second pass.
                STARgenome component can be used to create the input genome for both first and second pass.
            </li>
    	</ul>
    	<p>Alignments produced by BWA, Bowtie or Bowtie2 are sorted using Picard. The output will always
    	be a coordinate sorted BAM alignment file. Except for Tophat, the alignment will be accompanied with
    	an index .bai and a checksum .md5 file.</p>
    </doc>
    <author email="rony.lindell@helsinki.fi">Rony Lindell</author>
    <author email="sirkku.karinen@helsinki.fi">Sirkku Karinen</author>
    <author email="ping.chen@helsinki.fi">Ping Chen</author>
    <author email="alejandra.cervera@helsinki.fi">Alejandra Cervera</author>
    <category>Alignment</category>
    <requires URL="http://picard.sourceforge.net/">picard-tools</requires>
    <requires URL="http://tophat.cbcb.umd.edu/downloads/tophat-2.0.9.Linux_x86_64.tar.gz">tophat</requires>
    <requires name="installer" optional="false">
         <resource type="bash">install.bash</resource>
    </requires>
    <inputs>
       <input name="reference" type="FASTA" optional="true">
            <doc>The reference genome along with aligner specific index files. The fasta file should have the extension .fasta (BWA and Bowtie) or .fa (Tophat). For Tophat you can simply create a symbolic link 'reference.fa' pointing to the .fasta file in the same directory. If the file does not exist, it will be attempted to be created on the fly. This file is required for all aligners except STAR.
            <br /><br />
            Note: the index files associated with the reference genome are specific for the aligner of choice and must be built correctly in advance, e.g. using ReferenceIndexer.</doc>
        </input>
        <input name="genome" type="BinaryFolder" optional="true">
        <doc>A STAR genome, which can be generated from a required FASTA file, and optional annotation and optional list of splice junctions. Either download or generate a genome 
        (STARGenome component) for your annotation and read length.</doc>
        </input>
        <input name="reads" type="SequenceSet" optional="true">
            <doc>Reads in fastq or in bam format. Use the <code>inputType</code> parameter to define the format.
            <br /><br />
            For paired-end alignment using a single bam input the same input bam file should be assigned to both 'reads' and 'mate'.</doc>
        </input>
        <input name="mate" type="SequenceSet" optional="true">
            <doc>Mate in fastq or in bam format.</doc>
        </input>
		<input name="annotation" type="GTF" optional="true">
            <doc>GTF file containing gene annotation, which will be used in junction search.
	         <br /><br />
	         This is an optional input for Tophat and STAR and not used by other aligners.</doc>
        </input>
        <input name="readsArray" type="SequenceSet" array="true" optional="true">
            <doc>Array of reads to be aligned together with TopHat or STAR.</doc>
        </input> 
        <input name="matesArray" type="SequenceSet" array="true" optional="true">
            <doc>Array of mates to be aligned together with TopHat or STAR.</doc> 
        </input> 
        <input name="parameters" type="TextFile" optional="true">
        <doc>This file overrides default STAR parameters, but will itself be overridden by the command line. Use parametersDefault from STAR source as template. This file is required by STAR and
        all parameters for STAR should be defined here except for threads and options parameters which can override this file, but also can be left blank. The rest of the parameters are for the 
        other aligning tools.</doc>
        </input>
    </inputs>
    <outputs>
        <output name="alignment" type="AlignedReadSet">
            <doc>Aligned reads in compressed bam format.</doc>
        </output>
        <output name="folder" type="BinaryFolder">
            <doc>Tophat output folder containing files for junctions, insertions and deletions etc. 'alignment' output will point to the bam file inside the folder; the folder is needed as input for 
            tophat-fusion. For STAR it includes only the chimeric output, the junctions are in a separate output file.</doc>
        </output>
        <output name="spliceJunctions" type="CSV">
            <doc>Splice junctions only when STAR is used. This CSV file is created by adding a header to STAR output.</doc>
        </output>
    </outputs>
    <parameters>
        <parameter name="picard" type="string" default="">
            <doc>Path to Picard directory, e.g. "/opt/picard", which containg the Picard-tools .jar files. If empty 
            string is given (default), PICARD_HOME environment variable is assumed to point to the Picard directory.</doc>
        </parameter>
        <parameter name="memory" type="string" default="4g">
            <doc>This value is used in the Picard sorting step. A non-default value appends -XmxVALUE to the java command to specify the maximum size, in bytes, of the memory allocation pool. This value must a multiple of 1024 greater than 2MB. Append the letter k or K to indicate kilobytes, m or M to indicate megabytes or g or G to indicate gigabytes. The default value is chosen at runtime based on system configuration. For example a value of "4g" would allocate 4 gigabytes of memory and a value of "512m" would allocate 512 megabytes of memory.
            </doc>
        </parameter>
        <parameter name="aligner" type="string" default="bwa">
            <doc>Defines the aligner to be used. Executable(s) for the aligner 
            used must be set in PATH. Possible values: {bwa, bowtie, bowtie2, bwamem, tophat, and star}
            <br /><br />
            Both Tophat and Tophat2 are supported in the choice 'tophat'. The version used is the one set in PATH.</doc>
        </parameter>
        <parameter name="colorspace" type="boolean" default="false">
	        <doc>Align in colorspace. Read characters are interpreted as colors. The index specified must be a 
	        colorspace index. Colorspace is automatically set to 'true' if 'platform' is 'solid', 'SOLID' or 'SOLiD'.</doc>
		</parameter>
		<parameter name="mismatches" type="float" default="-1">
            <doc>The number of mismatches allowed in the seed or in the whole read for alignment. If seeding is 
            disabled ('seed' is set to '-1') this will mean the maximum allowed mismatches in the whole read. If 
            seeding is enabled, this will mean the maximum allowed mismatches in the seed.
		    <br /><br />
		    For Tophat, this referes to the '--segment-mismatches' option. A value of '-1' uses the software specific default value. 
		    <br /><br />
		    This is not used in Bowtie2 alignment.
	        </doc>
        </parameter>
		<parameter name="seed" type="int" default="-1">
            <doc>In BWA and Bowtie, the seed length stands for the number of bases on the high-quality end of the read
                that will be used in base mismatch calculations. Using the value '-1' disables the usage of a seed, which
                means that the whole read is used in a software specific default manner or in the manner defined by other
                parameters ('mm').
                <br /><br />
		 		In Tophat, this is the segment length ('--segment-length'). Each read is cut up into segments with at least this long. 
		 		These segments are mapped independently to the reference. 
		 		<br /><br />
		 		This is not used in Bowtie2 alignment.
		 		<br /><br />
		 		In bwa mem, this will set the minimum seed length. If left to -1, the default length will be used.
	   		</doc>
        </parameter>
        <parameter name="trim" type="string" default="">
            <doc>Parameter for trimming the reads. 
            <br /><br />
            For BWA this stands for the '-q' option, which does trimming based on the
            base-quality sum function <code>argmax_x{\sum_{i=x+1}^l(INT-q_i)}</code> if q_l is less 
            than INT where l is the original read length. A value of 5 or 10 is recommended for most cases.
            <br /><br />
            For Bowtie this stands for the '--trim5' and '--trim3' options, which trims the amount of bases
            given from the high-quality (left) and low-quality (right) end respectively of each read. The string
            is given as a comma separated pair, e.g. '5,10' will trim 5 bases from the left and 10 from the right 
            of each read.
            <br /><br />
            This is not used in Tophat alignment.
            <br /><br />
            This parameter is not used in bwa mem where soft clipping is always on. You can set the clipping penalty
            using the L flag or use hard clipping with the H flag in the options parameter.</doc>
        </parameter>
        <parameter name="threads" type="int" default="1">
            <doc>The number of threads, if the aligner supports the paralellization.</doc>
        </parameter>
        <parameter name="inputType" type="string" default="fastq">
            <doc>Allowed input file format depends on the method used. 'bwa' can handle 'fastq' and 
            'bam' files as input, while 'bowtie'/'bowtie2' and 'tophat' accept 'fastq' or 'fasta' files.
            </doc>
        </parameter>
        <parameter name="mateDist" type="string" default="">
            <doc>Given as a comma-separated pair, this is the expected inner distance between mate pairs
            and the standard deviation for the distribution on inner distances between mate pairs. E.g.
            a value of '200,50' sets inner distance to 200 ("--mate-inner-dist" in Tophat) and standard
            deviation of the inner distances to 50 ("--mate-std-dev" in Tophat).
            <br /><br />
            This is only used in Tophat alignment for paired-end reads.</doc>
        </parameter>
        <parameter name="sample" type="string" default="sample">
            <doc>This sample name is added to the bam file using the 'SM' read group tag. This is needed for BWA 
            and Bowtie for read group construction. If this parameter is set, the 'ID', 'SM' and 'PL' read 
            group identifiers will be set using function parameters (see 'platform').
            <br /><br />
            If an empty string is given, the read group string is left out altogether unless it is defined
            in 'options'.</doc>
        </parameter>
        <parameter name="platform" type="string" default="ILLUMINA">
            <doc>CAPILLARY, LS454, ILLUMINA, SOLID, HELICOS, IONTORRENT or PACBIO. 
	         This is needed for BWA and Bowtie for read group construction.</doc>
        </parameter>
	    <parameter name="options" type="string" default="">
            <doc>Other options for alignment. This parameter is given as written to the aligner execution command.
            Example: "--read-mismatches 3 --bowtie1" (tophat) will set read-mismatches to 3 and will use Bowtie1
            instead of Bowtie2 in initial alignment. For STAR --outSAMstrandField intronMotif will create the XS attribute needed for Cufflinks. 
            Setting options here overrides parameter settings in settings file (only valid for STAR).</doc>
        </parameter>
        <parameter name="mainAlignmentType" type="string" default="">
            <doc>Only valid for STAR. Depending on the parameters passed more than one alignment may be produced (ex. sortedByCoord or toTranscriptome). The alignment not selected 
            will still be available in the folder output. The string defined here will define which alignment will be linked to the alignment output of this component.</doc>
       </parameter>
       <parameter name="genomeLoad" type="string" default="LoadAndKeep">
            <doc>Only valid for STAR. LoadAndRemove works for parallel STAR instances and if everything goes fine, should free memory after the last STAR exits. LoadAndKeep, LoadAndRemove, Remove, LoadAndExit and NoSharedMemory are the options.</doc>
       </parameter>
    </parameters>
</component>
