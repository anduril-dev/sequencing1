function Align {

    functionMetaData=std.metadata()   
    sort = false

    // For keeping track of current alignment and for splice junctions output when aligner is not STAR
    empty=StringInput(content="Nothing")

    std.echo("Aligner: "+aligner)
    if (reads == null) {
        if (aligner != "tophat" && aligner != "star" ) std.fail("Input read missing") 
        else if (readsArray == null) std.fail("Input read or readsArray missing")
    }

    if (picard == "") { // Load environment variable for picard path
        picard='${PICARD_HOME}'
    }
    //just used if picard path or home don't have the jars
    picard_setup=functionMetaData['bundlePath']+'/lib/picard'

    if (reads != null) alignment=reads // For keeping track of current alignment
    else {
        alignment=empty
    }

    paired=false

    if (mate != null || matesArray != null) paired=true // Set paired mode
    if (platform == "solid" || platform == "SOLiD" || platform == "SOLID") colorspace=true // Solid uses colorspace
    if (inputType == "bam" && aligner != "bwa") std.fail("ERROR: Please use BWA aligner for BAM input files.")

    if (aligner == "bwa") {

        // ------------------------------------------------------------
        // Run BWA alignment
        // 1. Create suffix array coordinate (.sai) files
        // 2. Align using the coordinates
        // 3. Sort the alignment
        // ------------------------------------------------------------

		typePar1=""
		typePar2=""
        alnPar=""
        sampsaPar=""
        filesStr=""
        modeStr=""

        // ------------------------------------------------------------
        // Step 1: Create suffix array coordinate (.sai) files
        // using 'bwa aln'
        // ------------------------------------------------------------

        if (options != "") alnPar=options+" -t "+threads
        else alnPar="-t "+threads
		if (colorspace) alnPar=alnPar+" -c "
		if (seed != -1) { // Use seed
		    alnPar=alnPar+" -l "+seed
		    if (mismatches != -1) {
		                alnPar=alnPar+" -k "+mismatches
		    }
		} else { // Do not use seed
		    if (mismatches != -1) {
		                alnPar=alnPar+" -n "+mismatches
		    }
	    }
	    if (trim != "") alnPar=alnPar+" -q "+trim
		if (inputType == "bam") {
			if (paired == true) typePar1=" -b1"
			else typePar1=" -b0"

			typePar2=" -b2"
		}

        @out.optOut1.filename="suffixarray1.sai"
        sai1 = BashEvaluate( // Create SA coordinates
		    var1   = reference,
                    var2   = reads,
                    script = "bwa aln "+alnPar+" @var1@"+typePar1+" @var2@ > @optOut1@")

        if (paired) {

            @out.optOut1.filename="suffixarray2.sai"
            sai2 = BashEvaluate( // Create SA coordinates
                var1=reference,
                var2=mate,
                script="bwa aln "+alnPar+" @var1@"+typePar2+" @var2@ > @optOut1@")

        }

        // Step 2: Align using the coordinates and
        // 'bwa samse/sampe'
        // ------------------------------------------------------------

        if (sample != "") sampsaPar = " -r '@RG\\tID:0\\tPL:"+platform+"\\tSM:"+sample+"'"
        if (paired) {
            filesStr=" @var1@ @var2@ @var3@ @var4@ @var5@"
            modeStr=" sampe"
            saiMate=sai2.optOut1
        } else {
            filesStr=" @var1@ @var2@ @var4@"
            modeStr=" samse"
            saiMate=sai1.optOut1 // Dummy
        }

        @out.optOut1.filename="aligned.sam"
		bwa = BashEvaluate( // Align reads
		    var1=reference,
			var2=sai1.optOut1,
			var3=saiMate,
			var4=reads,
			var5=mate,
			script="bwa" + modeStr + sampsaPar  + filesStr + " > @optOut1@")
        alignment=bwa.optOut1

        // ------------------------------------------------------------
        // Step 3: Sort the alignment using Picard SortSam
        // ------------------------------------------------------------
        
        // Implementation below
        sort = true
    
    } else if (aligner == "bowtie" || aligner == "bowtie2") {

        // ------------------------------------------------------------
        // Run Bowtie alignment
        // 1. Align the reads to reference genome
        // 2. Sort the alignment
        // ------------------------------------------------------------

        btIndexCommand=""
        btCommand=""
        fileStr=""
        refStr=""
        bowtieStr=""
        outStr=""

        // ------------------------------------------------------------
        // Step 1: Align the reads using Bowtie/Bowtie2
        // ------------------------------------------------------------

	    if (paired) fileStr=" -1 @var2@ -2 @var3@"

        // Construct command for Bowtie 1
		if (aligner == "bowtie") {
		    btIndexCommand="export BOWTIE_INDEXES=$( dirname @var1@ )/; "
		    bowtieStr="bowtie "
            btCommand=bowtieStr+"-S --threads "+threads
		    refStr=" ${input_var1%.*}"
		    if (!paired) fileStr=" @var2@"
         	if(sample != "") {
        	    btCommand=btCommand+" --sam-RG ID:0 --sam-RG PL:"+platform+" --sam-RG SM:"+sample
	        }
		    if (colorspace) {
			    btCommand=btCommand+" -C"
		    }
		    if (seed != -1) { // Use seed
		        btCommand=btCommand+" --seedlen "+seed
		        if (mismatches != -1) {
			        btCommand=btCommand+" --seedmms "+mismatches
		        }
		    } else { // Do not use seed
		        if (mismatches != -1) {
			        btCommand=btCommand+" -v "+mismatches
		        }
	        }
	    }

	    // Construct command for Bowtie 2
	    if (aligner == "bowtie2") {
	        btIndexCommand="export BOWTIE2_INDEXES=$( dirname @var1@ )/; "
		    bowtieStr="bowtie2 "
		    btCommand=bowtieStr+"--threads "+threads
         	if (sample != "") {
         	    btCommand=btCommand+" --rg-id 0"
        	    btCommand=btCommand+" --rg PL:"+platform+" --rg SM:"+sample
	        }
		    refStr=" -x ${input_var1%.*}" //$( basename @var1@ .fa )"
		    if (!paired) fileStr=" -U @var2@"
		    outStr=" -S"
	    }

	    // Add common Bowtie options
     	if (options != "") {
     	    btCommand=btCommand+" "+options
     	}
		if (inputType == "fastq") {
		    btCommand=btCommand+" -q"
		} else if (inputType == "fasta") {
		    btCommand=btCommand+" -f"
		}
		if (trim != "") {
		    trim="--trim5 " + trim
		    trim=std.strReplace(trim,",","--trim3")
		}

        @out.optOut1.filename="aligned.sam"
		bowtie = BashEvaluate( // Align reads
		    var1=reference,
            var2=reads,
	        var3=mate,
            script=btIndexCommand+btCommand+refStr+fileStr+outStr+" @optOut1@"
            )
        alignment=bowtie.optOut1

        // ------------------------------------------------------------
        // Step 2: Sort the alignment using Picard SortSam
        // ------------------------------------------------------------

        // Implementation below
        sort = true
    } else if (aligner == "bwamem" ) {

        // ------------------------------------------------------------
        // Run bwa-mem alignment
        // 1. Align the reads to reference genome
        // 2. Sort the alignment
        // ------------------------------------------------------------

        // Construct command for bwa mem
        
	    if (paired) fileStr=" @var1@ @var2@ @var3@ > @optOut1@"
	    if (!paired) fileStr=" @var1@ @var2@ > @optOut1@"
		
		bwamemStr = "bwa mem "
		bwamemCommand = bwamemStr+"-M -t "+threads
		
		if (seed != -1) { // set minimum seed length
		    bwamemCommand=bwamemCommand + " -k "+seed
		}
        
        if (sample != "") bwamemCommand = bwamemCommand + " -R '@RG\\tID:0\\tPL:"+platform+"\\tSM:"+sample+"'"

		if (options != "") {
     	    bwamemCommand=bwamemCommand+" "+options
     	}
		bwamemCommand = bwamemCommand + fileStr
		@out.optOut1.filename="aligned.sam"
		bwamem = BashEvaluate( // Align reads
		    var1=reference,
            var2=reads,
	        var3=mate,
            script=bwamemCommand)
        alignment=bwamem.optOut1
	    
        sort = true
            
	} else if (aligner  == "tophat") {

        // ------------------------------------------------------------
        // Run Tophat alignment
        // - Align the reads to reference genome and output
        // the sorted alignment
        // - Bowtie and Bowtie2 alignments are both
        // supported
        // ------------------------------------------------------------

        btIndexCommand="export BOWTIE_INDEXES=$( dirname @var1@ )/; " +
                             "export BOWTIE2_INDEXES=$( dirname @var1@ )/; "
        pairedStr=""
        if (colorspace) options=options + " --color"
        if (mismatches != -1) options=options + " --segment-mismatches " + mismatches
        if (seed != -1) options=options + " --segment-length " + seed
        if (annotation != null) options=options + " -G @var4@ "
        options=options + " --num-threads " + threads
        if (paired) {
            pairedStr=" @var3@"
            if (mateDist != "") {
                mateDist=" --mate-inner-dist " + mateDist
                mateDist=std.strReplace(mateDist,","," --mate-std-dev ")
                options=options + mateDist
            }
        }
        
        if (reads != null) {
            @out.folder1.filename="tophat_output"    
            tophat = BashEvaluate( // Align the reads
                var1=reference,
                var2=reads,
                var3=mate,
                var4=annotation,
                script="FA=${input_var1%.*}; " +
                    btIndexCommand + ' tophat -o @folder1@ ' + options +
                    ' $FA @var2@' + pairedStr + '; ' +
                    'rm @optOut1@; ln -s @folder1@/accepted_hits.bam @optOut1@'
                )
        }
        else { //Input given in an array
            makeReadsList = BashEvaluate(   array1 = readsArray,
                                            script = "getarrayfiles array1 | tr '\n' ','  | sed 's/,$//' > @optOut1@")
            
           
            makeMatesList = BashEvaluate(   array1 = matesArray,
                                            param1 = paired,
                                            script = '''if [ @param1@==true ]
                                                        then 
                                                            getarrayfiles array1 | tr '\n' ',' | sed 's/,$//' > @optOut1@
                                                        fi''')
            
            @out.folder1.filename="tophat_output"
            tophat = BashEvaluate( // Align the reads
                var1=reference,
                var2=makeReadsList.optOut1,
                var3=makeMatesList.optOut1,
                var4=annotation,
                script='''FA=${input_var1%.*}; reads=$( cat @var2@ ); if [ -s @var3@ ]; then mates=$( cat @var3@ ); else mates=""; fi; echo $reads; echo $mates''' +
                    btIndexCommand + ' tophat -o @folder1@ ' + options +
                    ' $FA $reads $mates ;' +
                    'rm @optOut1@; ln -s @folder1@/accepted_hits.bam @optOut1@'
                )

        }
        return record(alignment=tophat.optOut1, folder=tophat.folder1, spliceJunctions=empty) 

    } else if (aligner  == "star") {		
        
        if (reads != null) {
            tmpReads={}
            tmpMates={}
            tmpReads["read"]=reads
            tmpMates["mate"]=mate
            myReads=std.makeArray(tmpReads)
            if (mate != null) {
                myMates=std.makeArray(tmpMates)
            }
            else {myMates = mate}    
            //myReads = reads
            //myMates = mate
        }
        else {
            std.echo("Reads are in array")
            myReads = readsArray
            myMates = matesArray
        }

        //STAR component can take either reads or readsArray as input
        star = STAR(genome              = genome,
                    reads               = myReads,
                    mates               = myMates,
                    annotation          = annotation,
                    parameters          = parameters,
                    options             = options,
                    genomeLoad          = genomeLoad,
                    mainAlignmentType   = mainAlignmentType,
                    threads             = threads,
                    //@memory             = 39000,
                    @cpu                = threads)

        return record(alignment=star.alignment, folder=star.folder, spliceJunctions=star.spliceJunctions)

    } else { std.fail("Unknown aligner: ", aligner) }
	
    if (sort) {

        // ------------------------------------------------------------
        // Sort the raw alignment using Picard SortSam
        // ------------------------------------------------------------
       
        // Sort the alignment
        @out.optOut1.filename="sorted.bam"
        sortSam = BashEvaluate( var1    = alignment,
                                param1  = picard,
                                param2  = picard_setup,
//        script='java -Xmx' + memory + ' -jar ' + picard +
//            '/SortSam.jar VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate ' +
//            'CREATE_INDEX=true CREATE_MD5_FILE=true INPUT=@var1@ OUTPUT=@optOut1@ TMP_DIR=$( gettempdir )'            )
                                script  ="""PICARD=@param1@
                                            NEW_VERSION=true
                                            FOUND=false
                                            [[ -e @param1@/picard.jar ]] || PICARD=@param2@
                                            [[ -e $PICARD/picard.jar ]] && PICARDEXT=$PICARD'/picard.jar SortSam' || NEW_VERSION=false
                                            if [ $NEW_VERSION = true ] ; then
                                                FOUND=true
                                            else
                                                [[ -e $PICARD/SortSam.jar ]] || PICARD=@param1@
                                                [[ -e $PICARD/SortSam.jar ]] && FOUND=true || {
                                                    echo cannot find picard or SortSam at @param1@ or @param2@
                                                    exit 1
                                                }
                                                PICARDEXT=$PICARD/SortSam.jar
                                            fi
                                            java -Xmx"""+memory+' -jar $PICARDEXT VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate CREATE_INDEX=true CREATE_MD5_FILE=true INPUT=@var1@ OUTPUT=@optOut1@ TMP_DIR=$( gettempdir )')

        return record(alignment=sortSam.optOut1, folder=sortSam.folder1, spliceJunctions=empty)

    }

}
