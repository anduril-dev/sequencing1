#convert table1 to log2 matrix

#table1 <- read.table("exprTableGenes.csv", sep='\t', header=TRUE, check.names=FALSE, stringsAsFactors=FALSE, row.names=NULL, quote='"')
n <- sapply(table1,is.numeric)
l <- sapply(table1,is.character)
nums <- table1[,n]
labels <- table1[,l]
#mode(nums) <- "numeric"
m1 <- nums+1
m2 <- log2(m1)
mask <- apply(m2,2,is.nan)
m2[mask] <- 0
mask <- apply(m2,2,is.na)
m2[mask] <- 0
mask <- apply(m2,2,is.infinite)
m2[mask] <- 0
table.out <- cbind(labels,m2)
colnames(table.out) <- colnames(table1)

#table.out <- m3[ apply(m3!=0,1,any), ,drop=FALSE]
