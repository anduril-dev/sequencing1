stringtomap @param1@

# NOTE: fq.gz is the extension of read outputs of QCfasta. 
# GZ file extensions are NOT acceptable if trimming by fastx-toolkit is intended.
if [ "$zip" = "true" ]
then
    gzip=" -z"
    outFile="trimmed_reads.fq.gz"
else
    gzip=""
    outFile="trimmed_reads.fq"
fi

if [[ @var1@ =~ \.gz$ ]]
then
    if [ "$a" != "NA" ] # Only run if reads need trimming
    then
        zcat @var1@ | fastx_clipper -v -a $a -l $Lmin $qual -M $M $extra | fastx_artifacts_filter -v $qual | fastq_quality_filter -v -q $minQ -p $minPercent $qual | fastx_trimmer -v -l $Lmax $qual$gzip -o @folder2@/$outFile
    fi
else
    if [ "$a" != "NA" ] # Only run if reads need trimming
    then 
        fastx_clipper -v -a $a -l $Lmin $qual -M $M $extra -i @var1@ | fastx_artifacts_filter -v $qual | fastq_quality_filter -v -q $minQ -p $minPercent $qual | fastx_trimmer -v -l $Lmax $qual$zip -o @folder2@/$outFile
    fi
fi

cat stdErr >> @optOut2@
cat stdOut >> @optOut2@

